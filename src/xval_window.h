/*
 xval_window.h

 Cross validation to estimate spline parameters (e.g. smoothing,
 number of knots).

 The SPLITS splview program to fit and visualize spline models to
 remotely sensed time series images.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2016  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_XVAL_WINDOW_H
#define SPLITS_XVAL_WINDOW_H

#include <string>
#include <FL/Fl_Window.H>
#include <FL/Fl_Progress.H>
#include <FL/Fl_Multiline_Output.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Round_Button.H>
#include <FL/Fl_Check_Button.H>
#include "real_input.h"

namespace splits {

class Main_window;

class Xval_window : public Fl_Window
{
  static std::string dir;

protected:
  Real_input *domain_inp1, *domain_inp2, *tol_inp, *iter_inp;
  Fl_Progress *progress;
  Fl_Multiline_Output *outp;
  Fl_Button *save_btn, *reset_btn, *run_btn;
  Fl_Round_Button *grid_btn, *gold_btn;
  Fl_Check_Button *ignore_btn;

  Main_window *mw;

public:
  enum {GRID_SEARCH, GOLDEN_SECTION};

public:
  Xval_window(Main_window* MW);
  virtual ~Xval_window() {}

  void search_domain(float* mn, float* mx);

  int search_type();

  float tolerance() {return tol_inp->number();}

  int iterations() {return (int)iter_inp->number();}

  const char* result() {return outp->value();}
  void add_result(const char* str);

  void erase();

  void enable(bool b=true);

  void start();
  void finish();

  static void report(float fraction, void* data);

  static void round_btn_cb(Fl_Widget* w, void* data);
  static void save_cb(Fl_Widget* w, void* data);
};


class Span_xval_window : public Xval_window
{
public:
  Span_xval_window(Main_window* MW);
  ~Span_xval_window() {};

  void reset();

  static void reset_cb(Fl_Widget* w, void* data);
  static void run_cb(Fl_Widget* w, void* data);
};

} //namespace splits

#endif
