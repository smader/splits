/*
 golden.cpp

 Golden section search to find the minimum of a function.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath>
#include "golden.h"

const float r = 0.61803398875f;

int splits::golden(float (*f)(float,void*),
		   void* p,
		   float a,
		   float b,
		   float* xa,
		   float* xb,
		   float* y,
		   int k,
		   float eps,
		   void (*progress)(float,void*),
		   void* data)
{
  if (progress) progress(0, data);

  double ya = f(a,p);
  double yb = f(b,p);
  
  int i = 0;
  do {
    float x1 = b-r*(b-a);
    float x2 = a+r*(b-a);
    float y1 = f(x1,p);
    float y2 = f(x2,p);
    if (y1<=y2) {
      b = x2;
      yb = y2;
    } else {
      a = x1;
      ya = y1;
    }
    xa[i] = a;
    xb[i] = b;
    y[i] = (ya+yb)/2.0f;
    i += 1;
    if (progress) progress((float)(i)/(float)k, data);
  } while (((float)fabs(b-a)>eps) && (i<k));

  if (progress) progress(1, data);

  return i;
}
