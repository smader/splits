/*
 goto_window.cpp

 An FLTK window to specify image or map coordinates to go to.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2016  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <cstring>
#include <FL/Fl_Group.H>
#include <FL/Fl_Box.H>
#include "main_window.h"
#include "goto_window.h"
#include "static_matrix.h"

using namespace splits;

Goto_window::Goto_window(Main_window* MW)
  : Fl_Window(300,100,"Go to Data Point"), mw(MW)
{
  Fl_Group *hor1 = new Fl_Group(0,0,w(),35);
  pix_btn = new Fl_Toggle_Button(5,5,100,25, "Pixel Based");
  pix_btn->type(FL_RADIO_BUTTON);
  pix_btn->callback(pix_btn_cb, this);
  map_btn = new Fl_Toggle_Button(107,5,100,25, "Map Based");
  map_btn->type(FL_RADIO_BUTTON);
  map_btn->callback(map_btn_cb, this);
  Fl_Box *box = new Fl_Box(210,5,105,25);
  hor1->end();
  hor1->resizable(box);
  Fl_Group *hor2 = new Fl_Group(0,35,w(),65);
  inp1 = new Fl_Float_Input(150,40,w()-155,25, "Column or Easting:");
  inp1->maximum_size(64);
  inp1->callback(input_cb, this);
  inp1->when(FL_WHEN_ENTER_KEY|FL_WHEN_NOT_CHANGED);
  inp2 = new Fl_Float_Input(150,70,w()-155,25, "Row or Northing:");
  inp2->maximum_size(64);
  inp2->callback(input_cb, this);
  inp2->when(FL_WHEN_ENTER_KEY|FL_WHEN_NOT_CHANGED);
  hor2->end();
  hor2->resizable(inp1);
  resizable(this);
  size_range(320,100,0,100);
  set_non_modal();
  pix_btn->setonly();
}

void Goto_window::update()
{
  if (mw->data[mw->current].is_geo_transformed()) {
    map_btn->activate();
  } else {
    pix_btn->setonly();
    map_btn->deactivate();
  }

  char str[65];
  if (map_btn->value()) {
    sprintf(str, "%f", mw->mon->cur[0]);
    inp1->value(str);
    sprintf(str, "%f", mw->mon->cur[1]);
    inp2->value(str);
  } else {
    Static_matrix<double,3,3> m =
      inverse(mw->data[mw->current].geo_transform());
    Static_vector<double,2> v(mw->mon->cur);
    v = m*v;
    sprintf(str, "%f", v[0]);
    inp1->value(str);
    sprintf(str, "%f", v[1]);
    inp2->value(str);
  }
}

void Goto_window::reset()
{
  inp1->value("");
  inp2->value("");
  pix_btn->setonly();
}

void Goto_window::pixel_pos(double* pos)
{
  if (pix_btn->value()) {
    Static_vector<double,2> v(atof(inp1->value()), atof(inp2->value()));
    Static_matrix<double,3,3> m = mw->data[mw->current].geo_transform();
    v = m*v;
    pos[0] = v[0];
    pos[1] = v[1];
  } else {
    pos[0] = atof(inp1->value());
    pos[1] = atof(inp2->value());
  }
}

void Goto_window::pix_btn_cb(Fl_Widget* w, void* data)
{
  Goto_window *win = (Goto_window*)data;
  win->update();
}

void Goto_window::map_btn_cb(Fl_Widget* w, void* data)
{
  Goto_window *win = (Goto_window*)data;
  win->update();
}

void Goto_window::input_cb(Fl_Widget* w, void* data)
{
  Goto_window* win = (Goto_window*)data;
  double pos[2];
  win->pixel_pos(pos);
  win->mw->mon->set_viewport(pos[0], pos[1]);
}
