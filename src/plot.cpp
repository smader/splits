/*
 plot.cpp

 FLTK widget for plotting time series.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include "plot.h"
#include <FL/Fl_Group.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Hor_Value_Slider.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Counter.H>
#include <FL/fl_ask.H>
#include <FL/Fl_File_Chooser.H>

using namespace arma;
using namespace splits;

static unsigned char colors[][3] = {
  {255,255,255}, {191,191,191}, {128,128,128}, {0,0,0}, {255,0,0}, {128,0,0},
  {255,166,0}, {255,255,0}, {128,128,0}, {0,255,0}, {0,128,0}, {0,255,255},
  {0,128,128}, {0,0,255}, {0,0,128}, {255,0,255}, {128,0,128}
};

static char *colornames[] = {
  "white", "silver", "gray", "black", "red", "maroon", "orange", "yellow",
  "olive", "lime", "green", "aqua", "teal", "blue", "navy", "fuchsia", "purple"
};

int x2scr(int xx, double xmin, double sx, double x)
{
  return (int)((x-xmin)*sx + xx + 0.5);
}

int y2scr(int yy, int hh, double ymin, double sy, double y)
{
  return (int)(yy - ((y-ymin)*sy-hh) + 0.5);
}

double scr2x(int xx, double xmin, double sx, int x)
{
  return (x-xx)/sx+xmin;
}

double scr2y(int yy, int hh, double ymin, double sy, int y)
{
  return -((y-hh-yy)/sy)+ymin;
}

void Plot::Object::save(FILE* fp)
{
  int i;
  for (i=0; i<x.n_elem; i++)
  {
    fprintf(fp, "%f\t%f\n", x[i], y[i]);
  }
}

void Plot::LineObject::draw(int xx, int yy, int hh, double xmin, double ymin, double sx, double sy)
{
  Fl_Color oldc = fl_color();
  fl_color(c);

  fl_line_style(style,lw);

  int i;
  for (i=1; i<x.n_elem; i++)
  {
    int x1, y1, x2, y2;
    x1 = x2scr(xx,xmin,sx,x(i-1));
    x2 = x2scr(xx,xmin,sx,x(i));
    y1 = y2scr(yy,hh,ymin,sy,y(i-1));
    y2 = y2scr(yy,hh,ymin,sy,y(i));
    fl_line(x1,y1,x2,y2);
  }

  fl_line_style(0);
  fl_color(oldc);
}

void Plot::PointObject::draw(int xx, int yy, int hh, double xmin, double ymin, double sx, double sy)
{
  Fl_Color oldc = fl_color();

  if (fill)
  {
    //fl_line_style(FL_SOLID,2);
    fl_color(c);

    int i;
    for (i=0; i<x.n_elem; i++)
    {
      int x1, y1;
      x1 = x2scr(xx,xmin,sx,x(i));
      y1 = y2scr(yy,hh,ymin,sy,y(i));
      fl_pie(x1-sz/2,y1-sz/2,sz,sz,0.,360.);
      //fl_line(x1-sz/2,y1,x1+sz/2,y1);
      //fl_line(x1,y1-sz/2,x1,y1+sz/2);
    }

    //fl_line_style(0);
  }
  else
  {
    fl_line_style(FL_SOLID,lw);
    int i;
    for (i=0; i<x.n_elem; i++)
    {
      int x1, y1;
      x1 = x2scr(xx,xmin,sx,x(i));
      y1 = y2scr(yy,hh,ymin,sy,y(i));
      fl_pie(x1-sz/2,y1-sz/2,sz-1,sz-1,0.,360.);
      fl_color(c);
      fl_arc(x1-sz/2,y1-sz/2,sz-1,sz-1,0.,360.);
      fl_color(oldc);
    }
    fl_line_style(0);
  }

  fl_color(oldc);
}

void Plot::draw()
{
  draw_box();

  Fl_Boxtype b = box();
  int xx = x()+Fl::box_dx(b);
  int yy = y()+Fl::box_dy(b);
  int ww = w()-Fl::box_dw(b);
  int hh = h()-Fl::box_dh(b);

  fl_push_clip(xx,yy,ww,hh);

  ww--; hh--; //account for linewidth of box

  Object *o;
  std::list<Object*>::iterator it;

  if (autoscale && !obj.empty())
  {
    o = *obj.begin();
    xmin = o->x.min();
    xmax = o->x.max();
    ymin = o->y.min();
    ymax = o->y.max();
    for (it=obj.begin(); it!=obj.end(); ++it)
    {
      double mn, mx;
      o = *it;
      mn = o->x.min();
      if (mn<xmin) xmin = mn;
      mx = o->x.max();
      if (mx>xmax) xmax = mx;
      mn = o->y.min();
      if (mn<ymin) ymin = mn;
      mx = o->y.max();
      if (mx>ymax) ymax = mx;
    }
  }

  double sx = (double)ww/(xmax-xmin);
  double sy = (double)hh/(ymax-ymin);
  
  if ((vgrd.n_elem>0) && !obj.empty()) {
    Fl_Color oldc = fl_color();
    fl_color(fl_rgb_color(colors[2][0],colors[2][1],colors[2][2]));
    fl_line_style(FL_SOLID,0);
    int i;
    for (i=0; i<vgrd.n_elem; i++) {
      int x, y1, y2;
      x = x2scr(xx,xmin,sx,vgrd(i));
      y1 = y2scr(yy,hh,ymin,sy,ymin);
      y2 = y2scr(yy,hh,ymin,sy,ymax);
      fl_line(x,y1,x,y2);
    }
    fl_line_style(0);
    fl_color(oldc);
  }

  for (it=obj.begin(); it!=obj.end(); ++it)
  {
    (*it)->draw(xx,yy,hh,xmin,ymin,sx,sy);
  }

  fl_pop_clip();
}

Plot::Plot(int x, int y, int w, int h, const char *l, const Fl_Menu_Item* m)
  : Fl_Widget(x,y,w,h,l), menu(m), autoscale(true), xmin(0.), ymin(0.),
    xmax(1.), ymax(1.)
{
  box(FL_BORDER_BOX);
  fgcol = fl_rgb_color(colors[3][0],colors[3][1],colors[3][2]);
}

Plot::~Plot()
{
}

int Plot::handle(int event)
{
  switch(event)
  {
  case FL_PUSH:
    if (Fl::event_button()==FL_RIGHT_MOUSE && menu!=0)
    {
      const Fl_Menu_Item* m;
      m = menu->popup(Fl::event_x(), Fl::event_y(), 0, 0, 0);
      if (m) m->do_callback(this, (void*)m);
      return 1;
    }
    return 0;
  default:
    return Fl_Widget::handle(event);
  }
}

void Plot::set_autorange(bool a)
{
  autoscale = a;
  redraw();
}

void Plot::set_range(double xmn, double xmx, double ymn, double ymx)
{
  autoscale = false;
  xmin = xmn; ymin = ymn;
  xmax = xmx; ymax = ymx;
  redraw();
}

void Plot::get_range(double* xmn, double* xmx, double* ymn, double* ymx)
{
  if (xmn!=0) *xmn = xmin;
  if (ymn!=0) *ymn = ymin;
  if (xmx!=0) *xmx = xmax;
  if (ymx!=0) *ymx = ymax;
}

void Plot::set_color(int c)
{
  if (c<0)
    fgcol = fl_rgb_color(colors[0][0],colors[0][1],colors[0][2]);
  else if (c>16)
    fgcol = fl_rgb_color(colors[16][0],colors[16][1],colors[16][2]);
  else
    fgcol = fl_rgb_color(colors[c][0],colors[c][1],colors[c][2]);
}

void Plot::erase()
{
  obj.clear();
  vgrd=fvec();
  redraw();
}

void Plot::lines(const fvec& x, const fvec& y, int lw, int style)
{
  LineObject *o = new LineObject(x,y,fgcol,lw,style);
  obj.push_back(o);
  redraw();
}

void Plot::points(const fvec& x, const fvec& y, int sz, bool fill, int lw)
{
  PointObject *o = new PointObject(x,y,fgcol,sz,fill,lw);
  obj.push_back(o);
  redraw();
}

void Plot::gridv(const arma::fvec& x)
{
  vgrd=x;
  redraw();
}

bool Plot::save(const char* fname)
{
  FILE *fp = fopen(fname, "wt");
  if (!fp) return false;

  bool first=true;
  std::list<Object*>::iterator it;
  for (it=obj.begin(); it!=obj.end(); ++it)
  {
    if (first)
      first = false;
    else
      fprintf(fp, "\n\n");
    (*it)->save(fp);
  }

  fclose(fp);
  return true;
}
