/*
 real_input.cpp

 FLTK input field for real numbers.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <sstream>
#include <string>
#include "real_input.h"

using namespace std;
using namespace splits;

Real_input::Real_input(int X, int Y, int W, int H, const char* l)
  : Fl_Float_Input(X,Y,W,H,l)
{
}

float Real_input::number() const
{
  string str = value();
  stringstream sstr(str);
  float num;
  sstr >> num;
  return num;
}

void Real_input::set_number(float num)
{
  stringstream sstr;
  sstr << num;
  string str = sstr.str();
  value(str.c_str());
}

bool Real_input::is_empty() const
{
  string str = value();
  return (str=="");
}
