/*
 main_window.cpp

 The FLTK main window for the splview application.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2020  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <sstream>
#include <fstream>
#include <cfloat>
#include <cstdio>
#include <FL/Fl_Window.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Round_Button.H>
#include <FL/Fl_Int_Input.H>
#include <FL/Fl_File_Chooser.H>
#include <FL/fl_ask.H>
#include <gdal_priv.h>
#include "main_window.h"
#include "doy_window.h"
#include "prepro_window.h"
#include "load_window.h"
#include "about_window.h"
#include "spline_window.h"
#include "weight_window.h"
#include "xval_window.h"
#include "optim_window.h"
#include "splfit_window.h"
#include "splcal_window.h"
#include "phencal_window.h"
#include "meta_window.h"
#include "dataset_input.h"
#include "time_axis.h"
#include "knot.h"
#include "b_spline.h"
#include "season.h"
#include "weight.h"
#include "dataset.h"
#include "common.h"
#include "convex_hull.h"
#include "conf.h"

using namespace arma;
using namespace splits;

Fl_Menu_Item mainmenu_table[] = {
  {"&File",0,0,0,FL_SUBMENU},
    {"&Open...",0,Main_window::load_data_cb},
    {"&Load Config...",0,Main_window::load_conf_cb},
    {"&Close",0,Main_window::unload_data_cb,0,FL_MENU_DIVIDER},
    {"&Save Config...",0,Main_window::save_conf_cb},
    {"S&ave Plot Data...",0,Main_window::save_plot_cb},
    {"&Export",0,0,0,FL_SUBMENU|FL_MENU_DIVIDER},
      {"Raw Time Series Data...",0,Main_window::export_raw_cb},
      {"Data, Curve, Derivatives...",0,Main_window::export_data_cb},
      {"Knot Vector...",0,Main_window::export_knots_cb},
      {"Extreme Points...",0,Main_window::export_points_cb},
      {"Roots...",0,Main_window::export_roots_cb},
      {"Full Seasons...",0,Main_window::export_seasons_cb},
      {0},
    {"Create &DOY File (Abscissae)...",0,Main_window::doy_cb},
    {"&Prepare Data (prepro)...",0,Main_window::prepro_cb},
    {"&Materialize Metafile...",0,Main_window::materialize_cb,0,FL_MENU_DIVIDER},
    {"&Quit",0,Main_window::quit_cb},
    {0},
  {"&Edit",0,0,0,FL_SUBMENU},
    {"&Spline...",0,Main_window::spline_cb},
    {"&Weights...",0,Main_window::weight_cb,0,FL_MENU_DIVIDER},
    {"&Hemisphere...",0,Main_window::hemi_cb},
    {0},
  {"&View",0,0,0,FL_SUBMENU},
    {"&Roots...",0,Main_window::roots_cb},
    {"&Show",0,0,0,FL_SUBMENU|FL_MENU_DIVIDER},
      {"Data",0,Main_window::show_cb,(void*)0,FL_MENU_TOGGLE|FL_MENU_VALUE},
      {"Data Points",0,Main_window::show_cb,(void*)1,FL_MENU_TOGGLE},
      {"Spline Curve",0,Main_window::show_cb,(void*)2,FL_MENU_TOGGLE|FL_MENU_VALUE},
      {"Knots",0,Main_window::show_cb,(void*)3,FL_MENU_TOGGLE},
      {"Control Polyline",0,Main_window::show_cb,(void*)4,FL_MENU_TOGGLE},
      {"Extreme Points",0,Main_window::show_cb,(void*)5,FL_MENU_TOGGLE},
      {"Inflection Points",0,Main_window::show_cb,(void*)6,FL_MENU_TOGGLE},
      {"Roots",0,Main_window::show_cb,(void*)7,FL_MENU_TOGGLE},
      {"Full Seasons",0,Main_window::show_cb,(void*)8,FL_MENU_TOGGLE},
      {"Skeleton",0,Main_window::show_cb,(void*)9,FL_MENU_TOGGLE},
      {"Convex Hull",0,Main_window::show_cb,(void*)10,FL_MENU_TOGGLE|FL_MENU_DIVIDER},
      {"Single",0,Main_window::multi_cb,(void*)0,FL_MENU_RADIO|FL_MENU_VALUE},
      {"All",0,Main_window::multi_cb,(void*)1,FL_MENU_RADIO},
      {0},
    {"Use Color",0,Main_window::color_cb,0,FL_MENU_TOGGLE},
    {"Autorange",0,Main_window::auto_cb,0,FL_MENU_TOGGLE|FL_MENU_VALUE},
    {"Range...",0,Main_window::range_cb,0,FL_MENU_DIVIDER},
    {0},
  {"&Tools",0,0,0,FL_SUBMENU},
    {"&Cross Validation",0,0,0,FL_SUBMENU},
      {"# of &Spans...",0,Main_window::spans_cb},
      {0},
    {"&Optimize # of Spans...",0,Main_window::optim_cb},
    {0},
  {"&Options",0,0,0,FL_SUBMENU},
    {"&Parallel Processing...",0,Main_window::parallel_cb},
    {"&Quiet mode",0,Main_window::quiet_cb,0,FL_MENU_TOGGLE},
    {0},
  {"&Commands",0,0,0,FL_SUBMENU},
    {"&Fit (splfit)...",0,Main_window::splfit_cb,0,FL_MENU_DIVIDER},
    {"&Interpolate (splcal)...",0,Main_window::splcal_cb},
    {"&Phenology (phencal)...",0,Main_window::phencal_cb},
    {0},
  {"&Help",0,0,0,FL_SUBMENU},
    {"&About...",0,Main_window::about_cb},
    {0},
  {0}
};

Fl_Menu_Item monmenu[] =
  {
    {"Goto...",0,Main_window::goto_cb},
    {0}
  };

Fl_Menu_Item pltmenu[] =
  {
    {"Range...",0,Main_window::range_cb},
    {0}
  };

const float Main_window::in_offset = 0.0f;
const float Main_window::in_gain = 1.0f;
const float Main_window::in_nodata = -32767.0f;

Main_window::Main_window()
  : Fl_Double_Window(640, 480, "splview"), quiet(false), nthread(1), spline(0),
    knots(0), yzero(0.0f), current(-1), multiple(false), useclr(false)
{
  mainmenu = new Fl_Menu_Bar(0,0,w(),25);
  mainmenu->menu(mainmenu_table);

  int ht = mainmenu->h();

  Fl_Group *bargrp = new Fl_Group(0,ht,w(),30);
  toolbar = new Toolbar(0,ht,w(),30,this);
  bargrp->end();
  ht+=30;

  Fl_Tile *tile = new Fl_Tile(0,ht,w(),480-ht);
  Fl_Group *pltgrp = new Fl_Group(0,ht,w(),125);
  Fl_Box *pltbox = new Fl_Box(0,ht,w(),125);
  pltbox->box(FL_DOWN_BOX);
  plot = new Plot(5,ht+5,w()-10,115,0,pltmenu);
  pltgrp->end();
  pltgrp->resizable(plot);
  ht+=125;
  Fl_Group *mongrp = new Fl_Group(0,ht,w(),480-ht);
  mon = new Monitor(0,ht,w(),480-ht,0,monmenu);
  mon->box(FL_DOWN_BOX);
  mon->callback(mon_cb, this);
  mongrp->end();
  mongrp->resizable(mon);
  tile->end();
  resizable(tile);

  callback(window_cb);

  sw = new Spline_window(this);
  ww = new Weight_window(this);
  xw_span = new Span_xval_window(this);
  ow = new Optim_window(this);

  int i;
  for (i=1; i<11; i++) showarr[i]=false;
  showarr[0]=true;
  showarr[2]=true;
}

Main_window::~Main_window()
{
}

void Main_window::update(bool new_data)
{
  if (current<0) {
    mon->erase();
    plot->erase();
    return;
  }

  if (new_data) {
    mon->display(data[current], band[current]);
    sw->set_spline_type(spline_type[current]);
    sw->set_knot_type(knot_type[current]);
    sw->set_degree(degree[current]);
    sw->set_smoothness(lambda[current]);
    sw->set_penalty(pord[current]);
    sw->set_nspan(nspan[current]);
    xw_span->reset();
    ow->reset();
  }

  switch(data[current].weight_mode())
  {
  case Archive::QUALITY:
    ww->set_mode(Weight_window::TABLE);
    ww->set_use(use_weights[current]);
    break;
  case Archive::CONSTANT:
    ww->set_mode(Weight_window::NO_TABLE);
    ww->set_use(use_weights[current]);
    break;
  default:
    ww->set_mode(Weight_window::OFF);
  }

  if (spline_type[current]==Spline_window::B_SPLINE
      && (knot_type[current]==Spline_window::UNIFORM
	  || knot_type[current]==Spline_window::PERIODIC)) {
    xw_span->enable();
  } else {
    xw_span->enable(false);
  }

  setup_skeleton();

  double coords[2];
  toolbar->gw->pixel_pos(coords);
  plot->erase();
  if (showarr[9]) plot->gridv(seas_grid);
  if (multiple) {
    Date d0 = data[0].begin();
    Date d1 = data[0].end();
    int item;
    for (item=1; item<data.size(); item++) {
      if (data[item].begin()<d0) d0 = data[item].begin();
      if (data[item].end()>d1) d1 = data[item].end();
    }
    for (item=0; item<data.size(); item++) {
      fit(coords, item, d0, d1);
    }
  } else {
    Date d0 = data[current].begin();
    Date d1 = data[current].end();
    fit(coords, current, d0, d1);
  }
}

void Main_window::load_data()
{
  current = data.size()-1;
  if (current<0) return;

  char **files = data[current].file_list();

  std::string str;
  int i, j=1;
  do {
    str = "&View/";
    str += fl_filename_name(files[0]);
    if (j>1) {
      char tmp[16];
      sprintf(tmp, " (%d)", j);
      str += tmp;
    }
    i = get_index_by_name(mainmenu, str.c_str());
    j += 1;
  } while (i!=-1);
  i = mainmenu->add(str.c_str(), "", view_cb, 0, FL_MENU_RADIO);
  Fl_Menu_Item *m = (Fl_Menu_Item*)&(mainmenu->menu()[i]);
  m->setonly();
  items.push_back(str);

  update(true);

  for (i=0; i<3; i++) delete [] files[i];
  delete [] files; 
}

void Main_window::unload_data()
{
  if (current<0) return;

  int i = get_index_by_name(mainmenu, items[current].c_str());
  mainmenu->remove(i);

  data.erase(data.begin()+current);
  band.erase(band.begin()+current);
  nspan.erase(nspan.begin()+current);
  degree.erase(degree.begin()+current);
  spline_type.erase(spline_type.begin()+current);
  lambda.erase(lambda.begin()+current);
  pord.erase(pord.begin()+current);
  knot_type.erase(knot_type.begin()+current);
  use_weights.erase(use_weights.begin()+current);
  w_tab_file.erase(w_tab_file.begin()+current);
  items.erase(items.begin()+current);

  if (data.size()>0) {
    if (current>=data.size()) current = data.size()-1;
    set_menu_item_state(mainmenu, items[current].c_str(), 1);
  } else {
    current = -1;
  }

  update(true);
}

void Main_window::fit(double* coords, int item, const Date& d0, const Date& d1)
{
  delete [] knots;
  delete spline;

  knots = 0;
  spline = 0;

  int nobs = data[item].poke(coords, xt, yt, wt, d0, d1);
  if (nobs<=0) return;

  if (useclr) plot->set_color(item+3);

  if (showarr[0]) plot->lines(xt,yt);
  if (showarr[1]) plot->points(xt,yt,9,false,0);
 
  int dim_basis;
  bool peri;
  switch (knot_type[item]) {
  case Spline_window::UNIFORM:
  case Spline_window::NONUNIFORM:
    knots = clamped_uniform_knot_vector(xt[0], xt[xt.n_elem-1], nspan[item],
					degree[item]+1, &dim_basis);
    peri = false;
    break;
  case Spline_window::PERIODIC:
  case Spline_window::NONUNIFORM_PERIODIC:
    knots = periodic_uniform_knot_vector(xt[0], xt[xt.n_elem-1], nspan[item],
					 degree[item]+1, &dim_basis);
    peri = true;
    break;
  case Spline_window::AVERAGE:
    knots = average_knot_vector(xt.memptr(), data[item].count(), degree[item]+1,
				&dim_basis);
    peri = false;
    break;
  case Spline_window::AVERAGE_PERIODIC:
    knots = periodic_average_knot_vector(xt.memptr(),data[item].count(),
					 degree[item]+1,&dim_basis);
    peri = true;
  default:
    if (useclr) plot->set_color();
    return;
  }

  switch (spline_type[item]) {
  case Spline_window::B_SPLINE:
    spline = new B_spline(knots,dim_basis,degree[item]+1,peri);
    break;
  case Spline_window::SMOOTH_B_SPLINE:
    spline = new Smooth_spline(knots,dim_basis,degree[item]+1,lambda[item],peri);
    break;
  case Spline_window::P_SPLINE:
    spline = new P_spline(knots,dim_basis,degree[item]+1,lambda[item],
			  pord[item],peri);
    break;
  default:
    if (useclr) plot->set_color();
    return;
  }

  bool fit_good;
  if (wt.is_empty()||!use_weights[current]||(ww->mode()==Weight_window::OFF)) {
    if ((knot_type[item]==Spline_window::NONUNIFORM)
	|| knot_type[item]==Spline_window::NONUNIFORM_PERIODIC)
      fit_good = spline->opt(xt, yt);
    else
      fit_good = spline->fit(xt, yt);
  } else {
    if ((knot_type[item]==Spline_window::NONUNIFORM)
	|| knot_type[item]==Spline_window::NONUNIFORM_PERIODIC)
      fit_good = spline->opt(xt, yt, wt);
    else
      fit_good = spline->fit(xt, yt, wt);
  }
  
  if (!fit_good) {
    if (useclr) plot->set_color();
    delete spline;
    spline = 0;
    return;
  }

  if (showarr[2]) {
    xh = xt;
    yh = spline->eval(xh);
    plot->lines(xh,yh,2);
  }
  if (showarr[3]) {
    xk = spline->knots();
    yk = spline->eval(xk);
    plot->points(xk,yk,9);
  }
  if (showarr[4]) {
    B_spline *bspl = (B_spline*)spline;
    bspl->ctrl_polyln(xp,yp);
    plot->lines(xp,yp,0,FL_DOT);
  }
  if (showarr[5]) {
    extreme_points(spline,xmn,ymn,xmx,ymx);
    plot->points(xmn,ymn,9,false);
    plot->points(xmx,ymx,9);
  }
  if (showarr[6]) {
    fvec x, y;
    inflection_points(spline,x,y);
    plot->points(x,y,7);
  }
  if (showarr[7]) {
    xr = spline->inv(yzero);
    yr = spline->eval(xr);
    if (xr.n_elem>0) plot->points(xr,yr,9);
  }
  if (showarr[8]) {
    identify_seasons(seas,spline,1);
    get_peaking_time_and_val();
    get_start_time_and_val();
    get_end_time_and_val();
    plot->points(xpeak,ypeak,9);
    plot->points(xstart,ystart,9,false);
    plot->points(xend,yend,9,false);
  }
  if (showarr[10]) {
    fvec x = spline->roots(1);
    fvec y = spline->eval(x);
    std::vector<int> hull = convex_hull(x,y);
    fvec hull_x(hull.size());
    fvec hull_y(hull.size());
    int i;
    for (i=0; i<hull.size(); i++) {
      hull_x[i] = x[hull[i]];
      hull_y[i] = y[hull[i]];
    }
    plot->points(hull_x,hull_y,9);
    plot->lines(hull_x,hull_y,2,FL_DASH);
  }

  if (useclr) plot->set_color();
}

void Main_window::get_start_time_and_val()
{
  int i, n = seas.size();
  for (i=0; i<seas.size(); i++) {
    if (!seas[i]) n-= 1;
  }
  xstart = fvec(n);
  ystart = fvec(n);
  int j = 0;
  for (i=0; i<seas.size(); i++)
  {
    if (!seas[i]) continue;
    xstart(j) = seas[i].start.x;
    ystart(j) = seas[i].start.y;
    j += 1;
  }
}

void Main_window::get_peaking_time_and_val()
{
  int i, n = seas.size();
  for (i=0; i<seas.size(); i++) {
    if (!seas[i]) n-= 1;
  }
  xpeak = fvec(n);
  ypeak = fvec(n);
  int j = 0;
  for (i=0; i<seas.size(); i++)
  {
    if (!seas[i]) continue;
    xpeak(j) = seas[i].peak.x;
    ypeak(j) = seas[i].peak.y;
    j += 1;
  }
}

void Main_window::get_end_time_and_val()
{
  int i, n = seas.size();
  for (i=0; i<seas.size(); i++) {
    if (!seas[i]) n-= 1;
  }
  xend = fvec(n);
  yend = fvec(n);
  int j = 0;
  for (i=0; i<seas.size(); i++)
  {
    if (!seas[i]) continue;
    xend(j) = seas[i].end.x;
    yend(j) = seas[i].end.y;
    j += 1;
  }
}

void Main_window::setup_skeleton()
{
  Date d0, d1;
  if (multiple) {
    d0 = data[0].begin();
    d1 = data[0].end();
    int item;
     for (item=1; item<data.size(); item++) {
      if (data[item].begin()<d0) d0 = data[item].begin();
      if (data[item].end()>d1) d1 = data[item].end();
    }
  } else {
    d0 = data[current].begin();
    d1 = data[current].end();
  }

  seas = create_skeleton(d0, d1, data[current].is_southern(), 0, 1);
  if (!seas.empty()) {
    seas_grid = fvec(seas.size()+1);
    int i;
    for (i=0; i<seas.size(); i++) {
      seas_grid(i) = seas[i].startIndex;
    }
    seas_grid(seas.size()) = seas[seas.size()-1].endIndex;
  }
}

void Main_window::doy_cb(Fl_Widget* w, void* data)
{
  DOY_window win;
  while (win.visible()) {
    Fl::wait();
  }
  if (!win.okay()) return;

  Date start = win.start_date();
  if (!start) {
    fl_alert("Invalid start date!");
    return;
  }
  int period = win.period();
  int length = win.length();

  std::vector<Date> ttab;
  switch(win.bound()) {
  case DOY_window::BOUNDED_BY_YEAR:
    ttab = gen_timetable_by_year(start, period, length);
    break;
  case DOY_window::BOUNDED_BY_MONTH:
    ttab = gen_timetable_by_month(start, period, length);
    break;
  default:
    break;
  }

  if (win.center()) {
    int shift = period/2;
    std::vector<Date>::iterator it;
    for (it=ttab.begin(); it!=ttab.end(); ++it) (*it)+=shift;
  }

  std::ofstream ofs;
  ofs.open(win.filename());
  if (ofs.is_open()) {
    std::vector<Date>::iterator it;
    for (it=ttab.begin(); it!=ttab.end(); ++it) {
      ofs << (*it) << std::endl;
    }
    ofs.close();
  } else {
    fl_alert("Cannot open file for output!");
  }
}

void Main_window::prepro_cb(Fl_Widget* w, void* data)
{
  Prepro_window win;
  while (win.visible()) {
    Fl::wait();
  }
}

void Main_window::load_data_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)w->parent();
  Load_window win(mainwin->x(), mainwin->y());
  win.set_time_series_offset(mainwin->in_offset);
  win.set_time_series_scale(mainwin->in_gain);
  win.set_nodata(mainwin->in_nodata);
  win.show();
  while (win.visible()) {
    Fl::wait();
  }

  if (!win.okay()) return;

  Archive ar(win.time_series_dataset(),
	     win.doy_dataset(),
	     win.quality_dataset());

  ar.set_offset(win.time_series_offset());
  ar.set_gain(win.time_series_scale());

  ar.set_nodata(win.nodata());

  ar.set_begin(win.start_date());
  ar.set_end(win.end_date());

  if (ar.open()) {
    mainwin->data.push_back(ar);
    mainwin->band.push_back(0);
    mainwin->nspan.push_back(ar.count()-1);
    mainwin->degree.push_back(3);
    mainwin->spline_type.push_back(Spline_window::B_SPLINE);
    mainwin->lambda.push_back(0.0f);
    mainwin->pord.push_back(2.0f);
    mainwin->knot_type.push_back(Spline_window::AVERAGE);
    mainwin->use_weights.push_back(false);
    mainwin->w_tab_file.push_back(std::string(""));
    mainwin->load_data();
  } else {
    fl_alert("Error loading input!");
  }
}

void Main_window::unload_data_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)(w->parent());
  mainwin->unload_data();
}

void Main_window::load_conf_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)w->parent();
  Fl_File_Chooser fc(".", "*", Fl_File_Chooser::SINGLE, "Load Config");
  fc.show();
  while (fc.visible())
  {
    Fl::wait();
  }
  const char *filename = fc.value();
  if (filename==0) return;

  Conf c(filename);

  Archive ar(c.ts_dset.c_str(), c.dy_dset.c_str(), c.qa_dset.c_str());

  ar.set_offset(c.offs);
  ar.set_gain(c.gain);

  ar.set_nodata(c.nodata);

  ar.set_begin(c.d_beg);
  ar.set_end(c.d_end);

  if (ar.open()) {
    mainwin->data.push_back(ar);
    mainwin->band.push_back(0);
    mainwin->nspan.push_back(ar.count()-1);
    mainwin->degree.push_back(3);
    mainwin->spline_type.push_back(Spline_window::B_SPLINE);
    mainwin->lambda.push_back(0.0f);
    mainwin->pord.push_back(2.0f);
    mainwin->knot_type.push_back(Spline_window::AVERAGE);
    if (c.w_tab_file.empty()) {
      mainwin->use_weights.push_back(false);
      mainwin->w_tab_file.push_back(std::string(""));
    } else {
      if (mainwin->ww->load(c.w_tab_file.c_str())) {
	mainwin->ww->set_use(c.use_wt);
	mainwin->ww->check();
	mainwin->use_weights.push_back(c.use_wt);
	mainwin->w_tab_file.push_back(c.w_tab_file);
	int cur = mainwin->data.size()-1;
	mainwin->data[cur].set_weight_table(mainwin->ww->table());
      } else {
	mainwin->use_weights.push_back(false);
	mainwin->w_tab_file.push_back(std::string(""));
      }
    }
    mainwin->load_data();
  } else {
    fl_alert("Error loading input!");
  }
}

void Main_window::save_conf_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)w->parent();
  Conf c(mainwin);
  Fl_File_Chooser fc(".", "*", Fl_File_Chooser::CREATE, "Save Config");
  fc.show();
  while (fc.visible())
  {
    Fl::wait();
  }
  const char *filename = fc.value();
  if (filename==0) return;
  c.save(filename);
}

void Main_window::save_plot_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)(w->parent());
  Fl_File_Chooser fc(".", "*", Fl_File_Chooser::CREATE, "Save Plot Data");
  fc.show();
  while (fc.visible())
  {
    Fl::wait();
  }
  const char *filename = fc.value();
  if (filename==0) return;
  mainwin->plot->save(filename);
}

void Main_window::export_raw_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)(w->parent());
  Fl_File_Chooser fc(".", "*", Fl_File_Chooser::CREATE,
		     "Export Raw Time Series Data");
  fc.show();
  while (fc.visible())
  {
    Fl::wait();
  }
  const char *filename = fc.value();
  if ((filename==0) || (mainwin->current<0) || (mainwin->spline==0)) return;

  double coords[2];
  mainwin->toolbar->gw->pixel_pos(coords);

  Date d0 = mainwin->data[mainwin->current].begin();
  Date d1 = mainwin->data[mainwin->current].end();

  fvec doy, xv, vi, yv, qa, wv;

  mainwin->data[mainwin->current].poke_raw(coords, doy, vi, qa);
  
  int nobs = mainwin->data[mainwin->current].poke(coords, xv, yv, wv, d0, d1);

  if (nobs<=0) return;

  if ((int)wv.n_elem == 0) {
    wv = fvec(nobs);
    wv.ones();
  }

  FILE *fp = fopen(filename, "wt");

  fprintf(fp,"# DOY\tTime [0..1]\tRaw Data\tScaled Data\tQuality Flag\tWeight\
             \n");
  
  int i;
  for (i=0; i<nobs; i++) {
    fprintf(fp, "%f\t%f\t%f\t%f\t%f\t%f\n", doy(i), xv(i), vi(i), yv(i), qa(i),
	    wv(i));
  }
  
  fclose(fp);
}

void Main_window::export_data_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)(w->parent());
  Fl_File_Chooser fc(".", "*", Fl_File_Chooser::CREATE,
		     "Export Data, Curve and Derivatives");
  fc.show();
  while (fc.visible())
  {
    Fl::wait();
  }
  const char *filename = fc.value();
  if ((filename==0) || (mainwin->current<0) || (mainwin->spline==0)) return;

  fvec* varray = new fvec[mainwin->spline->order()];
  int k;
  for (k=0; k<mainwin->spline->order(); k++)
    varray[k] = mainwin->spline->eval(mainwin->xt, k);

  FILE *fp = fopen(filename, "wt");
  int i, n=mainwin->xt.n_elem;
  for (i=0; i<n; i++) {
    fprintf(fp, "%f %f", mainwin->xt(i), mainwin->yt(i));
    for (k=0; k<mainwin->spline->order(); k++) fprintf(fp, " %f", varray[k](i));
    fprintf(fp, "\n");
  }
  fclose(fp);

  delete [] varray;
}

void Main_window::export_knots_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)(w->parent());
  if (mainwin->spline == NULL) return;
  
  Fl_File_Chooser fc(".", "*", Fl_File_Chooser::CREATE,
		     "Export Knot Vector");
  fc.show();
  while (fc.visible()) {
    Fl::wait();
  }
  const char *filename = fc.value();
  if ((filename==0) || (mainwin->current<0) || (mainwin->spline==0)) return;

  fvec knots = mainwin->spline->knots();

  Date d0 = mainwin->data[mainwin->current].begin();
  Date d1 = mainwin->data[mainwin->current].end();
  
  FILE *fp = fopen(filename, "wt");
  int i;
  for (i=0; i<knots.n_elem; i++) {
    Date d = convert_index_to_date(knots(i), d0, d1);
    fprintf(fp, "%f %f %s\n", knots(i), convert_index_to_doy(knots(i), d0, d1),
	    d.to_string().c_str());
  }
  fclose(fp);
}

void Main_window::export_points_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)(w->parent());
  Fl_File_Chooser fc(".", "*", Fl_File_Chooser::CREATE,
		     "Export Extreme Points");
  fc.show();
  while (fc.visible())
  {
    Fl::wait();
  }
  const char *filename = fc.value();
  if ((filename==0) || (mainwin->current<0) || (mainwin->spline==0)) return;

  extreme_points(mainwin->spline,
		 mainwin->xmn,
		 mainwin->ymn,
		 mainwin->xmx,
		 mainwin->ymx);

  FILE *fp = fopen(filename, "wt");
  int i;
  for (i=0; i<(mainwin->xmn.n_elem); i++) {
    fprintf(fp, "%f %f 1\n", mainwin->xmn(i), mainwin->ymn(i));
  }
  for (i=0; i<(mainwin->xmx.n_elem); i++) {
    fprintf(fp, "%f %f -1\n", mainwin->xmx(i), mainwin->ymx(i));
  }
  fclose(fp);
}

void Main_window::export_roots_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)(w->parent());
  Fl_File_Chooser fc(".", "*", Fl_File_Chooser::CREATE,
		     "Export Roots");
  fc.show();
  while (fc.visible())
  {
    Fl::wait();
  }
  const char *filename = fc.value();
  if ((filename==0) || (mainwin->current<0) || (mainwin->spline==0)) return;

  mainwin->xr = mainwin->spline->inv(mainwin->yzero);
  mainwin->yr = mainwin->spline->eval(mainwin->xr);

  if (mainwin->xr.n_elem == 0) return;

  FILE *fp = fopen(filename, "wt");
  int i;
  for (i=0; i<(mainwin->xr.n_elem); i++) {
    fprintf(fp, "%f %f\n", mainwin->xr(i), mainwin->yr(i));
  }
  fclose(fp);
}

void Main_window::export_seasons_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)(w->parent());
  Fl_File_Chooser fc(".", "*", Fl_File_Chooser::CREATE,
		     "Export Full Seasons");
  fc.show();
  while (fc.visible())
  {
    Fl::wait();
  }
  const char *filename = fc.value();
  if ((filename==0) || (mainwin->current<0) || (mainwin->spline==0)) return;

  identify_seasons(mainwin->seas, mainwin->spline, 1);
  mainwin->get_peaking_time_and_val();
  mainwin->get_start_time_and_val();
  mainwin->get_end_time_and_val();

  FILE *fp = fopen(filename, "wt");
  fprintf(fp, "# xstart xpeak xend ystart ypeak yend\n");
  int i;
  for (i=0; i<(mainwin->xstart.n_elem); i++) {
    fprintf(fp, "%f %f %f %f %f %f\n",
	    mainwin->xstart(i), mainwin->xpeak(i), mainwin->xend(i),
	    mainwin->ystart(i), mainwin->ypeak(i), mainwin->yend(i));
  }
  fclose(fp);
}

void Main_window::materialize_cb(Fl_Widget* w, void* data)
{
  Meta_window win;
  while (win.visible()) Fl::wait();
  if (!win.okay()) return;
  const char *mfile = win.meta_inp->filename();
  Meta_dataset m(mfile);
  const char *ofile = win.outp_inp->filename();
  m.materialize(ofile);
}

void Main_window::about_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)w->parent();
  About_window win(mainwin->x(), mainwin->y());
  win.show();
  while (win.visible()) {
    Fl::wait();
  }
}

void Main_window::quit_cb(Fl_Widget* w, void* data)
{
  Main_window* win = (Main_window*)w->parent();
  if (win->current >= 0) {
    if (!fl_ask("Close all files and quit splview?")) return;
  }
  win->sw->hide();
  win->ww->hide();
  win->xw_span->hide();
  win->toolbar->gw->hide();
  Fl::default_atclose(win, data);
}

void Main_window::window_cb(Fl_Widget* w, void* data)
{
  Main_window* win = (Main_window*)w;
  if (win->current >= 0) {
    if (!fl_ask("Close all files and quit splview?")) return;
  }
  win->sw->hide();
  win->ww->hide();
  win->xw_span->hide();
  win->toolbar->gw->hide();
  Fl::default_atclose(win, data);
}

void Main_window::color_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)w->parent();
  Fl_Menu_Item *item = (Fl_Menu_Item*)w;

  if (mainwin->useclr) {
    mainwin->useclr = false;
    item->clear();
  } else {
    mainwin->useclr = true;
    item->set();
  }
  mainwin->update(false);
}

void Main_window::auto_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)w->parent();
  Fl_Menu_Item *item = (Fl_Menu_Item*)w;

  if (mainwin->plot->is_autorange())
  {
    mainwin->plot->set_autorange(false);
    item->clear();
  }
  else
  {
    mainwin->plot->set_autorange();
    item->set();
  }
}

void input_cb(Fl_Widget*, void* p)
{
  Fl_Window *w = (Fl_Window*)p;
  w->hide();
}
void Main_window::range_cb(Fl_Widget* w, void* data)
{
  char str[33];
  double xmin, ymin, xmax, ymax;
  Fl_Widget *p = w;
  while (p->parent()!=0) {
    p = p->parent();
  }
  Main_window *mainwin = (Main_window*)p;
  mainwin->plot->get_range(&xmin, &xmax, &ymin, &ymax);
  Fl_Window win(210,175, "Axis Ranges");
  Fl_Group g1(5,20,200,65,"Horizontal:");
  g1.box(FL_BORDER_BOX);
  g1.align(Fl_Align(FL_ALIGN_TOP_LEFT));
  Fl_Float_Input g1_inp1(50,25,145,25,"Min:");
  g1_inp1.maximum_size(32);
  sprintf(str, "%g", xmin);
  g1_inp1.value(str);
  g1_inp1.callback(input_cb, &win);
  g1_inp1.when(FL_WHEN_ENTER_KEY|FL_WHEN_NOT_CHANGED);
  Fl_Float_Input g1_inp2(50,55,145,25,"Max:");
  g1_inp2.maximum_size(32);
  sprintf(str, "%g", xmax);
  g1_inp2.value(str);
  g1_inp2.callback(input_cb, &win);
  g1_inp2.when(FL_WHEN_ENTER_KEY|FL_WHEN_NOT_CHANGED);
  g1.end();
  Fl_Group g2(5,105,200,65,"Vertical:");
  g2.box(FL_BORDER_BOX);
  g2.align(Fl_Align(FL_ALIGN_TOP_LEFT));
  Fl_Float_Input g2_inp1(50,110,145,25,"Min:");
  g2_inp1.maximum_size(32);
  sprintf(str, "%g", ymin);
  g2_inp1.value(str);
  g2_inp1.callback(input_cb, &win);
  g2_inp1.when(FL_WHEN_ENTER_KEY|FL_WHEN_NOT_CHANGED);
  Fl_Float_Input g2_inp2(50,140,145,25,"Max:");
  g2_inp2.maximum_size(32);
  sprintf(str, "%g", ymax);
  g2_inp2.value(str);
  g2_inp2.callback(input_cb, &win);
  g2_inp2.when(FL_WHEN_ENTER_KEY|FL_WHEN_NOT_CHANGED);
  g2.end();
  win.set_modal();
  win.hotspot(&win);
  win.show();
  while (win.visible())
  {
    Fl::wait();
  }
  xmin = atof(g1_inp1.value());
  ymin = atof(g2_inp1.value());
  xmax = atof(g1_inp2.value());
  ymax = atof(g2_inp2.value());
  mainwin->plot->set_range(xmin, xmax, ymin, ymax);
  set_menu_item_state(mainwin->mainmenu, "&View/Autorange", 0);
}

void Main_window::mon_cb(Fl_Widget* w, void* data)
{
  Monitor *m = (Monitor*)w;
  Main_window *mainwin = (Main_window*)data;
  mainwin->toolbar->gw->update();
  mainwin->update(false);
}

void Main_window::goto_cb(Fl_Widget* w, void* data)
{
  Fl_Widget *p = w;
  while (p->parent()!=0) {
    p = p->parent();
  }
  Main_window *mainwin = (Main_window*)p;
  mainwin->toolbar->gw->hotspot(mainwin->toolbar->gw);
  mainwin->toolbar->gw->show();
}

void Main_window::roots_cb(Fl_Widget* w, void* data)
{
  char str[33];
  Main_window* mainwin = (Main_window*)w->parent();
  Fl_Window win(150, 25, "Ordinate");
  Fl_Float_Input inp(0,0,150,25);
  inp.maximum_size(32);
  sprintf(str, "%g", mainwin->yzero);
  inp.value(str);
  inp.callback(Main_window::roots_input_cb, &win);
  inp.when(FL_WHEN_ENTER_KEY|FL_WHEN_NOT_CHANGED);
  win.set_modal();
  win.hotspot(&win);
  win.show();
  while (win.visible()) {
    Fl::wait();
  }
  mainwin->yzero = (float)atof(inp.value());
  mainwin->update(false);
}

void Main_window::roots_input_cb(Fl_Widget* w, void* data)
{
  Fl_Window *win = (Fl_Window*)data;
  win->hide();
}

void Main_window::show_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)w->parent();
  Fl_Menu_Item *item = (Fl_Menu_Item*)w;
  long int n = (long int)data;
  if (mainwin->showarr[n]) {
    mainwin->showarr[n] = false;
    item->clear();
  } else {
    mainwin->showarr[n] = true;
    item->set();
  }
  mainwin->update(false);
}

void Main_window::spline_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)w->parent();
  if (mainwin->current<0) return;
  if (mainwin->sw->visible()) return;
  mainwin->sw->set_spline_type(mainwin->spline_type[mainwin->current]);
  mainwin->sw->set_knot_type(mainwin->knot_type[mainwin->current]);
  mainwin->sw->set_degree(mainwin->degree[mainwin->current]);
  mainwin->sw->set_smoothness(mainwin->lambda[mainwin->current]);
  mainwin->sw->set_penalty(mainwin->pord[mainwin->current]);
  mainwin->sw->set_nspan(mainwin->nspan[mainwin->current]);
  mainwin->sw->hotspot(mainwin->sw);
  mainwin->sw->show();
}

void Main_window::weight_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)w->parent();
  if (mainwin->current<0) return;
  if (mainwin->ww->mode()==Weight_window::OFF) return;
  mainwin->ww->hotspot(mainwin->ww);
  mainwin->ww->show();
  while (mainwin->ww->visible()) {
    Fl::wait();
  }
  mainwin->update(false);
}

void Main_window::hemi_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)w->parent();
  if (mainwin->current < 0) return;
  Fl_Window win (200,70);
  Fl_Box *b = new Fl_Box(5,5,190,60);
  b->box(FL_BORDER_BOX);
  Fl_Round_Button *r1 = new Fl_Round_Button(10,10,180,25,"Northern Hemisphere");
  r1->type(FL_RADIO_BUTTON);
  Fl_Round_Button *r2 = new Fl_Round_Button(10,35,180,25,"Southern Hemisphere");
  r2->type(FL_RADIO_BUTTON);
  if (mainwin->data[mainwin->current].is_southern())
    r2->setonly();
  else
    r1->setonly();
  win.size_range(200,70,200,70);
  win.hotspot(&win);
  win.set_modal();
  win.show();
  while (win.visible()) {
    Fl::wait();
  }
  if (r1->value())
    mainwin->data[mainwin->current].hemisphere(false);
  else
    mainwin->data[mainwin->current].hemisphere(true);
  mainwin->setup_skeleton();
  mainwin->update(false);
}

void Main_window::quiet_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)w->parent();
  Fl_Menu_Item *item = (Fl_Menu_Item*)w;
  if (mainwin->quiet) {
    mainwin->quiet = false;
    item->clear();
  } else {
    mainwin->quiet = true;
    item->set();
  }
}

void Main_window::parallel_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)w->parent();
  Fl_Window win(200,35, "Num. of Threads");
  Fl_Int_Input *inp = new Fl_Int_Input(5,5,190,25);
  win.resizable(inp);
  std::stringstream sstr;
  sstr << mainwin->nthread;
  inp->value(sstr.str().c_str());
  win.size_range(200,35,0,35);
  win.hotspot(&win);
  win.set_modal();
  win.show();
  while (win.visible()) {
    Fl::wait();
  }
  mainwin->nthread = atoi(inp->value());
  if (mainwin->nthread < 1) mainwin->nthread = 1;
}

void Main_window::splfit_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)(w->parent());
  if (mainwin->current<0) return;
  int t = mainwin->knot_type[mainwin->current];
  if ((t==Spline_window::NONUNIFORM)
      ||(t==Spline_window::NONUNIFORM_PERIODIC)) {
    return;
  }
  if ((mainwin->ww->use())&&(mainwin->w_tab_file[mainwin->current].empty())) {
    fl_alert("You have not saved your weight table to a file!");
    return;
  }
  Splfit_window win(mainwin);
  while (win.visible()) {
    Fl::wait();
  }
}

void Main_window::splcal_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)(w->parent());
  Splcal_window win(mainwin);
  while (win.visible()) {
    Fl::wait();
  }
}

void Main_window::phencal_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)(w->parent());
  Phencal_window win(mainwin);
  while (win.visible()) {
    Fl::wait();
  }
}

void Main_window::spans_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)w->parent();
  if (mainwin->current<0) return;
  if (mainwin->xw_span->visible()) return;
  mainwin->xw_span->hotspot(mainwin->xw_span);
  mainwin->xw_span->show();
}

void Main_window::optim_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)w->parent();
  if (mainwin->current<0) return;
  if (mainwin->ow->visible()) return;
  mainwin->ow->hotspot(mainwin->ow);
  mainwin->ow->show();
}

void Main_window::view_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)w->parent();
  char picked[1024];
  mainwin->mainmenu->item_pathname(picked, sizeof(picked)-1);
  std::string str(picked);

  int i, j=-1, k;
  for (i=0; i<mainwin->items.size(); i++) {
    if (str.find(mainwin->items[i])!=std::string::npos) {
      j=i;
    }
  }

  mainwin->current = j;
  mainwin->toolbar->gw->reset();
  mainwin->update(true);
}

void Main_window::multi_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)w->parent();
  long int d = (long int)data;
  mainwin->multiple = (bool)d;
  mainwin->update(false);
}


//From Erco's FLTK Cheat Page
//http://seriss.com/people/erco/fltk/#Menu_Toggle
void splits::set_menu_item_state(Fl_Menu_Bar* menubar,
				 const char* name,
				 int state)
{
  Fl_Menu_Item *m = (Fl_Menu_Item*)menubar->find_item(name);
  if ( ! m ) return;
  if ( state ) { m->set(); }
  else         { m->clear(); }
}

//From Erco's FLTK Cheat Page
//http://seriss.com/people/erco/fltk/#Fl_Menu_Bar
int splits::get_index_by_name(Fl_Menu_Bar* menubar, const char* findname)
{
  char menupath[1024] = "";
    for ( int t=0; t < menubar->size(); t++ ) {
        Fl_Menu_Item *m = (Fl_Menu_Item*)&(menubar->menu()[t]);
        if ( m->submenu() ) {
            // Submenu?
            if ( menupath[0] ) strcat(menupath, "/");
            strcat(menupath, m->label());
            if ( strcmp(menupath, findname) == 0 ) return(t);
        } else {
            if ( m->label() == NULL ) {
                // End of submenu? Pop back one level.
                char *ss = strrchr(menupath, '/');
                if ( ss ) *ss = 0;
                else      menupath[0] = '\0';
                continue;
            }
            // Menu item?
            char itempath[1024];
            strcpy(itempath, menupath);
            if ( itempath[0] ) strcat(itempath, "/");
            strcat(itempath, m->label());
            if ( strcmp(itempath, findname) == 0 ) {
                return(t);
            }
        }
    }
    return(-1);
}
