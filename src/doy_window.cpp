/*
 doy_window.cpp

 Create text files with time axis data (DOY files).

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <FL/Fl_Box.H>
#include "doy_window.h"

using namespace splits;

DOY_window::DOY_window() : Fl_Window(590,235), ok(false)
{
  int xx=205, yy=10;
  {
    Fl_Group *g = new Fl_Group(0,yy,580,25);
    file_inp = new Dataset_input(xx,yy,580-xx,25, "Output File:",
				 Dataset_input::CREATE);
    file_inp->callback(check_cb,this);
    file_inp->when(FL_WHEN_CHANGED);
    g->end();
    g->resizable(file_inp);
    yy+=30;
  }
  {
    Fl_Group *g = new Fl_Group(0,yy,580,25);
    start_inp = new Date_input(xx,yy,580-xx,25, "Time Series Start Date:");
    g->end();
    g->resizable(start_inp);
    yy+=30;
  }
  {
    Fl_Group *g = new Fl_Group(0,yy,580,25);
    period_inp = new Real_input(xx,yy,580-xx,25, "Compositing Period (Days):");
    period_inp->callback(check_cb,this);
    period_inp->when(FL_WHEN_CHANGED);
    g->end();
    g->resizable(period_inp);
    yy+=30;
  }
  {
    Fl_Group *g = new Fl_Group(0,yy,580,25);
    center_btn = new Fl_Check_Button(xx,yy,100,25, "Center");
    Fl_Box *b = new Fl_Box(xx+100,yy,480-xx,25);
    g->end();
    g->resizable(b);
    yy+=30;
  }
  {
    Fl_Group *g = new Fl_Group(0,yy,580,25);
    length_inp = new Real_input(xx,yy,580-xx,25, "Time Series Length:");
    length_inp->callback(check_cb,this);
    length_inp->when(FL_WHEN_CHANGED);
    g->end();
    g->resizable(length_inp);
    yy+=30;
  }
  {
    Fl_Group *g = new Fl_Group(0,yy,580,25);
    year_btn = new Fl_Round_Button(55,yy,240,25,
				   "Bounded by Year (e.g. MODIS)");
    year_btn->type(FL_RADIO_BUTTON);
    month_btn = new Fl_Round_Button(295,yy,275,25,
				    "Bounded by Month (e.g. SPOT VGT)");
    month_btn->type(FL_RADIO_BUTTON);
    Fl_Box *b = new Fl_Box(570,yy,10,25);
    g->end();
    g->resizable(b);
    yy += 40;
  }

  year_btn->setonly();

  Fl_Group *g = new Fl_Group(0,yy,580,25);
  Fl_Box *b = new Fl_Box(0,yy,415,25);
  g->resizable(b);
  ok_btn = new Fl_Return_Button(415,yy,80,25, "OK");
  ok_btn->callback(ok_cb, this);
  ok_btn->deactivate();
  cancel_btn = new Fl_Button(500,yy,80,25, "Cancel");
  cancel_btn->callback(cancel_cb, this);
  g->end();

  resizable(g);
  size_range(590,235,0,235);
  set_modal();

  label("Create DOY File (Abscissae)");

  hotspot(this);
  show();
}

bool DOY_window::center()
{
  if (center_btn->value()) return true; else return false;
}

int DOY_window::bound()
{
  if (month_btn->value())
    return BOUNDED_BY_MONTH;
  else
    return BOUNDED_BY_YEAR;
}

void DOY_window::check_cb(Fl_Widget* w, void* data)
{
  DOY_window *win = (DOY_window*)data;

  if (win->file_inp->is_empty()) {
    win->ok_btn->deactivate();
    return;
  }
  if (win->period_inp->is_empty()) {
    win->ok_btn->deactivate();
    return;
  }
  if (win->length_inp->is_empty()) {
    win->ok_btn->deactivate();
    return;
  }

  win->ok_btn->activate();
}

void DOY_window::ok_cb(Fl_Widget* w, void* data)
{
  DOY_window* win = (DOY_window*)data;
  win->set_okay();
  win->hide();
}

void DOY_window::cancel_cb(Fl_Widget* w, void* data)
{
  ((DOY_window*)data)->hide();
}
