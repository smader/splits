/*
 dataset.cpp

 A class to handle special header information and meta files used by
 SPLITS programs.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdio>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <cpl_string.h>
#include <cpl_conv.h>
#include "dataset.h"

using namespace std;
using namespace arma;
using namespace splits;

bool Dataset::is_good()
{
  GDALDataset *dset = (GDALDataset*) GDALOpen(fname.c_str(), GA_ReadOnly);
  if (!dset) return false;
  char **list = dset->GetFileList();
  GDALClose(dset);
  if (!list) return false;
  bool ok=false;
  int i=0;
  while (list[i++]) {
    string str = list[i];
    transform(str.begin(), str.end(), str.begin(), ::tolower);
    if (str.find(".hdr")!=string::npos) {
      ok=true;
      break;
    }
  }
  CSLDestroy(list);
  if (ok) return true; else return false;
}

string Dataset::header()
{
  GDALDataset *dset = (GDALDataset*) GDALOpen(fname.c_str(), GA_ReadOnly);
  if (!dset) return "";
  char **list = dset->GetFileList();
  GDALClose(dset);
  if (!list) return "";
  int i=0, pos=-1;
  while (list[i++]) {
    string str = list[i];
    transform(str.begin(), str.end(), str.begin(), ::tolower);
    if (str.find(".hdr")!=string::npos) {
      pos = i;
      break;
    }
  }
  string hdr;
  if (pos>0) hdr = list[pos]; 
  CSLDestroy(list);
  return hdr;
}

bool Dataset::write_int(const char* key, int value)
{
  FILE *fp;

  if ((fp=fopen(header().c_str(), "rt+"))==NULL) return false;
  fseek(fp,0,SEEK_END);
  fprintf(fp, "%s = %d\n", key, value);
  fclose(fp);
  return true;
}

bool Dataset::read_int(const char* key, int* value)
{
  char line[256], ch;
  int i, line_num, line_start, found, key_len;
  FILE *fp;

  key_len = strlen(key);

  if ((fp=fopen(header().c_str(), "rt"))==NULL) return false;

  line_start = -1;
  line_num = 0;
  while (!feof(fp)) {
    fgets(line, 256, fp);
    for (i=0;i<(int)strlen(line);i++) line[i] = tolower(line[i]);
    if (strncmp(line, key, key_len) == 0) {
      line_start = line_num;
    }
    line_num++;
  }
  if (line_start == -1) {
    fclose(fp);
    return false;
  }

  rewind(fp);
  for (i=0;i<line_start;i++) fgets(line, 256, fp);
  found = 0;
  ch = '\0';
  while (!feof(fp) && ch != '=') {
    ch = fgetc(fp);
    if (ch == '=') found = 1;
  }
		
  if (!found) {
    fclose(fp);
    return false;
  }

  fscanf(fp, "%d", value);
  fclose(fp);
  return true;
}

bool Dataset::write_real_set(const char* key,const float* set,int n)
{
  char *fill;
  int i, n1, c, nfill;
  FILE *fp;

  nfill = strlen(key)+5;
  fill = (char*) malloc(nfill*sizeof(char));
  for (i=0; i<(nfill-1); i++) fill[i] = ' ';
  fill[nfill-1] = '\0';

  if ((fp=fopen(header().c_str(), "rt+"))==NULL) return false;
  fseek(fp,0,SEEK_END);
  fprintf(fp, "%s = {", key);
  n1 = n-1;
  c = 1;
  for (i=0; i<n1; i++) {
    if (c<5) {
      fprintf(fp, "%f, ", set[i]);
    } else { 
      fprintf(fp, "%f,\n%s", set[i], fill);
      c=0;
    }
    c+=1;
  }
  fprintf(fp, "%f}\n", set[n1]);
  fclose(fp);
  free((void*)fill);
  return true;
}

float* Dataset::read_real_set(const char* key, int* n)
{		
  char line[256], ch;
  int i, line_num, line_start, found, count, key_len;
  float *set;
  FILE *fp;

  key_len = strlen(key);

  fp = 0;
  fp = fopen(header().c_str(), "rt");
  if (!fp) {
    *n = -1;	
    return 0;
  }
		
  line_start = -1;
  line_num = 0;
  while (!feof(fp)) {
    fgets(line, 256, fp);
    for (i=0;i<(int)strlen(line);i++) line[i] = tolower(line[i]);
    if (strncmp(line, key, key_len) == 0) {
      line_start = line_num;
    }
    line_num++;
  }
  if (line_start == -1) {
    fclose(fp);
    *n = 0;
    return 0;
  }
		
  rewind(fp);
  for (i=0;i<line_start;i++) fgets(line, 256, fp);
  count = 0;
  found = 0;
  ch = '\0';
  while (!feof(fp) && ch != '}') {
    ch = fgetc(fp);
    if (ch == ',') count ++;
    if (ch == '}') found = 1;
  }
		
  if (!found || count == 0) {
    fclose(fp);
    *n = 0;
    return 0;
  }
		
  count++;
  set = 0;
  set = (float*) malloc(count*sizeof(float));
  if (!set) {
    fclose(fp);
    *n = 0;
    return 0;
  }
		
  rewind(fp);
  for (i=0;i<line_start;i++) fgets(line, 256, fp);
  found = 0;
  while (ch != '{') {
    ch = fgetc(fp);
    if (ch == '{') found = 1;
    if (ch == '\n' && !found) {
      fclose(fp);
      *n = 0;
      free((void*)set);
      return 0;
    }
  }
  for (i=0;i<count;i++) if (fscanf(fp, "%f,", &set[i]) != 1) {
      fclose(fp);
      *n = 0;
      free((void*)set);
      return 0;
    }
		
  fclose(fp);
  *n = count;
  return set;
}

fvec Dataset::read_real_set(const char* key)
{
  int n;
  float *set = read_real_set(key, &n);
  fvec v(set, n, true);
  free((void*)set);
  return v;
}

void Dataset::set_band_names(std::vector<Season> seas,
			     int nparam,
			     char* paramNames[])
{
  GDALDataset *ds = (GDALDataset*) GDALOpen(fname.c_str(), GA_Update);
  if (!ds) return;
  GDALRasterBand *band;
  int i, j, index=1;
  for (i=0; i<seas.size(); i++) {
    Season si = seas[i];
    for (j=0; j<nparam; j++) {
      std::stringstream s;
      if (si.startDate.year!=si.endDate.year) 
	s << si.startDate.year << "-" << si.endDate.year << ": " << paramNames[j] << " (" << index << ")";
      else
	s << si.startDate.year << ": " << paramNames[j] << " (" << index << ")";
      band = ds->GetRasterBand(index);
      std::string str = s.str();
      band->SetDescription(str.c_str());
      index+=1;
    }
  }
  GDALClose(ds);
}

std::vector<std::string> Dataset::band_names()
{
  std::vector<std::string> n;

  GDALDataset *ds = (GDALDataset*) GDALOpen(fname.c_str(), GA_ReadOnly);
  if (!ds) return n;
  GDALRasterBand *band;
  int i, count = ds->GetRasterCount();
  for (i=0; i<count; i++) {
    band = ds->GetRasterBand(i+1);
    std::string s = band->GetDescription();
    n.push_back(s);
  }
  return n;
}

void Dataset::mk_meta_files(int nparam, char* paramNames[], int nParamSpace)
{
  GDALDataset *ds = (GDALDataset*) GDALOpen(fname.c_str(), GA_ReadOnly);
  if (!ds) return;
  int xSize = ds->GetRasterXSize();
  int ySize = ds->GetRasterYSize();
  GDALClose(ds);

  int dims[4] = {1,xSize,1,ySize};

  int *bands = new int[nParamSpace];

  const char *path = CPLGetPath(fname.c_str());
  const char *base = CPLGetBasename(fname.c_str());

  const char *name = CPLFormFilename(path, base, NULL);

  const char *file = CPLGetFilename(fname.c_str());

  int i, j, c=1;
  for (i=0; i<nparam; i++) {
    for (j=0; j<nParamSpace; j++) bands[j]=c+j*nparam;
    std::string str = name;
    str += "_";
    str += paramNames[i];
    str += ".mhd";
    metafile(str.c_str(), file, bands, nParamSpace, dims);
    c+=1;
  }

  delete [] bands;
}

void Dataset::mk_meta_files(std::vector<Season> seas, int nYearSpace)
{
  GDALDataset *ds = (GDALDataset*) GDALOpen(fname.c_str(), GA_ReadOnly);
  if (!ds) return;
  int xSize = ds->GetRasterXSize();
  int ySize = ds->GetRasterYSize();
  GDALClose(ds);

  int dims[4] = {1,xSize,1,ySize};

  int *bands = new int[nYearSpace];
  char year_str[16];

  const char *path = CPLGetPath(fname.c_str());
  const char *base = CPLGetBasename(fname.c_str());

  const char *name = CPLFormFilename(path, base, NULL);

  const char *file = CPLGetFilename(fname.c_str());

  int i, j, c=1;
  for (i=0; i<seas.size(); i++) {
    Season si = seas[i];
    for (j=0; j<nYearSpace; j++) bands[j]=c+j;
    std::string str = name;
    str += "_";
    if (si.startDate.year!=si.endDate.year)
      sprintf(year_str, "%4d-%4d", si.startDate.year, si.endDate.year);
    else
      sprintf(year_str, "%4d", si.startDate.year);
    str += year_str;
    str += ".mhd";
    metafile(str.c_str(), file, bands, nYearSpace, dims);
    c+=nYearSpace;
  }

  delete [] bands;
}

void Dataset::mk_meta_files(const Date& date, int nseas, int nYearSpace)
{
  GDALDataset *ds = (GDALDataset*) GDALOpen(fname.c_str(), GA_ReadOnly);
  if (!ds) return;
  int xSize = ds->GetRasterXSize();
  int ySize = ds->GetRasterYSize();
  GDALClose(ds);

  int dims[4] = {1,xSize,1,ySize};

  int *bands = new int[nYearSpace];
  char year_str[16];

  const char *path = CPLGetPath(fname.c_str());
  const char *base = CPLGetBasename(fname.c_str());

  const char *name = CPLFormFilename(path, base, NULL);

  const char *file = CPLGetFilename(fname.c_str());

  int i, j, c=1;
  for (i=0; i<nseas; i++) {
    Date startDate = date + i*365;
    Date endDate = date + i*365 + 364;
    for (j=0; j<nYearSpace; j++) bands[j]=c+j;
    std::string str = name;
    str += "_";
    if (startDate.year!=endDate.year)
      sprintf(year_str, "%4d-%4d", startDate.year, endDate.year);
    else
      sprintf(year_str, "%4d", startDate.year);
    str += year_str;
    str += ".mhd";
    metafile(str.c_str(), file, bands, nYearSpace, dims);
    c+=nYearSpace;
  }

  delete [] bands;
}

void Dataset::metafile(const char* fn, const char* file, int bands[], int n,
		       int dims[])
{
  int i;
  FILE *fp;

  fp = fopen(fn, "wt");

  fprintf(fp, "ENVI META FILE\n");
  fprintf(fp, "File : %s\n", file);
  fprintf(fp, "Bands: %d", bands[0]);
  for (i=1; i<n; i++) fprintf(fp, ",%d", bands[i]);
  fprintf(fp, "\n");
  fprintf(fp, "Dims : %d-%d,%d-%d\n", dims[0],dims[1],dims[2],dims[3]);

  fclose(fp);
}


bool Meta_dataset::is_good()
{
  FILE *fp;
  fp = fopen(fname.c_str(), "rt");
  if (!fp) return false;

  char line[256];
  fgets(line, 256, fp);
  fclose(fp);
  int i;
  for (i=0;  i<(int)strlen(line); i++) line[i]=toupper(line[i]);
  if (strncmp(line, "ENVI META FILE", 12)!=0) return false;

  std::string s = file();
  if (s.empty()) return false;

  GDALDataset *d = (GDALDataset*) GDALOpen(s.c_str(), GA_ReadOnly);
  if (d) {
    GDALClose(d);
    return true;
  }

  return false;
}

std::string Meta_dataset::file()
{
  FILE *fp;
  fp = fopen(fname.c_str(), "rt");
  if (!fp) return "";

  char line[256], lnlow[256], s1[256], s2[256], s3[256], *ch=NULL;

  int i;
  while (!feof(fp)) {
    fgets(line, 256, fp);
    strcpy(lnlow, line);
    for (i=0;i<(int)strlen(lnlow);i++) lnlow[i] = tolower(lnlow[i]);
    if (strncmp(lnlow, "file", 4) == 0) {
      int n = sscanf(line, "%s%s%s", &s1, &s2, &s3);
      switch (n) {
      case 3:
	ch = (char*)&s3;
	break;
      case 2:
	ch = (char*)&s2;
	break;
      default:
	ch = NULL;
	break;
      }
      break;
    }
  }
  fclose(fp);

  if (ch==NULL) return "";

  const char *path = CPLGetPath(fname.c_str());

  const char *name = CPLFormFilename(path, ch, NULL);

  std::string str = name;
  return str;
}

std::string Meta_dataset::suggest_file()
{
  const char *path = CPLGetPath(fname.c_str());
  const char *base = CPLGetBasename(fname.c_str());
  const char *name = CPLFormFilename(path, base, ".dat");
  std::string s = name;
  return s;
}

std::vector<int> Meta_dataset::bands()
{
  std::vector<int> v;
  FILE *fp;
  fp = fopen(fname.c_str(), "rt");
  if(!fp) return v;

  char line[256];

  int i, line_start=-1, line_num=0;
  while (!feof(fp)) {
    fgets(line, 256, fp);
    for (i=0; i<(int)strlen(line); i++) line[i] = tolower(line[i]);
    if (strncmp(line, "bands", 5)==0) {
      line_start = line_num;
      break;
    }
    line_num++;
  }
  if (line_start==-1) {
    fclose(fp);
    return v;
  }

  rewind(fp);
  for (i=0; i<line_start; i++) fgets(line, 256, fp);

  char ch;
  do {
    if (feof(fp)) {
      fclose(fp);
      return v;
    }
    ch = fgetc(fp);
  } while (ch!=':');

  int b;
  while (fscanf(fp, "%d,", &b)==1) {
    v.push_back(b);
  }

  return v;
}

void Meta_dataset::materialize(const char* name)
{
  std::string in_file = file();

  std::string ot_file;
  if (name==0) ot_file=suggest_file(); else ot_file=name;

  GDALDataset *in_dset = (GDALDataset*) GDALOpen(in_file.c_str(), GA_ReadOnly);
  int xSize = in_dset->GetRasterXSize();
  int ySize = in_dset->GetRasterYSize();

  std::vector<int> b = bands();

  GDALDriver *drv = (GDALDriver*) GDALGetDriverByName("ENVI");

  char **opts = NULL;
  opts = CSLSetNameValue(opts, "INTERLEAVE", "BIL");
  opts = CSLSetNameValue(opts, "SUFFIX", "ADD");

  GDALDataset *ot_dset;
  ot_dset = drv->Create(ot_file.c_str(),xSize,ySize,b.size(),GDT_Float32,opts);

  if (!ot_dset) {
    GDALClose(in_dset);
    return;
  }

  ot_dset->SetProjection(in_dset->GetProjectionRef());
  double adfGeoTransform[6];
  if (in_dset->GetGeoTransform(adfGeoTransform) == CE_None)
    ot_dset->SetGeoTransform(adfGeoTransform);

  float *buff = new float[xSize*ySize];

  GDALRasterBand *in_band, *ot_band;
  int i;
  for (i=0; i<b.size(); i++) {
    in_band = in_dset->GetRasterBand(b[i]);
    in_band->RasterIO(GF_Read,0,0,xSize,ySize,(void*)buff,xSize,ySize,
		      GDT_Float32,0,0);
    ot_band = ot_dset->GetRasterBand(i+1);
    ot_band->RasterIO(GF_Write,0,0,xSize,ySize,(void*)buff,xSize,ySize,
		      GDT_Float32,0,0);
    const char *str = in_band->GetDescription();
    ot_band->SetDescription(str);
  }

  GDALClose(ot_dset);
  GDALClose(in_dset);

  delete [] buff;
}


GDALDataType splits::get_data_type(const char* str)
{
  GDALDataType type;

  if (strcmp(str,"Byte")==0)
    type = GDT_Byte;
  else if (strcmp(str,"Integer")==0)
    type = GDT_Int16;
  else if (strcmp(str,"Real")==0)
    type = GDT_Float32;
  else
    type = GDT_Unknown;
  return type;
}
