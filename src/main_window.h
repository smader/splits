/*
 main_window.h

 The FLTK main window for the splview application.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2020  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_MAIN_WINDOW_H
#define SPLITS_MAIN_WINDOW_H

#include <string>
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Progress.H>
#define ARMA_NO_DEBUG
#include <armadillo>
#include "toolbar.h"
#include "plot.h"
#include "monitor.h"
#include "date.h"
#include "season.h"
#include "arbitrary_vector.h"
#include "archive.h"

namespace splits {

class Spline_window;
class Spline;
class Weight_window;
class Span_xval_window;
class Optim_window;

class Main_window : public Fl_Double_Window
{
  Fl_Menu_Bar *mainmenu;
  Toolbar *toolbar;
  bool showarr[11];

public:
  Spline_window *sw;
  Weight_window *ww;
  Span_xval_window *xw_span;
  Optim_window *ow;

public:
  std::vector<std::string>  items, w_tab_file;
  static const float in_offset, in_gain;
  static const float in_nodata;
  std::vector<int> band;
  arma::fvec xt, xh, yt, yh, xk, yk, xp, yp, xr, yr;
  arma::fvec xmn, ymn, xmx, ymx;
  arma::fvec xstart, ystart, xpeak, ypeak, xend, yend;
  std::vector<int> nspan;
  std::vector<int> degree;
  std::vector<int> spline_type;
  std::vector<float> lambda, pord;
  std::vector<int> knot_type;
  std::vector<bool> use_weights;
  bool quiet;
  int nthread;
  Spline *spline;
  const float *knots;
  arma::fvec wt;
  float yzero;
  std::vector<Season> seas;
  arma::fvec seas_grid;
  std::vector<Archive> data;
  int current;
  bool multiple, useclr;

public:
  Plot *plot;
  Monitor *mon;

public:
  Main_window();
  ~Main_window();

  void update(bool new_data);

private:
  void load_data();
  void unload_data();

  void fit(double* coords, int item, const Date& d0, const Date& d1);

  void get_start_time_and_val();
  void get_peaking_time_and_val();
  void get_end_time_and_val();

  void setup_skeleton();

public:
  static void doy_cb(Fl_Widget* w, void* data);
  static void prepro_cb(Fl_Widget* w, void* data);
  static void load_data_cb(Fl_Widget* w, void* data);
  static void unload_data_cb(Fl_Widget* w, void* data);
  static void load_conf_cb(Fl_Widget* w, void* data);
  static void save_conf_cb(Fl_Widget* w, void* data);
  static void save_plot_cb(Fl_Widget* w, void* data);
  static void export_raw_cb(Fl_Widget* w, void* data);
  static void export_data_cb(Fl_Widget* w, void* data);
  static void export_knots_cb(Fl_Widget* w, void* data);
  static void export_points_cb(Fl_Widget* w, void* data);
  static void export_roots_cb(Fl_Widget* w, void* data);
  static void export_seasons_cb(Fl_Widget* w, void* data);
  static void materialize_cb(Fl_Widget* w, void* data);
  static void about_cb(Fl_Widget* w, void* data);
  static void quit_cb(Fl_Widget* w, void* data);
  static void window_cb(Fl_Widget* w, void* data);
  static void color_cb(Fl_Widget* w, void* data);
  static void auto_cb(Fl_Widget* w, void* data);
  static void range_cb(Fl_Widget* w, void* data);
  static void mon_cb(Fl_Widget* w, void* data);
  static void goto_cb(Fl_Widget* w, void* data);
  static void roots_cb(Fl_Widget* w, void* data);
  static void roots_input_cb(Fl_Widget* w, void* data);
  static void show_cb(Fl_Widget* w, void* data);
  static void spline_cb(Fl_Widget* w, void* data);
  static void weight_cb(Fl_Widget* w, void* data);
  static void hemi_cb(Fl_Widget* w, void* data);
  static void parallel_cb(Fl_Widget* w, void* data);
  static void quiet_cb(Fl_Widget* w, void* data);
  static void splfit_cb(Fl_Widget* w, void* data);
  static void splcal_cb(Fl_Widget* w, void* data);
  static void phencal_cb(Fl_Widget* w, void* data);
  static void spans_cb(Fl_Widget* w, void* data);
  static void optim_cb(Fl_Widget* w, void* data);
  static void view_cb(Fl_Widget* w, void* data);
  static void multi_cb(Fl_Widget* w, void* data);
};

void set_menu_item_state(Fl_Menu_Bar* menubar, const char* name, int state);
int get_index_by_name(Fl_Menu_Bar* menubar, const char* findname);

} //namespace splits

#endif
