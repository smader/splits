/*
 monitor.cpp

 Monitor is a FLTK widget to display imagery data from a GDAL dataset.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2020  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <cmath>
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include "monitor.h"

using namespace splits;

/*
 * Algorithm from N. Wirth's book, implementation by N. Devillard.
 * This code in public domain.
 */

#define ELEM_SWAP(a,b) { register float t=(a);(a)=(b);(b)=t; }

/*---------------------------------------------------------------------------
   Function :   kth_smallest()
   In       :   array of elements, # of elements in the array, rank k
   Out      :   one element
   Job      :   find the kth smallest element in the array
   Notice   :   use the median() macro defined below to get the median. 

                Reference:

                  Author: Wirth, Niklaus 
                   Title: Algorithms + data structures = programs 
               Publisher: Englewood Cliffs: Prentice-Hall, 1976 
    Physical description: 366 p. 
                  Series: Prentice-Hall Series in Automatic Computation 

 ---------------------------------------------------------------------------*/

float kth_smallest(float a[], int n, int k)
{
    register int i,j,l,m ;
    register float x ;

    l=0 ; m=n-1 ;
    while (l<m) {
        x=a[k] ;
        i=l ;
        j=m ;
        do {
            while (a[i]<x) i++ ;
            while (x<a[j]) j-- ;
            if (i<=j) {
                ELEM_SWAP(a[i],a[j]) ;
                i++ ; j-- ;
            }
        } while (i<=j) ;
        if (j<k) l=i ;
        if (k<i) m=j ;
    }
    return a[k] ;
}

#undef ELEM_SWAP


Monitor::Object::Object(Archive& ar, int band, float clip) :
  xSize(ar.xsize()), ySize(ar.ysize())
{
  data = new float*[ySize];
  data[0] = new float[ySize*xSize];
  int i;
  for (i=1; i<ySize; i++) data[i] = data[i-1]+xSize;
  ar.load(band, data[0]);
  no_data = ar.nodata();
  Static_matrix<double,3,3> m = ar.geo_transform();
  Static_vector<double,2> u(0,0), v(xSize,ySize);
  u = m*u;
  v = m*v;
  xmin = u[0];
  xmax = v[0];
  ymin = v[1];
  ymax = u[1];
  trans = inverse(m);
  contrast(clip);
}

Monitor::Object::~Object()
{
  if (data) {
    delete [] data[0];
    delete [] data;
  }
}

void Monitor::Object::contrast(float c)
{
  int i;
  int n=xSize*ySize;
  float *tmp = new float[n];
  int m = 0;
  for (i=0; i<n; i++) {
    if (fabsf(data[0][i]-no_data)>=1e-3) {
      tmp[i] = data[0][i];
      m += 1;
    }
  }
  if (m==0) {
    factor = 0.0f;
    return;
  }
  int q=(int)((double)m*c+.5);
  data_mn=kth_smallest(tmp, m, q);
  data_mx=kth_smallest(tmp, m, m-q-1);
  factor = 255./(data_mx-data_mn);
  delete [] tmp;
}

bool Monitor::Object::nearest(double x, double y, int* c, int* r)
{
  Static_vector<double,2> v = trans*Static_vector<double,2>(x,y);

  int j0=(int)v[0], j1=j0+1;
  int i0=(int)v[1], i1=i0+1;

  double d1 = j0*x+i0*y;
  double d2 = j1*x+i0*y;
  double d3 = j0*x+i1*y;
  double d4 = j1*x+i1*y;

  double d=d1; int jj=j0, ii=i0;
  if (d2 < d) { d=d2; jj=j1; ii=i0; }
  if (d3 < d) { d=d3; jj=j0; ii=i1; }
  if (d4 < d) { jj=j1; ii=i1; }

  if ( (jj<0) || (jj>(xSize-1)) || (ii<0) || (ii>(ySize-1)) ) return false;

  *c = jj;
  *r = ii;

  return true;
}

void Monitor::Object::draw(int xx, int yy, int ww, int hh, double xmn,
			   double ymn, double xmx, double ymx)
{
  double sx=(xmx-xmn)/double(ww);
  double sy=(ymx-ymn)/double(hh);
  if (sx>sy) sy=sx; else if (sy>sx) sx=sy;
  Static_matrix<double,3,3> transmat;
  transmat[0][0] = sx; transmat[0][1] =   0; transmat[0][2] = xmn;
  transmat[1][0] =  0; transmat[1][1] = -sy; transmat[1][2] = ymx;
  transmat[2][0] =  0; transmat[2][1] =   0; transmat[2][2] =   1;
  unsigned char *buf = new unsigned char[ww*hh];
  int i, j, r, c;
  for (i=0; i<hh; i++) {
    for (j=0; j<ww; j++) {
      Static_vector<double,2> pt = transmat*Static_vector<double,2>(j,i);
      if (nearest(pt[0],pt[1],&c,&r)) {
	buf[i*ww+j] = pixel(data[r][c]);
      } else {
	buf[i*ww+j] = (unsigned char)0;
      }
    }
  }
  fl_draw_image_mono(buf,xx,yy,ww,hh);
  delete [] buf;
}

unsigned char Monitor::Object::pixel(float d)
{
  unsigned char c;
  if (fabsf(d-no_data)<1e-3)
    c=0;
  else if (d<data_mn)
    c=0;
  else if (d>data_mx)
    c=255;
  else
    c = (unsigned char)((d-data_mn)*factor);
  return c;
}


Monitor::Monitor(int x, int y, int w, int h, const char *l,
		 const Fl_Menu_Item* m)
  : Fl_Widget(x,y,w,h,l), menu(m), opmode(POINT), clip(0.02f)
{
  cur[0] = cur[1] = 0;
  color(FL_BLACK);
  box(FL_FLAT_BOX);
}

Monitor::~Monitor()
{
}

void Monitor::draw()
{
  draw_box();
  if (obj) obj->draw(x(),y(),w(),h(),xmin,ymin,xmax,ymax);
}

void Monitor::erase_box()
{
  window()->make_current();
  fl_overlay_clear();
}

int Monitor::handle(int event)
{
  static int ix, iy;
  static bool dragged;
  static int button;
  int x2,y2;

  double sx=(xmax-xmin)/(double)w();
  double sy=(ymax-ymin)/(double)h();
  if (sx>sy) sy=sx; else if (sy>sx) sx=sy;
  Static_matrix<double,3,3> transmat;
  transmat[0][0] = sx; transmat[0][1] =   0; transmat[0][2] = xmin;
  transmat[1][0] =  0; transmat[1][1] = -sy; transmat[1][2] = ymax;
  transmat[2][0] =  0; transmat[2][1] =   0; transmat[2][2] =    1;

  switch(event)
  {
  case FL_PUSH:
    ix = Fl::event_x(); if (ix<x()) ix=x(); if (ix>=x()+w()) ix=x()+w()-1;
    iy = Fl::event_y(); if (iy<y()) iy=y(); if (iy>=y()+h()) iy=y()+h()-1;
    if ((Fl::event_button()==FL_LEFT_MOUSE) && opmode==ZOOM) {
      erase_box();
      dragged = false;
      button = Fl::event_button();
      return 1;
    } else if ((Fl::event_button()==FL_LEFT_MOUSE) && opmode==PAN) {
      button = Fl::event_button();
      return 1;
    } else if (Fl::event_button()==FL_LEFT_MOUSE && opmode==POINT) {
      Static_vector<double,2> pworld =
	transmat*Static_vector<double,2>(ix-x(),iy-y());
      cur[0] = pworld[0];
      cur[1] = pworld[1];
      do_callback();
    } else if (Fl::event_button()==FL_MIDDLE_MOUSE) {
      set_viewport();
    } else if (Fl::event_button()==FL_RIGHT_MOUSE && menu!=0) {
      const Fl_Menu_Item* m;
      m = menu->popup(Fl::event_x(), Fl::event_y(), 0, 0, 0);
      if (m) m->do_callback(this, (void*)m);
      return 1;
    }
    return 0;
  case FL_DRAG:
    dragged = true;
    if (opmode==ZOOM) {
      erase_box();
      x2 = Fl::event_x(); if (x2<x()) x2=x(); if (x2>=x()+w()) x2=x()+w()-1;
      y2 = Fl::event_y(); if (y2<y()) y2=y(); if (y2>=y()+h()) y2=y()+h()-1;
      if (ix < x2) {bx = ix; bw = x2-ix;} else {bx = x2; bw = ix-x2;}
      if (iy < y2) {by = iy; bh = y2-iy;} else {by = y2; bh = iy-y2;}
      window()->make_current();
      fl_overlay_rect(bx,by,bw,bh);
    }
    return 1;
  case FL_RELEASE:
    if ((button == FL_LEFT_MOUSE) && opmode==ZOOM) {
      erase_box();
      if (dragged && bw > 5 && bh > 5) {
	Static_vector<double,2> pworld0 =
	  transmat*Static_vector<double,2>(bx-x(),by-y());
	Static_vector<double,2>  pworld1 =
	  transmat*Static_vector<double,2>(bx+bw-x(),by+bh-y());
	set_viewport(pworld0[0],pworld1[0],pworld1[1],pworld0[1]);
      }
    } else if ((button == FL_LEFT_MOUSE) && opmode==PAN) {
      x2 = Fl::event_x();
      y2 = Fl::event_y();
      Static_vector<double,2> wclick =
	transmat*Static_vector<double,2>(ix-x(),iy-y());
      Static_vector<double,2> wrelease =
	transmat*Static_vector<double,2>(x2-x(),y2-y());
      Static_vector<double,2> delta = wrelease-wclick;
      set_viewport(xmin-delta[0],xmax-delta[0],ymin-delta[1],ymax-delta[1]);
    }
    button = -999;
    return 0;
  case FL_MOUSEWHEEL:
    {
      if (Fl::event_dy()<0)
	zoom(true);
      else
	zoom(false);
    }
    return 0;
  case FL_FOCUS:
    return 1;
  default:
    return Fl_Widget::handle(event);
  }
}

void Monitor::set_viewport()
{
  if (obj) set_viewport(obj->xmin, obj->xmax, obj->ymin, obj->ymax);
}

void Monitor::set_viewport(double xmn, double xmx, double ymn, double ymx)
{
  xmin=xmn; xmax=xmx;
  ymin=ymn; ymax=ymx;
  redraw();
}

void Monitor::set_viewport(double x, double y)
{
  double sx=(xmax-xmin)/(double)w();
  double sy=(ymax-ymin)/(double)h();
  if (sx>sy) sy=sx; else if (sy>sx) sx=sy;
  double dx = ((xmin+sx*(double)w())-xmin)/2.0;
  double dy = (ymax-(ymax-sy*(double)h()))/2.0;
  xmin = x-dx; xmax = x+dx;
  ymin = y-dy; ymax = y+dy;
  redraw();
  cur[0] = x;
  cur[1] = y;
  do_callback();
}

void Monitor::set_percent_clip(float c)
{
  clip = c/100.0f;
}

void Monitor::zoom(bool in)
{
  double dx = (xmax-xmin)*.01;
  double dy = (ymax-ymin)*.01;
  if (in)
    set_viewport(xmin+dx,xmax-dx,ymin+dy,ymax-dy);
  else
    set_viewport(xmin-dx,xmax+dx,ymin-dy,ymax+dy);
}

void Monitor::erase()
{
  delete obj;
  obj = 0;
  redraw();
}

void Monitor::display()
{
  if (obj) obj->contrast(clip);
  redraw();
}

void Monitor::display(Archive& a, int band, bool set_vwpt)
{
  obj = new Object(a,band,clip);
  if (set_vwpt) set_viewport();
  redraw();
}
