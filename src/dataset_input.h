/*
 dataset_input.h

 FLTK input field for filenames of imagery datasets or directories
 containing images.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_DATASET_INPUT_H
#define SPLITS_DATASET_INPUT_H

#include <string>
#include <FL/Fl_Group.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Button.H>

namespace splits {

class Dataset_input : public Fl_Group
{
  int type;
  Fl_Input *input;
  Fl_Button *button;
  static std::string dir;

  void browse();

  void update();

  static void button_cb(Fl_Widget* w, void* data);
  static void input_cb(Fl_Widget* w, void* data);

public:
  enum {SINGLE, CREATE, DIRECTORY};

  Dataset_input(int X, int Y, int W, int H, const char* l, int mode);
  ~Dataset_input() {}

  Fl_When when() const {return input->when();}
  void when(uchar i) {input->when(i);}

  const char* tooltip() const {return input->tooltip();}
  void tooltip(const char* text) {input->tooltip(text);}

  const char* filename() const {return input->value();}
  void set_filename(const char* name) {input->value(name);}

  bool is_empty() const;
};

} //namespace splits

#endif
