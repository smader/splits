/*
 b_spline.cpp

 Various spline models that use a B basis.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2017  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <list>
#include "rpoly.h"
#include "b_spline.h"
#include "knot.h"
#include "season.h"

using namespace arma;
using namespace splits;

float b_jk(int j, int k, const float* x, int n, float t, bool p)
{
  float b, om;
  b = 0;
  if (k==1) {
    if (t>=x[j] && t<x[j+1]) b = 1;
  } else {
    om = x[j+k-1]-x[j];
    if (om>(float)0) b = (t-x[j])/om * b_jk(j,k-1,x,n,t,p);
    om = x[j+k]-x[j+1];
    if (om>(float)0) b += (x[j+k]-t)/om * b_jk(j+1,k-1,x,n,t,p);
  }
  if ((!p) && j==(n-1) && t==x[n+k-1]) b = 1;
  return b;
}

float bderv_jk(int l, int j, int k, const float* x, int n, float t, bool p)
{
  float b, om;
  b = 0;
  if (l==1) {
    om = x[j+k-1]-x[j];
    if (om>(float)0) b = b_jk(j,k-1,x,n,t,p)/om;
    om = x[j+k]-x[j+1];
    if (om>(float)0) b -= b_jk(j+1,k-1,x,n,t,p)/om;
  }
  if (l>1) {
    om = x[j+k-1]-x[j];
    if (om>(float)0) b = bderv_jk(l-1,j,k-1,x,n,t,p)/om;
    om = x[j+k]-x[j+1];
    if (om>(float)0) b -= bderv_jk(l-1,j+1,k-1,x,n,t,p)/om;
  }
  return b*(k-1);
}


fmat b_spline_basis(const fvec& t, const float* x, int n, int k, bool p)
{
  int m = t.n_elem;
  fmat b(m,n);

  int i, j;
  for (i=0; i<m; i++) {
    for (j=0; j<n; j++) {
      b(i,j) = b_jk(j,k,x,n,t(i),p);
    }
  }

  return b;
}

fmat b_spline_basis_derivative(int l, const fvec& t, const float* x, int n,
			       int k, bool p)
{
  int m = t.n_elem;
  fmat b(m,n);

  int i, j;
  for (i=0; i<m; i++) {
    for (j=0; j<n; j++) {
      b(i,j) = bderv_jk(l,j,k,x,n,t(i),p);
    }
  }

  return b;
}



B_spline::B_spline(const float* x_, int n_, int k_, bool p)
  : periodic(p), n(n_), k(k_), c(fvec(n))
{
  int i, m = n+k;
  x = new float[m];
  for (i=0; i<m; i++) x[i]=x_[i];
  
  c.fill(0);
}

B_spline::~B_spline()
{
  delete [] x;
}

fvec B_spline::knots()
{
  fvec xk(n-k+2);
  int i, k1=k-1;
  for (i=k1; i<=n; i++) {
    xk(i-k1) = x[i];
  }
  return xk;
}

fvec B_spline::domain()
{
  fvec xd(2);
  xd(0) = x[k-1];
  xd(1) = x[n];
  return xd;
}

bool B_spline::fit(const fvec& xt, const fvec& yt)
{
  fmat b, bt;

  b = basis(xt);

  if (periodic) {
    fmat bb = b.submat(0,0,xt.n_elem-1,n-k);
    bb.submat(0,0,xt.n_elem-1,k-2) += b.submat(0,n-k+1,xt.n_elem-1,n-1);
    bt = trans(bb);
    try {
      c.subvec(0,n-k) = solve(bt*bb, bt*yt);
    }
    catch (...) {
      c.fill(0);
      return false;
    }
    c.subvec(n-k+1,n-1) = c.subvec(0,k-2);
  } else {
    bt = trans(b);
    try {
      c = solve(bt*b, bt*yt);
    }
    catch (...) {
      c.fill(0);
      return false;
    }
  }

  return true;
}

bool B_spline::fit(const fvec& xt, const fvec& yt, const fvec& w)
{
  fmat b, btw;

  b = basis(xt);

  if (periodic) {
    fmat bb = b.submat(0,0,xt.n_elem-1,n-k);
    bb.submat(0,0,xt.n_elem-1,k-2) += b.submat(0,n-k+1,xt.n_elem-1,n-1);
    btw = trans(bb)*diagmat(w);
    try {
      c.subvec(0,n-k) = solve(btw*bb, btw*yt);
    }
    catch (...) {
      c.fill(0);
      return false;
    }
    c.subvec(n-k+1,n-1) = c.subvec(0,k-2);
  } else {
    btw = trans(b)*diagmat(w);
    try {
      c = solve(btw*b, btw*yt);
    }
    catch (...) {
      c.fill(0);
      return false;
    }
  }

  return true;
}

bool B_spline::opt(const fvec& xt, const fvec& yt, int iter)
{
  if (!fit(xt,yt)) return false;

  bool ok = false;
  
  int i;
  for (i=0; i<iter; i++) {
    fvec infl_x, infl_y;
    if (!inflection_points(this, infl_x, infl_y)) return false;
    int l = infl_x.n_elem+1;
    float *v = new float[l+1];
    fvec xk = knots();
    v[0] = xk[0];
    v[l] = xk[xk.n_elem-1];
    int j;
    for (j=1; j<l; j++) v[j]=infl_x[j-1];
    delete [] x;
    if (periodic)
      x = periodic_knot_vector(v,l+1,order(),&n);
    else
      x = clamped_knot_vector(v,l+1,order(),&n);
    delete [] v;
    c = fvec(n);
    ok = fit(xt, yt);
  }

  return ok;
}

bool B_spline::opt(const fvec& xt, const fvec& yt, const fvec& w, int iter)
{
  if (!fit(xt,yt,w)) return false;

  bool ok = false;
  
  int i;
  for (i=0; i<iter; i++) {
    fvec infl_x, infl_y;
    if (!inflection_points(this, infl_x, infl_y)) return false;
    int l = infl_x.n_elem+1;
    float *v = new float[l+1];
    fvec xk = knots();
    v[0] = xk[0];
    v[l] = xk[xk.n_elem-1];
    int j;
    for (j=1; j<l; j++) v[j]=infl_x[j-1];
    delete [] x;
    if (periodic)
      x = periodic_knot_vector(v,l+1,order(),&n);
    else
      x = clamped_knot_vector(v,l+1,order(),&n);
    delete [] v;
    c = fvec(n);
    ok = fit(xt, yt, w);
  }

  return ok;
}

fmat B_spline::basis(const fvec& xt, int l)
{
  fmat b;
  if (l==0) {
    b = b_spline_basis(xt,x,n,k,periodic);
  } else {
    b = b_spline_basis_derivative(l,xt,x,n,k,periodic);
  }
  return b;
}

fvec B_spline::eval(const fvec& xt, int l)
{
  fmat bs = basis(xt,l);
  return bs*c;
}

float B_spline::eval(float x, int l)
{
  fvec xt(1);
  xt(0) = x;
  fmat bs = basis(xt,l);
  fvec yh = bs*c;
  return yh(0);
}

fvec B_spline::inv(float y)
{
  c = c-y;
  fvec v = roots();
  c = c+y;
  return v;
}

fvec B_spline::inv(float y, float a, float b)
{
  c = c-y;
  fvec v = roots(a,b);
  c = c+y;
  return v;
}

float B_spline::integral(float a, float b)
{
  float *aint = new float[k]-1;
  float *h = new float[k]-1;
  float *h1 = new float[k]-1;
  float *bint = new float[n]-1;

  const float *tt = x-1;

  int i,ia,ib,it,j,j1,kk,k1,l,li,lj,lk,l0,min;
  float ak,arg,f,one;

  one = 0.1e+01;
  k1 = k;
  ak = k1;
  kk = k1-1;
  for (i=1; i<=n; i++)
    bint[i] = 0.;
  min = 0;
  if ((a-b)==0.0)
  {
    return 0.0;
  }
  else if ((a-b)>0.0)
  {
    float tmp = a;
    a = b;
    b = tmp;
    min = 1;
  }
  if (a<tt[k1]) a = tt[k1];
  if (b>tt[n+1]) b = tt[n+1];
  l = k1;
  l0 = l+1;
  arg = a;
  for (it=1; it<=2; it++)
  {
    while ((arg>=tt[l0]) && (l<n))
    {
      l = l0;
      l0 = l+1;
    }
    for (j=1; j<=k1; j++)
      aint[j] = 0.;
    aint[1] = (arg-tt[l])/(tt[l+1]-tt[l]);
    h1[1] = one;
    for (j=1; j<=kk; j++)
    {
      h[1] = 0.;
      for (i=1; i<=j; i++)
      {
	li = l+i;
	lj = li-j;
	f = h1[i]/(tt[li]-tt[lj]);
	h[i] = h[i]+f*(tt[li]-arg);
	h[i+1] = f*(arg-tt[lj]);
      }
      j1 = j+1;
      for (i=1; i<=j1; i++)
      {
	li = l+i;
	lj = li-j1;
	aint[i] = aint[i]+h[i]*(arg-tt[lj])/(tt[li]-tt[lj]);
	h1[i] = h[i];
      }
    }
    if (it==1)
    {
      lk = l-kk;
      ia = lk;
      for (i=1; i<=k1; i++)
      {
	bint[lk] = -aint[i];
	lk = lk+1;
      }
      arg = b;
    }
  }

  lk = l-kk;
  ib = lk-1;
  for (i=1; i<=k1; i++)
  {
    bint[lk] = bint[lk]+aint[i];
    lk = lk+1;
  }

  if (ib>=ia)
  {
    for (i=ia; i<=ib; i++)
    {
      bint[i] = bint[i]+one;
    }
  }

  f = one/ak;
  for (i=1; i<=n; i++)
  {
    j = i+k1;
    bint[i] = bint[i]*(tt[j]-tt[i])*f;
  }

  if (min==1)
  {
    for (i=1; i<=n; i++)
    {
      bint[i] = -bint[i];
    }
  }

  float splint=0.0;
  for (i=1; i<=n; i++) splint += c(i-1)*bint[i];

  delete [] (aint+1);
  delete [] (h+1);
  delete [] (h1+1);
  delete [] (bint+1);

  return splint;
}

fmat B_spline::ppoly(int l)
{
  int kl = k-l;
  fvec xk = knots();
  fmat yk(xk.n_elem,kl);
  int i;
  for (i=l; i<k; i++) yk.col(i-l) = eval(xk,i);
  fvec factorial(kl);
  factorial(0) = 1;
  for (i=1; i<kl; i++) factorial(i) = factorial(i-1)*i;
  int n = xk.n_elem-1;
  fmat pp(n,kl);
  for (i=0; i<n; i++) {
    int j, kl1=kl-1;
    for (j=0; j<kl; j++) {
      pp(i,kl1-j) = yk(i,j)/factorial(j);
    }
  }
  return pp;
}

arma::fvec B_spline::roots(int l)
{
  std::list<float> z = jenkins_traub(ppoly(l),knots(),l);
  fvec v(z.size());
  int i=0;
  std::list<float>::iterator it;
  for (it=z.begin(); it!=z.end(); ++it) {
    v(i++) = (*it);
  }
  return v;
}

fvec B_spline::roots(float a, float b, int l)
{
  int kl = k-l;
  fvec xt = knots();
  int i, n=xt.n_elem-1, ia=-1, ib=-1;
  for (i=0; i<n; i++) {
    if ((xt(i)<=a) && (xt(i+1)>=a)) {
      ia = i;
      break;
    }
  }
  for (i=0; i<n; i++) {
    if ((xt(i)<=b) && (xt(i+1)>=b)) {
      ib = i;
      break;
    }
  }
  if ((ia==-1) && (ib==-1)) return fvec();
  n = ib-ia+2;
  int ib1=ib+1;
  fvec xk(n);
  for (i=ia; i<=ib1; i++) xk(i-ia) = xt(i);
  fmat yk(n,kl);
  for (i=l; i<k; i++) yk.col(i-l) = eval(xk,i);
  fvec factorial(kl);
  factorial(0) = 1;
  for (i=1; i<kl; i++) factorial(i) = factorial(i-1)*i;
  n = xk.n_elem-1;
  fmat pp(n,kl);
  for (i=0; i<n; i++) {
    int j, kl1=kl-1;
    for (j=0; j<kl; j++) {
      pp(i,kl1-j) = yk(i,j)/factorial(j);
    }
  }

  std::list<float> z = jenkins_traub(pp,xk,l);

  std::list<float>::iterator it = z.begin();
  while(it != z.end()) {
    if ((*it)<a || (*it)>b) it=z.erase(it); else ++it;
  }
  fvec v(z.size());
  i=0;
  for (it=z.begin(); it!=z.end(); ++it) {
    v(i++) = (*it);
  }
  return v;
}

fvec B_spline::greville()
{
  fvec xi = fvec(n);
  int i, j, k1=k-1;
  for (i=1; i<=n; i++) {
    float sum=0;
    for (j=0; j<k1; j++) sum += x[i+j];
    xi(i-1) = sum/(float)k1;
  }
  return xi;
}

void B_spline::ctrl_polyln(fvec& xi, fvec& yi)
{
  xi = greville();
  yi = c;
}

std::list<float> B_spline::jenkins_traub(const fmat& pp, const fvec& xt, int l)
{
  std::list<float> z;
  double *op = new double[k];
  double *zeror = new double[k];
  double *zeroi = new double[k];
  int kl1 = k-l-1;
  int m1 = pp.n_rows-1;
  int i;
  for (i=0; i<pp.n_rows; i++) {
    float span = xt(i+1)-xt(i);
    int j;
    for (j=0; j<pp.n_cols; j++) op[j]=pp(i,j);
    Rpoly rpoly;
    int n = rpoly.find(op, kl1, zeror, zeroi);
    for (j=0; j<n; j++) {
      float zj = (float)zeror[j];
      if (i==m1) {
	if (zeroi[j]==(float)0 && zj>=(float)0 && zj<=span) {
	  z.push_back(xt(i)+zj);
	}
      } else {
	if (zeroi[j]==(float)0 && zj>=(float)0 && zj<span) {
	  z.push_back(xt(i)+zj);
	}
      }
    }
  }
  z.sort();
  
  delete [] op;
  delete [] zeror;
  delete [] zeroi;

  return z;
}


Smooth_spline::Smooth_spline(const float* x_, int n_, int k_, float lambda_,
			     bool p) : B_spline(x_, n_, k_, p), lambda(lambda_)
{
}

bool Smooth_spline::fit(const fvec& xt, const fvec& yt)
{
  fmat b, b2, bb, bb2, bt, d;

  b = basis(xt);
  b2 = basis(xt,2);

  if (periodic) {
    fmat bb = b.submat(0,0,xt.n_elem-1,n-k);
    bb.submat(0,0,xt.n_elem-1,k-2) += b.submat(0,n-k+1,xt.n_elem-1,n-1);
    bt = trans(bb);
    bb2 = b2.submat(0,0,xt.n_elem-1,n-k);
    bb2.submat(0,0,xt.n_elem-1,k-2) += b2.submat(0,n-k+1,xt.n_elem-1,n-1);
    d = lambda*(trans(b2)*b2);
    try {
      c.subvec(0,n-k) = solve(bt*bb + d, bt*yt);
    }
    catch (...) {
      c.fill(0);
      return false;
    }
    c.subvec(n-k+1,n-1) = c.subvec(0,k-2);
  } else {
    bt = trans(b);
    d = lambda*(trans(b2)*b2);
    try {
      c = solve(bt*b + d, bt*yt);
    }
    catch (...) {
      c.fill(0);
      return false;
    }
  }

  return true;
}

bool Smooth_spline::fit(const fvec& xt, const fvec& yt, const fvec& w)
{
  fmat b, b2, bb, bb2, btw, d;

  b = basis(xt);
  b2 = basis(xt,2);

  if (periodic) {
    fmat bb = b.submat(0,0,xt.n_elem-1,n-k);
    bb.submat(0,0,xt.n_elem-1,k-2) += b.submat(0,n-k+1,xt.n_elem-1,n-1);
    btw = trans(bb)*diagmat(w);
    bb2 = b2.submat(0,0,xt.n_elem-1,n-k);
    bb2.submat(0,0,xt.n_elem-1,k-2) += b2.submat(0,n-k+1,xt.n_elem-1,n-1);
    d = lambda*(trans(b2)*b2);
    try {
      c.subvec(0,n-k) = solve(btw*bb + d, btw*yt);
    }
    catch (...) {
      c.fill(0);
      return false;
    }
    c.subvec(n-k+1,n-1) = c.subvec(0,k-2);
  } else {
    btw = trans(b)*diagmat(w);
    d = lambda*(trans(b2)*b2);
    try {
      c = solve(btw*b + d, btw*yt);
    }
    catch (...) {
      c.fill(0);
      return false;
    }
  }

  return true;
}



fmat diff(const fmat& a)
{
  int m1 = a.n_rows-1;
  fmat d(m1,a.n_cols);
  int i, j;
  for (j=0; j<a.n_cols; j++) {
    for (i=0; i<m1; i++) {
      d(i,j) = a(i+1,j)-a(i,j);
    }
  }
  return d;
}


P_spline::P_spline(const float* x_, int n_, int k_, float lambda_, int pord_,
		   bool p) : B_spline(x_, n_, k_,p), pord(pord_),
			     lambda(lambda_)
{
}

bool P_spline::fit(const fvec& xt, const fvec& yt)
{
  fmat b, bt;

  fmat d = eye<fmat>(n,n);

  int i;
  for (i=0; i<pord; i++) d = diff(d);

  b = basis(xt);

  if (periodic) {
    fmat bb = b.submat(0,0,xt.n_elem-1,n-k);
    bb.submat(0,0,xt.n_elem-1,k-2) += b.submat(0,n-k+1,xt.n_elem-1,n-1);
    bt = trans(bb);
    fmat dd = d.submat(0,0,n-k,n-k);
    dd.submat(0,0,n-k,k-2) += d.submat(0,n-k+1,n-k,n-1);
    d = lambda * trans(dd)*dd;
    try {
      c.subvec(0,n-k) = solve(bt*bb + d, bt*yt);
    }
    catch (...) {
      c.fill(0);
      return false;
    }
    c.subvec(n-k+1,n-1) = c.subvec(0,k-2);
  } else {
    bt = trans(b);
    d = lambda * trans(d)*d;
    try {
      c = solve(bt*b + d, bt*yt);
    }
    catch (...) {
      c.fill(0);
      return false;
    }
  }

  return true;
}

bool P_spline::fit(const fvec& xt, const fvec& yt, const fvec& w)
{
  fmat b, btw;

  fmat d = eye<fmat>(n,n);

  int i;
  for (i=0; i<pord; i++) d = diff(d);

  b = basis(xt);

  if (periodic) {
    fmat bb = b.submat(0,0,xt.n_elem-1,n-k);
    bb.submat(0,0,xt.n_elem-1,k-2) += b.submat(0,n-k+1,xt.n_elem-1,n-1);
    btw = trans(bb)*diagmat(w);
    fmat dd = d.submat(0,0,n-k,n-k);
    dd.submat(0,0,n-k,k-2) += d.submat(0,n-k+1,n-k,n-1);
    d = lambda * trans(dd)*dd;
    try {
      c.subvec(0,n-k) = solve(btw*bb + d, btw*yt);
    }
    catch (...) {
      c.fill(0);
      return false;
    }
    c.subvec(n-k+1,n-1) = c.subvec(0,k-2);
  } else {
    btw = trans(b)*diagmat(w);
    d = lambda * trans(d)*d;
    try {
      c = solve(btw*b + d, btw*yt);
    }
    catch (...) {
      c.fill(0);
      return false;
    }
  }

  return true;
}
