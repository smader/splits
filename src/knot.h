/*
 knot.h

 Functions to generate knot vectors for B-splines.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPLITS_KNOT_H
#define SPLITS_KNOT_H

#include <cstdlib>

namespace splits {

float* clamped_knot_vector(float* x, int m, int k, int* size=NULL);

float* clamped_uniform_knot_vector(float a,
				   float b,
				   int n,
				   int k,
				   int* size=NULL);

float* periodic_knot_vector(float* x, int m, int k, int* size=NULL);

float* periodic_uniform_knot_vector(float a,
				    float b,
				    int n,
				    int k,
				    int* size=NULL);

float* average_knot_vector(float* x, int nx, int k, int* size=NULL);

float* periodic_average_knot_vector(float* x, int nx, int k, int* size=NULL);

} //namespace splits

#endif
