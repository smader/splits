/*
 optim_window.cpp

 Optimize the number of knots based on residual error.

 The SPLITS splview program to fit and visualize spline models to
 remotely sensed time series images.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2017  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <cstdio>
#include <cstring>
#include <FL/Fl_Box.H>
#include <FL/Fl_File_Chooser.H>
#include "optim_window.h"
#include "main_window.h"
#include "spline_window.h"
#include "weight_window.h"
#include "knot.h"
#include "b_spline.h"
#include "bootstrap.h"

using namespace std;
using namespace arma;
using namespace splits;

std::string Optim_window::dir = ".";

Optim_window::Optim_window(Main_window* MW)
  : Fl_Window(400,335,"Optimize # of Spans"), mw(MW)
{
  {
    Fl_Group *g = new Fl_Group(0,10,400,25);
    Fl_Group *h1 = new Fl_Group(0,10,195,25);
    domain_inp1 = new Real_input(110,10,85,25, "Lower Bound:");
    h1->end();
    h1->resizable(domain_inp1);

    Fl_Group *h2 = new Fl_Group(195,10,205,25);
    domain_inp2 = new Real_input(305,10,85,25, "Upper Bound:");
    h2->end();
    h2->resizable(domain_inp2);
    g->end();
  }
  {
    Fl_Group *g = new Fl_Group(0,40,400,25);
    step_inp = new Real_input(110,40,280,25, "Step Size:");
    g->end();
    g->resizable(tol_inp);
  }
  {
    Fl_Group *g = new Fl_Group(0,70,400,25);
    progress = new Fl_Progress(10,70,380,25);
    progress->minimum(0);
    progress->maximum(1);
    progress->value(0);
    g->end();
    g->resizable(progress);
  }

  outp = new Fl_Multiline_Output(10,100,380,190);

  {
    Fl_Group *g = new Fl_Group(0,300,400,25);
    Fl_Box *b = new Fl_Box(10,300,220,25);
    run_btn = new Fl_Button(350,300,40,25, "Run");
    run_btn->callback(run_cb, this);
    reset_btn = new Fl_Button(285,300,60,25, "Reset");
    reset_btn->callback(reset_cb, this);
    save_btn = new Fl_Button(230,300,50,25, "Save");
    save_btn->callback(save_cb, this);
    g->end();
    g->resizable(b);
  }

  domain_inp1->tooltip("Number of spans (lowest)");
  domain_inp2->tooltip("Number of spans (highest)");

  resizable(outp);

  set_non_modal();
}

void Optim_window::search_domain(int* mn, int* mx)
{
  *mn = (int) domain_inp1->number();
  *mx = (int) domain_inp2->number();
}

int Optim_window::step_size()
{
  return (int) step_inp->number();
}

void Optim_window::add_result(const char* str)
{
  char *buf = (char*)malloc(strlen(outp->value()) + strlen(str) +1);
  strcpy(buf, outp->value());
  strcat(buf, str);
  outp->value(buf);
  free(buf);
}

void Optim_window::erase()
{
  progress->value(0);
  outp->value("");
  Fl::check();
}

void Optim_window::enable(bool b)
{
  if (b) run_btn->activate(); else run_btn->deactivate();
}

void Optim_window::start()
{
  run_btn->deactivate();
  reset_btn->deactivate();
  Fl::check;
}

void Optim_window::finish()
{
  run_btn->activate();
  reset_btn->activate();
  Fl::check;
}

void Optim_window::reset()
{
  domain_inp1->set_number(1);
  domain_inp2->set_number(mw->data[mw->current].count()-1);
}

void Optim_window::report(float fraction, void* data)
{
  Optim_window *win = (Optim_window*)data;
  win->progress->value(fraction);
  Fl::check();
}

void Optim_window::save_cb(Fl_Widget* w, void* data)
{
  Optim_window *win = (Optim_window*)data;

  Fl_File_Chooser chooser(dir.c_str(), "*", Fl_File_Chooser::CREATE, "File?");
  chooser.preview(0);
  const char *val = chooser.value();
  if (val!=NULL) chooser.value(val);
  chooser.show();
  while (chooser.shown()) Fl::wait();
  if (chooser.value() != NULL) {
    std::string fname = chooser.value();
    const char *name = fl_filename_name(fname.c_str());
    if (name!=NULL) {
      int pos = fname.find(name);
      std::string str = fname.substr(0, pos-1);
      if (fl_filename_isdir(str.c_str())>0) win->dir = str;

      FILE *fp = fopen(fname.c_str(), "wt");
      if (fp==NULL) {
	fl_alert("Error saving result!");
      } else {
	fprintf(fp, "%s", win->result());
	fclose(fp);
      }
    }
  }
}

void Optim_window::reset_cb(Fl_Widget* w, void* data)
{
  Optim_window *win = (Optim_window*)data;
  win->reset();
}

void Optim_window::run_cb(Fl_Widget* w, void* data)
{
  Optim_window *win = (Optim_window*)data;

  bool peri;
  if (win->mw->knot_type[win->mw->current] == Spline_window::PERIODIC)
    peri = true;
  else
    peri = false;

  int l, r;
  win->search_domain(&l, &r);

  int s = win->step_size();
  
  win->erase();

  win->start();
  win->report(0, win);

  int d = win->mw->degree[win->mw->current];
  
  int n;
  for (n=l; n<=r; n+=s) {

    int dim_basis;
    float *knots;
    if (peri) {
      knots = periodic_uniform_knot_vector(0.0f, 1.0f, n, d+1, &dim_basis);
    } else {
      knots = clamped_uniform_knot_vector(0.0f, 1.0f, n, d+1, &dim_basis);
    }

    Spline *spline;

    switch (win->mw->spline_type[win->mw->current]) {
    case Spline_window::B_SPLINE:
      spline = new B_spline(knots,dim_basis,d+1,peri);
      break;
    case Spline_window::SMOOTH_B_SPLINE:
      spline = new Smooth_spline(knots,dim_basis,d+1,
				 win->mw->lambda[win->mw->current],peri);
      break;
    case Spline_window::P_SPLINE:
      spline = new P_spline(knots,dim_basis,d+1,
			    win->mw->lambda[win->mw->current],
			    win->mw->pord[win->mw->current],peri);
      break;
    default:
      spline = new B_spline(knots,dim_basis,d+1,peri);
    }

    bool ok;
    
    if (win->mw->wt.is_empty() ||
	!win->mw->ww->use() ||
	win->mw->ww->mode()==Weight_window::OFF) {
      ok = spline->fit(win->mw->xt, win->mw->yt);
    } else {
      ok = spline->fit(win->mw->xt, win->mw->yt, win->mw->wt);
    }

    if (!ok) {
      delete spline;
      continue;
    }

    fvec yhat = spline->eval(win->mw->xt);

    double e;
    
    if (win->mw->wt.is_empty() ||
	!win->mw->ww->use() ||
	win->mw->ww->mode()==Weight_window::OFF) {
      e = mse(win->mw->yt, yhat, dim_basis);
    } else {
      e = mse(win->mw->yt, yhat, win->mw->wt, dim_basis);
    }

    char str[128];
    sprintf(str, "%d\t%g\n", n, e);
    win->add_result(str);
    
    win->report((float)n/(float)(r-l+1), win);
  }

  win->report(100, win);
  win->finish();
}
