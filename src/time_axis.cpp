/*
 time_axis.cpp

 Support functions for creating and using time axes for spline models.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2017  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <fstream>
#include "time_axis.h"

using namespace std;
using namespace arma;
using namespace splits;

vector<Date> splits::load_dates(const char* filename)
{
  vector<Date> dates;
  string s;
  ifstream f(filename);
  if (f.is_open()) {
    while (f.good()) {
      s = "";
      f >> s;
      Date d(s);
      if (!d) ; else dates.push_back(d);
    }
    f.close();
  }
  return dates;
}

int splits::convert_date_to_band(const Date& date,
				 const Date& start,
				 const Date& end,
				 int count)
{
  double delta = (double)(count-1)/(double)(end-start);
  double ndays = (double)(date-start);
  int band = (int)(ndays*delta+0.5);
  if (band<1) band=1; else if (band>count) band=count;
  return band;
}

Date splits::convert_band_to_date(int band,
				  const Date& start,
				  const Date& end,
				  int count)
{
  double delta = (double)(end-start)/(double)(count-1);
  int ndays = (int)(((double)band-1.0)*delta+0.5);
  Date date = start+ndays;
  if (date<start) date = start; else if (date>end) date=end;
  return date;
}

Date splits::convert_index_to_date(float index, const Date& d0, const Date& d1,
				   float a, float b)
{
  return d0 + (int)((float)(d1-d0)/(b-a)*(index-a));
}

float splits::convert_index_to_doy(float index, float ndays, const Date& d0)
{
  float iday = index*ndays;
  float ifloor = floorf(iday);
  Date d = d0 + (int)iday;
  float doy = (float)d.doy() + (iday-ifloor);
  return doy;
}

float splits::convert_index_to_doy(float index, const Date& d0, const Date& d1,
				   float a, float b)
{
  float days = (float)(d1-d0)/(b-a) * (index-a);
  Date d = d0 + (int)(days);
  return (float)d.doy() + (days-floorf(days));
}

vector<Date> splits::gen_timetable(fvec& doy, int year)
{
  vector<Date> tab;

  bool flag=false;
  /* flag prevents doubly incrementing the year if subsequent observations
     are >365 (e.g. ...,366,366,...). This happens e.g. when combining
     MODIS Aqua and Terra data: both Aqua and Terra observations in
     the last compositing period of a year may fall into the next year
     (since last compositing period of MODIS MVC schedule extends into the
     following year). -- 2013-12-13 (with David Frantz) */
  int i, n=doy.n_elem;
  float d=doy[0], di;
  for (i=0; i<n; i++) {
    di = doy[i];
    if (di<d) { year++; flag=false; }
    else if (di>365.0f) {
      di -= 365;
      if (flag==false) year++;
      flag = true;
    }
    tab.push_back(Date((int)di, year));
    d=di;
  }

  return tab;
}

fvec splits::make_scaled_time_axis(vector<Date>& timetab,
				   const Date& d0,
				   const Date& d1,
				   float mn,
				   float mx)
{
  int ndays = d1 - d0;
  int sz = timetab.size();
  fvec xt(sz);
  float factor = (mx-mn)/((float)ndays-1.0f);
  int i;
  for (i=0; i<sz; i++) {
    xt[i] = mn + (timetab[i]-d0)*factor;
  }
  return xt;
}

void splits::merge_axes(arma::fvec& a1, const arma::fvec& a2)
{
}


vector<Season> splits::create_skeleton(const Date& beg,
				       const Date& end,
				       bool hemi,
				       float xmn,
				       float xmx)
{
  vector <Season> seasv;

  float delta = (xmx-xmn)/(float)(end-beg);

  int year;
  float x0, x1;
  Date d0, d1;
  for (year=beg.year; year<=end.year; year++) {
    if (hemi) { //southern hemisphere
      d0 = Date(7, 1, year);
      d1 = Date(6, 30, year+1);
    } else { //northern hemisphere
      d0 = Date(1, 1, year);
      d1 = Date(12, 31, year);
    }
    x0 = (float)(d0-beg)*delta;
    x1 = (float)(d1-beg)*delta;
    if ((x0>=xmn) && (x1<=xmx)) {
      Season s;
      s.startDate = d0;
      s.endDate = d1;
      s.startIndex = x0;
      s.endIndex = x1;
      seasv.push_back(s);
    }
  }

  return seasv;
}

std::vector<float> splits::create_simple_skeleton(const Date& beg,
						  const Date& end,
						  bool hemi,
						  float xmn,
						  float xmx)
{
  vector <float> seasv;

  float delta = (xmx-xmn)/(float)(end-beg);

  int n = end.year-beg.year+1;

  Date d;
  if (hemi) d=Date(6,30,beg.year); else d=Date(12,31,beg.year-1);
  seasv.push_back((float)(d-beg)*delta);
  d+=365;

  int i;
  for (i=0; i<n; i++) {
    seasv.push_back((float)(d-beg)*delta);
    d+=365;
  }

  return seasv;
}


vector<int> splits::skeleton_search(const vector<float>& skel,
				    const fvec& xt,
				    int i)
{
  vector<int> index;

  if (i > skel.size()-2) return index;

  int a = skel[i];
  int b = skel[i+1];

  int j;
  for (j=0; j<xt.n_elem; j++) {
    if ((xt[j]>a) && (xt[j]<=b)) index.push_back(j);
  }

  return index;
}


vector<Date> splits::gen_timetable_by_month(const Date& startDate,
					    int mvcPeriod,
					    int length)
{
  vector<Date> tab;

  int n = ceil(28./(double)mvcPeriod);
  Date d = startDate;
  int c = 1;

  int i;
  for (i=0; i<length; i++) {
    tab.push_back(d);
    if (c==n) {
      d.day = 1;
      if (d.month==12) {
	d.month=1;
	d.year+=1;
      } else {
	d.month+=1;
      }
      c=1;
    } else {
      d+=mvcPeriod;
      c+=1;
    }
  }

  return tab;
}

vector<Date> splits::gen_timetable_by_year(const Date& startDate,
					   int mvcPeriod,
					   int length)
{
  vector<Date> tab;

  Date d = startDate;
  int yr = startDate.year;

  int i;
  for (i=0; i<length; i++) {
    if (d.year>yr) {
      d.day = 1;
      d.month = 1;
      yr = d.year;
    }
    tab.push_back(d);
    d+=mvcPeriod;
  }

  return tab;
}
