/*
 weight_window.cpp

 Manage weights in splview.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <cstdio>
#include <cstring>
#include <sstream>
#include <FL/Fl_Group.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_File_Chooser.H>
#include <FL/Fl_Int_Input.H>
#include <FL/Fl_Float_Input.H>
#include <FL/fl_ask.H>
#include "main_window.h"
#include "weight.h"
#include "weight_window.h"

using namespace splits;

Weight_window::Weight_window(Main_window* MW)
  : Fl_Window(300,400), mw(MW), opmode(OFF)
{
  Fl_Group *h1 = new Fl_Group(0,0,300,355);
  Fl_Group *v1 = new Fl_Group(0,0,215,355);
  browser = new Fl_Browser(5,10,210,345);
  int widths[] = {100, 100, 0};
  browser->column_widths(widths);
  browser->column_char('\t');
  browser->type(FL_HOLD_BROWSER);
  v1->end();
  v1->resizable(browser);
  btn_grp = new Fl_Group(215,0,85,345);
  load_btn = new Fl_Button(220,10,70,25, "Load...");
  load_btn->callback(load_cb, this);
  save_btn = new Fl_Button(220,40,70,25, "Save...");
  save_btn->callback(save_cb, this);
  add_btn = new Fl_Button(220,70,70,25, "Add...");
  add_btn->callback(add_cb, this);
  edit_btn = new Fl_Button(220,100,70,25, "Edit...");
  edit_btn->callback(edit_cb, this);
  remove_btn = new Fl_Button(220,130,70,25, "Remove");
  remove_btn->callback(remove_cb, this);
  Fl_Box *b1 = new Fl_Box(220,155,140,195);
  btn_grp->end();
  btn_grp->resizable(b1);
  h1->end();
  h1->resizable(v1);
  Fl_Group *h2 = new Fl_Group(0,355,300,45);
  use_btn = new Fl_Check_Button(5,365,120,25,"Use Weights");
  use_btn->callback(use_cb, this);
  Fl_Box *b2 = new Fl_Box(105,365,180,25);
  h2->resizable(b2);
  h2->end();

  resizable(v1);

  set_modal();
  label("Weights");

  check();
}

void Weight_window::set_mode(int m)
{
  opmode=m;
  check();
}

Arbitrary_vector<float> Weight_window::table()
{
  if (browser->size()==0) return Arbitrary_vector<float>();

  int i0, i1;
  float tmp;

  sscanf(browser->text(1), "%d %f", &i0, &tmp);
  sscanf(browser->text(browser->size()), "%d %f", &i1, &tmp);

  Arbitrary_vector<float> v(i0, i1);

  int t;
  for (t=1; t<=browser->size(); t++) {
    sscanf(browser->text(t), "%d %f", &i0, &tmp);
    v[i0]=tmp;
  }

  return v;
}

void Weight_window::set_use(bool b)
{
  if (b) {
    use_btn->value(1);
  } else {
    use_btn->value(0);
  }
}

void Weight_window::load()
{
  Fl_File_Chooser chooser(".", "*", Fl_File_Chooser::SINGLE, "File?");
  chooser.preview(0);
  chooser.show();
  while (chooser.shown()) {
    Fl::wait();
  }
  const char *val = chooser.value();
  if (val==0) return;
  FILE *fp = fopen(val, "rt");
  if (!fp) {
    fl_alert("Cannot open file for input!");
    return;
  }
  browser->clear();
  std::stringstream s;
  int tmp1; 
  float tmp2;
  while (!feof(fp)) {
    if (fscanf(fp, "%d %f", &tmp1, &tmp2)==2) {
      s.str("");
      s << tmp1 << "\t" << tmp2;
      browser->add(s.str().c_str());
    }
  }
  fclose(fp);
  sort();
  mw->w_tab_file[mw->current] = val;
  mw->data[mw->current].set_weight_table(table());
}

bool Weight_window::load(const char* file)
{
  FILE *fp = fopen(file, "rt");
  if (!fp) {
    return false;
  }
  browser->clear();
  std::stringstream s;
  int tmp1; 
  float tmp2;
  while (!feof(fp)) {
    if (fscanf(fp, "%d %f", &tmp1, &tmp2)==2) {
      s.str("");
      s << tmp1 << "\t" << tmp2;
      browser->add(s.str().c_str());
    }
  }
  fclose(fp);
  sort();
  return true;
}

void Weight_window::save()
{
  Fl_File_Chooser chooser(".", "*", Fl_File_Chooser::CREATE, "File?");
  chooser.preview(0);
  chooser.show();
  while (chooser.shown()) {
    Fl::wait();
  }
  const char *val = chooser.value();
  if (val==0) return;
  FILE *fp = fopen(val, "wt");
  if (!fp) {
    fl_alert("Cannot open file for output!");
    return;
  }
  int t;
  for (t=1; t<=browser->size(); t++) {
    fprintf(fp, "%s\n", browser->text(t));
  }
  fclose(fp);
  mw->w_tab_file[mw->current] = val;
  mw->data[mw->current].set_weight_table(table());
}

void Weight_window::sort()
{
  int t;
  for (t=1; t<=browser->size(); t++) {
    int r;
    for (r=t+1; r<=browser->size(); r++) {
      std::string str = browser->text(t);
      int tt = atoi(strtok((char*)(str.c_str()), "\t"));
      str = browser->text(r);
      int rr = atoi(strtok((char*)(str.c_str()), "\t"));
      if (tt > rr) {
	browser->swap(t,r);
      }
    }
  }
}

void Weight_window::check()
{
  switch (opmode) {
  case NO_TABLE:
    browser->deactivate();
    btn_grp->deactivate();
    use_btn->activate();
    break;
  case TABLE:
    browser->activate();
    btn_grp->activate();
    if (browser->size()>0)
      use_btn->activate();
    else
      use_btn->deactivate();
    break;
  case OFF:
  default:
    browser->deactivate();
    btn_grp->deactivate();
    use_btn->deactivate();
    break;
  }
}

void Weight_window::load_cb(Fl_Widget* w, void* data)
{
  Weight_window *win = (Weight_window*)data;
  win->load();
  win->check();
}

void Weight_window::save_cb(Fl_Widget* w, void* data)
{
  Weight_window *win = (Weight_window*)data;
  win->save();
}

static void input_cb(Fl_Widget* w, void* data)
{
  Fl_Window *win = (Fl_Window*)w->parent();
  win->hide();
}

void Weight_window::add_cb(Fl_Widget* w, void* data)
{
  Weight_window *win = (Weight_window*)data;
  Fl_Window dlg(250,75, "Add");
  Fl_Int_Input inp1(100,5,145,25, "Quality Flag:");
  inp1.callback(input_cb);
  inp1.when(FL_WHEN_ENTER_KEY|FL_WHEN_NOT_CHANGED);
  Fl_Float_Input inp2(100,35,145,25, "Weight:");
  inp2.callback(input_cb);
  inp2.when(FL_WHEN_ENTER_KEY|FL_WHEN_NOT_CHANGED);
  dlg.set_modal();
  dlg.hotspot(&dlg);
  dlg.show();
  while (dlg.visible()) Fl::wait();
  std::string str;
  str = inp1.value();
  if (str.empty()) return;
  int flag = atoi(str.c_str());
  str = inp2.value();
  if (str.empty()) return;
  float weight = (float)atof(str.c_str());
  std::stringstream s;
  s << flag << "\t" << weight;
  win->browser->add(s.str().c_str());
  win->sort();
  win->check();
  win->mw->data[win->mw->current].set_weight_table(win->table());
}

void Weight_window::edit_cb(Fl_Widget* w, void* data)
{
  Weight_window *win = (Weight_window*)data;
  int val = win->browser->value();
  if (val<=0) return;
  int flag;
  float weight;
  sscanf(win->browser->text(val), "%d %f", &flag, &weight);
  std::stringstream s;
  s << flag;
  Fl_Window dlg(250,75, "Edit");
  Fl_Int_Input inp1(100,5,145,25, "Quality Flag:");
  inp1.value(s.str().c_str());
  inp1.deactivate();
  s.str("");
  s << weight;
  Fl_Float_Input inp2(100,35,145,25, "Weight:");
  inp2.value(s.str().c_str());
  inp2.callback(input_cb);
  inp2.when(FL_WHEN_ENTER_KEY|FL_WHEN_NOT_CHANGED);
  dlg.set_modal();
  dlg.hotspot(&dlg);
  dlg.show();
  while (dlg.visible()) Fl::wait();
  std::string str;
  str = inp2.value();
  if (str.empty()) return;
  weight = (float)atof(str.c_str());
  s.str("");
  s << flag << "\t" << weight;
  win->browser->text(val, s.str().c_str());
  win->mw->data[win->mw->current].set_weight_table(win->table());
}

void Weight_window::remove_cb(Fl_Widget* w, void* data)
{
  Weight_window *win = (Weight_window*)data;
  int val = win->browser->value();
  if (val>0) {
    win->browser->remove(val);
    win->sort();
    win->check();
  }
  win->mw->data[win->mw->current].set_weight_table(win->table());
}

void Weight_window::use_cb(Fl_Widget* w, void* data)
{
  Weight_window *win = (Weight_window*)data;
  win->mw->use_weights[win->mw->current] = win->use();
}
