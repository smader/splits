/*
 load_window.cpp

 FLTK dialog window for loading image data into splview.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <FL/Fl_Group.H>
#include <FL/Fl_Box.H>
#include "load_window.h"

using namespace splits;

Load_window::Load_window(int X, int Y)
  : Fl_Window(X,Y,590,320), ok(false)
{
  int xx=205, yy=10;
  { Fl_Group *g = new Fl_Group(0,yy,580,25);
    ts_input = new Dataset_input(xx,yy,580-xx,25, "Time Series (TS) Dataset:",
				 Dataset_input::SINGLE);
    g->end();
    g->resizable(ts_input);
    yy+=30;
  }
  { Fl_Group *g = new Fl_Group(0,yy,580,25);
    ts_offset_inp = new Real_input(xx,yy,580-xx,25, "TS Offset:");
    g->end();
    g->resizable(ts_offset_inp);
    yy+=30;
  }
  { Fl_Group *g = new Fl_Group(0,yy,580,25);
    ts_scale_inp = new Real_input(xx,yy,580-xx,25, "TS Scale Factor:");
    g->end();
    g->resizable(ts_scale_inp);
    yy+=35;
  }
  { Fl_Group *g = new Fl_Group(0,yy,580,25);
    start_inp = new Date_input(xx,yy,580-xx,25, "TS Start Date:");
    g->end();
    g->resizable(start_inp);
    yy+=30;
  }
  { Fl_Group *g = new Fl_Group(0,yy,580,25);
    end_inp = new Date_input(xx,yy,580-xx,25, "TS End Date:");
    g->end();
    g->resizable(end_inp);
    yy+=35;
  }
  { Fl_Group *g = new Fl_Group(0,yy,580,25);
    dy_input = new Dataset_input(xx,yy,580-xx,25, "Day of Year (DOY) Dataset:",
				 Dataset_input::SINGLE);
    dy_input->tooltip("Image or text file");
    g->end();
    g->resizable(dy_input);
    yy+=30;
  }
  { Fl_Group *g = new Fl_Group(0,yy,580,25);
    nodata_inp = new Real_input(xx,yy,580-xx,25, "Nodata Value:");
    g->end();
    g->resizable(nodata_inp);
    yy+=35;
  }
  { Fl_Group *g = new Fl_Group(0,yy,580,25);
    qf_input = new Dataset_input(xx,yy,580-xx,25, "Quality Dataset:",
				 Dataset_input::SINGLE);
    g->end();
    g->resizable(ts_input);
    yy+=40;
  }

  Fl_Group *g = new Fl_Group(0,yy,580,25);
  Fl_Box *b = new Fl_Box(0,yy,415,25);
  g->resizable(b);
  ok_btn = new Fl_Return_Button(415,yy,80,25, "OK");
  ok_btn->callback(ok_cb, this);
  cancel_btn = new Fl_Button(500,yy,80,25, "Cancel");
  cancel_btn->callback(cancel_cb, this);
  g->end();

  resizable(g);
  size_range(590,320,0,320);
  set_modal();

  label("Load Data");
}

void Load_window::show()
{
  ok = false;
  Fl_Window::show();
}

void Load_window::ok_cb(Fl_Widget* w, void* data)
{
  Load_window* win = (Load_window*)data;
  win->set_okay();
  win->hide();
}

void Load_window::cancel_cb(Fl_Widget* w, void* data)
{
  Load_window* win = (Load_window*)data;
  win->hide();
}
