/*
 time_axis.h

 Support functions for creating and using time axes for spline models.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2017  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPLITS_TIME_AXIS_H
#define SPLITS_TIME_AXIS_H

#include <vector>
#define ARMA_NO_DEBUG
#include <armadillo>
#include "date.h"
#include "season.h"

namespace splits {

std::vector<Date> load_dates(const char* filename);


int convert_date_to_band(const Date& date,
			 const Date& start,
			 const Date& end,
			 int count);

Date convert_band_to_date(int band,
			  const Date& start,
			  const Date& end,
			  int count);

Date convert_index_to_date(float index, const Date& d0, const Date& d1,
			   float a=0.0f, float b=1.0f);
 
float convert_index_to_doy(float index, float ndays, const Date& d0);
float convert_index_to_doy(float index, const Date& d0, const Date& d1,
			   float a=0.0f, float b=1.0f);

std::vector<Date> gen_timetable(arma::fvec& doy, int year);


arma::fvec make_scaled_time_axis(std::vector<Date>& timetab,
				 const Date& d0,
				 const Date& d1,
				 float mn=0.0f,
				 float mx=1.0f);


void merge_axes(arma::fvec& a1, const arma::fvec& a2);


// hemi is true for southern, false for northern hemisphere
std::vector<Season> create_skeleton(const Date& beg,
				    const Date& end,
				    bool hemi,
				    float xmn,
				    float xmx);

std::vector<float> create_simple_skeleton(const Date& beg,
					  const Date& end,
					  bool hemi,
					  float xmn,
					  float xmx);


std::vector<int> skeleton_search(const std::vector<float>& skel,
				 const arma::fvec& xt,
				 int i);


std::vector<Date> gen_timetable_by_month(const Date& startDate,
					 int mvcPeriod,
					 int length);

std::vector<Date> gen_timetable_by_year(const Date& startDate,
					int mvcPeriod,
					int length);

} //namespace splits

#endif
