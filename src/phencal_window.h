/*
 phencal_window.h

 An FLTK dialog for the phencal program.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2020  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_PHENCAL_WINDOW_H
#define SPLITS_PHENCAL_WINDOW_H

#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Double_Window.H>
#include "dataset_input.h"
#include "date_input.h"
#include "real_input.h"
#include "terminal.h"

namespace splits {

class Main_window;

class Phencal_window : public Fl_Double_Window
{
  Dataset_input *spl_file_inp, *out_file_inp;
  Date_input *start_inp, *end_inp;
  Real_input *green_inp;
  Fl_Check_Button *index_btn;
  Terminal *term;

  Main_window *mw;
  bool wrt_idx;

public:
  Phencal_window(Main_window* MW);
  ~Phencal_window() {}

  void update();

  static void cb(Fl_Widget* w, void* data);
  static void window_cb(Fl_Widget* w, void* data);
};

} //namespace splits

#endif
