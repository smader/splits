/*
 doy_window.h

 Create text files with time axis data (DOY files).

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_DOY_WINDOW_H
#define SPLITS_DOY_WINDOW_H

#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Round_Button.H>
#include "dataset_input.h"
#include "date_input.h"
#include "real_input.h"

namespace splits {

class DOY_window : public Fl_Window
{
  Dataset_input *file_inp;
  Date_input *start_inp;
  Real_input *period_inp, *length_inp;
  Fl_Check_Button *center_btn;
  Fl_Round_Button *year_btn, *month_btn;
  Fl_Return_Button *ok_btn;
  Fl_Button *cancel_btn;
  bool ok;

public:
  enum {BOUNDED_BY_YEAR=0, BOUNDED_BY_MONTH};

public:
  DOY_window();
  ~DOY_window() {}

  bool okay() {return ok;}
  void set_okay() {ok=true;}

  const char* filename() {return file_inp->filename();}

  Date start_date() {return start_inp->date();}

  int period() {return (int)(period_inp->number());}

  bool center();

  int length() {return (int)(length_inp->number());}

  int bound();

  static void check_cb(Fl_Widget* w, void* data);
  static void ok_cb(Fl_Widget* w, void* data);
  static void cancel_cb(Fl_Widget* w, void* data);
};

} //namespace splits

#endif
