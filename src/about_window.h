/*
 about_window.h

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2020  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_ABOUT_WINDOW_H
#define SPLITS_ABOUT_WINDOW_H

#include <FL/Fl_Window.H>

namespace splits {

class About_window : public Fl_Window
{
public:
  About_window(int X, int Y);
  ~About_window() {}
};

} //namespace splits

#endif
