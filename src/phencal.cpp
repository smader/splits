/*
 phencal.cpp

 Program to calculate phenological parameters from fitted spline models.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2020  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <gdal_priv.h>
#include <cpl_multiproc.h>
#define ARMA_NO_DEBUG
#include <armadillo>
#include "clargp.h"
#include "season.h"
#include "time_axis.h"
#include "dataset.h"
#include "spline.h"
#include "b_spline.h"
#include "common.h"

using namespace arma;
using namespace splits;

const char *in_file, *ot_file;
volatile int xSize, ySize, count, nseas, nband;
fvec xt;
int degree, num_knots, dim_basis;
float *knots;
bool peri = false;
Date d_beg, d_end;
std::vector<std::vector<Season> > seasons;
float period;
int indices = false;
bool raw = false;
bool southern = false;
volatile float fraction;
bool verbose = true;
int nthread = 1;
void *mutex;
static volatile int line = 0;
static volatile int pending = 1;

ClArg_t arglst[] =
{
  strlist_arg("C",1),
  real_arg("g",0),           //greenness parameter
  flag_arg("i",0),           //write indices from beginning instead of doy
  int_arg("M",0),            //number of threads in multiprocessing
  flag_arg("Qq",0),
  flag_arg("r",0),           //write raw time axis data
  flag_arg("S",0),           //southern hemisphere: shift skeleton by 172 days
  pos_arg("in_file"),
  pos_arg("ot_file"),
  last_arg
};

struct Buffer
{
  float *input;
  float *output;
  Spline *spline;
  std::vector<Season> seas;

  Buffer() : input(0), output(0), spline(0) {}

  Buffer(float* in, int off1, float* ot, int off2, Spline* s,
	 std::vector<Season>& v)
  {
    input = in+off1;
    output = ot+off2;
    spline = s;
    seas = v;
  }

  ~Buffer() {}
};

static void worker_func(void* p)
{
  Buffer *buf = (Buffer*)p;

  B_spline *bspl = (B_spline*)(buf->spline);

  std::vector<Season> seas = buf->seas;

  int i;
  CPLAcquireMutex(mutex, 1000.0);
  i = line++;
  CPLReleaseMutex(mutex);

  GDALDataset *in_dset;
  CPLAcquireMutex(mutex, 1000.0);
  in_dset = (GDALDataset*) GDALOpen(in_file, GA_ReadOnly);
  CPLReleaseMutex(mutex);

  in_dset->RasterIO(GF_Read,
		    0,i,xSize,1,
		    buf->input,xSize,1,
		    GDT_Float32,
		    count,
		    NULL,
		    count*sizeof(float),
		    count*xSize*sizeof(float),
		    sizeof(float));

  CPLAcquireMutex(mutex, 1000.0);
  GDALClose(in_dset);
  CPLReleaseMutex(mutex);

  int j, k;
  for (j=0; j<xSize; j++) {
    for (k=0; k<nband; k++) buf->output[j*nband+k] = 0.0f;
    fvec coefs = fvec((const float*)(buf->input+j*count), count);
    bool nonzero = false;
    for (k=0; k<coefs.n_elem; k++) {
      if (coefs[k]!=(float)0) nonzero = true;
      break;
    }
    if (nonzero) {
      bspl->coef(coefs);
      std::vector<Season>::iterator it;
      for (it=seas.begin(); it!=seas.end(); ++it) it->reset();
      identify_seasons(seas,bspl,1);
      if (indices) {
	extract_phenology_index(seas,bspl,buf->output+j*nband,period,fraction);
      } else if (raw) {
	extract_phenology_raw(seas,bspl,buf->output+j*nband,fraction);
      } else {
	extract_phenology(seas,bspl,buf->output+j*nband,period,fraction,d_beg,
			  d_end);
      }
    }
  }

  GDALDataset *ot_dset;
  CPLAcquireMutex(mutex, 1000.0);
  ot_dset = (GDALDataset*) GDALOpen(ot_file, GA_Update);

  ot_dset->RasterIO(GF_Write,
		    0,i,xSize,1,
		    buf->output,xSize,1,
		    GDT_Float32,
		    nband,
		    NULL,
		    nband*sizeof(float),
		    nband*xSize*sizeof(float),
		    sizeof(float));

  GDALClose(ot_dset);
  CPLReleaseMutex(mutex);

  CPLAcquireMutex(mutex, 1000.0);
  pending--;
  CPLReleaseMutex(mutex);
}

int main(int argc, char* argv[])
{
  GDALAllRegister();
  CPLSetErrorHandler(CPLQuietErrorHandler);

  mutex = CPLCreateMutex();
  CPLReleaseMutex(mutex);

  parse_cl_args(argc, argv, arglst);

  int argsiz;
  void *parg;

  parg = get_cl_arg(arglst, "C", &argsiz);
  if (argsiz!=2) bail_out("Argument -C is invalid! Exit.");
  char **papch = (char**)parg;
  d_beg = Date(papch[0]);
  d_end = Date(papch[1]);

  parg = get_cl_arg(arglst, "g", &argsiz);
  if (parg) fraction = *(float*)parg; else fraction = (float)-1;

  parg = get_cl_arg(arglst, "i", &argsiz);
  if (argsiz>0) indices=true;

  parg = get_cl_arg(arglst, "r", &argsiz);
  if (argsiz>0) {
    if (indices) {
      bail_out("Only one of '-i' or '-r' may be given! Exit."); 
    } else {
      raw = true;
    }
  } 

  parg = get_cl_arg(arglst, "M", &argsiz);
  if (parg) {
    nthread = *(int*)parg;
    pending = nthread;
  }

  parg = get_cl_arg(arglst, "Qq", &argsiz);
  if (argsiz>0) verbose = false;

  parg = get_cl_arg(arglst, "S", &argsiz);
  if (argsiz>0) southern = true;

  in_file = (const char*)get_cl_arg(arglst, "in_file", &argsiz);
  ot_file = (const char*)get_cl_arg(arglst, "ot_file", &argsiz);

  Dataset d(in_file);

  knots = d.read_real_set("knot vector", &num_knots);
  if (knots==0) bail_out("Unable to get knot vector from header file! Exit.");

  if (!d.read_int("degree", &degree))
    bail_out("Unable to get degree from header file! Exit.");

  dim_basis = num_knots - (degree+1);

  int iper;
  d.read_int("periodic",&iper);
  if (iper==1) peri=true; else peri=false;

  xt = d.read_real_set("sites");

  period = (float)(d_end-d_beg);

  GDALDataset *in_dset = (GDALDataset*) GDALOpen(in_file,GA_ReadOnly);
  if (!in_dset) bail_out("Unable to open dataset for input! Exit.");
  xSize = in_dset->GetRasterXSize();
  ySize = in_dset->GetRasterYSize();
  count = in_dset->GetRasterCount();

  if (count!=dim_basis) {
    GDALClose(in_dset);
    bail_out("Invalid spline parameters in input dataset! Exit.");
  }

  int j;
  for (j=0; j<nthread; j++)
    seasons.push_back(create_skeleton(d_beg,
				      d_end,
				      southern,
				      0,
				      1));
  nseas = seasons[0].size();

  if (fraction<(float)0) nband = nseas*npar; else nband = nseas*npar_g;

  GDALDriver *drv = (GDALDriver*) GDALGetDriverByName("ENVI");

  char **opts = NULL;
  opts = CSLSetNameValue(opts, "INTERLEAVE", "BIL");
  opts = CSLSetNameValue(opts, "SUFFIX", "ADD");

  GDALDataset *ot_dset;
  ot_dset = drv->Create(ot_file,xSize,ySize,nband,GDT_Float32,opts);

  if (!ot_dset) {
    GDALClose(in_dset);
    bail_out("Unable to create output dataset! Exit.");
  }

  ot_dset->SetProjection(in_dset->GetProjectionRef());
  double adfGeoTransform[6];
  if (in_dset->GetGeoTransform(adfGeoTransform) == CE_None)
    ot_dset->SetGeoTransform(adfGeoTransform);

  GDALClose(ot_dset);
  GDALClose(in_dset);

  float *ibuf = new float[nthread*xSize*count];
  float *obuf = new float[nthread*xSize*nband];

  int offs_in = xSize*count;
  int offs_ot = xSize*nband;

  Spline **spl = new Spline*[nthread];

  for (j=0; j<nthread; j++) {
    spl[j] = new B_spline(knots,dim_basis,degree+1,peri);
  }

  Buffer *buffers = new Buffer[nthread];
  for (j=0; j<nthread; j++) {
    buffers[j] = Buffer(ibuf, j*offs_in, obuf, j*offs_ot, spl[j], seasons[j]);
  }

  int i, n;
  for (i=0; i<ySize; i+=nthread) {
    if ((i+nthread)>=ySize)
      n = ySize-i;
    else
      n = nthread;
    pending = n;
    for (j=0; j<n; j++) {
      CPLCreateThread(worker_func, (void*)&buffers[j]);
    }
    while (pending > 0) CPLSleep(0.5);
    if (verbose) report_progress(nthread, ySize);
  }

  int np;
  if (fraction<0.0f) np=npar; else np=npar_g; 

  d = Dataset(ot_file);
  d.set_band_names(seasons[0], np, param_names);

  d.mk_meta_files(np, param_names, nseas);
  d.mk_meta_files(seasons[0], np);

  delete [] buffers;

  delete [] ibuf;
  delete [] obuf;

  delete [] spl;

  return 0;
}
