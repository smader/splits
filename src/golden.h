/*
 golden.h

 Golden section search to find the minimum of a function.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPLITS_GOLDEN_H
#define SPLITS_GOLDEN_H

#include <cfloat>

namespace splits {

int golden(float (*f)(float,void*),
	   void* p,
	   float a,
	   float b,
	   float* xa,
	   float* xb,
	   float* y,
	   int k,
	   float eps=FLT_EPSILON,
	   void (*progress)(float,void*)=0,
	   void* data=0);

} //namespace splits

#endif
