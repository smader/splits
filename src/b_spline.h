/*
 b_spline.h

 Various spline models that use a B basis.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2017  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPLITS_B_SPLINE_H
#define SPLITS_B_SPLINE_H

#include <list>
#include "spline.h"

namespace splits {

class B_spline : public Spline
{
protected:
  bool periodic;
  int n, k;
  float *x;
  arma::fvec c;

public:
  B_spline(const float* x_, int n_, int k_, bool p=false);
  virtual ~B_spline();

  arma::fvec knots();

  int order() const {return k;}
  int degree() const {return k-1;}

  int dim_basis() const {return n;}

  void coef(const arma::fvec& c_) {c=c_;}
  arma::fvec coef() {return c;}

  arma::fvec domain();

  virtual bool fit(const arma::fvec& xt, const arma::fvec& yt);
  virtual bool fit(const arma::fvec& xt, const arma::fvec& yt,
		   const arma::fvec& w);

  virtual bool opt(const arma::fvec& xt, const arma::fvec& yt, int iter=1);
  virtual bool opt(const arma::fvec& xt, const arma::fvec& yt,
		   const arma::fvec& w, int iter=1);
  
  arma::fmat basis(const arma::fvec& xt, int l=0);

  arma::fvec eval(const arma::fvec& xt, int l=0);
  float eval(float x, int l=0);

  arma::fvec inv(float y);
  arma::fvec inv(float y, float a, float b);

  float integral(float a, float b);

  arma::fmat ppoly(int l=0);

  arma::fvec roots(int l=0);
  arma::fvec roots(float a, float b, int l=0);

  arma::fvec greville();

  void ctrl_polyln(arma::fvec& xi, arma::fvec& yi);

protected:
  B_spline() {}

  std::list<float> jenkins_traub(const arma::fmat& pp,
				 const arma::fvec& xt, int l);
};


class Smooth_spline : public B_spline
{
protected:
  float lambda;

public:
  Smooth_spline(const float* x_, int n_, int k_, float lambda_=0, bool p=false);
  ~Smooth_spline() {}

  float smoothness() const {return lambda;}

  bool fit(const arma::fvec& xt, const arma::fvec& yt);
  bool fit(const arma::fvec& xt, const arma::fvec& yt,
	   const arma::fvec& w);
};


class P_spline : public B_spline
{
protected:
  int pord;
  float lambda;

public:
  P_spline(const float* x_, int n_, int k_, float lambda_=0, int pord_=2,
	   bool p=false);
  ~P_spline() {}

  float smoothness() const {return lambda;}
  int penalty() const {return pord;}

  bool fit(const arma::fvec& xt, const arma::fvec& yt);
  bool fit(const arma::fvec& xt, const arma::fvec& yt, const arma::fvec& w);
};

} //namespace splits

#endif
