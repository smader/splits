/*
 archive.cpp

 A class representing a remote sensing data archive.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2020  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstring>
#include <cfloat>
#include <ogr_spatialref.h>
#include "archive.h"
#include "time_axis.h"
#include "weight.h"

using namespace arma;
using namespace splits;

Archive::Archive() : dset(0), dy_dset(0), wt_dset(0), in_offset(0.0f),
		     in_gain(1.0f), in_nodata(-32767.0f), southern(false)
{
}

Archive::Archive(const char* in, const char* x, const char* w)
  : dset(0), dy_dset(0), wt_dset(0), in_offset(0.0f), in_gain(1.0f),
					     in_nodata(-32767.0f),
					     southern(false)
{
  in_file = in;
  x_file = x;
  if (w!=0) w_file = w;
  hemisphere();
}

Archive::Archive(const Archive& other) : dset(0), dy_dset(0), wt_dset(0)
{
  in_file = other.in_file;
  x_file = other.x_file;
  w_file = other.w_file;
  xt_dates = other.xt_dates;
  d_beg = other.d_beg;
  d_end = other.d_end;
  in_offset = other.in_offset;
  in_gain = other.in_gain;
  in_nodata = other.in_nodata;
  southern = other.southern;
  w_tab = other.w_tab;
  w_vec = other.w_vec;

  if (other.dset!=0||other.dy_dset!=0||other.wt_dset!=0) open();
}

Archive::~Archive()
{
  close();
}

Archive& Archive::operator=(const Archive& other)
{
  in_file = other.in_file;
  x_file = other.x_file;
  w_file = other.w_file;
  xt_dates = other.xt_dates;
  d_beg = other.d_beg;
  d_end = other.d_end;
  in_offset = other.in_offset;
  in_gain = other.in_gain;
  in_nodata = other.in_nodata;
  southern = other.southern;
  w_tab = other.w_tab;
  w_vec = other.w_vec;

  dset = dy_dset = wt_dset = 0;
  if (other.dset!=0||other.dy_dset!=0||other.wt_dset!=0) open();

  return *this;
}

char** Archive::file_list()
{
  char **list = new char*[3];

  if (!in_file.empty()) {
    list[0] = new char[in_file.length()+1];
    strcpy(list[0], in_file.c_str());
  } else {
    list[0] = 0;
  }

  if (!x_file.empty()) {
    list[1] = new char[x_file.length()+1];
    strcpy(list[1], x_file.c_str());
  } else {
    list[1] = 0;
  }

  if (!w_file.empty()) {
    list[2] = new char[w_file.length()+1];
    strcpy(list[2], w_file.c_str());
  } else {
    list[2] = 0;
  }

  return list;
}

bool Archive::open()
{
  int xSize, ySize, count;

  if (!in_file.empty()) {
    dset = (GDALDataset*) GDALOpen(in_file.c_str(), GA_ReadOnly);
    if (!dset) return false;
    xSize = dset->GetRasterXSize();
    ySize = dset->GetRasterYSize();
    count = dset->GetRasterCount();
  } else {
    return false;
  }
  
  if (!x_file.empty()) {
    dy_dset = (GDALDataset*) GDALOpen(x_file.c_str(), GA_ReadOnly);
    if (dy_dset) {
      bool xWrong, yWrong, cWrong;
      xWrong = ((dy_dset->GetRasterXSize())!=xSize);
      yWrong = ((dy_dset->GetRasterYSize())!=ySize);
      cWrong = ((dy_dset->GetRasterCount())!=count);
      if (xWrong|yWrong|cWrong) {
	GDALClose(dset);
	dset = 0;
	GDALClose(dy_dset);
	dy_dset = 0;
	return false;
      }
      if ((!d_beg)|(!d_end)) {
	GDALClose(dset);
	dset = 0;
	GDALClose(dy_dset);
	dy_dset = 0;
	return false;
      }
    } else {
      dy_dset = 0;
      xt_dates = load_dates(x_file.c_str());
      if (xt_dates.size()!=count) {
	GDALClose(dset);
	dset = 0;
	return false;
      }
      d_beg = xt_dates[0];
      d_end = xt_dates[count-1];
    }
  } else {
    GDALClose(dset);
    dset = 0;
    return false;
  }
  
  if (!w_file.empty()) {
    wt_dset = (GDALDataset*) GDALOpen(w_file.c_str(), GA_ReadOnly);
    if (wt_dset) {
      bool xWrong, yWrong, cWrong;
      xWrong = ((wt_dset->GetRasterXSize())!=xSize);
      yWrong = ((wt_dset->GetRasterYSize())!=ySize);
      cWrong = ((wt_dset->GetRasterCount())!=count);
      if (xWrong|yWrong|cWrong) {
	close();
	return false;
      }
      wmode = QUALITY;
    } else {
      int nwt = read_weight_file(w_file.c_str(), w_vec);
      if (nwt!=count) {
	close();
	return false;
      }
      wmode = CONSTANT;
    }
  } else {
    wt_dset = 0;
    w_vec = fvec();
  }

  return true;
}

void Archive::close()
{
  if (dset!=0) GDALClose(dset); dset=0;
  if (dy_dset!=0) GDALClose(dy_dset); dy_dset=0;
  if (wt_dset!=0) GDALClose(wt_dset); wt_dset=0;
}

Static_matrix<double,3,3> Archive::geo_transform()
{
  Static_matrix<double,3,3> m;

  double g[6];
  if (dset->GetGeoTransform(g)==CE_None) {
    m[0][0] = g[1]; m[0][1] =    0; m[0][2] = g[0];
    m[1][0] =    0; m[1][1] = g[5]; m[1][2] = g[3];
    m[2][0] =    0; m[2][1] =    0; m[2][2] =    1;
  } else {
    m[0][0] = 2.0/xsize(); m[0][1] =            0; m[0][2] = -1;
    m[1][0] =           0; m[1][1] = -2.0/ysize(); m[1][2] =  1;
    m[2][0] =           0; m[2][1] =            0; m[2][2] =  1;
  }

  return m;
}

bool Archive::is_geo_transformed()
{
  double g[6];
  if (dset->GetGeoTransform(g)==CE_None) return true; else return false;
}

void Archive::hemisphere()
{
  GDALDataset *ds = (GDALDataset*) GDALOpen(in_file.c_str(), GA_ReadOnly);
  if (dset==0) return;
  double g[6];
  if ((ds->GetGeoTransform(g))!=CE_None) {
    GDALClose(ds);
    return;
  }
  OGRSpatialReference ref(ds->GetProjectionRef()), geog_ref;
  geog_ref.SetWellKnownGeogCS("WGS84");
  OGRCoordinateTransformation *trans =
    OGRCreateCoordinateTransformation(&ref, &geog_ref);
  double x = g[0];
  double y = g[3]+g[5]*(double)(ds->GetRasterYSize());
  if (trans==0) return;
  if (trans->Transform(1, &x, &y)) {
    if (y<0) southern=true; else southern=false;
  }
  GDALClose(ds);
  delete trans;
}

void Archive::load(int band, float* buffer)
{
  if (dset==0) return;
  GDALRasterBand *pband = dset->GetRasterBand(band+1);
  if (pband==0) return;
  pband->RasterIO(GF_Read, 0, 0, xsize(), ysize(), (void*)buffer,
		  xsize(), ysize(), GDT_Float32, 0, 0);
}

bool Archive::load(Date& date, float* buffer)
{
  if (dset==0) return false;
  int band;
  if (!xt_dates.empty()) {
    band = -1;
    int i;
    for (i=1; i<xt_dates.size(); i++) {
      if (date>=xt_dates[i-1] && date<=xt_dates[i]) {
	if (date==xt_dates[i-1]) {
	  date = xt_dates[i-1];
	  band = i;
	} else if (date==xt_dates[i]) {
	  date = xt_dates[i];
	  band = i+1;
	} else {
	  if (xt_dates[i]-date < date-xt_dates[i-1]) {
	    date = xt_dates[i];
	    band = i+1;
	  } else {
	    date = xt_dates[i-1];
	    band = i;
	  }
	}
      }
    }
    if (band==-1) return false;
  } else {
    band = convert_date_to_band(date, begin(), end(), count());
  }
  GDALRasterBand *pband = dset->GetRasterBand(band);
  if (pband==0) return false;
  pband->RasterIO(GF_Read, 0, 0, xsize(), ysize(), (void*)buffer,
		  xsize(), ysize(), GDT_Float32, 0, 0);
  return true;
}

int Archive::poke_raw(double* coords, fvec& xt, fvec& yt, fvec& qa)
{
  int i, j, k;

  Static_vector<double,2> v(coords);
  v = inverse(geo_transform())*v;
  j = (int)(v[0]+.5);
  i = (int)(v[1]+.5);

  if ((j<0) || (j>=xsize()) || (i<0) || (i>=ysize())) return -1;

  yt = fvec(count());
  dset->RasterIO(GF_Read,
		 j, i, 1, 1,
		 (void*)yt.memptr(),
		 1, 1,
		 GDT_Float32,
		 count(), NULL,
		 0, 0, 0);

  if (all(yt==in_nodata)) return -1;
  
  int nobs;
  if (dy_dset) {
    fvec doy(count());
    dy_dset->RasterIO(GF_Read,
		      j, i, 1, 1,
		      (void*)doy.memptr(),
		      1, 1,
		      GDT_Float32,
		      count(), NULL,
		      0, 0, 0);
    k = count()-1;
    while (fabsf(doy[k]-in_nodata)<FLT_EPSILON) k--;
    nobs = k+1;
    doy.resize(nobs);
    xt = doy;
  } else if (!x_file.empty()) {
    nobs = xt_dates.size();
    xt = fvec(nobs);
    for (k=0; k<nobs; k++) xt(k) = (float)(xt_dates[k].doy());
  } else {
    xt = linspace<fvec>(0,count()-1,count());
    nobs = count();
  }

  yt.resize(nobs);

  if (wt_dset && nobs>0) {
    short int * wbuf = new short int [count()];
    qa = fvec(nobs);
    wt_dset->RasterIO(GF_Read,
		      j, i, 1, 1,
		      (void*)wbuf,
		      1, 1,
		      GDT_Int16,
		      count(), NULL,
		      0, 0, 0);
    for (k=0; k<nobs; k++) qa(k) = wbuf[k];
    delete [] wbuf;
  } else {
    qa = fvec(nobs);
    qa.zeros();
  }

  return nobs;
}

int Archive::poke(double* coords, fvec& xt, fvec& yt, fvec& wt, const Date& d0,
		  const Date& d1)
{
  int i, j, k;

  Static_vector<double,2> v(coords); 
  v = inverse(geo_transform())*v;
  j = (int)(v[0]+.5);
  i = (int)(v[1]+.5);

  if ((j<0) || (j>=xsize()) || (i<0) || (i>=ysize())) return -1;

  yt = fvec(count());
  dset->RasterIO(GF_Read,
		 j, i, 1, 1,
		 (void*)yt.memptr(),
		 1, 1,
		 GDT_Float32,
		 count(), NULL,
		 0, 0, 0);

  if (all(yt==in_nodata)) return -1;

  int nobs;
  if (dy_dset) {
    fvec doy(count());
    dy_dset->RasterIO(GF_Read,
		      j, i, 1, 1,
		      (void*)doy.memptr(),
		      1, 1,
		      GDT_Float32,
		      count(), NULL,
		      0, 0, 0);
    k = count()-1;
    while (fabsf(doy[k]-in_nodata)<FLT_EPSILON) k--;
    nobs = k+1;
    doy.resize(nobs);
    xt_dates = gen_timetable(doy, d_beg.year);
    xt = make_scaled_time_axis(xt_dates, d0, d1);
    xt_dates.clear();
  } else if (!x_file.empty()) {
    xt = make_scaled_time_axis(xt_dates, d0, d1);
    nobs = count();
  } else {
    xt = linspace<fvec>(0,count()-1,count());
    nobs = count();
  }

  yt.resize(nobs);
  for (k=0; k<nobs; k++) yt[k]=yt[k]*in_gain+in_offset;

  if (wt_dset && w_tab.size()>0 && nobs>0) {
    short int * wbuf = new short int [count()];
    wt = fvec(nobs);
    wt_dset->RasterIO(GF_Read,
		      j, i, 1, 1,
		      (void*)wbuf,
		      1, 1,
		      GDT_Int16,
		      count(), NULL,
		      0, 0, 0);
    safe_load_weights(wbuf, nobs, w_tab, wt);
    delete [] wbuf;
  } else {
    wt = w_vec;
  }

  return nobs;
}

int Archive::poke(int j, int i, fvec& xt, fvec& yt, fvec& wt, const Date& d0,
		  const Date& d1)
{
  int k;

  if ((j<0) || (j>=xsize()) || (i<0) || (i>=ysize())) return -1;

  yt = fvec(count());
  dset->RasterIO(GF_Read,
		 j, i, 1, 1,
		 (void*)yt.memptr(),
		 1, 1,
		 GDT_Float32,
		 count(), NULL,
		 0, 0, 0);

  int nobs;
  if (dy_dset) {
    fvec doy(count());
    dy_dset->RasterIO(GF_Read,
		      j, i, 1, 1,
		      (void*)doy.memptr(),
		      1, 1,
		      GDT_Float32,
		      count(), NULL,
		      0, 0, 0);
    k = count()-1;
    while (fabsf(doy[k]-in_nodata)<FLT_EPSILON) k--;
    nobs = k+1;
    doy.resize(nobs);
    xt_dates = gen_timetable(doy, d_beg.year);
    xt = make_scaled_time_axis(xt_dates, d0, d1);
    xt_dates.clear();
  } else if (!x_file.empty()) {
    xt = make_scaled_time_axis(xt_dates, d0, d1);
    nobs = count();
  } else {
    xt = linspace<fvec>(0,count()-1,count());
    nobs = count();
  }

  yt.resize(nobs);
  for (k=0; k<nobs; k++) yt[k]=yt[k]*in_gain+in_offset;

  if (wt_dset && w_tab.size()>0 && nobs>0) {
    short int * wbuf = new short int [count()];
    wt = fvec(nobs);
    wt_dset->RasterIO(GF_Read,
		      j, i, 1, 1,
		      (void*)wbuf,
		      1, 1,
		      GDT_Int16,
		      count(), NULL,
		      0, 0, 0);
    safe_load_weights(wbuf, nobs, w_tab, wt);
    delete [] wbuf;
  } else {
    wt = w_vec;
  }

  return nobs;
}
