/*
 common.h

 Miscellaneous common functions.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdarg>
#include <vector>
#define ARMA_NO_DEBUG
#include <armadillo>
#include "date.h"

#ifndef SPLITS_COMMON_H
#define SPLITS_COMMON_H

namespace splits {

void bail_out(const char* format, ...);

void report_progress(int incr, int total);

int scan_nodata(std::vector<splits::Date>& timetab,
		arma::fvec& yt,
		arma::fmat& wt,
		float nodata);

arma::fvec load_reals(const char* fname);

} //namespace splits

#endif
