/*
 dataset_input.cpp

 FLTK input field for filenames of imagery datasets or directories
 containing images.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <string>
#include <FL/Fl_File_Chooser.H>
#include <FL/filename.H>
#include "dataset_input.h"

using namespace splits;

std::string Dataset_input::dir = ".";

Dataset_input::Dataset_input(int X, int Y, int W, int H, const char* l, int t)
  : Fl_Group(X,Y,W,H), type(t)
{
  int btn_w = 80, spacing = 2;
  input = new Fl_Input(X, Y, W-btn_w-spacing, H, l);
  input->callback(input_cb, (void*)this);
  input->when(FL_WHEN_CHANGED);
  button = new Fl_Button(X+W-btn_w+spacing, Y, btn_w, H, "Browse...");
  button->callback(button_cb, (void*)this);
  resizable(input);
}

bool Dataset_input::is_empty() const
{
  std::string str = input->value();
  return (str=="");
}

void Dataset_input::browse()
{
  Fl_File_Chooser chooser(dir.c_str(), "*",
			  Fl_File_Chooser::SINGLE, "Dataset?");

  chooser.preview(0);

  const char *val;

  switch (type) {
    case SINGLE:
      val = chooser.value();
      if (val!=NULL) {
	chooser.value(val);
      } else {
	chooser.directory(dir.c_str());
      }
      break;
    case CREATE:
      chooser.type(Fl_File_Chooser::CREATE);
      val = chooser.value();
      if (val!=NULL) {
	chooser.value(val);
      } else {
	chooser.directory(dir.c_str());
      }
      break;
    case DIRECTORY:
    {
      chooser.type(Fl_File_Chooser::DIRECTORY);
      chooser.label("Directory?");
      std::string fname = input->value();
      const char *name = fl_filename_name(fname.c_str());
      std::string str;
      if (name!=NULL) {
	int pos = fname.find(name);
	str = fname.substr(0, pos-1);
	if (fl_filename_isdir(str.c_str())>0) {
	  chooser.directory(str.c_str());
	}
      }
      break;
    }
  }

  chooser.show();
  while (chooser.shown()) {
    Fl::wait();
  }

  if (chooser.value() != NULL) {
    input->value(chooser.value());
    update();
  }
}

void Dataset_input::update()
{
  std::string fname = input->value();
  const char *name = fl_filename_name(fname.c_str());
  if (name!=NULL) {
    int pos = fname.find(name);
    std::string str = fname.substr(0, pos-1);
    if (fl_filename_isdir(str.c_str())>0) dir = str;
  }
  do_callback();
}

void Dataset_input::button_cb(Fl_Widget* w, void* data)
{
  ((Dataset_input*)data)->browse();
}

void Dataset_input::input_cb(Fl_Widget* w, void* data)
{
  ((Dataset_input*)data)->update();
}
