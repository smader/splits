/*
 season.cpp

 Analyze seasons and phenology using a spline model.

 The SPLITS splview program to fit and visualize spline models to
 remotely sensed time series images.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2017  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "season.h"
#include "time_axis.h"

using namespace std;
using namespace arma;
using namespace splits;

void Season::reset()
{
  start.x = 0;
  start.y = 0;
  peak.x = 0;
  peak.y = 0;
  end.x = 0;
  end.y = 0;
}


void splits::extreme_points(Spline* spl,
			    fvec& xmn,
			    fvec& ymn,
			    fvec& xmx,
			    fvec& ymx)
{
  fvec x0 = spl->roots(1);
  fvec y0 = spl->eval(x0);
  fvec ydblpr0 = spl->eval(x0,2);
  int i, nmn=0, nmx=0;
  for (i=0; i<ydblpr0.n_elem; i++) {
    if (ydblpr0(i)<(float)0) nmx++;
    else if (ydblpr0(i)>(float)0) nmn++;
  }
  xmn = fvec(nmn); ymn = fvec(nmn);
  xmx = fvec(nmx); ymx = fvec(nmx);
  int imn=0, imx=0;
  for (i=0; i<ydblpr0.n_elem; i++) {
    if (ydblpr0(i)<(float)0) {
      xmx(imx) = x0(i);
      ymx(imx) = y0(i);
      imx++;
    } else if (ydblpr0(i)>(float)0) {
      xmn(imn) = x0(i);
      ymn(imn) = y0(i);
      imn++;
    }
  }
}


vector<int> splits::extreme_points(Spline* spl, fvec& x, fvec& y)
{
  fvec x0 = spl->roots(1);
  fvec y0 = spl->eval(x0);
  fvec ydblpr0 = spl->eval(x0,2);

  int i, n=0;
  for (i=0; i<ydblpr0.n_elem; i++) {
    if (ydblpr0[i]<(float)0) n++;
    else if (ydblpr0[i]>(float)0) n++;
  }

  x = fvec(n);
  y = fvec(n);
  vector<int> sgn(n);

  int j=0;
  for (i=0; i<ydblpr0.n_elem; i++) {
    if (ydblpr0[i]<(float)0) {
      x[j] = x0[i];
      y[j] = y0[i];
      sgn[j++]=-1;
    } else if (ydblpr0[i]>(float)0) {
      x[j] = x0[i];
      y[j] = y0[i];
      sgn[j++]=1;
    }
  }

  return sgn;
}


bool splits::inflection_points(Spline* spl, arma::fvec& x, arma::fvec& y)
{
  if (spl->degree()<3) {
    x = fvec();
    y = fvec();
    return false;
  }

  fvec x0 = spl->roots(2);
  fvec y0 = spl->eval(x0);
  fvec ytrplpr0 = spl->eval(x0,3);

  int i, n=0;
  for (i=0; i<ytrplpr0.n_elem; i++) {
    if (ytrplpr0[i]!=(float)0) n++;
  }

  x = fvec(n);
  y = fvec(n);

  int j=0;
  for (i=0; i<ytrplpr0.n_elem; i++) {
    if (ytrplpr0[i]!=(float)0) {
      x[j] = x0[i];
      y[j] = y0[i];
      j++;
    }
  }

  return true;
}


static void seasons1(fvec& xmn, fvec& ymn, vector<Season>& seasv, float imin,
		     float imax)
{
  int i, j;

  /* Find the smallest minimum in between two consecutive peaks. Define
     it to be the end of the former and the start of the latter season */
  int n1=seasv.size()-1;
  float left, right, mn_x, mn_y;
  bool flag;
  for (i=0; i<n1; i++) {
    left = seasv[i].peak.x;
    right = seasv[i+1].peak.x;
    flag = false;
    for (j=0; j<xmn.n_elem; j++) {
      if ((xmn(j)>=left) && (xmn(j)<=right)) {
	if (!flag) {
	  mn_x = xmn(j);
	  mn_y = ymn(j);
	  flag = true;
	} else {
	  if (ymn(j)<mn_y) {
	    mn_x = xmn(j);
	    mn_y = ymn(j);
	  }
	}
      }
    }
    if (flag) {
      DataPt p(mn_x,mn_y);
      seasv[i].end = p;
      seasv[i+1].start = p;
    }
  }

  /* Find the end of the last season of the time series. The end is
   located between the peak of the last season and imax or the smallest
   minimum following imax */
  left = seasv[n1].peak.x;
  right = imax;
  flag = false;
  for (j=xmn.n_elem-1; j>=0; j--) {
    if (xmn(j)<=imax) {
      break;
    } else {
      if (!flag) {
	right = xmn(j);
	mn_y = ymn(j);
	flag = true;
      } else {
	right = xmn(j);
	mn_y = ymn(j);
      }
    }
  }
  flag = false;
  for (j=0; j<xmn.n_elem; j++) {
    if ((xmn(j)>=left) && (xmn(j)<=right)) {
      if (!flag) {
	mn_x = xmn(j);
	mn_y = ymn(j);
	flag = true;
      } else {
	if (ymn(j)<mn_y) {
	  mn_x = xmn(j);
	  mn_y = ymn(j);
	}
      }
    }
  }
  if (flag) {
    DataPt p(mn_x,mn_y);
    seasv[n1].end = p;
  }

  /* Find the start of the first season of the time series. The start is
   located between imin or the smallest minimum before imin and the peak
   of the first season */
  right = seasv[0].peak.x;
  left = imin;
  flag = false;
  for (j=0; j<xmn.n_elem; j++) {
    if (xmn(j)>=imin) {
      break;
    } else {
      if (!flag) {
	left = xmn(j);
	mn_y = ymn(j);
	flag = true;
      } else {
	left = xmn(j);
	mn_y = ymn(j);
      }
    }
  }
  flag = false;
  for (j=0; j<xmn.n_elem; j++) {
    if ((xmn(j)>=left) && (xmn(j)<=right)) {
      if (!flag) {
	mn_x = xmn(j);
	mn_y = ymn(j);
	flag = true;
      } else {
	if (ymn(j)<mn_y) {
	  mn_x = xmn(j);
	  mn_y = ymn(j);
	}
      }
    }
  }
  if (flag) {
    DataPt p(mn_x,mn_y);
    seasv[0].start = p;
  }
}

void splits::identify_seasons(vector<Season>& seas,
			      Spline* spl,
			      float imax,
			      float range,
			      bool bigT,
			      float low,
			      float high,
			      bool atBeg,
			      bool atEnd)
{
  fvec xmn, ymn, xmx, ymx;
  extreme_points(spl, xmn, ymn, xmx, ymx);

  // Initialize every season and find the biggest peak
  int j;
  bool flag;
  float peak_x, peak_y;
  vector<Season>::iterator it;
  for (it=seas.begin(); it!=seas.end(); ++it) {
    it->reset();
    flag = false;
    for (j=0; j<xmx.n_elem; j++) {
      float mx = xmx[j];
      if (mx>=it->startIndex && mx<=it->endIndex) {
	if (flag) {
	  if (ymx[j]>peak_y) {
	    peak_x = mx;
	    peak_y = ymx[j];
	  }
	} else {
	  peak_x = mx;
	  peak_y = ymx[j];
	  flag = true;
	}
      }
    }
    if (flag) {
      it->peak.x = peak_x;
      it->peak.y = peak_y;
    }
  }

  seasons1(xmn, ymn, seas, seas.begin()->startIndex, (seas.end()-1)->endIndex);

  // Reset (zero out) incomplete seasons
  for (it=seas.begin(); it!=seas.end(); ++it) {
    if (!(it->end)) it->reset();
  }
}

void splits::greenness_start_end(Spline* spl,
				 float fraction,
				 const Season& seas,
				 float* gstart,
				 float* gend)
{
  int i;
  float y0s = seas.start.y + (seas.peak.y-seas.start.y)*fraction;
  float y0e = seas.end.y + (seas.peak.y-seas.end.y)*fraction;
  fvec xGs = spl->inv(y0s, seas.start.x, seas.end.x);
  float gs = seas.start.x;
  if (xGs.n_elem>0) {
    for (i=0; i<xGs.n_elem; i++)
      if (xGs(i)<seas.peak.x) gs = xGs(i);
  }
  fvec xGe = spl->inv(y0e, seas.start.x, seas.end.x);
  float ge = seas.end.x;
  if (xGe.n_elem>0) {
    for (i=xGe.n_elem-1; i>=0; i--)
      if (xGe(i)>seas.peak.x) ge = xGe(i);
  }
  *gstart = gs;
  *gend = ge;
}

void splits::extract_flexpts(const Season& s,
			     const fvec& x,
			     const fvec& y,
			     const fvec& r,
			     int* p,
			     int* q)
{
  bool b = false;
  float rj;
  int i, j;
  for (i=0; i<x.n_elem; i++) {
    if (x[i]>=s.peak.x) break;
    if (x[i]>s.start.x) {
      if (b) {
	if (r[i]>=rj) {
	  j = i;
	  rj = r[i];
	}
      } else {
	j = i;
	rj = r[i];
	b = true;
      }
    }
  }
  if (b) *p=j; else *p=-1;

  b = false;
  for (i=0; i<x.n_elem; i++) {
    if (x[i]>=s.end.x) break;
    if (x[i]>s.peak.x) {
      if (b) {
	if (r[i]<=rj) {
	  j = i;
	  rj = r[i];
	}
      } else {
	j = i;
	rj = r[i];
	b = true;
      }
    }
  }
  if (b) *q=j; else *q=-1;
}

const int splits::npar = 18;
const int splits::npar_g = 26;

char *splits::param_names[] = {
  "DOY_Early_Min",
  "DOY_Early_Flex",
  "DOY_Peak",
  "DOY_Late_Flex",
  "DOY_Late_Min",
  "Early_Min_Val",
  "Early_Flex_Val",
  "Peak_Val",
  "Late_Flex_Val",
  "Late_Min_Val",
  "Min_Min_Duration",
  "Amplitude",
  "Latent_Val",
  "Min_Min_Integral",
  "Latent_Integral",
  "Total_Integral",
  "Early_Flex_Rate",
  "Late_Flex_Rate",
  "DOY_Start_Green",
  "DOY_End_Green",
  "Start_Green_Val",
  "End_Green_Val",
  "Green_Duration",
  "Green_Integral",
  "Greenup_Rate",
  "Senescence_Rate"
};

void splits::extract_phenology(std::vector<Season>& seas,
			       Spline* spl,
			       float* buf,
			       float period,
			       float fraction,
			       const Date& d0)
{
  fvec flexX, flexY, flexR;
  bool flex = inflection_points(spl, flexX, flexY);
  if (flex) {
    flexR = spl->eval(flexX, 1);
  }

  int i, k, n=seas.size();
  for (i=0; i<n; i++)
  {
    Season s = seas[i];
    
    if (!s.peak) continue;

    if (fraction<(float)0) k = i*npar; else k = i*npar_g;

    int p, q;
    if (flex) extract_flexpts(s, flexX, flexY, flexR, &p, &q);

    buf[k++] = convert_index_to_doy(s.start.x, period, d0);
    if (p<0)
      buf[k++] = 0.0f;
    else
      buf[k++] = convert_index_to_doy(flexX[p], period, d0);
    buf[k++] = convert_index_to_doy(s.peak.x, period, d0);
    if (q<0)
      buf[k++] = 0.0f;
    else
      buf[k++] = convert_index_to_doy(flexX[q], period, d0);
    buf[k++] = convert_index_to_doy(s.end.x, period, d0);
    buf[k++] = s.start.y;
    if (p<0) buf[k++] = 0.0f; else buf[k++] = flexY[p];
    buf[k++] = s.peak.y;
    if (q<0) buf[k++] = 0.0f; else buf[k++] = flexY[q];
    buf[k++] = s.end.y;
    
    float duration = (s.end.x-s.start.x)*period;
    buf[k++] = duration;
    float latentVI = (s.start.y+s.end.y)/2.0;
    float amplitude = s.peak.y-latentVI;
    buf[k++] = amplitude;
    buf[k++] = latentVI;
    float latentInt = latentVI*duration;
    float totalInt = spl->integral(s.start.x,s.end.x)*period;
    float seasInt = totalInt-latentInt;
    buf[k++] = seasInt;
    buf[k++] = latentInt;
    buf[k++] = totalInt;
    if (p<0) buf[k++] = 0.0f; else buf[k++] = flexR[p]/period;
    if (q<0) buf[k++] = 0.0f; else buf[k++] = -flexR[q]/period;
    if (fraction>=(float)0) {
      float gs, ge;
      greenness_start_end(spl, fraction, s, &gs, &ge);
      float dy_gs = convert_index_to_doy(gs, period, d0);
      float dy_ge = convert_index_to_doy(ge, period, d0);
      float gs_val = spl->eval(gs);
      float ge_val = spl->eval(ge);
      buf[k++] = dy_gs;
      buf[k++] = dy_ge;
      buf[k++] = gs_val;
      buf[k++] = ge_val;
      buf[k++] = (ge-gs)*period;
      buf[k++] = (spl->integral(gs,ge)-(((gs_val+ge_val)/2.0)*(ge-gs)))*period;
      buf[k++] = (s.peak.y-gs_val)/((s.peak.x-gs)*period);
      buf[k++] = (s.peak.y-ge_val)/((ge-s.peak.x)*period);
    }
  }
}

void splits::extract_phenology(std::vector<Season>& seas,
			       Spline* spl,
			       float* buf,
			       float period,
			       float fraction,
			       const Date& d0,
			       const Date& d1)
{
  fvec flexX, flexY, flexR;
  bool flex = inflection_points(spl, flexX, flexY);
  if (flex) {
    flexR = spl->eval(flexX, 1);
  }

  int i, k, n=seas.size();
  for (i=0; i<n; i++)
  {
    Season s = seas[i];
    
    if (!s.peak) continue;

    if (fraction<(float)0) k = i*npar; else k = i*npar_g;

    int p, q;
    if (flex) extract_flexpts(s, flexX, flexY, flexR, &p, &q);

    buf[k++] = convert_index_to_doy(s.start.x, d0, d1);
    if (p<0)
      buf[k++] = 0.0f;
    else
      buf[k++] = convert_index_to_doy(flexX[p], d0, d1);
    buf[k++] = convert_index_to_doy(s.peak.x, d0, d1);
    if (q<0)
      buf[k++] = 0.0f;
    else
      buf[k++] = convert_index_to_doy(flexX[q], d0, d1);
    buf[k++] = convert_index_to_doy(s.end.x, d0, d1);
    buf[k++] = s.start.y;
    if (p<0) buf[k++] = 0.0f; else buf[k++] = flexY[p];
    buf[k++] = s.peak.y;
    if (q<0) buf[k++] = 0.0f; else buf[k++] = flexY[q];
    buf[k++] = s.end.y;
    
    float duration = convert_index_to_doy(s.end.x, d0, d1)
      - convert_index_to_doy(s.start.x, d0, d1);
    buf[k++] = duration;
    float latentVI = (s.start.y+s.end.y)/2.0;
    float amplitude = s.peak.y-latentVI;
    buf[k++] = amplitude;
    buf[k++] = latentVI;
    float latentInt = latentVI*duration;
    float totalInt = spl->integral(s.start.x,s.end.x)*period;
    float seasInt = totalInt-latentInt;
    buf[k++] = seasInt;
    buf[k++] = latentInt;
    buf[k++] = totalInt;
    if (p<0) buf[k++] = 0.0f; else buf[k++] = flexR[p]/period;
    if (q<0) buf[k++] = 0.0f; else buf[k++] = -flexR[q]/period;
    if (fraction>=(float)0) {
      float gs, ge;
      greenness_start_end(spl, fraction, s, &gs, &ge);
      float dy_gs = convert_index_to_doy(gs, d0, d1);
      float dy_ge = convert_index_to_doy(ge, d0, d1);
      float gs_val = spl->eval(gs);
      float ge_val = spl->eval(ge);
      buf[k++] = dy_gs;
      buf[k++] = dy_ge;
      buf[k++] = gs_val;
      buf[k++] = ge_val;
      buf[k++] = convert_index_to_doy(ge,d0,d1)-convert_index_to_doy(gs,d0,d1);
      buf[k++] = (spl->integral(gs,ge)-(((gs_val+ge_val)/2.0)*(ge-gs)))*period;
      buf[k++] = (s.peak.y-gs_val)/((s.peak.x-gs)*period);
      buf[k++] = (s.peak.y-ge_val)/((ge-s.peak.x)*period);
    }
  }
}

void splits::extract_phenology_index(std::vector<Season>& seas,
				     Spline* spl,
				     float* buf,
				     float period,
				     float fraction)
{
  fvec flexX, flexY, flexR;
  bool flex = inflection_points(spl, flexX, flexY);
  if (flex) {
    flexR = spl->eval(flexX, 1);
  }

  int i, k, n=seas.size();
  for (i=0; i<n; i++)
  {
    Season s = seas[i];

    if (!s.peak) continue;

    if (fraction<(float)0) k = i*npar; else k = i*npar_g;

    int p, q;
    if (flex) extract_flexpts(s, flexX, flexY, flexR, &p, &q);

    buf[k++] = s.start.x*period;
     if (p<0)
      buf[k++] = 0.0f;
    else
      buf[k++] = flexX[p]*period;
    buf[k++] = s.peak.x*period;
    if (q<0)
      buf[k++] = 0.0f;
    else
      buf[k++] = flexX[q]*period;
    buf[k++] = s.end.x*period;
    buf[k++] = s.start.y;
    if (p<0) buf[k++] = 0.0f; else buf[k++] = flexY[p];
    buf[k++] = s.peak.y;
    if (q<0) buf[k++] = 0.0f; else buf[k++] = flexY[q];
    buf[k++] = s.end.y;
    float duration = (s.end.x-s.start.x)*period;
    buf[k++] = duration;
    float latentVI = (s.start.y+s.end.y)/2.0;
    float amplitude = s.peak.y-latentVI;
    buf[k++] = amplitude;
    buf[k++] = latentVI;
    float latentInt = latentVI*duration;
    float totalInt = spl->integral(s.start.x,s.end.x)*period;
    float seasInt = totalInt-latentInt;
    buf[k++] = seasInt;
    buf[k++] = latentInt;
    buf[k++] = totalInt;
    if (p<0) buf[k++] = 0.0f; else buf[k++] = flexR[p]/period;
    if (q<0) buf[k++] = 0.0f; else buf[k++] = -flexR[q]/period;
    if (fraction>=(float)0) {
      float gs, ge;
      greenness_start_end(spl, fraction, s, &gs, &ge);
      float gs_val = spl->eval(gs);
      float ge_val = spl->eval(ge);
      buf[k++] = gs*period+1;
      buf[k++] = ge*period+1;
      buf[k++] = gs_val;
      buf[k++] = ge_val;
      buf[k++] = (ge-gs)*period;
      buf[k++] = (spl->integral(gs,ge)-(((gs_val+ge_val)/2.0)*(ge-gs)))*period;
      buf[k++] = (s.peak.y-gs_val)/((s.peak.x-gs)*period);
      buf[k++] = (s.peak.y-ge_val)/((ge-s.peak.x)*period);
    }
  }
}

void splits::extract_phenology_raw(std::vector<Season>& seas,
				   Spline* spl,
				   float* buf,
				   float fraction)
{
  fvec flexX, flexY, flexR;
  bool flex = inflection_points(spl, flexX, flexY);
  if (flex) {
    flexR = spl->eval(flexX, 1);
  }

  int i, k, n=seas.size();
  for (i=0; i<n; i++)
  {
    Season s = seas[i];

    if (!s.peak) continue;

    if (fraction<(float)0) k = i*npar; else k = i*npar_g;

    int p, q;
    if (flex) extract_flexpts(s, flexX, flexY, flexR, &p, &q);

    buf[k++] = s.start.x;
     if (p<0)
      buf[k++] = 0.0f;
    else
      buf[k++] = flexX[p];
    buf[k++] = s.peak.x;
    if (q<0)
      buf[k++] = 0.0f;
    else
      buf[k++] = flexX[q];
    buf[k++] = s.end.x;
    buf[k++] = s.start.y;
    if (p<0) buf[k++] = 0.0f; else buf[k++] = flexY[p];
    buf[k++] = s.peak.y;
    if (q<0) buf[k++] = 0.0f; else buf[k++] = flexY[q];
    buf[k++] = s.end.y;
    float duration = s.end.x-s.start.x;
    buf[k++] = duration;
    float latentVI = (s.start.y+s.end.y)/2.0;
    float amplitude = s.peak.y-latentVI;
    buf[k++] = amplitude;
    buf[k++] = latentVI;
    float latentInt = latentVI*duration;
    float totalInt = spl->integral(s.start.x,s.end.x);
    float seasInt = totalInt-latentInt;
    buf[k++] = seasInt;
    buf[k++] = latentInt;
    buf[k++] = totalInt;
    if (p<0) buf[k++] = 0.0f; else buf[k++] = flexR[p];
    if (q<0) buf[k++] = 0.0f; else buf[k++] = -flexR[q];
    if (fraction>=(float)0) {
      float gs, ge;
      greenness_start_end(spl, fraction, s, &gs, &ge);
      float gs_val = spl->eval(gs);
      float ge_val = spl->eval(ge);
      buf[k++] = gs;
      buf[k++] = ge;
      buf[k++] = gs_val;
      buf[k++] = ge_val;
      buf[k++] = ge-gs;
      buf[k++] = spl->integral(gs,ge)-(((gs_val+ge_val)/2.0)*(ge-gs));
      buf[k++] = (s.peak.y-gs_val)/(s.peak.x-gs);
      buf[k++] = (s.peak.y-ge_val)/(ge-s.peak.x);
    }
  }
}
