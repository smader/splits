/*
 splcal_window.cpp

 An FLTK dialog for the splcal program.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <string>
#include <sstream>
#include <FL/Fl_Group.H>
#include <FL/Fl_Box.H>
#include "main_window.h"
#include "splcal_window.h"

using namespace splits;

Splcal_window::Splcal_window(Main_window* MW)
  : Fl_Double_Window(620,400, "Interpolate (splcal)"), mw(MW)
{
  {
    Fl_Group *g = new Fl_Group(0,10,620,25);
    spl_file_inp = new Dataset_input(170,10,430,25, "Spline File:",
				     Dataset_input::SINGLE);
    spl_file_inp->callback(cb, this);
    spl_file_inp->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);
    g->end();
    g->resizable(spl_file_inp);
  }
  {
    Fl_Group *g = new Fl_Group(0,40,620,25);
    doy_file_inp = new Dataset_input(170,40,430,25, "DOY File (Abscissae):",
				     Dataset_input::SINGLE);
    doy_file_inp->callback(cb, this);
    doy_file_inp->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);
    g->end();
    g->resizable(doy_file_inp);
  }
  {
    Fl_Group *g = new Fl_Group(0,70,620,25);

    Fl_Group *h1 = new Fl_Group(0,70,310,25);
    start_inp = new Date_input(170,70,165,25, "Start Date:");
    start_inp->callback(cb, this);
    start_inp->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);
    h1->end();
    h1->resizable(start_inp);

    Fl_Group *h2 = new Fl_Group(310,70,310,25);
    end_inp = new Date_input(435,70,165,25, "End Date:");
    end_inp->callback(cb, this);
    end_inp->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);
    h2->end();
    h2->resizable(end_inp);

    g->end();
  }
  {
    Fl_Group *g = new Fl_Group(0,100,620,25);
    deriv_inp = new Real_input(170,100,430,25,"Derivative:");
    deriv_inp->callback(cb, this);
    deriv_inp->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);
    g->end();
    g->resizable(deriv_inp);
  }
  {
    Fl_Group *g = new Fl_Group(0,135,620,25);
    dtype_choice = new Fl_Choice(170,135,180,25, "Output Data Type:");
    dtype_choice->callback(cb, this);
    dtype_choice->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED);
    Fl_Box *b = new Fl_Box(350,135,260,25);
    g->end();
    g->resizable(b);
  }
  {
    Fl_Group *g = new Fl_Group(0,165,620,25);

    Fl_Group *h1 = new Fl_Group(0,165,310,25);
    offs_inp = new Real_input(170,165,180,25,"Offset:");
    offs_inp->callback(cb, this);
    offs_inp->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);
    h1->end();
    h1->resizable(offs_inp);

    Fl_Group *h2 = new Fl_Group(310,165,310,25);
    gain_inp = new Real_input(420,165,180,25,"Scale:");
    gain_inp->callback(cb, this);
    gain_inp->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);
    h2->end();
    h2->resizable(gain_inp);

    g->end();
  }
  {
    Fl_Group *g = new Fl_Group(0,195,620,25);
    nodata_inp = new Real_input(170,195,430,25,"Nodata Value:");
    nodata_inp->callback(cb, this);
    nodata_inp->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);
    g->end();
    g->resizable(nodata_inp);
  }
  {
    Fl_Group *g = new Fl_Group(0,225,620,25);
    out_file_inp = new Dataset_input(170,225,430,25,"Output File:",
				     Dataset_input::CREATE);
    out_file_inp->callback(cb, this);
    out_file_inp->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);
    g->end();
    g->resizable(out_file_inp);
  }
  {
    Fl_Group *g = new Fl_Group(0,260,620,130);
    term = new Terminal(10,260,600,130,"splcal");
    g->end();
  }
  resizable(term);

  dtype_choice->add("Byte|Integer|Real");

  callback(window_cb);

  set_modal();
  hotspot(this);
  show();
}

void Splcal_window::update()
{
  std::stringstream s;
  std::string arguments = "";

  if (!doy_file_inp->is_empty()) {
    arguments += " -A ";
    arguments += doy_file_inp->filename();
  }

  if (!start_inp->is_empty() && !end_inp->is_empty()) {
    arguments += " -C ";
    s << start_inp->date() << "," << end_inp->date();
    arguments += s.str();
  }

  if (!deriv_inp->is_empty()) {
    arguments += " -D ";
    s.str("");
    s << (int)(deriv_inp->number());
    arguments += s.str();
  }

  if (!offs_inp->is_empty() || !gain_inp->is_empty()) {
    int offs = 0.0f, gain = 1.0f;
    if (!offs_inp->is_empty()) offs = offs_inp->number();
    if (!gain_inp->is_empty()) gain = gain_inp->number();
    arguments += " -L ";
    s.str("");
    s << offs << "," << gain;
    arguments += s.str();
  }

  if (mw->nthread>1) {
    s.str("");
    s << " -M " << mw->nthread;
    arguments += s.str();
  }

  if (!nodata_inp->is_empty()) {
    s.str("");
    s << " -N " << nodata_inp->number();
    arguments += s.str();
  }

  if (mw->quiet) {
    arguments += " -Q";
  }

  if (dtype_choice->value()!=-1) {
    switch (dtype_choice->value()) {
    case 0:
      arguments += " -t Byte";
      break;
    case 1:
      arguments += " -t Integer";
      break;
    case 2:
      arguments += " -t Real";
      break;
    default:
      break;
    }
  }

  if (!spl_file_inp->is_empty()) {
    arguments += " ";
    arguments += spl_file_inp->filename();
  }

  if (!out_file_inp->is_empty()) {
    arguments += " ";
    arguments += out_file_inp->filename();
  }

  term->set_args(arguments.c_str());
}

void Splcal_window::cb(Fl_Widget* w, void* data)
{
  Splcal_window *win = (Splcal_window*)data;
  win->update();
}

void Splcal_window::window_cb(Fl_Widget* w, void* data)
{
  Splcal_window* win = (Splcal_window*)w;
  if (win->term->is_busy()) return;
  Fl::default_atclose(win, data);
}
