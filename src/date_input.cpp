/*
 date_input.cpp

 FLTK input field for calendar dates as specified by the "Date"
 datatype (see "date.cpp").

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <cstdio>
#include <string>
#include <FL/Fl.H>
#include "date_input.h"

using namespace std;
using namespace splits;
using splits::Date;

Date_input::Date_input(int X, int Y, int W, int H, const char* l) : Fl_Input(X,Y,W,H,l)
{
  tooltip("Enter Date as MM/DD/YYYY or DOY/YYYY");
  when(FL_WHEN_ENTER_KEY|FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED);
}

Date Date_input::date() const
{
  Date d(value());
  return d;
}

void Date_input::set_date(const Date& d)
{
  string str = d.to_string();
  value(str.c_str());
}

bool Date_input::is_empty() const
{
  string str = value();
  return (str=="");
}

int Date_input::handle(int e)
{
  if (e==FL_KEYDOWN) {
    int key = Fl::event_key();
    if (key==FL_Enter) {
      Date d(value());
      if (!d) ; else set_date(d);
    }
  }
  return (Fl_Input::handle(e));
}
