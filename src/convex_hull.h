/*
 convex_hull.h

 Jarvis' march algorithm to compute the convex hull of a set of points.

 This file is part of Splits, a spline analysis tool for
 remotely sensed time series.

 Copyright (c) 2010-2016 Sebastian Mader

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the above copyright notice
 and this paragraph and the following paragraph appear in all copies.
	
 THIS SOFTWARE IS DISTRIBUTED "AS IS" AND WITHOUT ANY WARRANTY TO THE
 EXTENT PERMITTED BY APPLICABLE LAW; WITHOUT EVEN THE IMPLIED WARRANTY
 OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. THE COPYRIGHT
 HOLDER HAS NO OBLIGATIONS TO CORRECT DEFECTS, PROVIDE MAINTENANCE,
 SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS; NOR WILL HE BE LIABLE
 FOR ANY DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING
 IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SPLITS_CONVEX_HULL_H
#define SPLITS_CONVEX_HULL_H

#include <vector>
#define ARMA_NO_DEBUG
#include <armadillo>

namespace splits { 

std::vector<int> convex_hull(const arma::fvec& x, const arma::fvec& y);

arma::fvec hull_points(const arma::fvec& x,
		       const arma::fvec& y,
		       const std::vector<int>& hull);

} //namespace splits

#endif
