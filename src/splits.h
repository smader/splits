/*
 splits.h

 The SPLITS API.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2017  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPLITS_H
#define SPLITS_H

#include <cstdarg>
#include <vector>
#include <map>
#include "date.h"
#include "spline.h"

namespace splits {

// Supported spline and knot vector types:

enum Spline_type {
  UNIFORM_BSPLINE,
  PERIODIC_UNIFORM_BSPLINE,
  UNIFORM_SMOOTH_BSPLINE,
  PERIODIC_SMOOTH_BSPLINE,
  UNIFORM_PSPLINE,
  PERIODIC_PSPLINE,
  NONUNIFORM_BSPLINE,
  PERIODIC_NONUNIFORM_BSPLINE
};

 
// Report phenological timing parameters as DOY, composite index, or raw
// domain index (i.e. scaled between [0,1]), respectively. Composite
// indices are interpolated according to DOY within a compositing period.

enum {
  PHENOLOGY_DOY,
  PHENOLOGY_INDEX,
  PHENOLOGY_RAW,
};


// Domain object maps DOYs of a set of observations between two dates on
// a standard interval of [0,1].

class Domain
{
  Date d_beg, d_end;
  std::vector<Date> ttab;
  arma::fvec doy, xt;

public:
  // Create a domain by defining start and end date, the array of the days
  // of year (DOY) of the observations, and the number of observations,
  // respectively:
  Domain(const Date& d_beg_, const Date& d_end_, const float* doy_, int n);
  
  Domain(const std::vector<Date>& ttab_);
  ~Domain() {}

  Date begin() const {return d_beg;}
  Date end() const {return d_end;}

  int nobs() const {return xt.n_elem;}
  
  arma::fvec vect() const {return xt;}

  void axis(float* x) const;
};


// The set of phenological parameters:

struct Pheno_set
{
  float doy_early_min;    // Early minimum (DOY, composite or raw index)
  float doy_early_flex;   // Early inflection point DOY or index
  float doy_peak;         // Peak DOY or index
  float doy_late_flex;    // Late inflection point DOY or index
  float doy_late_min;     // Late minimum DOY or index
  float early_min_val;    // Early minimum data value
  float early_flex_val;   // Early inflection point data value
  float peak_val;         // Peak data value
  float late_flex_val;    // Late inflection point data value
  float late_min_val;     // Late minimum data value
  float min_min_duration; // Time between two minima in the appropriate units
  float amplitude;        // Diff. between peak and latent value
  float latent_val;       // Average data value of early and late min.
  float min_min_integral; // Integral between early and late min.
  float latent_integral;  // Box integral of latent val over time
  float total_integral;   // latent_integral + min_min_integral
  float early_flex_rate;  // 1st derivative in early inflection point
  float late_flex_rate;   // 1st derivative in late inflection point
  float doy_start_green;  // Start of greenness, see phenology() below
  float doy_end_green;    // End of greenness
  float start_green_val;  // Data value at start of greenness
  float end_green_val;    // Data value at end of greenness
  float green_duration;   // Time between start and end of greenness
  float green_integral;   // Integral over greenness period
  float greenup_rate;     // Avg. slope between start of greeness and peak time
  float senescence_rate;  // Avg. slope between end of greenness and peak time
};


// Fit a spline of a given type over domain x, using observations y,
// any additional parameters (...) must follow the spline type. For plain
// B-splines ... is degree, Smooth B-splines take degree as fourth
// argument, smoothing parameter as 5th argument. create_spline for
// a PSPLINE takes three additional arguments: degree, smoothing
// factor, and penalty. Nonuniform B-splines take a pointer to an array
// of floats (knot vector) followed by an integer with the number of
// knots and finally an integer giving the degree of the spline:

Spline* create_spline(const Domain& x, const float* y, Spline_type type, ...);

// Fit a spline as above, using weighted least squares with weights w
// on the observations:

Spline* create_spline(const Domain& x, const float* y, const float* w,
		      Spline_type type, ...);

 
// Evaluate spline (or its d-th derivative) on a Domain x, store the result
// in array y. Output arrays must be allocated by the caller:

void evaluate(Spline* spl, const Domain& x, float* y);
void derivative(Spline* spl, const Domain& x, int d, float* y);


// (Weighted) mean squared error based on the number of degrees of freedom
// in a fitted spline model (i.e. the number of samples used to fit the
//  model less the dimension of it's basis):
 
double mean_sq_error(Spline* spl, const Domain& x, float* y);
double weighted_mean_sq_error(Spline* spl, const Domain& x, float* y,
			      float* w);


// Calculate phenological parameters (Pheno_sets) for a time series fitted
// to a spline over the given domain. Boolean southern is false/true for
// observations on the Earth's northern/southern hemisphere. Argument
// fraction describes a fraction of the amplitude of a phenological cycle
// used to describe start and end of greenness (e.g. if fraction=0.2,
// greenness starts when a data value excesses an early minimum by 20%.
// End of greenness is defined similarly using a cycle's late minimum.
// Argument method is one of {PHENOLOGY_DOY, PHENOLOGY_INDEX, PHENOLOGY_RAW}
// (see the definitions at the top of this file). Each Pheno Set is mapped
// to an int representing the year of occurrence:

std::map<int, Pheno_set> phenology(Spline* spl, const Domain& x, bool southern,
				   float fraction, int method);


void destroy_spline(Spline* spl);

}

#endif
