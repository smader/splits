/*
 prepro_window.cpp

 An FLTK dialog for the prepro program.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <string>
#include <FL/Fl_Box.H>
#include "prepro_window.h"

using namespace splits;

Prepro_window::Prepro_window() : Fl_Double_Window(620,400,"Prepare Data")
{
  {
    Fl_Group *g = new Fl_Group(0,10,620,25);
    sensor_choice = new Fl_Choice(140,10,100,25, "Product:");
    sensor_choice->callback(cb, this);
    sensor_choice->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED);
    Fl_Box *b = new Fl_Box(240,10,380,25);
    g->end();
    g->resizable(b);
  }
  {
    Fl_Group *g = new Fl_Group(0,40,620,25);
    h_choice = new Fl_Choice(140,40,70,25, "Horizontal Tile:");
    h_choice->callback(cb, this);
    h_choice->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED);
    v_choice = new Fl_Choice(310,40,70,25, "Vertical Tile:");
    v_choice->callback(cb, this);
    v_choice->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED);
    Fl_Box *b = new Fl_Box(380,40,240,25);
    g->end();
    g->resizable(b);
  }
  {
    Fl_Group *g = new Fl_Group(0,70,620,25);
    evi_btn = new Fl_Round_Button(140,70,60,25, "EVI");
    evi_btn->type(FL_RADIO_BUTTON);
    evi_btn->callback(cb, this);
    ndvi_btn = new Fl_Round_Button(200,70,60,25, "NDVI");
    ndvi_btn->type(FL_RADIO_BUTTON);
    ndvi_btn->callback(cb, this);
    evi_btn->value(1);
    Fl_Box *b = new Fl_Box(240,70,360,25);
    g->end();
    g->resizable(b);
  }
  {
    Fl_Group *g = new Fl_Group(0,100,620,25);
    hdf_dir = new Dataset_input(140,100,465,25, "HDF Directory:",
				Dataset_input::DIRECTORY);
    hdf_dir->callback(cb, this);
    hdf_dir->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);
    hdf_dir->tooltip("Input directory storing MODIS M*D13Q1 hdf containers");
    g->end();
    g->resizable(hdf_dir);
  }
  {
    Fl_Group *g = new Fl_Group(0,130,620,25);
    output_dir = new Dataset_input(140,130,465,25, "Output Directory:",
				   Dataset_input::DIRECTORY);
    output_dir->callback(cb, this);
    output_dir->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);
    g->end();
    g->resizable(output_dir);
  }
  {
    Fl_Group *g = new Fl_Group(0,170,620,220);
    term = new Terminal(10,170,600,220, "prepro");
    g->end();
    g->resizable(term);
  }
  resizable(term);

  sensor_choice->add("MOD 13|MYD 13|MCD 13");

  char str[8];
  int t;
  for (t=0; t<=35; t++) {
    sprintf(str, "%d", t);
    h_choice->add(str);
  }

  for (t=0; t<=17; t++) {
    sprintf(str, "%d", t);
    v_choice->add(str);
  }

  callback(window_cb);

  set_modal();
  hotspot(this);
  show();
}

void Prepro_window::update()
{
  std::string arguments = "";

  if (ndvi_btn->value()) arguments += "-N ";

  switch (sensor_choice->value()) {
  case 0:
    arguments += "-s MOD";
    break;
  case 1:
    arguments += "-s MYD";
    break;
  case 2:
    arguments += "-s MCD";
    break;
  default:
    break;
  }

  if ((v_choice->value()!=-1) && h_choice->value()!=-1) {
    char str[16];
    sprintf(str, " -t h%02dv%02d", h_choice->value(), v_choice->value());
    arguments += str;
  }

  if (!hdf_dir->is_empty()) {
    arguments += " ";
    arguments += hdf_dir->filename();
  }

  if (!output_dir->is_empty()) {
    arguments += " ";
    arguments += output_dir->filename();
  }

  term->set_args(arguments.c_str());
}

void Prepro_window::cb(Fl_Widget* w, void* data)
{
  Prepro_window *win = (Prepro_window*)data;
  win->update();
}

void Prepro_window::window_cb(Fl_Widget* w, void* data)
{
  Prepro_window* win = (Prepro_window*)w;
  if (win->term->is_busy()) return;
  Fl::default_atclose(win, data);
}
