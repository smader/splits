/*
 archive.h

 A class representing a remote sensing data archive.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2020  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPLITS_ARCHIVE_H
#define SPLITS_ARCHIVE_H

#include <string>
#include <gdal_priv.h>
#include <armadillo>
#include "date.h"
#include "arbitrary_vector.h"
#include "static_matrix.h"

namespace splits {

class Archive
{
  std::string in_file, x_file, w_file;
  GDALDataset *dset, *dy_dset, *wt_dset;
  std::vector<Date> xt_dates;
  Date d_beg, d_end;
  float in_offset, in_gain;
  float in_nodata;
  bool southern;
  int wmode;
  Arbitrary_vector<float> w_tab;
  arma::fvec w_vec;

public:
  enum {NONE, CONSTANT, QUALITY};

public:
  Archive();
  Archive(const char* in, const char* x, const char* w=0);
  Archive(const Archive& other);
  ~Archive();

  Archive& operator=(const Archive& other);

  char** file_list();

  const char* dataset() {return in_file.c_str();}
  const char* doy_dataset() {return x_file.c_str();}
  const char* quality_dataset() {return w_file.c_str();}
  
  Date begin() {return d_beg;}
  void set_begin(const Date& d) {d_beg=d;}

  Date end() {return d_end;}
  void set_end(const Date& d) {d_end=d;}

  std::vector<Date> dates() {return xt_dates;}

  float offset() {return in_offset;}
  void set_offset(float offs) {in_offset=offs;}

  float gain() {return in_gain;}
  void set_gain(float gain) {in_gain=gain;}

  float nodata() {return in_nodata;}
  void set_nodata(float val) {in_nodata=val;}

  int weight_mode() {return wmode;}

  Arbitrary_vector<float> weight_table() {return w_tab;}
  void set_weight_table(const Arbitrary_vector<float>& tab) {w_tab=tab;}

  int xsize() {return dset->GetRasterXSize();}
  int ysize() {return dset->GetRasterYSize();}
  int count() {return dset->GetRasterCount();}

  bool open();
  void close();

  Static_matrix<double,3,3> geo_transform();

  bool is_geo_transformed();

  bool is_southern() {return southern;}

  void hemisphere();
  void hemisphere(bool hemi) {southern=hemi;}

  void load(int band, float* buffer);
  bool load(Date& date, float* buffer);

  int poke_raw(double* coords, arma::fvec& xt, arma::fvec& yt, arma::fvec& qa);
  
  int poke(double* coords, arma::fvec& xt, arma::fvec& yt, arma::fvec& wt,
	   const Date& d0, const Date& d1);

  int poke(int j, int i, arma::fvec& xt, arma::fvec& yt, arma::fvec& wt,
	   const Date& d0, const Date& d1);
};

} //namespace splits

#endif
