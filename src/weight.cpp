/*
 weight.cpp

 Support functions for weighted least squares fits of spline models.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "weight.h"

using namespace arma;
using namespace splits;

Arbitrary_vector<float> splits::read_weight_file(const char* fname)
{
  FILE *fp;

  fp = fopen(fname, "rt");
  if (fp==NULL) {
    return Arbitrary_vector<float>();
  }

  int newlines = 0;
  char ch;
  while (fscanf(fp, "%c", &ch)==1) {
    if (ch=='\n') newlines++;
  }
  rewind(fp);

  int index, from, to;
  float weight;
  bool first=true;
  int i;
  for (i=0; i<newlines; i++) {
    if (fscanf(fp, "%d %f", &index, &weight)!=2) {
      fclose(fp);
      return Arbitrary_vector<float>();
    }
    if (first) {
      from=to=index;
      first=false;
    } else {
      if (index<from) from=index;
      if (index>to) to=index;
    }
  }
  rewind(fp);

  Arbitrary_vector<float> v(from, to, 0);

  for (i=0; i<newlines; i++) {
    fscanf(fp, "%d %f", &index, &weight);
    v[index]=weight;
  }

  fclose(fp);

  return v;
}

int splits::read_weight_file(const char* fname, fmat& wt)
{
  FILE *fp = fopen(fname, "rt");
  if (!fp) {
    return -1;
  }
  int nwt = 0;
  float tmp;
  while (!feof(fp)) {
    if (fscanf(fp, "%f", &tmp)==1) nwt+=1;
  }
  if (nwt==0) {
    return 0;
  } else {
    wt = fmat(nwt,nwt);
    wt.fill(0);
    rewind(fp);
    int i;
    for (i=0; i<nwt; i++) {
      fscanf(fp, "%f", &tmp);
      wt(i,i) = tmp;
    }
    fclose(fp);
    return nwt;
  }
}

void splits::load_weights(short int* buffer,
			  int bufsize,
			  Arbitrary_vector<float> lut,
			  fvec& weights)
{
  weights.zeros();
  int i;
  for (i=0; i<bufsize; i++) {
    weights(i) = lut[buffer[i]];
  }
}

void splits::safe_load_weights(short int* buffer,
			  int bufsize,
			  Arbitrary_vector<float> lut,
			  fvec& weights)
{
  weights.zeros();
  int i;
  for (i=0; i<bufsize; i++) {
    weights(i) = lut(buffer[i]);
  }
}
