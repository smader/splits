/*
 season.h

 Analyze seasons and phenology using a spline model.

 The SPLITS splview program to fit and visualize spline models to
 remotely sensed time series images.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2017  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPLITS_SEASON_H
#define SPLITS_SEASON_H

#define ARMA_NO_DEBUG
#include <armadillo>
#include "date.h"
#include "spline.h"

namespace splits {

struct DataPt
{
  float x, y;
  DataPt() : x(0), y(0) {}
  DataPt(float xx, float yy) : x(xx), y(yy) {}
  ~DataPt() {}
  bool operator<(const DataPt& rhs) const {return (x<rhs.x);} //for sorting
  bool operator!() const {return (x==(float)0) && (y==(float)0);}
};

struct Season
{
  Date startDate, endDate;
  float startIndex, endIndex;
  DataPt start, peak, end;

  Season() : startIndex(0), endIndex(0) {}
  Season(const DataPt& s, const DataPt& p, const DataPt& e)
    : start(s), peak(p), end(e) {}
  ~Season() {}

  bool operator!() const {return (!start)&&(!peak)&&(!end);}

  void reset();
};


void extreme_points(Spline* spl,
		    arma::fvec& xmn,
		    arma::fvec& ymn,
		    arma::fvec& xmx,
		    arma::fvec& ymx);

std::vector<int> extreme_points(Spline* spl, arma::fvec& x, arma::fvec& y);


bool inflection_points(Spline* spl, arma::fvec& x, arma::fvec& y);


void identify_seasons(std::vector<Season>& seas,
		      Spline* spl,
		      float imax,
		      float range=0.0f,
		      bool bigT=false,
		      float low=0.0f,
		      float high=0.0f,
		      bool atBeg=false,
		      bool atEnd=false);
 
void greenness_start_end(Spline* spl,
			 float fraction,
			 const Season& seas,
			 float* gstart,
			 float* gend);

void extract_flexpts(const Season& s,
		     const arma::fvec& x,
		     const arma::fvec& y,
		     const arma::fvec& r,
		     int* p,
		     int* q);

extern const int npar;
extern const int npar_g;

extern char *param_names[];

// No longer used
void extract_phenology(std::vector<Season>& seas,
		       Spline* spl,
		       float* buf,
		       float period,
		       float fraction,
		       const Date& d0);

void extract_phenology(std::vector<Season>& seas,
		       Spline* spl,
		       float* buf,
		       float period,
		       float fraction,
		       const Date& d0,
		       const Date& d1);

void extract_phenology_index(std::vector<Season>& seas,
			     Spline* spl,
			     float* buf,
			     float period,
			     float fraction);

void extract_phenology_raw(std::vector<Season>& seas,
			   Spline* spl,
			   float* buf,
			   float fraction);
 
} //namespace splits

#endif
