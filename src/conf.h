/*
 conf.h

 Read/write GUI configuration.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2017  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_CONF_H
#define SPLITS_CONF_H

#include <cstdio>
#include <string>
#include "main_window.h"

namespace splits {

class Conf
{
public:
  std::string ts_dset, dy_dset, qa_dset;
  std::string w_tab_file;
  bool use_wt;
  float offs, gain;
  Date d_beg, d_end;
  float nodata;

private:
  std::string token(FILE* fp, const char* t);

  void trim_whitespace(char* str);
  void trim_quotes(char*str);

  bool to_bool(const char* str);
  
public:
  Conf() : use_wt(false), offs(0.0f), gain(1.0f), nodata(-32767.0f) {}
  explicit Conf(Main_window* MW);
  explicit Conf(const char* fname);
  ~Conf() {}

  bool save(const char* fname);
};
  
} //namespace splits

#endif
