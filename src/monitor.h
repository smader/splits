/*
 monitor.h

 Monitor is a FLTK widget to display imagery data from a GDAL dataset.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2020  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_MONITOR_H
#define SPLITS_MONITOR_H

#include <FL/Fl_Widget.H>
#include <FL/Fl_Menu_Item.H>
#include "static_matrix.h"
#include "archive.h"

namespace splits {

class Monitor : public Fl_Widget
{
  struct Object
  {
    int xSize, ySize;
    float **data;
    float xmin, ymin, xmax, ymax;
    float data_mn, data_mx, no_data;
    float factor;
    Static_matrix<double,3,3> trans;

    Object(Archive& ar, int band, float clip);
    ~Object();
    void contrast(float c);
    bool nearest(double x, double y, int* c, int* r);
    void draw(int xx, int yy, int ww, int hh, double xmn, double ymn,
	      double xmx, double ymx);
    unsigned char pixel(float d);
  };

  Object *obj;
  const Fl_Menu_Item* menu;
  double xmin, ymin, xmax, ymax;
  int bx, by, bw, bh;
  int opmode;
  float clip;

 public:
  double cur[2];
  enum { POINT, ZOOM, PAN };

private:
  void draw();

  void erase_box();

public:
  Monitor(int x, int y, int w, int h, const char* l=0, const Fl_Menu_Item* m=0);
  ~Monitor();

  int handle(int event);

  void set_viewport();
  void set_viewport(double xmn, double xmx, double ymn, double ymx);
  void set_viewport(double x, double y);

  float percent_clip() {return clip*100.0f;}
  void set_percent_clip(float c);

  void zoom(bool in=true);

  void erase();

  void display();
  void display(Archive& a, int band=0, bool set_vwpt=true);

  int mode() {return opmode;}
  void set_mode(int mode) {opmode=mode;}
};

} //namespace splits

#endif
