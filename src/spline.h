/*
 spline.h

 Abstract base class for spline models - all models implemented
 in the SPLITS framework should use this interface.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2017  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPLITS_SPLINE_H
#define SPLITS_SPLINE_H

#define ARMA_NO_DEBUG
#define ARMA_DONT_PRINT_ERRORS
#include <armadillo>

namespace splits {

class Spline
{
public:
  virtual ~Spline() {}

  virtual arma::fvec knots() = 0;

  virtual int order() const = 0;
  virtual int degree() const = 0;

  virtual arma::fvec domain() = 0;

  virtual bool fit(const arma::fvec& xt, const arma::fvec& yt) = 0;
  virtual bool fit(const arma::fvec& xt, const arma::fvec& yt,
		   const arma::fvec& w) = 0;

  virtual bool opt(const arma::fvec& xt, const arma::fvec& yt, int iter=1) = 0;
  virtual bool opt(const arma::fvec& xt, const arma::fvec& yt,
		   const arma::fvec& w, int iter=1) = 0;
  
  virtual arma::fvec eval(const arma::fvec& xt, int l=0) = 0;
  virtual float eval(float x, int l=0) = 0;

  virtual arma::fvec inv(float y) = 0;
  virtual arma::fvec inv(float y, float a, float b) = 0;

  virtual float integral(float a, float b) = 0;

  virtual arma::fvec roots(int l=0) = 0;
  virtual arma::fvec roots(float a, float b, int l=0) = 0;
};

} //namespace splits

#endif
