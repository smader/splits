/*
 splits.cpp

 The SPLITS API.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2020  Sebastian Mader, David Frantz

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "splits.h"
#include "time_axis.h"
#include "knot.h"
#include "b_spline.h"
#include "bootstrap.h"

using namespace std;
using namespace arma;
using namespace splits;

Domain::Domain(const Date& d_beg_, const Date& d_end_, const float* doy_,
	       int n) : d_beg(d_beg_), d_end(d_end_)
{
  doy = fvec(n);

  int i;
  for (i=0; i<n; i++) doy(i) = doy_[i];

  ttab = gen_timetable(doy, d_beg.year);

  xt = make_scaled_time_axis(ttab, d_beg, d_end, 0, 1);
}

Domain::Domain(const vector<Date>& ttab_) : ttab(ttab_)
{
  int n = ttab.size();

  d_beg = ttab[0];
  d_end = ttab[n-1];

  doy = fvec(n);  
  int i;
  for (i=0; i<n; i++) doy(i) = ttab[i].doy();
  
  xt = make_scaled_time_axis(ttab, ttab[0], ttab[n-1], 0, 1);
}

void Domain::axis(float* x) const
{
  int i;
  for (i=0; i<xt.n_elem; i++) x[i]=xt(i);
}

Spline* splits::create_spline(const Domain& x, const float* y,
			      Spline_type type, ...)
{
  va_list param;
  va_start(param, type);

  int nspan, ntemp, degree, dim_basis, pord;
  float *knots, *temp, lambda;
  Spline *spl;
  
  switch (type) {
  case UNIFORM_BSPLINE:
    nspan = va_arg(param, int);
    degree = va_arg(param, int);
    knots = clamped_uniform_knot_vector(0, 1, nspan, degree+1, &dim_basis);
    spl = new B_spline(knots, dim_basis, degree+1);
    break;
  case PERIODIC_UNIFORM_BSPLINE:
    nspan = va_arg(param, int);
    degree = va_arg(param, int);
    knots = periodic_uniform_knot_vector(0, 1, nspan, degree+1, &dim_basis);
    spl = new B_spline(knots, dim_basis, degree+1, true);
    break;
  case UNIFORM_SMOOTH_BSPLINE:
    nspan = va_arg(param, int);
    degree = va_arg(param, int);
    lambda = (float)va_arg(param, double);
    spl = new Smooth_spline(knots, dim_basis, degree+1, lambda);
    break;
  case PERIODIC_SMOOTH_BSPLINE:
    nspan = va_arg(param, int);
    degree = va_arg(param, int);
    lambda = (float)va_arg(param, double);
    spl = new Smooth_spline(knots, dim_basis, degree+1, lambda, true);
    break;
  case UNIFORM_PSPLINE:
    nspan = va_arg(param, int);
    degree = va_arg(param, int);
    lambda = (float)va_arg(param, double);
    pord = va_arg(param, int);
    spl = new P_spline(knots, dim_basis, degree+1, lambda, pord);
    break;
  case PERIODIC_PSPLINE:
    nspan = va_arg(param, int);
    degree = va_arg(param, int);
    lambda = (float)va_arg(param, double);
    pord = va_arg(param, int);
    spl = new P_spline(knots, dim_basis, degree+1, lambda, pord, true);
    break;
  case NONUNIFORM_BSPLINE:
    temp = va_arg(param, float*);
    ntemp = va_arg(param, int);
    degree = va_arg(param, int);
    knots = clamped_knot_vector(temp, ntemp, degree+1, &dim_basis);
    spl = new B_spline(knots, dim_basis, degree+1);
    break;
  case PERIODIC_NONUNIFORM_BSPLINE:
    temp = va_arg(param, float*);
    ntemp = va_arg(param, int);
    degree = va_arg(param, int);
    knots = periodic_knot_vector(temp, ntemp, degree+1, &dim_basis);
    spl = new B_spline(knots, dim_basis, degree+1, true);
    break;
  default:
    spl = 0;
  }
  
  va_end(param);

  if (spl==0) return 0;

  fvec v(x.nobs());
  int i;
  for (i=0; i<x.nobs(); i++) v(i) = y[i];
  
  bool ok = spl->fit(x.vect(), v); 

  if (!ok) {
    delete spl;
    return 0;
  }
  
  return spl;
}

Spline* splits::create_spline(const Domain& x, const float* y, const float* w,
			      Spline_type type, ...)
{
  va_list param;
  va_start(param, type);

  int nspan, ntemp, degree, dim_basis, pord;
  float *knots, *temp, lambda;
  Spline *spl;
  
  switch (type) {
  case UNIFORM_BSPLINE:
    nspan = va_arg(param, int);
    degree = va_arg(param, int);
    knots = clamped_uniform_knot_vector(0, 1, nspan, degree+1, &dim_basis);
    spl = new B_spline(knots, dim_basis, degree+1);
    break;
  case PERIODIC_UNIFORM_BSPLINE:
    nspan = va_arg(param, int);
    degree = va_arg(param, int);
    knots = periodic_uniform_knot_vector(0, 1, nspan, degree+1, &dim_basis);
    spl = new B_spline(knots, dim_basis, degree+1, true);
    break;
  case UNIFORM_SMOOTH_BSPLINE:
    nspan = va_arg(param, int);
    degree = va_arg(param, int);
    lambda = (float)va_arg(param, double);
    spl = new Smooth_spline(knots, dim_basis, degree+1, lambda);
    break;
  case PERIODIC_SMOOTH_BSPLINE:
    nspan = va_arg(param, int);
    degree = va_arg(param, int);
    lambda = (float)va_arg(param, double);
    spl = new Smooth_spline(knots, dim_basis, degree+1, lambda, true);
    break;
  case UNIFORM_PSPLINE:
    nspan = va_arg(param, int);
    degree = va_arg(param, int);
    lambda = (float)va_arg(param, double);
    pord = va_arg(param, int);
    spl = new P_spline(knots, dim_basis, degree+1, lambda, pord);
    break;
  case PERIODIC_PSPLINE:
    nspan = va_arg(param, int);
    degree = va_arg(param, int);
    lambda = (float)va_arg(param, double);
    pord = va_arg(param, int);
    spl = new P_spline(knots, dim_basis, degree+1, lambda, pord, true);
    break;
  case NONUNIFORM_BSPLINE:
    temp = va_arg(param, float*);
    ntemp = va_arg(param, int);
    degree = va_arg(param, int);
    knots = clamped_knot_vector(temp, ntemp, degree+1, &dim_basis);
    spl = new B_spline(knots, dim_basis, degree+1);
    break;
  case PERIODIC_NONUNIFORM_BSPLINE:
    temp = va_arg(param, float*);
    ntemp = va_arg(param, int);
    degree = va_arg(param, int);
    knots = periodic_knot_vector(temp, ntemp, degree+1, &dim_basis);
    spl = new B_spline(knots, dim_basis, degree+1, true);
    break;
  default:
    spl = 0;
  }
  
  va_end(param);

  if (spl==0) return 0;

  fvec v(x.nobs());
  int i;
  for (i=0; i<x.nobs(); i++) v(i) = y[i];

  fvec u(x.nobs());
  for (i=0; i<x.nobs(); i++) u(i) = w[i];
  
  bool ok = spl->fit(x.vect(), v, u); 

  if (!ok) {
    delete spl;
    return 0;
  }

  B_spline *bs = (B_spline*)spl;
  //cout << bs->coef();
  
  return spl;
}

void splits::destroy_spline(Spline* spl)
{
  delete spl;
  spl=NULL;
}

void splits::evaluate(Spline* spl, const Domain& x, float* y)
{
  fvec yt = spl->eval(x.vect());

  int i;
  for (i=0; i<yt.n_elem; i++) y[i] = yt(i);
}

void splits::derivative(Spline* spl, const Domain& x, int d, float* y)
{
  fvec yt = spl->eval(x.vect(), d);

  int i;
  for (i=0; i<yt.n_elem; i++) y[i] = yt(i);
}

double splits::mean_sq_error(Spline* spl, const Domain& x, float* y)
{
  fvec yt = spl->eval(x.vect());

  fvec y0(x.nobs());
  
  int i;
  for (i=0; i<y0.n_elem; i++) y0(i) = y[i];

  B_spline *bspl = (B_spline*)bspl;
  
  return mse(yt, y0, bspl->dim_basis());
}

double splits::weighted_mean_sq_error(Spline* spl, const Domain& x, float* y,
				      float* w)
{
  fvec yt = spl->eval(x.vect());

  fvec y0(x.nobs()), wt(x.nobs());
  
  int i;
  for (i=0; i<y0.n_elem; i++) {
    y0(i) = y[i];
    wt(i) = w[i];
  }

  B_spline *bspl = (B_spline*)bspl;
  
  return mse(yt, y0, wt, bspl->dim_basis());
}

map<int, Pheno_set> splits::phenology(Spline* spl, const Domain& x,
				      bool southern, float fraction,
				      int method)
{
  vector<Season> seas = create_skeleton(x.begin(),
					x.end(),
					southern,
					0,
					1);

  int nseas = seas.size();

  int nband;
  if (fraction<(float)0) nband = nseas*npar; else nband = nseas*npar_g;
  
  float *buf = new float[nband];

  float period = (float)(x.end()-x.begin());
  
  vector<Season>::iterator it;
  for (it=seas.begin(); it!=seas.end(); ++it) it->reset();

  identify_seasons(seas,spl,1);

  switch (method) {
  case PHENOLOGY_RAW:
    extract_phenology_raw(seas,spl,buf,fraction);
    break;
  case PHENOLOGY_INDEX:
    extract_phenology_index(seas,spl,buf,period,fraction);
    break;
  case PHENOLOGY_DOY:
  default:
    extract_phenology(seas,spl,buf,period,fraction,x.begin()); 
  }

  map<int, Pheno_set> res;
  
  int k;
  for (it=seas.begin(); it!=seas.end(); ++it) {
    k = 0;
    Pheno_set set;

    set.doy_early_min = buf[k++];
    set.doy_early_flex = buf[k++];
    set.doy_peak = buf[k++];
    set.doy_late_flex = buf[k++];
    set.doy_late_min = buf[k++];
    set.early_min_val = buf[k++];
    set.early_flex_val = buf[k++];
    set.peak_val = buf[k++];
    set.late_flex_val = buf[k++];
    set.late_min_val = buf[k++];
    set.min_min_duration = buf[k++];
    set.amplitude = buf[k++];
    set.latent_val = buf[k++];
    set.min_min_integral = buf[k++];
    set.latent_integral = buf[k++];
    set.total_integral = buf[k++];
    set.early_flex_rate = buf[k++];
    set.late_flex_rate = buf[k++];
    
    if (fraction>=(float)0) {
      set.doy_start_green = buf[k++];
      set.doy_end_green = buf[k++];
      set.start_green_val = buf[k++];
      set.end_green_val = buf[k++];
      set.green_duration = buf[k++];
      set.green_integral = buf[k++];
      set.greenup_rate = buf[k++];
      set.senescence_rate = buf[k++];
    } else {
      set.doy_start_green = 0.0f; k++; // edit DF 11.10.2019
      set.doy_end_green = 0.0f;   k++; // edit DF 11.10.2019
      set.start_green_val = 0.0f; k++; // edit DF 11.10.2019
      set.end_green_val = 0.0f;   k++; // edit DF 11.10.2019
      set.green_duration = 0.0f;  k++; // edit DF 11.10.2019
      set.green_integral = 0.0f;  k++; // edit DF 11.10.2019
      set.greenup_rate = 0.0f;    k++; // edit DF 11.10.2019
      set.senescence_rate = 0.0f; k++; // edit DF 11.10.2019
    }

    res[it->startDate.year] = set;
  }
  
  delete [] buf;
  
  return res;
}
