/*
 clargp.h

 A POSIX compatible parser for command line arguments that follows the
 utility syntax guidelines of IEEE standard 1003.1, see

 http://pubs.opengroup.org/onlinepubs/009696899/basedefs/xbd_chap12.html

 for more details.

 Copyright (c) 2008, Sebastian Mader
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef CLARGP_H
#define CLARGP_H

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum ClArgType {
  cl_arg_last = -1,
  cl_arg_pos,
  cl_arg_flag,
  cl_arg_string,
  cl_arg_int,
  cl_arg_real,
  cl_arg_strlist,
  cl_arg_intlist,
  cl_arg_reallist,
  cl_arg_sequence
} ClArgType_t;

typedef struct ClArg {
  ClArgType_t type;
  const char* name;
  void* value;
  int size;
  int mdty;
} ClArg_t;

void parse_cl_args(int argc, char* argv[], ClArg_t* cl_arg);

void* get_cl_arg(ClArg_t* arg, const char* name, int* size);

#define last_arg                  {cl_arg_last,NULL,NULL,0,0}
#define pos_arg(_name)            {cl_arg_pos,_name,NULL,0,1}
#define trailing_arg(_name)       {cl_arg_pos,_name,NULL,0,0}
#define flag_arg(_name,_mdty)     {cl_arg_flag,_name,NULL,0,(_mdty)}
#define string_arg(_name,_mdty)   {cl_arg_string,_name,NULL,0,(_mdty)}
#define int_arg(_name,_mdty)      {cl_arg_int,_name,NULL,0,(_mdty)}
#define real_arg(_name,_mdty)     {cl_arg_real,_name,NULL,0,(_mdty)}
#define strlist_arg(_name,_mdty)  {cl_arg_strlist,_name,NULL,0,(_mdty)}
#define intlist_arg(_name,_mdty)  {cl_arg_intlist,_name,NULL,0,(_mdty)}
#define reallist_arg(_name,_mdty) {cl_arg_reallist,_name,NULL,0,(_mdty)}
#define sequence_arg(_name,_mdty) {cl_arg_sequence,_name,NULL,0,(_mdty)}

#ifdef __cplusplus
}
#endif

#endif
