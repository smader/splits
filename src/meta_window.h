/*
 meta_window.h

 FLTK dialog to handle metaheaders in splview.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_META_WINDOW_H
#define SPLITS_META_WINDOW_H

#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Return_Button.H>
#include "dataset_input.h"

namespace splits {

class Meta_window : public Fl_Window
{
public:
  Dataset_input *meta_inp, *outp_inp;

private:
  Fl_Button *cancel_btn;
  Fl_Return_Button *ok_btn;

  bool ok;

public:
  Meta_window();
  ~Meta_window() {}

  bool okay() {return ok;}
  void set_okay() {ok=true;}

  static void ok_cb(Fl_Widget* w, void* data);
  static void cancel_cb(Fl_Widget* w, void* data);
  static void meta_cb(Fl_Widget* w, void* data);
};

} //namespace splits

#endif
