/*
 toolbar.h

 An FLTK toolbar widget for the splview program.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_TOOLBAR_H
#define SPLITS_TOOLBAR_H

#include <FL/Fl_Pack.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Toggle_Button.H>
#include <FL/Fl_Repeat_Button.H>
#include "date_input.h"
#include "goto_window.h"


namespace splits {

class Main_window;

class Toolbar : public Fl_Pack
{
  Fl_Toggle_Button *point_btn, *zoom_btn, *pan_btn;
  Fl_Repeat_Button *zoom_in_btn, *zoom_out_btn;
  Fl_Button *full_ext_btn;
  Fl_Button *goto_btn;
  Fl_Button *calendar_btn;
  Fl_Button *contrast_btn;
  Main_window *mw;

public:
  Goto_window *gw;

public:
  Toolbar(int X, int Y, int W, int H, Main_window* MW);
  ~Toolbar();

  Main_window* main_window() {return mw;}

  static void point_cb(Fl_Widget* w, void* data);
  static void zoom_cb(Fl_Widget* w, void* data);
  static void pan_cb(Fl_Widget* w, void* data);
  static void zoom_in_cb(Fl_Widget* w, void* data);
  static void zoom_out_cb(Fl_Widget* w, void* data);
  static void full_ext_cb(Fl_Widget* w, void* data);
  static void contrast_cb(Fl_Widget* w, void* data);
  static void contrast_slider_cb(Fl_Widget* w, void* data);
  static void calendar_cb(Fl_Widget* w, void* data);
  static void goto_cb(Fl_Widget* w, void* data);
};

} //namespace splits

#endif
