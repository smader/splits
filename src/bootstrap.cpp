/*
 bootstrap.cpp

 Bootstrap methods to select spline parameters.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cfloat>
#include "knot.h"
#include "b_spline.h"
#include "bootstrap.h"

using namespace std;
using namespace arma;
using namespace splits;

double splits::mse(const fvec& y0, const fvec& y1, int dim_basis)
{
  double sum = 0.0;

  int n;
  if (y1.n_elem<y0.n_elem) n=y1.n_elem; else n=y0.n_elem;
  
  int i;
  for (i=0; i<n; i++) {
    double err = (double)(y1[i]-y0[i]);
    sum += err*err;
  }
  
  return sum/(double)(n-dim_basis);
}

double splits::mse(const fvec& y0, const fvec& y1, const fvec& w, int dim_basis)
{
  double sum = 0.0;

  int n;
  if (y1.n_elem<y0.n_elem) n=y1.n_elem; else n=y0.n_elem;
  
  int i;
  for (i=0; i<n; i++) {
    double err = (double)((y1[i]-y0[i])*w[i]);
    sum += err*err;
  }
  
  return sum/(double)(n-dim_basis);
}

float splits::rss(const fvec& y0, const fvec& y1)
{
  if (y0.n_elem!=y1.n_elem) return -1;

  float s = 0;

  int i, n = y0.n_elem;
  for (i=0; i<n; i++) {
    float d = y1[i]-y0[i];
    s += d*d;
  }

  return s;
}

float splits::rss(const fvec& y0, const fvec& y1, const fvec& w)
{
  if (y0.n_elem!=y1.n_elem || y0.n_elem!=w.n_elem || y1.n_elem!=w.n_elem)
    return -1;

  float s  = 0;

  int i, n = y0.n_elem;
  for (i=0; i<n; i++) {
    float d = y1[i]-y0[i];
    s += d*d*w[i];
  }

  return s;
}


float splits::cv(const fmat& x, const fvec& y0, const fvec& y1)
{
  fmat h, xt = trans(x);
  try {
    h = x*inv(xt*x)*xt;
  } catch (...) {
    return -1;
  }
  float n = (float)y1.n_elem;
  float r = rss(y0,y1);
  float t = n-trace(h);
  return n*r/(t*t);
}

float splits::cv(const fmat& x, const fvec& y0, const fvec& y1, int k)
{
  fmat h, xt = trans(x);
  try {
    h = x*inv(xt*x)*xt;
  } catch (...) {
    return -1;
  }
  float n = (float)y1.n_elem;
  float r = rss(y0.subvec(k,n-k-1),y1.subvec(k,n-k-1));
  float t = n-2*k-trace(h.submat(k,k,n-k-1,n-k-1));
  return (n-2*k)*r/(t*t);
}

float splits::cv(const fmat& x, const fvec& y0, const fvec& y1,
		 const fmat& w)
{
  fmat h, xtw = trans(x)*w;
  try {
    h = x*inv(xtw*x)*xtw;
  } catch (...) {
    return -1;
  }
  float n = (float)y1.n_elem;
  float r = rss(y0,y1);
  float t = n-trace(h);
  return n*r/(t*t);
}

float splits::cv(const fmat& x, const fvec& y0, const fvec& y1,
		 const fmat& w, int k)
{
  fmat h, xtw = trans(x)*w;
  try {
    h = x*inv(xtw*x)*xtw;
  } catch (...) {
    return -1;
  }
  float n = (float)y1.n_elem;
  float r = rss(y0.subvec(k,n-k-1),y1.subvec(k,n-k-1));
  float t = n-2*k-trace(h);
  return (n-2*k)*r/(t*t);
}


int splits::select_num_pieces(int l, int r, int s,
			      float a, float b,
			      int d, bool peri,
			      const fvec& xt, const fvec& yt,
			      bool ignore,
			      int* num, float* score,
			      void (*progress)(float,void*), void* data)
{
  if (progress) progress(0, data);

  int k=0, n;
  for (n=l; n<=r; n+=s) {

    int dim_basis;
    float *knots;
    if (peri) {
      knots = periodic_uniform_knot_vector(a, b, n, d+1, &dim_basis);
    } else {
      knots = clamped_uniform_knot_vector(a, b, n, d+1, &dim_basis);
    }

    B_spline *spl = new B_spline(knots, dim_basis, d+1, peri);

    float s;
    if (spl->fit(xt, yt)) {
      if (ignore)
	s = cv(spl->basis(xt), spl->eval(xt), yt, spl->order());
      else
	s = cv(spl->basis(xt), spl->eval(xt), yt);
    } else {
      s = -1.0f;
    }

    if (s>=0.0f) {
      num[k] = n;
      score[k] = s;
      k++;
    }

    if (progress) progress((float)n/(float)(r-l+1), data);

    delete spl;
    delete [] knots;
  }

  if (progress) progress(100, data);

  return k;
}

int splits::select_num_pieces(int l, int r, int s,
			      float a, float b,
			      int d, bool peri,
			      const fvec& xt, const fvec& yt,
			      const fmat& w,
			      bool ignore,
			      int* num, float* score,
			      void (*progress)(float,void*), void* data)
{
  if (progress) progress(0, data);

  int k=0, n;
  for (n=l; n<=r; n+=s) {

    int dim_basis;
    float *knots;
    if (peri) {
      knots = periodic_uniform_knot_vector(a, b, n, d+1, &dim_basis);
    } else {
      knots = clamped_uniform_knot_vector(a, b, n, d+1, &dim_basis);
    }

    B_spline *spl = new B_spline(knots, dim_basis, d+1, peri);

    float s;
    if (spl->fit(xt, yt, w)) {
      if (ignore)
	s = cv(spl->basis(xt), spl->eval(xt), yt, w, spl->order());
      else
	s = cv(spl->basis(xt), spl->eval(xt), yt, w);
    } else {
      s = -1.0f;
    }

    if (s>=0.0f) {
      num[k] = n;
      score[k] = s;
      k++;
    }

    if (progress) progress((float)n/(float)(r-l+1), data);

    delete spl;
    delete [] knots;
  }

  if (progress) progress(100, data);

  return k;
}


float splits::pieces(float n, void* p)
{
  pieces_data_t *o = (pieces_data_t*)p;

  int dim_basis;
  float *knots;

  if (o->peri) {
    knots = periodic_uniform_knot_vector(o->a, o->b, (int)(n+.5), o->d+1,
					 &dim_basis);
  } else {
    knots = clamped_uniform_knot_vector(o->a, o->b, (int)(n+.5), o->d+1,
					&dim_basis);
  }

  B_spline *spl = new B_spline(knots, dim_basis, o->d+1, o->peri);

  float s;
  if (o->wt.is_empty()) {
    if (spl->fit(o->xt, o->yt)) {
      if (o->ignore)
	s = cv(spl->basis(o->xt), spl->eval(o->xt), o->yt, spl->order());
      else
	s = cv(spl->basis(o->xt), spl->eval(o->xt), o->yt);
    } else {
      s = FLT_MAX;
    }
  } else {
    if (spl->fit(o->xt, o->yt, o->wt)) {
      if (o->ignore)
	s = cv(spl->basis(o->xt), spl->eval(o->xt), o->yt, o->wt, spl->order());
      else
	s = cv(spl->basis(o->xt), spl->eval(o->xt), o->yt, o->wt);
    } else {
      s = FLT_MAX;
    }
  }

  delete spl;
  delete[] knots;

  return s;
}


int splits::min_index(float* arr, int size)
{
  int i, ii=0;
  float tmp=arr[0];
  for (i=1; i<size; i++) {
    if (arr[i]<tmp) {
      tmp=arr[i];
      ii=i;
    }
  }
  return ii;
}
