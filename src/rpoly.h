/*      rpoly.h -- Jenkins-Traub real polynomial root finder.
 *
 *      (C) 2002, C. Bond.  All rights reserved.
 *
 *      Translation of TOMS493 from FORTRAN to C. This
 *      implementation of Jenkins-Traub partially adapts
 *      the original code to a C environment by restruction
 *      many of the 'goto' controls to better fit a block
 *      structured form. It also eliminates the global memory
 *      allocation in favor of local, dynamic memory management.
 *
 *      The calling conventions are slightly modified to return
 *      the number of roots found as the function value.
 *
 *      INPUT:
 *      op - double precision vector of coefficients in order of
 *              decreasing powers.
 *      degree - integer degree of polynomial
 *
 *      OUTPUT:
 *      zeror,zeroi - output double precision vectors of the
 *              real and imaginary parts of the zeros.
 *
 *      RETURN:
 *      returnval:   -1 if leading coefficient is zero, otherwise
 *                  number of roots found. 
 */

#ifndef RPOLY_H
#define RPOLY_H

class Rpoly
{
  double *p,*qp,*k,*qk,*svk;
  double sr,si,u,v,a,b,c,d,a1,a2;
  double a3,a6,a7,e,f,g,h,szr,szi,lzr,lzi;
  double eta,are,mre;
  int n,nn,nmi,zerok;
  int itercnt;

  void quad(double a,double b1,double c,double *sr,double *si,
	    double *lr,double *li);
  void fxshfr(int l2, int *nz);
  void quadit(double *uu,double *vv,int *nz);
  void realit(double sss, int *nz, int *iflag);
  void calcsc(int *type);
  void nextk(int *type);
  void newest(int type,double *uu,double *vv);
  void quadsd(int n,double *u,double *v,double *p,double *q,
	      double *a,double *b);

public:
  int find(double *op, int degree, double *zeror, double *zeroi);
};

#endif
