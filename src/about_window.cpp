/*
 about_window.cpp

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2020  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <FL/Fl_Browser.H>
#include "about_window.h"

using namespace splits;

About_window::About_window(int X, int Y) : Fl_Window(X,Y,512,300,"About")
{
  Fl_Browser *b = new Fl_Browser(0,0,512,300);
  b->add("Copyright (C) 2010-2020  Sebastian Mader");
  b->add("");
  b->add("`splview' is free software: you can redistribute it and/or modify");
  b->add("it under the terms of the GNU General Public License as published by");
  b->add("the Free Software Foundation, either version 3 of the License, or");
  b->add("(at your option) any later version.");
  b->add("");
  b->add("`splview' is distributed in the hope that it will be useful,");
  b->add("but WITHOUT ANY WARRANTY; without even the implied warranty of");
  b->add("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the");
  b->add("GNU General Public License for more details.");
  b->add("");
  b->add("You should have received a copy of the GNU General Public License");
  b->add("along with `splview'. If not, see <http://www.gnu.org/licenses/>.");
  b->add("");
  b->add("`splview' is based in part on the work of");
  b->add("the FLTK project (http://www.fltk.org).");
  resizable(b);
  set_modal();
}
