/*
 splopt.cpp

 Program to optimize spline parameters.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2017  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include <cstdio>
#include <cmath>
#define ARMA_NO_DEBUG
#include <armadillo>
#include "clargp.h"
#include "archive.h"
#include "arbitrary_vector.h"
#include "date.h"
#include "weight.h"
#include "common.h"
#include "knot.h"
#include "b_spline.h"
#include "bootstrap.h"

using namespace arma;
using namespace splits;

const char *in_file, *ot_file, *x_file, *w_file=NULL, *m_file, *v_file=NULL;
float in_nodata = 0.0f;
float in_offset = 0.0f, in_gain = 1.0f;
int degree, dim_basis, *nspan, nspan_siz;
bool peri = false, verbose = true, root = false;
Arbitrary_vector<float> w_tab;
Date d_beg, d_end;

ClArg_t arglst[] =
{
  string_arg("a",1),
  strlist_arg("C",0),
  int_arg("d",1),
  sequence_arg("k",1),
  reallist_arg("l",0),
  string_arg("m",1),
  real_arg("n",0),
  flag_arg("p",0),
  flag_arg("Qq",0),
  flag_arg("r",0),
  string_arg("v",0),
  strlist_arg("W",0),
  pos_arg("in_file"),
  pos_arg("out_file"),
  last_arg
};

double var(fvec x)
{
  int n = x.n_elem;

  double sum=0, mu;
  
  int i;
  for (i=0; i<n; i++) sum+=x[i];
  mu = sum/(double)n;

  sum = 0;
  for (i=0; i<n; i++) sum += (x[i]-mu)*(x[i]-mu);

  return sum/(double)(n-1);
}

int main(int argc, char* argv[])
{
  GDALAllRegister();
  
  parse_cl_args(argc, argv, arglst);

  int argsiz;
  void *parg;

  x_file = (const char*)get_cl_arg(arglst, "a", &argsiz);

  parg = get_cl_arg(arglst, "C", &argsiz);
  if (parg) {
    if (argsiz!=2) bail_out("Argument -C is invalid! Exit.");
    char **papch = (char**)parg;
    d_beg = Date(papch[0]);
    d_end = Date(papch[1]);
  }
  
  degree = *(int*)get_cl_arg(arglst, "d", &argsiz);

  parg = get_cl_arg(arglst, "l", &argsiz);
  if (argsiz==2) {
    in_offset = ((float*)parg)[0];
    in_gain   = ((float*)parg)[1];
  }
  
  m_file = (const char*)get_cl_arg(arglst, "m", &argsiz);
  
  nspan = (int*)get_cl_arg(arglst, "k", &nspan_siz);

  get_cl_arg(arglst, "p", &argsiz);
  if (argsiz > 0) peri = true;

  get_cl_arg(arglst, "Qq", &argsiz);
  if (argsiz > 0) verbose = false;

  get_cl_arg(arglst, "r", &argsiz);
  if (argsiz > 0) root = true;

  parg = get_cl_arg(arglst, "v", &argsiz);
  if (parg) v_file = (const char*)parg;
  
  parg = get_cl_arg(arglst, "W", &argsiz);
  if (parg) {
    if (argsiz == 2) {
      char **papch = (char**)parg;
      w_file = papch[0];
      w_tab = read_weight_file(papch[1]);
      if (w_tab.size()==0) bail_out("Unable to read table of weights! Exit.");
    } else {
      bail_out("Argument -W is invalid! Exit.");
    }
  }
  
  in_file = (const char*)get_cl_arg(arglst, "in_file", &argsiz);
  ot_file = (const char*)get_cl_arg(arglst, "out_file", &argsiz);

  Archive ar(in_file, x_file, w_file);

  ar.set_offset(in_offset);
  ar.set_gain(in_gain);

  ar.set_nodata(in_nodata);

  ar.set_begin(d_beg);
  ar.set_end(d_end);
  
  if (w_file) ar.set_weight_table(w_tab);

  if (!ar.open()) bail_out("Error loading input! Exit.");
  
  FILE *fp;
  if ((fp=fopen(m_file,"rt"))==NULL) {
    bail_out ("Unable to open map input file (-m)! Exit.");
  }

  int c, r, n=0;
  while (fscanf(fp, "%d %d", &c, &r)==2) n+=1;
  rewind(fp);

  fmat err(nspan_siz,n);
  err.fill(0);

  Col<int> df(nspan_siz);
  df.fill(0);

  int i, k;
  for (i=0; i<n; i++) {

    fscanf(fp, "%d %d", &c, &r);

    fvec xt, yt, wt;
    ar.poke(c, r, xt, yt, wt, d_beg, d_end);

    //v[i] = var(yt);
    
    for (k=0; k<nspan_siz; k++) {

      float *knots;
      if (peri)
	knots = periodic_uniform_knot_vector(xt[0], xt[xt.n_elem-1], nspan[k],
					     degree, &dim_basis);
      else
	knots = clamped_uniform_knot_vector(xt[0], xt[xt.n_elem-1], nspan[k],
					    degree, &dim_basis);

      B_spline *spline = new B_spline(knots,dim_basis,degree+1,peri);

      bool ok;
      if (wt.is_empty())
	ok = spline->fit(xt, yt);
      else
	ok = spline->fit(xt, yt, wt);

      if (ok) {
	fvec ys = spline->eval(xt);

	double e;
	if (wt.is_empty())
	  e = mse(ys, yt, dim_basis);
	else
	  e = mse(ys, yt, wt, dim_basis);

	if (root)
	  err(k,i) = sqrt(e);
	else
	  err(k,i) = e;
      }

      df(k) = yt.n_elem-dim_basis;
    }

    if (verbose) report_progress(1, n);
  }

  ar.close();
  
  fclose(fp);
  
  fp = fopen(ot_file, "wt");
  
  for (k=0; k<nspan_siz; k++) {
    fprintf(fp, "%d %d", nspan[k], df(k));
    for (i=0; i<n; i++) {
      fprintf(fp, " %f", err(k,i));
    }
    fprintf(fp, "\n");
  }
  
  fclose(fp);
  
  return 0;
}
