/*
 common.cpp

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <cfloat>
#include "common.h"

using namespace arma;
using namespace splits;

void splits::bail_out(const char* format, ...)
{
  va_list a_list;
  va_start(a_list, format);
  vfprintf(stderr, format, a_list);
  fprintf(stderr, "\n");
  exit(1);
}

void splits::report_progress(int incr, int total)
{
  static int progr = 0;
  progr += incr;
  float perc = (float)progr/(float)total * 100.0f;
  if (perc<=100.0f) {
    fprintf(stdout, "\r%.1f%%", perc);
    fflush(stdout);
  }
  if (perc>=100.0f) fprintf(stdout, "\r100.0%%\n");
}

int splits::scan_nodata(std::vector<Date>& timetab,
			fvec& yt,
			fmat& wt,
			float nodata)
{
  int i, n=0, nn=0;
  for (i=0; i<yt.n_elem; i++)
    if (fabsf(yt[i]-nodata)>=FLT_EPSILON) nn+=1;
  if (nn==yt.n_elem) return nn;
  if (nn==0) {
    yt = fvec();
    wt = fmat();
    return 0;
  }
  std::vector<Date> t(nn);
  fvec y(nn);
  fmat w;
  if (!wt.is_empty()) {
    w = fmat(nn,nn);
    w.fill(0);
  }
  for (i=0; i<yt.n_elem; i++) {
    if (fabsf(yt[i]-nodata)>=FLT_EPSILON) {
      t[n] = timetab[i];
      y[n] = yt[i];
      if (!wt.is_empty()) w(n,n) = wt(i,i);
      n+=1;
    }
  }
  timetab = t;
  yt = y;
  if (!wt.is_empty()) wt = w;
  return nn;
}

fvec splits::load_reals(const char* fname)
{
  FILE *fp;

  fp = fopen(fname, "rt");

  if (!fp) return fvec();

  int n=0;
  float tmp;
  while (fscanf(fp, "%f", &tmp)==1) n+=1;

  fvec v(n);

  rewind(fp);

  int i;
  for (i=0; i<n; i++) {
    fscanf(fp, "%f", &tmp);
    v[i] = tmp;
  }

  fclose(fp);

  return v;
}
