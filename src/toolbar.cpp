/*
 toolbar.cpp

 An FLTK toolbar widget for the splview program.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <string>
#include <FL/Fl_Pixmap.H>
#include <FL/Fl_Slider.H>
#include <FL/Fl_Value_Output.H>
#include "main_window.h"
#include "toolbar.h"
#include "icons.h"
#include "time_axis.h"

using namespace splits;

Toolbar::Toolbar(int X, int Y, int W, int H, Main_window* MW)
  : Fl_Pack(X,Y,W,H), mw(MW)
{
  type(Fl_Pack::HORIZONTAL);
  box(FL_NO_BOX);
  spacing(2);

  point_btn = new Fl_Toggle_Button(0,0,30,30);
  point_btn->clear_visible_focus();
  point_btn->tooltip("Point to Data");
  Fl_Pixmap *point_pixmap = new Fl_Pixmap(point);
  point_btn->image(point_pixmap);
  point_btn->type(FL_RADIO_BUTTON);
  point_btn->callback(point_cb);

  zoom_btn = new Fl_Toggle_Button(0,0,30,30);
  zoom_btn->clear_visible_focus();
  zoom_btn->tooltip("Zoom");
  Fl_Pixmap *zoom_pixmap = new Fl_Pixmap(zoom);
  zoom_btn->image(zoom_pixmap);
  zoom_btn->type(FL_RADIO_BUTTON);
  zoom_btn->callback(zoom_cb);

  zoom_in_btn = new Fl_Repeat_Button(0,0,30,30);
  zoom_in_btn->clear_visible_focus();
  zoom_in_btn->tooltip("Zoom In");
  Fl_Pixmap *zoom_in_pixmap = new Fl_Pixmap(zoom_in);
  zoom_in_btn->image(zoom_in_pixmap);
  zoom_in_btn->callback(zoom_in_cb);

  zoom_out_btn = new Fl_Repeat_Button(0,0,30,30);
  zoom_out_btn->clear_visible_focus();
  zoom_out_btn->tooltip("Zoom Out");
  Fl_Pixmap *zoom_out_pixmap = new Fl_Pixmap(zoom_out);
  zoom_out_btn->image(zoom_out_pixmap);
  zoom_out_btn->callback(zoom_out_cb);

  pan_btn = new Fl_Toggle_Button(0,0,30,30);
  pan_btn->clear_visible_focus();
  pan_btn->tooltip("Pan");
  Fl_Pixmap *pan_pixmap = new Fl_Pixmap(pan);
  pan_btn->image(pan_pixmap);
  pan_btn->type(FL_RADIO_BUTTON);
  pan_btn->callback(pan_cb);

  full_ext_btn = new Fl_Button(0,0,30,30);
  full_ext_btn->clear_visible_focus();
  full_ext_btn->tooltip("Zoom to Full Extent");
  Fl_Pixmap *full_ext_pixmap = new Fl_Pixmap(full_ext);
  full_ext_btn->image(full_ext_pixmap);
  full_ext_btn->callback(full_ext_cb);

  goto_btn = new Fl_Button(0,0,30,30);
  goto_btn->clear_visible_focus();
  goto_btn->tooltip("Go to Data Point...");
  Fl_Pixmap *goto_pixmap = new Fl_Pixmap(goto_xpm);
  goto_btn->image(goto_pixmap);
  goto_btn->callback(goto_cb);

  calendar_btn = new Fl_Button(0,0,30,30);
  calendar_btn->clear_visible_focus();
  calendar_btn->tooltip("Select Calendar Date for Display...");
  Fl_Pixmap *calendar_pixmap = new Fl_Pixmap(calendar);
  calendar_btn->image(calendar_pixmap);
  calendar_btn->callback(calendar_cb, (void*)mw);

  contrast_btn = new Fl_Button(0,0,30,30);
  contrast_btn->clear_visible_focus();
  contrast_btn->tooltip("Set Display Contrast...");
  Fl_Pixmap *contrast_pixmap = new Fl_Pixmap(contrast);
  contrast_btn->image(contrast_pixmap);
  contrast_btn->callback(contrast_cb, (void*)mw);

  end();

  point_btn->setonly();

  gw = new Goto_window(mw);
}

Toolbar::~Toolbar()
{
  delete gw;
}

void Toolbar::point_cb(Fl_Widget* w, void* data)
{
  Fl_Toggle_Button *btn = (Fl_Toggle_Button*)w;
  btn->setonly();
  Toolbar *tb = (Toolbar*)w->parent();
  tb->main_window()->mon->set_mode(Monitor::POINT);
}

void Toolbar::zoom_cb(Fl_Widget* w, void* data)
{
  Fl_Toggle_Button *btn = (Fl_Toggle_Button*)w;
  btn->setonly();
  Toolbar *tb = (Toolbar*)w->parent();
  tb->main_window()->mon->set_mode(Monitor::ZOOM);
}

void Toolbar::pan_cb(Fl_Widget* w, void* data)
{
  Fl_Toggle_Button *btn = (Fl_Toggle_Button*)w;
  btn->setonly();
  Toolbar *tb = (Toolbar*)w->parent();
  tb->main_window()->mon->set_mode(Monitor::PAN);
}

void Toolbar::zoom_in_cb(Fl_Widget* w, void* data)
{
  Toolbar *tb = (Toolbar*)w->parent();
  tb->main_window()->mon->zoom();
}

void Toolbar::zoom_out_cb(Fl_Widget* w, void* data)
{
  Toolbar *tb = (Toolbar*)w->parent();
  tb->main_window()->mon->zoom(false);
}

void Toolbar::full_ext_cb(Fl_Widget* w, void* data)
{
  Toolbar *tb = (Toolbar*)w->parent();
  tb->main_window()->mon->set_viewport();
}

void Toolbar::contrast_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)data;
  float clip = mainwin->mon->percent_clip();
  Fl_Window win(200,25,"Clip (%)");
  Fl_Value_Output *o = new Fl_Value_Output(0,0,60,25);
  o->value(clip);
  Fl_Slider *s = new Fl_Slider(60,0,140,25);
  s->type(FL_HOR_NICE_SLIDER);
  s->minimum(0); s->maximum(50);
  s->value(clip);
  s->callback(Toolbar::contrast_slider_cb,(void*)o);
  win.resizable(s);
  win.size_range(200,25,0,25);
  win.hotspot(&win);
  win.set_modal();
  win.show();
  while (win.visible()) {
    Fl::wait();
  }
  mainwin->mon->set_percent_clip(s->value());
  mainwin->mon->display();
}

void Toolbar::contrast_slider_cb(Fl_Widget* w, void* data)
{
  float v = ((Fl_Slider*)w)->value();
  ((Fl_Value_Output*)data)->value(v);
}

void Toolbar::calendar_cb(Fl_Widget* w, void* data)
{
  Main_window *mainwin = (Main_window*)data;
  if (mainwin->current<0) return;
  Fl_Window win(200,35, "Date on Display");
  Date_input *inp = new Date_input(5,5,190,25);
  win.resizable(inp);
  win.size_range(200,35,0,35);
  Date d;
  std::vector<Date> ttab = mainwin->data[mainwin->current].dates();
  if (ttab.empty()) {
    d = convert_band_to_date(mainwin->band[mainwin->current],
			     mainwin->data[mainwin->current].begin(),
			     mainwin->data[mainwin->current].end(),
			     mainwin->data[mainwin->current].count());
  } else {
    d = ttab[mainwin->band[mainwin->current]];
  }
  inp->set_date(d);
  win.hotspot(&win);
  win.set_modal();
  win.show();
  while (win.visible()) {
    Fl::wait();
  }
  d = inp->date();
  if (!d) return;
  int b;
  if (ttab.empty()) {
    b = convert_date_to_band(d,
			     mainwin->data[mainwin->current].begin(),
			     mainwin->data[mainwin->current].end(),
			     mainwin->data[mainwin->current].count());
  } else {
    b = -1;
    int i;
    for (i=1; i<ttab.size(); i++) {
      if (d>=ttab[i-1] && d<=ttab[i]) {
	if (d==ttab[i-1]) {
	  d = ttab[i-1];
	  b = i;
	} else if (d==ttab[i]) {
	  d = ttab[i];
	  b = i+1;
	} else {
	  if (ttab[i]-d < d-ttab[i-1]) {
	    d = ttab[i];
	    b = i+1;
	  } else {
	    d = ttab[i-1];
	    b = i;
	  }
	}
      }
    }
    if (b==-1) return;
  }
  mainwin->mon->display(mainwin->data[mainwin->current],
			b,
			false);
  mainwin->band[mainwin->current] = b;
}

void Toolbar::goto_cb(Fl_Widget* w, void* data)
{
  Toolbar *tb = (Toolbar*)w->parent();
  tb->gw->hotspot(tb->gw);
  tb->gw->show();
}
