/*
 terminal.cpp

 FLTK terminal widget based on a code snippet from Erco's FLTK cheat page
 (http://seriss.com/people/erco/fltk/#SimpleTerminal).

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <iostream>
#include <cstdio>
#include "thread.h"
#include "terminal.h"

#ifdef _WIN32
#define popen _popen
#define pclose _pclose
#endif

using namespace splits;

Terminal::Terminal(int X, int Y, int W, int H, const char* command)
  : Fl_Text_Editor(X,Y,W,H), fail(false), runn(false)
{
  buff = new Fl_Text_Buffer();
  buffer(buff);
  textfont(FL_COURIER);
  textsize(14);
  cmd = command;
  cmd += " ";
  append(cmd.c_str());
  args = "";
  tooltip("Press 'Enter' to execute command");
}

void Terminal::append(char ch)
{
  if (ch=='\r') {
    buffer()->remove(buffer()->line_start(buffer()->length()),
		     buffer()->length());
  } else {
    char str[2];
    sprintf(str, "%c", ch);
    append(str);
  }
}

// Append to buffer, keep cursor at end
void Terminal::append(const char* s)
{
  buff->append(s);
  // Go to end of line
  insert_position(buffer()->length());
  scroll(count_lines(0, buffer()->length(), 1), 0);
  linepos = buffer()->line_start(buffer()->length())+cmd.length();
}

void Terminal::set_args(const char* s)
{
  if (is_busy()) return;
  int len = buffer()->length();
  buffer()->remove(len-args.length(), len);
  append(s);
  args = s;
  insert_position(buffer()->length());
  scroll(count_lines(0, buffer()->length(), 1), 0);
}

void* Terminal::runner(void* data)
{
  Terminal *t = (Terminal*)data;
  t->set_busy();
  std::string c = t->cmd + t->args + " 2>&1";     // stderr + stdout
  FILE *fp = popen(c.c_str(), "r");
  if ( fp == 0 ) {
    t->fail = true;
  } else {
    char ch;
    while ( (ch=fgetc(fp)) != EOF ) {
      Fl::lock();
      t->append(ch);
      Fl::unlock();
      Fl::awake((void*)0);
    }
    pclose(fp);
    t->fail = false;
  }
  Fl::lock();
  t->append("\n");
  t->append(t->cmd.c_str());
  t->tooltip("Press 'Enter' to execute command");
  Fl::unlock();
  t->args = "";
  t->set_busy(false);
  return NULL;
}

// Run the specified command in the shell, append output to terminal
void Terminal::run()
{
  tooltip("Command is currently running...");
  append("\n");
  Thread thread0;
  create_thread(thread0, runner, this);
}

int Terminal::handle(int e) {
  switch (e) {
  case FL_KEYUP: {
    if (is_busy()) return 1;
    if (insert_position()<linepos) return(1);
    int key = Fl::event_key();
    if ( key == FL_Enter ) return(1);              // hide Enter from editor
    if ( key == FL_BackSpace && cmd[0] == 0 ) return(0);
    break;
  }
  case FL_KEYDOWN: {
    if (is_busy()) return 1;
    if (insert_position()<linepos) return(1);
    int key = Fl::event_key();
    // Enter key? Execute the command, clear command buffer
    if ( key == FL_Enter ) {
      // Execute your commands here
      run();
      return(1);                          // hide 'Enter' from text widget
    }
    if ( key == FL_BackSpace ) {
      if ( args.length()>0 ) {
	int pos = insert_position();
	pos = pos-buffer()->line_start(pos)-cmd.length()-1;
	args.erase(pos,1);
	break;
      } else {
	return(0);
      }
    } else {
      // use Fl::event_text() to handle non-ascii codes
      int pos = insert_position();
      pos = pos-buffer()->line_start(pos)-cmd.length();
      args.insert(pos, Fl::event_text());
    }
    break;
  }
  }
  return(Fl_Text_Editor::handle(e));
}
