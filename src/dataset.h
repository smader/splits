/*
 dataset.h

 A class to handle special header information and meta files used by
 SPLITS programs.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPLITS_DATASET_H
#define SPLITS_DATASET_H

#include <string>
#include <vector>
#include <gdal_priv.h>
#define ARMA_NO_DEBUG
#include <armadillo>
#include "season.h"

namespace splits {

class Dataset
{
  std::string fname;

public:
  Dataset(const char* name) : fname(name) {}
  ~Dataset() {}

  bool is_good();

  std::string header();

  //Write and read integer values to the dataset's header

  bool write_int(const char* key, int value);
  bool read_int(const char* key, int* value);

  //Write and read a set of real numbers (e.g. a vector) to the header

  bool write_real_set(const char* key,const float* set,int n);
  float* read_real_set(const char* key, int* n);
  arma::fvec read_real_set(const char* key);

  void set_band_names(std::vector<Season> seas, int nparam, char* paramNames[]);
  std::vector<std::string> band_names();

  void mk_meta_files(int nparam, char* paramNames[], int nParamSpace);
  void mk_meta_files(std::vector<Season> seas, int nYearSpace);
  void mk_meta_files(const Date& date, int nseas, int nYearSpace);

private:
  Dataset() {}

  void metafile(const char* fn, const char* file, int bands[], int n,
		int dims[]);
};

class Meta_dataset
{
  std::string fname;

public:
 Meta_dataset(const char* name) : fname(name) {}
  ~Meta_dataset() {}

  bool is_good();

  std::string file();

  std::string suggest_file();

  std::vector<int> bands();

  void materialize(const char* name=0);

private:
  Meta_dataset() {}
};

GDALDataType get_data_type(const char* str);

} //namespace splits

#endif
