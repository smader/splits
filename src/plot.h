/*
 plot.h

 FLTK widget for plotting time series.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_PLOT_H
#define SPLITS_PLOT_H

#include <FL/fl_draw.H>
#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/Fl_Menu_Item.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Progress.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Light_Button.H>
#define ARMA_NO_DEBUG
#include <armadillo>
#include "clargp.h"
#include <list>
#include <stdio.h>

namespace splits {

class Plot : public Fl_Widget
{
  struct Object
  {
    arma::fvec x;
    arma::fvec y;
    Fl_Color c;

    Object(arma::fvec x_, arma::fvec y_, Fl_Color c_) : x(x_), y(y_), c(c_) {}
    ~Object() {}
    virtual void draw(int, int, int, double, double, double, double) = 0;
    void save(FILE* fp);
  };

  struct LineObject : public Object
  {
    int lw, style;

    LineObject(arma::fvec x, arma::fvec y, Fl_Color c, int lw_, int style_)
      : Object(x,y,c),lw(lw_),style(style_) {}
    ~LineObject() {}
    void draw(int xx, int yy, int hh, double xmin, double ymin, double sx,
	      double sy);
  };

  struct PointObject : public Object
  {
    int sz, lw;
    bool fill;

    PointObject(arma::fvec x, arma::fvec y, Fl_Color c, int sz_, bool fill_, bool lw_)
      : Object(x,y,c), sz(sz_), fill(fill_), lw(lw_) {}
    ~PointObject() {}
    void draw(int xx, int yy, int hh, double xmin, double ymin, double sx,
	      double sy);
  };

  std::list<Object*> obj;

  arma::fvec vgrd;

  const Fl_Menu_Item* menu;

  bool autoscale;
  double xmin, ymin, xmax, ymax;
  Fl_Color fgcol;

  void draw();

public:
  Plot(int x, int y, int w, int h, const char* l=0, const Fl_Menu_Item* m=0);
  ~Plot();

  int handle(int event);

  void set_autorange(bool a=true);
  bool is_autorange() {return autoscale;}
  void set_range(double xmn, double xmx, double ymn, double ymx);
  void get_range(double* xmn, double* xmx, double* ymn, double* ymx);

  void set_color(int c=3);

  void erase();

  void lines(const arma::fvec& x, const arma::fvec& y, int lw=0,
	     int style=FL_SOLID);
  void points(const arma::fvec& x, const arma::fvec& y, int sz, bool fill=true,
	      int lw=2);

  void gridv(const arma::fvec& x);

  bool save(const char* fname);
};

} //namespace splits

#endif
