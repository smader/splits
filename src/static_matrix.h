/*
 static_matrix.h

 A template class for matrices whose size is known at compile time.
 Provides special templates for the multiplication operator to be
 used in computer graphics applications (i.e. it allows multiplication
 of e.g. a 3x3-matrix with a 2-vector). See also "static_vector.h".

 Copyright (c) 2012 Sebastian Mader
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STATIC_MATRIX_H
#define STATIC_MATRIX_H

#include <ostream>
#include "static_vector.h"

template<class T, int M, int N> class Static_matrix
{
  Static_vector<T,N> el[M];

public:
  Static_matrix() {}
  Static_matrix(const T a[])
  {
    int i, j;
    for (i=0; i<M; i++)
      for (j=0; j<N; j++) el[i][j]=a[i*N+j];
  }
  Static_matrix(const T a[][N])
  {
    int i, j;
    for (i=0; i<M; i++)
      for (j=0; j<N; j++) el[i][j]=a[i][j];
  }
  ~Static_matrix() {}

  Static_vector<T,N>& operator[](int i) {return el[i];}
  const Static_vector<T,N>& operator[](int i) const {return el[i];}

  T& operator()(int i, int j) {return el[i][j];}
  const T& operator()(int i, int j) const {return el[i][j];}

  Static_matrix<T,M,N> operator+()
  {
    Static_matrix<T,M,N> ans;
    int i;
    for (i=0; i<M; i++) ans[i] = +el[i];
    return ans;
  }

  Static_matrix<T,M,N> operator-()
  {
    Static_matrix<T,M,N> ans;
    int i;
    for (i=0; i<M; i++) ans[i] = -el[i];
    return ans;
  }

  void zeros();
  void ones();

  void identity();
};

template<class T, int M, int N> void Static_matrix<T,M,N>::zeros()
{
  int i;
  for (i=0; i<M; i++) {
    el[i].zeros();
  }
}

template<class T, int M, int N> void Static_matrix<T,M,N>::ones()
{
  int i;
  for (i=0; i<M; i++) {
    el[i].ones();
  }
}

template<class T, int M, int N> void Static_matrix<T,M,N>::identity()
{
  int i, j;
  for (i=0; i<M; i++) {
    for (j=0; j<N; j++) {
      if (i==j) {
	el[i][j] = 1;
      } else {
	el[i][j] = 0;
      }
    }
  }
}

template<class T, int M, int N>
Static_matrix<T,M,N>& operator+=(Static_matrix<T,M,N>& a, T s)
{
  int i;
  for (i=0; i<M; i++) a[i]+=s;
  return a;
}

template<class T, int M, int N>
Static_matrix<T,M,N> operator+(const Static_matrix<T,M,N>& a, T s)
{
  Static_matrix<T,M,N>  ans = a;
  ans += s;
  return ans;
}

template<class T, int M, int N>
Static_matrix<T,M,N> operator+(T s, const Static_matrix<T,M,N>& a)
{
  return a+s;
}

template<class T, int M, int N>
Static_matrix<T,M,N>& operator+=(Static_matrix<T,M,N>& a, const Static_matrix<T,M,N>& b)
{
  int i;
  for (i=0; i<M; i++) a[i]+=b[i];
  return a;
}

template<class T, int M, int N>
Static_matrix<T,M,N> operator+(const Static_matrix<T,M,N>& a, const Static_matrix<T,M,N>& b)
{
  Static_matrix<T,M,N> ans = a;
  ans += b;
  return ans;
}


template<class T, int M, int N>
Static_matrix<T,M,N>& operator*=(Static_matrix<T,M,N>& a, T s)
{
  int i;
  for (i=0; i<M; i++) a[i]+=s;
  return a;
}

template<class T, int M, int N>
Static_matrix<T,M,N> operator*(const Static_matrix<T,M,N>& a, T s)
{
  Static_matrix<T,M,N> ans = a;
  ans += s;
  return ans;
}

template<class T, int M, int N>
Static_matrix<T,M,N> operator*(T s, const Static_matrix<T,M,N>& a)
{
  return a*s;
}

template<class T, int M, int N, int P>
Static_matrix<T,N,P> operator*(const Static_matrix<T,M,N>& a, const Static_matrix<T,N,P>& b)
{
  Static_matrix<T,N,P> ans;

  int i, j, k;
  for (i=0; i<M; i++) {
    for (j=0; j<P; j++) {
      T tmp = 0;
      for (k=0; k<N; k++) tmp += a[i][k]*b[k][j];
      ans[i][j] = tmp;
    }
  }

  return ans;
}

template<class T, int M, int N>
Static_vector<T,M> operator*(const Static_matrix<T,M,N>& a, const Static_vector<T,N>& v)
{
  Static_vector<T,M> ans;

  int i, k;
  for (i=0; i<M; i++) {
    T tmp = 0;
    for (k=0; k<N; k++) tmp += a[i][k]*v[k];
    ans[i] = tmp;
  }

  return ans;
}

template<class T, int N>
Static_vector<T,N-1> operator*(const Static_matrix<T,N,N>& a, const Static_vector<T,N-1>& v)
{
  Static_vector<T,N-1> ans;

  int i, k, n1=N-1;
  for (i=0; i<n1; i++) {
    T tmp = 0;
    for (k=0; k<n1; k++) tmp += a[i][k]*v[k];
    ans[i] = tmp + a[i][n1];
  }

  return ans;
}

template<class T, int M, int N>
Static_matrix<T,M,N>& operator/=(Static_matrix<T,M,N>& a, T s)
{
  int i;
  for (i=0; i<M; i++) a[i]/=s;
  return a;
}

template<class T, int M, int N>
Static_matrix<T,M,N> operator/(const Static_matrix<T,M,N>& a, T s)
{
  Static_matrix<T,M,N> ans = a;
  ans /= s;
  return ans;
}

template<class T, int M, int N>
Static_matrix<T,N,M> transpose(const Static_matrix<T,M,N>& a)
{
  Static_matrix<T,N,M> ans;

  int i, j;
  for (i=0; i<M; i++)
    for (j=0; j<N; j++) ans[j][i]=a[i][j];

  return ans;
}


template<class T, int N>
Static_matrix<T,N-1,N-1> minor_matrix(const Static_matrix<T,N,N>& a, int i, int j)
{
  Static_matrix<T,N-1,N-1> ans;

  int ii, jj, p, q;
  for (p=0, ii=0; p<N; p++) {
    if (p==i) continue;
    for (q=0, jj=0; q<N; q++) {
      if (q==j) continue;
      ans[ii][jj++] = a[p][q];
    }
    ii++;
  }

  return ans;
}


template<class T, int N> T det(const Static_matrix<T,N,N>& a)
{
  T ans = 0;

  int j;
  for (j=0; j<N; j++) {
    Static_matrix<T,N-1,N-1> tmp = minor_matrix(a,0,j);
    if (j%2==0)
      ans += a[0][j]*det(tmp);
    else
      ans -= a[0][j]*det(tmp);
  }

  return ans;
}

template<class T> T det(const Static_matrix<T,2,2>& a)
{
  return a[0][0]*a[1][1] - a[1][0]*a[0][1];
}


template<class T, int N>
Static_matrix<T,N,N> inverse(const Static_matrix<T,N,N>& a)
{
  Static_matrix<T,N,N> ans;

  double d = det(a);

  int i, j;
  for (i=0; i<N; i++) {
    for (j=0; j<N; j++) {
      Static_matrix<T,N-1,N-1> tmp = minor_matrix(a,i,j);
      if ((i+j)%2 == 0)
	ans[j][i] = det(tmp)/d;
      else
	ans[j][i] = -det(tmp)/d;
    }
  }

  return ans;
}

template<class T, int M, int N>
std::ostream& operator<<(std::ostream& out, const Static_matrix<T,M,N>& a)
{
  int i, j;
  for (i=0; i<M; i++) {
    for (j=0; j<N; j++) {
      if (j>0) out << '\t';
      out << a[i][j];
    }
    out << std::endl;
  }

  return out;
}

#endif
