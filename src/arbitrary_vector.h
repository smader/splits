/*
 arbitrary_vector.h

 A vector class that provides arbitrary index ranges.

 Copyright (c) 2013, Sebastian Mader
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef ARBITRARY_VECTOR_H
#define ARBITRARY_VECTOR_H

#include <cstdlib>
#include <ostream>

template<class T> class Arbitrary_vector
{
  int i0, i1;
  T *el;

public:
  Arbitrary_vector();
  Arbitrary_vector(int i0_, int i1_);
  Arbitrary_vector(int i0_, int i1_, T el_);
  Arbitrary_vector(const Arbitrary_vector& other);
  ~Arbitrary_vector();

  int from() const {return i0;}
  int to() const {return i1;}

  int size() const;

  T& operator[](int i) {return el[i];}
  const T& operator[](int i) const {return el[i];}
  T& operator()(int i);
  const T& operator()(int i) const;

  Arbitrary_vector& operator=(const Arbitrary_vector& other);
  Arbitrary_vector& operator=(T el_);
};

template<class T>
std::ostream& operator<<(std::ostream& out, const Arbitrary_vector<T>& v);


template<class T>
Arbitrary_vector<T>::Arbitrary_vector() : i0(0), i1(0), el(0)
{
}

template<class T>
Arbitrary_vector<T>::Arbitrary_vector(int i0_, int i1_) : i0(i0_), i1(i1_)
{
  T *ptr = (T*) malloc(size()*sizeof(T));
  el = ptr-i0;
}

template<class T>
Arbitrary_vector<T>::Arbitrary_vector(int i0_, int i1_, T el_) : i0(i0_), i1(i1_)
{
  T *ptr = (T*) malloc(size()*sizeof(T));
  el = ptr-i0;
  int i;
  for (i=i0; i<=i1; i++) {
    el[i] = el_;
  }
}

template<class T>
Arbitrary_vector<T>::Arbitrary_vector(const Arbitrary_vector& other)
{
  if ((other.el+other.i0)==0) {
    i0 = 0;
    i1 = 0;
    el = 0;
  } else {
    i0 = other.i0;
    i1 = other.i1;
    T *ptr = (T*) malloc(other.size()*sizeof(T));
    el = ptr-i0;
    int i;
    for (i=i0; i<=i1; i++) {
      el[i] = other[i];
    }
  }
}

template<class T>
Arbitrary_vector<T>::~Arbitrary_vector()
{
  if ((el+i0)!=0) {
    free((void*)(el+i0));
  }
}

template<class T>
int Arbitrary_vector<T>::size() const
{
  if ((i1==0) && (i0==0)) {
    return 0;
  } else {
    return i1-i0+1;
  }
}

template<class T>
T& Arbitrary_vector<T>::operator()(int i)
{
  if (i<i0) {
    return el[i0];
  } else if (i>i1) {
    return el[i1];
  } else {
    return el[i];
  }
}

template<class T>
const T& Arbitrary_vector<T>::operator()(int i) const
{
  if (i<i0) {
    return el[i0];
  } else if (i>i1) {
    return el[i1];
  } else {
    return el[i];
  }
}

template<class T>
Arbitrary_vector<T>& Arbitrary_vector<T>::operator=(const Arbitrary_vector& other)
{
  if (this != &other) {
    if ((other.el+other.i0)==0) {
      i0 = 0;
      i1 = 0;
      el = 0;
    } else {
      int i;
      if ((i0!=other.i0) || (i1!=other.i1)) {
	if ((el+i0) != 0) {
	  free ((void*)(el+i0));
	}
	i0 = other.i0;
	i1 = other.i1;
	if (size()>0) {
	  T *ptr = (T*) malloc(size()*sizeof(T));
	  el = ptr-i0;
	  for (i=i0; i<=i1; i++) {
	    el[i] = other[i];
	  }
	} else {
	  el = 0;
	}
      } else {
	for (i=i0; i<=i1; i++) {
	  el[i] = other[i];
	}
      }
    }
  }

  return *this;
}

template<class T>
Arbitrary_vector<T>& Arbitrary_vector<T>::Arbitrary_vector::operator=(T el_)
{
  int i;
  for (i=i0; i<=i1; i++) {
    el[i] = el_;
  }
  return *this;
}


template<class T>
std::ostream& operator<<(std::ostream& out, const Arbitrary_vector<T>& v)
{
  int i;
  for (i=v.from(); i<v.to(); i++) {
    out << i << '\t' << v[i] << std::endl;
  }

  out << v.to() << '\t' << v[v.to()];

  return out;
}

#endif
