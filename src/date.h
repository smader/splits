/*
 date.h

 A data type for simple calendar dates (no leap years).

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPLITS_DATE_H
#define SPLITS_DATE_H

#include <iostream>
#include <sstream>
#include <string>

namespace splits {

struct Date
{
  int month, day, year;

  Date() : month(0), day(0), year(0) {}
  Date(int m, int d, int y) : month(m), day(d), year(y) {}
  Date(int d_of_y, int y);
  Date(int ref);
  Date(const char* c_str);
  Date(const std::string& str);
  ~Date() {}

  bool operator==(const Date& rhs) const;
  bool operator!=(const Date& rhs) const;

  bool operator!() const;

  bool operator<=(const Date& rhs) const;
  bool operator>=(const Date& rhs) const;

  bool operator<(const Date& rhs) const;
  bool operator>(const Date& rhs) const;

  Date& operator+=(const int& rhs);
  Date operator+(const int& rhs) const;

  Date& operator-=(const int& rhs);
  Date operator-(const int& rhs) const;

  Date& operator++();
  Date operator++(int);

  Date& operator--();
  Date operator--(int);

  int operator-(const Date& rhs) const; 

  int doy() const;
  void doy(int d_of_y);

  std::string to_string(const char* sep="/") const;
  void from_string(const std::string& str);
};

} //namespace splits

std::istream& operator>>(std::istream& in, splits::Date& d);
std::ostream& operator<<(std::ostream& out, const splits::Date& d);

#endif
