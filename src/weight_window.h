/*
 weight_window.h

 Manage weights in splview.

 The SPLITS splview program to fit and visualize spline models to
 remotely sensed time series images.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_WEIGHT_WINDOW_H
#define SPLITS_WEIGHT_WINDOW_H

#include <string>
#include <FL/Fl_Window.H>
#include <FL/Fl_Browser.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>

namespace splits {

class Main_window;

class Weight_window : public Fl_Window
{
  Fl_Browser *browser;
  Fl_Group *btn_grp;
  Fl_Button *load_btn, *save_btn, *add_btn, *edit_btn, *remove_btn;
  Fl_Check_Button *use_btn;

  Main_window *mw;

  int opmode;

public:
  enum {OFF=-1, NO_TABLE, TABLE};

public:
  Weight_window(Main_window* MW);
  ~Weight_window() {}

  int mode() {return opmode;}
  void set_mode(int m);

  Arbitrary_vector<float> table();

  bool use() {if (use_btn->value()) return true; else return false;}
  void set_use(bool b);

  void load();
  bool load(const char* file);
  
  void save();

  void sort();

  void check();

  static void load_cb(Fl_Widget* w, void* data);
  static void save_cb(Fl_Widget* w, void* data);
  static void add_cb(Fl_Widget* w, void* data);
  static void edit_cb(Fl_Widget* w, void* data);
  static void remove_cb(Fl_Widget* w, void* data);
  static void use_cb(Fl_Widget* w, void* data);
};

} //namespace splits

#endif
