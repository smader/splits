/*
 load_window.h

 FLTK dialog window for loading image data into splview.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_LOAD_WINDOW_H
#define SPLITS_LOAD_WINDOW_H

#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Return_Button.H>
#include "dataset_input.h"
#include "real_input.h"
#include "date_input.h"

namespace splits {

class Load_window : public Fl_Window
{
  Dataset_input *ts_input, *dy_input, *qf_input;
  Real_input *ts_offset_inp, *ts_scale_inp, *nodata_inp;
  Date_input *start_inp, *end_inp;
  Fl_Return_Button *ok_btn;
  Fl_Button *cancel_btn;
  bool ok;

public:
  Load_window(int X, int Y);
  ~Load_window() {}

  void show();

  bool okay() {return ok;}
  void set_okay() {ok=true;}

  bool time_series_dataset_empty() const {return ts_input->is_empty();}
  const char* time_series_dataset() const {return ts_input->filename();}
  void set_time_series_dataset(const char* name) {ts_input->set_filename(name);}

  bool time_series_offset_empty() const {return ts_offset_inp->is_empty();}
  float time_series_offset() const {return ts_offset_inp->number();}
  void set_time_series_offset(float num) {ts_offset_inp->set_number(num);}

  bool time_series_scale_empty() const {return ts_scale_inp->is_empty();}
  float time_series_scale() const {return ts_scale_inp->number();}
  void set_time_series_scale(float num) {ts_scale_inp->set_number(num);}

  bool start_date_empty() const {return start_inp->is_empty();}
  splits::Date start_date() const {return start_inp->date();}
  void set_start_date(const splits::Date& d) {start_inp->set_date(d);}

  bool end_date_empty() const {return end_inp->is_empty();}
  splits::Date end_date() const {return end_inp->date();}
  void set_end_date(const splits::Date& d) {end_inp->set_date(d);}

  bool doy_dataset_empty() const {return dy_input->is_empty();}
  const char* doy_dataset() const {return dy_input->filename();}
  void set_doy_dataset(const char* name) {dy_input->set_filename(name);}

  bool nodata_empty() const {return nodata_inp->is_empty();}
  float nodata() const {return nodata_inp->number();}
  void set_nodata(float num) {nodata_inp->set_number(num);}

  bool quality_dataset_empty() const {return qf_input->is_empty();}
  const char* quality_dataset() const {return qf_input->filename();}
  void set_quality_dataset(const char* name) {qf_input->set_filename(name);}

  static void ok_cb(Fl_Widget* w, void* data);
  static void cancel_cb(Fl_Widget* w, void* data);
};

} //namespace splits

#endif
