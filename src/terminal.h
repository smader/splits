/*
 terminal.h

 FLTK terminal widget based on a code snippet from Erco's FLTK cheat page
 (http://seriss.com/people/erco/fltk/#SimpleTerminal).

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_TERMINAL_H
#define SPLITS_TERMINAL_H

#include <string>
#include <FL/Fl.H>
#include <FL/Fl_Text_Editor.H>

namespace splits {

class Terminal : public Fl_Text_Editor
{
  Fl_Text_Buffer *buff;
  std::string cmd, args;
  int linepos;
  bool fail, runn;

  static void* runner(void* data);

public:
  Terminal(int X, int Y, int W, int H, const char* command);
  ~Terminal() {}

  bool failed() {return fail;}

  bool is_busy() {return runn;}
  void set_busy(bool b=true) {runn=b;}

  void append(char ch);
  void append(const char* s);
  void set_args(const char* s);

  void run();

  int handle(int e);
};

} //namespace splits

#endif
