/*
 spline_window.h

 FLTK dialog window to edit spline parameters.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2017  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_SPLINE_WINDOW_H
#define SPLITS_SPLINE_WINDOW_H

#include <FL/Fl_Window.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Round_Button.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Int_Input.H>
#include <FL/Fl_Spinner.H>

namespace splits {

class Main_window;

class Spline_window : public Fl_Window
{
  Fl_Group *g2;
  Fl_Round_Button *rd_bspline, *rd_smoothspline, *rd_pspline;
  Fl_Round_Button *rd_average, *rd_average_periodic, *rd_uniform, *rd_periodic,
    *rd_nonunif, *rd_nonunif_periodic;
  Fl_Float_Input *fl_smoothness, *fl_penalty;
  Fl_Int_Input *in_nspan;
  Fl_Spinner *ct_degree;

  Main_window *mw;

public:
  enum Spline_type {
    B_SPLINE = 1,
    SMOOTH_B_SPLINE,
    P_SPLINE
  };
  //This could be in the style of flags, so knot types could be
  //addressed hierarchically like AVERAGE|PERIODIC or UNIFORM|PERIODIC
  //or B_SPLINE|SMOOTH for the Spline_type. It would make life easier
  //when dealing with different settings.
  enum Knot_type {
    AVERAGE = 1,
    AVERAGE_PERIODIC,
    UNIFORM,
    PERIODIC,
    NONUNIFORM,
    NONUNIFORM_PERIODIC
  };

public:
  Spline_window(Main_window* MW);
  ~Spline_window() {}

  void check();

  void set_spline_type(int type);
  int spline_type();

  void set_knot_type(int type);
  int knot_type();

  void set_degree(int d) {ct_degree->value(d);}
  int degree() {return ct_degree->value();}

  void set_smoothness(float s);
  float smoothness() {return (float)atof(fl_smoothness->value());}

  void set_penalty(float p);
  float penalty() {return (float)atof(fl_penalty->value());}

  void set_nspan(int n);
  int nspan() {return atoi(in_nspan->value());}

public:
  static void spline_button_cb(Fl_Widget* w, void* data);
  static void knot_button_cb(Fl_Widget* w, void* data);
  static void degree_cb(Fl_Widget* w, void* data);
  static void smoothness_cb(Fl_Widget* w, void* data);
  static void penalty_cb(Fl_Widget* w, void* data);
  static void nspan_cb(Fl_Widget* w, void* data);
};

} //namespace splits

#endif
