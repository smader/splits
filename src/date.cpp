/*
 date.cpp

 A data type for simple calendar dates (no leap years).

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath>
#include <cctype>
#include "date.h"

using namespace std;
using namespace splits;
using splits::Date;

void doy2md(int doy, int* m, int* d)
{
  if (doy>365) {
    *m = 0;
    *d = 0;
  } else if (doy>334) {
    *m = 12;
    *d = doy-334;
  } else if (doy>304) {
    *m = 11;
    *d = doy-304;
  } else if (doy>273) {
    *m = 10;
    *d = doy-273;
  } else if (doy>243) {
    *m = 9;
    *d = doy-243;
  } else if (doy>212) {
    *m = 8;
    *d = doy-212;
  } else if (doy>181) {
    *m = 7;
    *d = doy-181;
  } else if (doy>151) {
    *m = 6;
    *d = doy-151;
  } else if (doy>120) {
    *m = 5;
    *d = doy-120;
  } else if (doy>90) {
    *m = 4;
    *d = doy-90;
  } else if (doy>59) {
    *m = 3;
    *d = doy-59;
  } else if (doy>31) {
    *m = 2;
    *d = doy-31;
  } else if (doy>0) {
    *m = 1;
    *d = doy;
  } else {
    *m = 0;
    *d = 0;
  }

  return;
}

int md2doy(int m, int d)
{
  int doy = 0;

  if (d<1) return doy;

  switch(m)
  {
  case 1:
    if (d<=31) doy = d;
    break;
  case 2:
    if (d<=28) doy = 31+d;
    break;
  case 3:
    if (d<=31) doy = 59+d;
    break;
  case 4:
    if (d<=30) doy = 90+d;
    break;
  case 5:
    if (d<=31) doy = 120+d;
    break;
  case 6:
    if (d<=30) doy = 151+d;
    break;
  case 7:
    if (d<=31) doy = 181+d;
    break;
  case 8:
    if (d<=31) doy = 212+d;
    break;
  case 9:
    if (d<=30) doy = 243+d;
    break;
  case 10:
    if (d<=31) doy = 273+d;
    break;
  case 11:
    if (d<=30) doy = 304+d;
    break;
  case 12:
    if (d<=31) doy = 334+d;
    break;
  default:
    doy = 0;
  }

  return doy;
}

int date2ref(int m, int d, int y)
{
  return y*365+md2doy(m,d);
}

void ref2date(int ref, int* m, int* d, int* y)
{
  int year, doy;
  year = (int)floor((double)ref/365.);
  doy = ref-year*365;
  if (doy==0) {
    *y = year-1;
    *m = 12;
    *d = 31;
  } else {
    *y = year;
    doy2md(doy, m, d);
  }
}

Date::Date(int d_of_y, int y) : year(y)
{
  doy2md(d_of_y, &month, &day);
}

Date::Date(int ref)
{
  ref2date(ref, &month, &day, &year);
}

Date::Date(const char* c_str)
{
  string str(c_str);
  from_string(str);
}

Date::Date(const string& str)
{
  from_string(str);
}

bool Date::operator==(const Date& rhs) const
{
  return ((day==rhs.day) && (month==rhs.month) && (year==rhs.year));
}

bool Date::operator!=(const Date& rhs) const
{
  return !((*this)==rhs);
}

bool Date::operator!() const
{
  return (doy()==0);
}

bool Date::operator<=(const Date& rhs) const
{
  int ref, rhs_ref;
  ref = date2ref(month, day, year);
  rhs_ref = date2ref(rhs.month, rhs.day, rhs.year);
  if ((ref-rhs_ref)<=0) return true; else return false;
}

bool Date::operator>=(const Date& rhs) const
{
  int ref, rhs_ref;
  ref = date2ref(month, day, year);
  rhs_ref = date2ref(rhs.month, rhs.day, rhs.year);
  if ((ref-rhs_ref)>=0) return true; else return false;
}

bool Date::operator<(const Date& rhs) const
{
  int ref, rhs_ref;
  ref = date2ref(month, day, year);
  rhs_ref = date2ref(rhs.month, rhs.day, rhs.year);
  if ((ref-rhs_ref)<0) return true; else return false;
}

bool Date::operator>(const Date& rhs) const
{
  int ref, rhs_ref;
  ref = date2ref(month, day, year);
  rhs_ref = date2ref(rhs.month, rhs.day, rhs.year);
  if ((ref-rhs_ref)>0) return true; else return false;
}

Date& Date::operator+=(const int& rhs)
{
  int ref = date2ref(month, day, year);
  *this = Date(ref+rhs);
  return *this;
}

Date Date::operator+(const int& rhs) const
{
  Date ret(*this);
  ret += rhs;
  return ret;
}

Date& Date::operator-=(const int& rhs)
{
  int ref = date2ref(month, day, year);
  *this = Date(ref-rhs);
  return *this;
}

Date Date::operator-(const int& rhs) const
{
  Date ret(*this);
  ret -= rhs;
  return ret;
}

Date& Date::operator++()
{
  *this+=1;
  return *this;
}

Date Date::operator++(int)
{
  Date ret(*this);
  *this+=1;
  return ret;
}

Date& Date::operator--()
{
  *this-=1;
  return *this;
}

Date Date::operator--(int)
{
  Date ret(*this);
  *this-=1;
  return ret;
}

int Date::operator-(const Date& rhs) const
{
  return date2ref(month, day, year)-date2ref(rhs.month, rhs.day, rhs.year);
}

int Date::doy() const
{
  return md2doy(month, day);
}

void Date::doy(int d_of_y)
{
  if ((d_of_y<1)||(d_of_y>365)) {
    month = 0;
    day = 0;
  } else {
    doy2md(d_of_y, &month, &day);
  }
}

string Date::to_string(const char* sep) const
{
  if (!(*this)) {
    return string("");
  } else {
    stringstream sstr;
    sstr << month << sep << day << sep << year;
    return sstr.str();
  }
}

void Date::from_string(const string& str)
{
  bool sep=false, err=false;
  int count=0;

  int i;
  for (i=0; i<str.length(); i++) {
    if (!isdigit(str[i])) {
      if (sep==true) {
	err = true;
	break;
      }
      sep=true;
    } else {
      if (sep==true) {
	sep=false;
	count += 1;
      }
    }
  }

  if (err==true) {
    month = 0;
    day = 0;
    year = 0;
  }

  char ch;
  stringstream sstr(str);

  if (count==1) {
    int d_of_y;
    sstr >> d_of_y >> ch >> year;
    doy(d_of_y);
  } else if (count==2) {
    sstr >> month >> ch >> day >> ch >> year;
  } else {
    month = 0;
    day = 0;
    year = 0;
  }
}

std::istream& operator>>(std::istream& in, Date& d)
{
  string str;
  in >> str;
  d.from_string(str);
  return in;
}

ostream& operator<<(ostream& out, const Date& d)
{
  out << d.to_string();
  return out;
}
