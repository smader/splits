/*
 conf.cpp

 Read/write GUI configuration.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2017  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include "conf.h"
#include "archive.h"
#include "config.h"

using namespace std;
using namespace splits;

string Conf::token(FILE* fp, const char* t)
{
  rewind(fp);

  bool b = false, comment=false;
  char c;
  int i = 0, n = strlen(t);

  do {
    if (i>(n-1)) return NULL;
    c = fgetc(fp);
    if (c=='#') comment=true;
    if (!comment) {
      if (c==t[i]) {
	i+=1;
	if (i==n) {
	  b = true;
	  break;
	}
      } else {
	i=0;
      }
    } else {
      if (c=='\n') comment=false;
    }
  } while (c!=EOF);

  if (!b) return string();

  b = false;

  long int p;
  int l = 0;
  
  do {
    c = fgetc(fp);
    if (c=='=') {
      b = true;
      p = ftell(fp);
      break;
    }
  } while (c!=EOF);

  if (!b) return string();

  do {
    c = fgetc(fp);
    if (c != '\n') l+=1; else break;
  } while (c!=EOF);

  if (l==0) return string();
  
  char *v = new char[l+1];
  
  fseek(fp, p, SEEK_SET);

  i = 0;
  for (n=0; n<l; n++) {
    v[i++] = fgetc(fp);
  }
  v[i] = '\0';

  trim_whitespace(v);
  trim_quotes(v);
  
  string s = v;
  
  delete [] v;
  
  return s;
}

void Conf::trim_whitespace(char* str)
{
  int n = strlen(str);

  int i, k=0, l=n;
  for (i=0; i<n; i++) {
    if (str[i]==' ' || str[i]=='\t') k++; else break;
  }
  for (i=(n-1); i>=0; i--) {
    if (str[i]==' ' || str[i]=='\t') l--; else break;
  }

  char* buf = new char[l-k+1];

  n=0;
  for (i=k; i<l; i++) buf[n++]=str[i];
  buf[n]='\0';

  strcpy(str, buf);

  delete [] buf;
}

void Conf::trim_quotes(char* str)
{
  int n = strlen(str);

  int i, k=0, l=n;
  for (i=0; i<n; i++) {
    if (str[i]=='\'' || str[i]=='"') k++; else break;
  }
  for (i=(n-1); i>=0; i--) {
    if (str[i]=='\'' || str[i]=='"') l--; else break;
  }

  char* buf = new char[l-k+1];

  n=0;
  for (i=k; i<l; i++) buf[n++]=str[i];
  buf[n]='\0';

  strcpy(str, buf);

  delete [] buf;
}

bool Conf::to_bool(const char* str)
{
  int n = strlen(str);
  if (n==0) return false;
  char *buf = new char[n+1];
  strcpy(buf,str);
  int i;
  for (i=0; i<n; i++) buf[i]=tolower(buf[i]);
  bool b = false;
  if (strcmp(buf,"yes")==0) b=true;
  if (strcmp(buf,"true")==0) b=true;
  if (strcmp(buf,"on")==0) b=true;
  if (strcmp(buf,"y")==0) b=true;
  if (strcmp(buf,"t")==0) b=true;
  if (strcmp(buf,"1")==0) b=true;
  delete [] buf;
  return b;
}

Conf::Conf(Main_window* MW) : use_wt(false), offs(0.0f), gain(1.0f),
			      nodata(-32767.0f)
{
  if (MW->current < 0) return;
  
  Archive &ar(MW->data[MW->current]);

  ts_dset = ar.dataset();
  dy_dset = ar.doy_dataset();
  qa_dset = ar.quality_dataset();

  d_beg = ar.begin();
  d_end = ar.end();

  offs = ar.offset();
  gain = ar.gain();

  nodata = ar.nodata();

  string str = MW->w_tab_file[MW->current];
  if (!str.empty()) {
    w_tab_file = str;
    use_wt = (MW->use_weights[MW->current]);
  }
}

Conf::Conf(const char* fname) : use_wt(false), offs(0.0f), gain(1.0f),
				nodata(-32767.0f)
{
  FILE *fp;
  if ((fp=fopen(fname,"rt"))==NULL) return;

  ts_dset = token(fp, "ts dataset");
  dy_dset = token(fp, "doy dataset");
  qa_dset = token(fp, "quality dataset");

  string str;

  str = token(fp, "ts start date");
  d_beg.from_string(str);

  str = token(fp, "ts end date");
  d_end.from_string(str);

  str = token(fp, "ts offset");
  if (!str.empty()) {
    offs = (float) atof(str.c_str());
  }

  str = token(fp, "ts scale factor");
  if (!str.empty()) {
    gain = (float) atof(str.c_str());
  }

  str = token(fp, "nodata value");
  if (!str.empty()) {
    nodata = (float) atof(str.c_str());
  }

  str = token(fp, "weight table");
  if (!str.empty()) {
    w_tab_file = str;
    str = token(fp, "use weights");
    use_wt = to_bool(str.c_str());
  }
}

bool Conf::save(const char* fname)
{
  FILE *fp;
  if (!(fp=fopen(fname, "wt"))) return false;

  fprintf(fp, "# SPLITS v%s config file\n\n", VERSION);
  
  fprintf(fp, "ts dataset = %s\n", ts_dset.c_str());
  fprintf(fp, "ts offset = %f\n", offs);
  fprintf(fp, "ts scale factor = %f\n", gain);
  fprintf(fp, "ts start date = %s\n", d_beg.to_string().c_str());
  fprintf(fp, "ts end date = %s\n", d_end.to_string().c_str());
  fprintf(fp, "doy dataset = %s\n", dy_dset.c_str());
  fprintf(fp, "nodata value = %f\n", nodata);
  fprintf(fp, "quality dataset = %s\n", qa_dset.c_str());

  if (!w_tab_file.empty()) {
    fprintf(fp, "weight table = %s\n", w_tab_file.c_str());
    if (use_wt)
      fprintf(fp, "use weights = yes\n");
    else
      fprintf(fp, "use weights = no\n");
  }
  
  fclose(fp);

  return true;
}
