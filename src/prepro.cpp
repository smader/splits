/*
 prepro.cpp

 A preprocessor for MODIS Aqua/Terra data.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2016  Sebastian Mader, David Frantz

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <dirent.h>

#if _WIN32
#include <errno.h>
#include <string.h>
int scandir (const char *dir,
	     struct dirent ***namelist,
	     int (*select) (const struct dirent *),
	     int (*compar) (const struct dirent **, const struct dirent **))
{
  /*Copyright 1998, 1999, 2000, 2001 Red Hat, Inc.

    Written by Corinna Vinschen <corinna.vinschen@cityweb.de>

    This software is a copyrighted work licensed under the terms of the
    Cygwin license.  Please consult the file "CYGWIN_LICENSE" for
    details.*/

  DIR *dirp;
  struct dirent *ent, *etmp, **nl = NULL, **ntmp;
  int count = 0;
  int allocated = 0;

  if (!(dirp = opendir (dir)))
    return -1;

  int prior_errno = errno;
  errno = 0;

  while ((ent = readdir (dirp)))
    {
      if (!select || select (ent))
	{

	  /* Ignore error from readdir/select. See POSIX specs. */
	  errno = 0;

	  if (count == allocated)
	    {

	      if (allocated == 0)
		allocated = 10;
	      else
		allocated *= 2;

	      ntmp = (struct dirent **) realloc (nl, allocated * sizeof *nl);
	      if (!ntmp)
		{
		  errno = ENOMEM;
		  break;
		}
	      nl = ntmp;
	  }

	  if (!(etmp = (struct dirent *) malloc (sizeof *ent)))
	    {
	      errno = ENOMEM;
	      break;
	    }
	  *etmp = *ent;
	  nl[count++] = etmp;
	}
    }

  if ((prior_errno = errno) != 0)
    {
      closedir (dirp);
      if (nl)
	{
	  while (count > 0)
	    free (nl[--count]);
	  free (nl);
	}
      /* Ignore errors from closedir() and what not else. */
      errno = prior_errno;
      return -1;
    }

  closedir (dirp);
  errno = prior_errno;

  qsort (nl, count, sizeof *nl, (int (*)(const void *, const void *)) compar);
  if (namelist)
    *namelist = nl;
  return count;
}
int alphasort (const struct dirent **a, const struct dirent **b)
{
  /*Copyright 1998, 1999, 2000, 2001 Red Hat, Inc.

    Written by Corinna Vinschen <corinna.vinschen@cityweb.de>

    This software is a copyrighted work licensed under the terms of the
    Cygwin license.  Please consult the file "CYGWIN_LICENSE" for
    details.*/

  return strcoll ((*a)->d_name, (*b)->d_name);
}
#endif

#include <gdal.h>
#include <cpl_conv.h>
#include <cpl_string.h>
#include "clargp.h"
#include "common.h"

using namespace splits;

//-------------------------------------------------------------------
typedef signed short int int2s;
typedef unsigned char int1u;

//-------------------------------------------------------------------
// holds the address of the array of which the sorted index
//  order needs to be found
int *addr_arr;

//-------------------------------------------------------------------
//  allocate integer array
void ialloc(int **var, int n){
	*var = (int*) malloc(n*sizeof(int));
	if (var == NULL){ bail_out("Unable to allocate memory! Exit.");}
}

//-------------------------------------------------------------------
//  allocate integer array, INT2S method
void ialloc2s(int2s **var, int n){
	*var = (int2s*) malloc(n*sizeof(int2s));
	if (var == NULL){ bail_out("Unable to allocate memory! Exit.");}
}

//-------------------------------------------------------------------
//  allocate integer array, INT1U method
void ialloc1u(int1u **var, int n){
	*var = (int1u*) malloc(n*sizeof(int1u));
	if (var == NULL){ bail_out("Unable to allocate memory! Exit.");}
}

//-------------------------------------------------------------------
// compare function for sorting
static int compar(const void *a, const void *b){
int aa = *((int *) a), bb = *((int *) b);

	if (addr_arr[aa] <  addr_arr[bb]) return -1;
	if (addr_arr[aa] == addr_arr[bb]) return  0;
	if (addr_arr[aa] >  addr_arr[bb]) return  1;
}

//-------------------------------------------------------------------
// convert VI QA SDS to VI usefulness Index
int bit_to_viq(int *buf, int n){
int i;

	for (i=0; i<n; i++){

	  // if adjacent or mixed cloud, set VIQ to 17
	  if ((buf[i]&(1<<8)) || (buf[i]&(1<<10))){
	    buf[i] = 17;
	  } else {
	    // get integer value of VI Usefulness Bit Fields + 1
	    buf[i] = (buf[i]>>2&0xf)+1;
	  }

	}
}

//-------------------------------------------------------------------
// merge TERRA and AQUA stacks
int merge_aqua_terra(char *d_out, char *tile_id, 
                     int nx, int ny, int tnb, int anb, 
                     int *tyy, int *ayy, int *tdoy, int *adoy){

char fname[1024];
FILE *f_aqua[3], *f_terra[3], *f_merge[3];
int b, i, j, l;
size_t res;

int pdoy; // temporary doy to get turn of year right

int2s *TEVI; // one line  of               TERRA EVI time-series
int2s *TDOY; // one line  of               TERRA DOY time-series
int1u *TVIQ; // one line  of               TERRA VIQ time-series
int2s *AEVI; // one line  of          AQUA       EVI time-series
int2s *ADOY; // one line  of          AQUA       DOY time-series
int1u *AVIQ; // one line  of          AQUA       VIQ time-series
int2s *PEVI; // one pixel of unsorted AQUA/TERRA EVI time series
int2s *PDOY; // one pixel of unsorted AQUA/TERRA DOY time series
int1u *PVIQ; // one pixel of unsorted AQUA/TERRA VIQ time series
int2s *LEVI; // one line  of   sorted AQUA/TERRA EVI time series
int2s *LDOY; // one line  of   sorted AQUA/TERRA DOY time series
int1u *LVIQ; // one line  of   sorted AQUA/TERRA VIQ time series
int   *DATE; // one pixel of DATES,   used for sorting
int    *IDX; // one pixel of INDICES, used for sorting


GDALDatasetH iDataset, oDataset[3]; // datasets for input/output

char **ocOptions = NULL; // output creation options
GDALDriverH oDriver;    // output driver

double GeoTransform[6]; // geotransformation
const char *wkt;       // projection

GDALDataType dt[3] = { GDT_Int16, GDT_Byte, GDT_Int16 }; // datatypes
char layer[3][4];                                       // output subnames


	/** start **/

	strcpy(layer[0], "EVI");
	strcpy(layer[1], "VIQ");
	strcpy(layer[2], "DOY");


	GDALAllRegister();
	oDriver = GDALGetDriverByName("ENVI");
	ocOptions = CSLSetNameValue(ocOptions, "INTERLEAVE", "BIL");

	// open first image and retrieve dataset properties
	sprintf(fname, "%s/MOD13Q1_%s_%s_STACK.dat", d_out, tile_id, layer[0]);

	if ((iDataset = GDALOpen(fname, GA_ReadOnly)) == NULL){
	  fprintf(stderr,"unable to open image\n"); exit(1);}
	GDALGetGeoTransform(iDataset, GeoTransform);
	wkt = GDALGetProjectionRef(iDataset);

	//printf("%s\n", wkt);
	//printf("dimension: %d/%d\n", nx, ny);
	//printf("%f %f %f %f %f %f\n", GeoTransform[0],GeoTransform[1],GeoTransform[2],GeoTransform[3],GeoTransform[4],GeoTransform[5]);

	// create the output images
	for (b=0; b<3; b++){
		sprintf(fname, "%s/MCD13Q1_%s_%s_STACK.dat", d_out, tile_id, layer[b]);
		oDataset[b] = GDALCreate(oDriver, fname, nx, ny, tnb+anb, dt[b], ocOptions);
		GDALSetProjection(oDataset[b], wkt);
		GDALSetGeoTransform(oDataset[b], GeoTransform);
	}

	// close iDataset, wkt is lost hereafter somehow
	GDALClose(iDataset);


	// open images in binary mode
	for (b=0; b<3; b++){
		sprintf(fname, "%s/MYD13Q1_%s_%s_STACK.dat", d_out, tile_id, layer[b]);
		f_aqua[b]  = fopen(fname, "rb");
		sprintf(fname, "%s/MOD13Q1_%s_%s_STACK.dat", d_out, tile_id, layer[b]);
		f_terra[b] = fopen(fname, "rb");
		sprintf(fname, "%s/MCD13Q1_%s_%s_STACK.dat", d_out, tile_id, layer[b]);
		f_merge[b] = fopen(fname, "wb");
	}

	// allocate all arrays
	ialloc2s(&TEVI, nx*tnb); ialloc1u(&TVIQ, nx*tnb);
	ialloc2s(&TDOY, nx*tnb);
	ialloc2s(&AEVI, nx*anb); ialloc1u(&AVIQ, nx*anb);
	ialloc2s(&ADOY, nx*anb);
	ialloc2s(&PEVI, tnb+anb); ialloc1u(&PVIQ, tnb+anb);
	ialloc2s(&PDOY, tnb+anb);
	ialloc2s(&LEVI, nx*(tnb+anb)); ialloc1u(&LVIQ, nx*(tnb+anb));
	ialloc2s(&LDOY, nx*(tnb+anb));
	ialloc(&DATE, tnb+anb); ialloc(&IDX, tnb+anb);

	// for every line
	for (i=0; i<ny; i++){

		// read one line of data
		if ((res = fread((void*)TEVI, sizeof(int2s), nx*tnb, f_terra[0])) != nx*tnb){
		  fprintf(stderr,"reading error!\n"); exit(1);}
		if ((res = fread((void*)TVIQ, sizeof(int1u), nx*tnb, f_terra[1])) != nx*tnb){
		  fprintf(stderr,"reading error!\n"); exit(1);}
		if ((res = fread((void*)TDOY, sizeof(int2s), nx*tnb, f_terra[2])) != nx*tnb){
		  fprintf(stderr,"reading error!\n"); exit(1);}
		if ((res = fread((void*)AEVI, sizeof(int2s), nx*anb, f_aqua[0])) != nx*anb){
		  fprintf(stderr,"reading error!\n"); exit(1);}
		if ((res = fread((void*)AVIQ, sizeof(int1u), nx*anb, f_aqua[1])) != nx*anb){
		  fprintf(stderr,"reading error!\n"); exit(1);}
		if ((res = fread((void*)ADOY, sizeof(int2s), nx*anb, f_aqua[2])) != nx*anb){
		  fprintf(stderr,"reading error!\n"); exit(1);}

		// for every pixel
		for (j=0; j<nx; j++){
		
			// copy single pixel to buffer
			for (l=0; l<tnb; l++){
				IDX[l] = l;
				PDOY[l] = pdoy = TDOY[nx*l+j];
				if (pdoy > 0){ // valid data
					PEVI[l] = TEVI[nx*l+j];
					PVIQ[l] = TVIQ[nx*l+j];
					if (pdoy < 25 && tdoy[l] > 340){ // turn of year
						DATE[l] = (tyy[l]+1)*1000+pdoy;
					} else {
						DATE[l] = tyy[l]*1000+pdoy;
					}
				} else { // fill
					PDOY[l] = -32767;
					PEVI[l] = -32767;
					PVIQ[l] = 0;
					DATE[l] = 2100000;
				}
			}
			for (l=0; l<anb; l++){
				IDX[l+tnb] = l+tnb;
				PDOY[l+tnb] = pdoy = ADOY[nx*l+j];
				if (pdoy > 0){ // valid data
					PEVI[l+tnb] = AEVI[nx*l+j];
					PVIQ[l+tnb] = AVIQ[nx*l+j];
					if (pdoy < 25 && adoy[l] > 340){ // turn of year
						DATE[l+tnb] = (ayy[l]+1)*1000+pdoy;
					} else {
						DATE[l+tnb] = ayy[l]*1000+pdoy;
					}
				} else { // fill
					PDOY[l+tnb] = -32767;
					PEVI[l+tnb] = -32767;
					PVIQ[l+tnb] = 0;
					DATE[l+tnb] = 2100000;
				}
			}

			// sort the pixel
			addr_arr = DATE;
			qsort(IDX, tnb+anb, sizeof(int), compar);

			// copy the pixel back to line buffer
			for (l=0; l<tnb+anb; l++){
				LEVI[nx*l+j] = PEVI[IDX[l]];
				LVIQ[nx*l+j] = PVIQ[IDX[l]];
				LDOY[nx*l+j] = PDOY[IDX[l]];
			}
		
		}

		// write one line of data
		fwrite((const void*)LEVI, sizeof(int2s), nx*(tnb+anb), f_merge[0]);
		fwrite((const void*)LVIQ, sizeof(int1u), nx*(tnb+anb), f_merge[1]);
		fwrite((const void*)LDOY, sizeof(int2s), nx*(tnb+anb), f_merge[2]);

	}


	/** clean **/

	free((void*)TEVI); free((void*)TVIQ); free((void*)TDOY);
	free((void*)AEVI); free((void*)AVIQ); free((void*)ADOY);
	free((void*)PEVI); free((void*)PVIQ); free((void*)PDOY);
	free((void*)LEVI); free((void*)LVIQ); free((void*)LDOY);
	free((void*)DATE); free((void*)IDX);

	for (b=0; b<3; b++){
		fclose(f_aqua[b]);
		fclose(f_terra[b]);
		fclose(f_merge[b]);
	}

}

//-------------------------------------------------------------------
// stack TERRA/AQUA HDF images
int stack_eos_hdf(char *d_inp, char *d_out, char *sensor_id, char *tile_id,
                  int *re_nx, int *re_ny, int *re_nb, int **re_yy, int **re_doy, bool ndvi){

char fname[1024], oname[1024]; // names of input/output

GDALDatasetH iDataset, sdsDataset, oDataset[3]; // datasets for input/input-SDS/output
GDALRasterBandH sdsBand, outBand;              // corresponding bands

char **ocOptions = NULL; // output creation options
GDALDriverH oDriver;    // output driver

int nx, ny;              // number of samples, lines
double GeoTransform[6]; // geotransformation
const char *wkt;       // projection

char **sdslist;      // SDS listing
char KeyName[1024]; // key pattern to retrieve name of SDS from SDS listing
char *sdsname;     // name of SDS

int psds[3] = { 2, 3, 11 };                               // layer IDs of the relevant SDS
GDALDataType dt[3] = { GDT_Int16, GDT_Byte, GDT_Int16 }; // datatypes of the relevant SDS
char layer[3][5];                                       // output subnames of the relevant SDS

int *buf; // image data is temorariliy read to this buffer

int i, b; // iteration over matching images and relevant SDS

struct dirent **namelist; // directory listing
int nfile;               // number of files in directory

int *ptr;    // array containing the matching indices of the directory listing
int nmatch; // number of matching images. number of valid items in ptr.

int *yy, *doy; // arrays that contain the year/doy of the matching images
char tmp[10]; // helping variable to retrieve year and doy from file name

        if (ndvi) psds[0] = 1;

	/** start **/

	// output subnames
	if (ndvi) {
	  strcpy(layer[0], "NDVI");
	} else {
	  strcpy(layer[0], "EVI");
	}
	strcpy(layer[1], "VIQ");
	strcpy(layer[2], "DOY");

	GDALAllRegister();
	oDriver = GDALGetDriverByName("ENVI");
	ocOptions = CSLSetNameValue(ocOptions, "INTERLEAVE", "BIL");

	// get directory listing
	if ((nfile = scandir(d_inp, &namelist, 0, alphasort)) < 0){
		perror("scandir"); exit(1);}
	//printf("count is %d\n", nfile);

	// get matching images
	ialloc(&ptr, nfile);
	for (i=0, nmatch=0; i<nfile; i++){
		if (strstr(namelist[i]->d_name, ".hdf")    != NULL &&
			strstr(namelist[i]->d_name, sensor_id) != NULL &&
			strstr(namelist[i]->d_name, tile_id)   != NULL){

			//printf("%s\n", namelist[i]->d_name);
			ptr[nmatch] = i;
			nmatch++;
		}
	}
	//printf("count is %d\n", nmatch);

	if (nmatch<1){
	  bail_out("No matching hdf container was found! Exit."); 
		free((void*)ptr); exit(1);}

	ialloc(&yy, nmatch);
	ialloc(&doy, nmatch);

	// open first image and retrieve dataset properties
	sprintf(fname, "%s/%s", d_inp, namelist[ptr[0]]->d_name);
	if ((iDataset = GDALOpen(fname, GA_ReadOnly)) == NULL){
	  fprintf(stderr,"unable to open image\n"); exit(1);}

	sdslist = GDALGetMetadata(iDataset, "SUBDATASETS");

	sprintf(KeyName, "SUBDATASET_%d_NAME", psds[0]);
	sdsname = CPLStrdup(CSLFetchNameValue(sdslist, KeyName));
	GDALClose(iDataset);

	sdsDataset = GDALOpen(sdsname, GA_ReadOnly);

	nx = GDALGetRasterXSize(sdsDataset); 
	ny = GDALGetRasterYSize(sdsDataset);
	GDALGetGeoTransform(sdsDataset, GeoTransform);
	wkt = GDALGetProjectionRef(sdsDataset);

	// clean
	CPLFree(sdsname);
	//CSLDestroy(sdslist);

	//printf("%s\n", wkt);
	//printf("dimension: %d/%d\n", nx, ny);
	//printf("%f %f %f %f %f %f\n", GeoTransform[0],GeoTransform[1],GeoTransform[2],GeoTransform[3],GeoTransform[4],GeoTransform[5]);


	// create the output images
	for (b=0; b<3; b++){
		sprintf(oname, "%s/%s13Q1_%s_%s_STACK.dat", d_out, sensor_id, tile_id, layer[b]);
		//printf("%s\n", oname);
		oDataset[b] = GDALCreate(oDriver, oname, nx, ny, nmatch, dt[b], ocOptions);
		GDALSetProjection(oDataset[b], wkt);
		GDALSetGeoTransform(oDataset[b], GeoTransform);
	}

	// close sdsDataset, wkt is lost hereafter somehow
	GDALClose(sdsDataset);


	buf = (int*) CPLMalloc(sizeof(int)*nx*ny); // buffer for reading images

	// for every matching image
	for (i=0; i<nmatch; i++){

		// get name
		sprintf(fname, "%s/%s", d_inp, namelist[ptr[i]]->d_name);

		// get date
		strncpy(tmp, namelist[ptr[i]]->d_name+13, 3);
		tmp[3] = '\0';
		doy[i] = atoi(tmp);
		strncpy(tmp, namelist[ptr[i]]->d_name+9, 4);
		tmp[4] = '\0';
		yy[i] = atoi(tmp);
		//printf("yy: %d, doy: %d\n", yy[i], doy[i]);

		// copy from input to output
		if ((iDataset = GDALOpen(fname, GA_ReadOnly)) == NULL){
		  fprintf(stderr,"unable to open image\n"); exit(1);}
		sdslist = GDALGetMetadata(iDataset, "SUBDATASETS");

		for (b=0; b<3; b++){
			sprintf(KeyName, "SUBDATASET_%d_NAME", psds[b]);
			sdsname = CPLStrdup(CSLFetchNameValue(sdslist, KeyName));
			sdsDataset = GDALOpen(sdsname, GA_ReadOnly);
			sdsBand = GDALGetRasterBand(sdsDataset, 1);
			GDALRasterIO(sdsBand, GF_Read,  0, 0, nx, ny, 
							buf, nx, ny, GDT_Int32, 0, 0);
			if (b == 1) bit_to_viq(buf, nx*ny);
			outBand = GDALGetRasterBand(oDataset[b], i+1);
			GDALRasterIO(outBand, GF_Write, 0, 0, nx, ny, 
							buf, nx, ny, GDT_Int32, 0, 0);
			GDALClose(sdsDataset);
		}
		GDALClose(iDataset);
		//printf("done with %s, band %d\n", sensor_id, i+1);
	}

	/** clean **/

	CPLFree(buf);
	CPLFree(sdsname);
	//CSLDestroy(sdslist);
	//CSLDestroy(ocOptions);

	for (b=0; b<3; b++){
		GDALClose(oDataset[b]);
	}

	for (i=0; i<nfile; i++) free(namelist[i]);
	free(namelist);

	free((void*)ptr);

	// return values
	*re_nx = nx;
	*re_ny = ny;
	*re_nb = nmatch;
	*re_yy = yy;
	*re_doy = doy;

}


ClArg_t arglst[] =
{
  string_arg("a", 0),
  flag_arg("c", 0),
  string_arg("s", 1), //sensor-id
  string_arg("t", 1), //tile-id
  flag_arg("M", 0),
  flag_arg("N", 0),
  flag_arg("Qq", 0),
  pos_arg("in-dir"),
  trailing_arg("out-dir"),
  last_arg
};

//-------------------------------------------------------------------
// MAIN
int main( int argc, char *argv[] ){

parse_cl_args(argc, argv, arglst);

int argsiz;
void *parg;

// copy input;
char *sensor_id = (char*)get_cl_arg(arglst, "s", &argsiz);
char *tile_id   = (char*)get_cl_arg(arglst, "t", &argsiz);
char *dir_inp   = (char*)get_cl_arg(arglst, "in-dir", &argsiz);
char *dir_out   = (char*)get_cl_arg(arglst, "out-dir", &argsiz);
if (dir_out==NULL) dir_out = dir_inp;

// algorithm control flags
 bool domod, domyd, domcd, ndvi;

// number of samples/lines/bands of TERRA (t) and AQUA (a)
// year and doy vectors of availble TERRA (t) and AQUA (a) images
int tnx, tny, tnb, *tyy, *tdoy;
int anx, any, anb, *ayy, *adoy;

	/** start **/

	// rudimentary check if arguments seem ok
	if ((strstr(sensor_id, "MOD") == NULL && 
	     strstr(sensor_id, "MYD") == NULL && 
	     strstr(sensor_id, "MCD") == NULL)){
	  bail_out("Argument -s is invalid! Exit.");}
	if (!(strlen(tile_id) == 6)){
		bail_out("Argument -t is invalid! Exit.");}

	// set algorithm control flags
	if (strstr(sensor_id, "MCD") != NULL){
		domod = domyd = domcd = true;
	} else if (strstr(sensor_id, "MOD") != NULL){
		domod = true; domyd = domcd = false;
	} else if (strstr(sensor_id, "MYD") != NULL){
		domyd = true; domod = domcd = false;
	}

	parg = get_cl_arg(arglst, "N", &argsiz);
	if (argsiz>0) ndvi=true; else ndvi=false;

	// stack available data to physical ENVI BIL stacks
	if (domod) stack_eos_hdf(dir_inp, dir_out, "MOD", tile_id, 
				 &tnx, &tny, &tnb, &tyy, &tdoy, ndvi);
	if (domyd) stack_eos_hdf(dir_inp, dir_out, "MYD", tile_id, 
				 &anx, &any, &anb, &ayy, &adoy, ndvi);


	// if no merge is asked for, stop
	if (!domcd) return 0;

	// if AQUA and TERRA stacks differ in x or y, stop
	if (tnx != anx){
	  bail_out("TERRA and AQUA have different number of samples! Exit.");}
	if (tny != any){
	  bail_out("TERRA and AQUA have different number of lines! Exit.");}

	// merge the TERRA and AQUA stacks
	merge_aqua_terra(dir_out, tile_id, tnx, tny, tnb, anb, tyy, ayy, tdoy, adoy);

	/** clean **/
	free((void*)tyy); free((void*)tdoy);
	free((void*)ayy); free((void*)adoy); 

}
