/*
 static_vector.h

 A template class for vectors whose size is known at compile time.
 Provides special templates for the multiplication operator to be
 used in computer graphics applications (i.e. it allows multiplication
 of e.g. a 3x3-matrix with a 2-vector). See also "static_matrix.h".

 Copyright (c) 2012 Sebastian Mader
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STATIC_VECTOR_H
#define STATIC_VECTOR_H

#include <ostream>

template <class T, int N> class Static_vector
{
  T el[N];

public:
  Static_vector() {}
  Static_vector(const T v[])
  {
    int i;
    for (i=0; i<N; i++) el[i]=v[i];
  }
  ~Static_vector() {}

  T& operator[](int i) {return el[i];}
  const T& operator[](int i) const {return el[i];}

  T& operator()(int i) {return el[i];}
  const T& operator()(int i) const {return el[i];}

  Static_vector<T,N> operator+()
  {
    Static_vector<T,N> ans;
    int i;
    for (i=0; i<N; i++) ans[i] = +el[i];
    return ans;
  }

  Static_vector<T,N> operator-()
  {
    Static_vector<T,N> ans;
    int i;
    for (i=0; i<N; i++) ans[i] = -el[i];
    return ans;
  }

  void zeros();
  void ones();
};

template<class T, int N> void Static_vector<T,N>::zeros()
{
  int i;
  for (i=0; i<N; i++) {
    el[i] = 0;
  }
}

template<class T, int N> void Static_vector<T,N>::ones()
{
  int i;
  for (i=0; i<N; i++) {
    el[i] = 1;
  }
}

template <class T> class Static_vector<T,2>
{
  T el[2];

public:
  Static_vector() {}
  Static_vector(const T v[])
  {
    el[0]=v[0];
    el[1]=v[1];
  }
  Static_vector(T x, T y)
  {
    el[0]=x;
    el[1]=y;
  }
  ~Static_vector() {}

  T& operator[](int i) {return el[i];}
  const T& operator[](int i) const {return el[i];}

  T& operator()(int i) {return el[i];}
  const T& operator()(int i) const {return el[i];}

  Static_vector<T,2> operator+()
  {
    Static_vector<T,2> ans;
    ans[0] = +el[0];
    ans[1] = +el[1];
    return ans;
  }

  Static_vector<T,2> operator-()
  {
    Static_vector<T,2> ans;
    ans[0] = -el[0];
    ans[1] = -el[1];
    return ans;
  }

  void zeros();
  void ones();
};

template<class T> void Static_vector<T,2>::zeros()
{
  el[0] = 0;
  el[1] = 0;
}

template<class T> void Static_vector<T,2>::ones()
{
  el[0] = 1;
  el[1] = 1;
}

template<class T, int N>
T dot_product(const Static_vector<T,N>& u, const Static_vector<T,N> v)
{
  T ans = 0;

  int i;
  for (i=0; i<N; i++) ans += u[i]*v[i];

  return ans;
}


template<class T, int N>
Static_vector<T,3> cross_product(const Static_vector<T,3>& u, const Static_vector<T,3>& v)
{
  Static_vector<T,3> ans;

  ans[0] = u[1]*v[2] - u[2]*v[1];
  ans[1] = u[0]*v[2] - u[2]*v[0];
  ans[2] = u[0]*v[1] - u[1]*v[0];

  return ans;
}


template<class T, int N>
Static_vector<T,N>& operator+=(Static_vector<T,N>& v, T s)
{
  int i;
  for (i=0; i<N; i++) v[i]+=s;
  return v;
}

template<class T, int N>
Static_vector<T,N> operator+(const Static_vector<T,N>& v, T s)
{
  Static_vector<T,N> ans = v;
  ans += s;
  return ans;
}

template<class T, int N>
Static_vector<T,N> operator+(T s, const Static_vector<T,N>& v)
{
  return v+s;
}

template<class T, int N>
Static_vector<T,N>& operator+=(Static_vector<T,N>& u, const Static_vector<T,N>& v)
{
  int i;
  for (i=0; i<N; i++) u[i]+=v[i];
  return u;
}

template<class T, int N>
Static_vector<T,N> operator+(const Static_vector<T,N>& u, const Static_vector<T,N>& v)
{
  Static_vector<T,N> ans = u;
  ans += v;
  return ans;
}


template<class T, int N>
Static_vector<T,N>& operator-=(Static_vector<T,N>& v, T s)
{
  int i;
  for (i=0; i<N; i++) v[i]-=s;
  return v;
}

template<class T, int N>
Static_vector<T,N> operator-(const Static_vector<T,N>& v, T s)
{
  Static_vector<T,N> ans = v;
  ans -= s;
  return ans;
}

template<class T, int N>
Static_vector<T,N>& operator-=(Static_vector<T,N>& u, const Static_vector<T,N>& v)
{
  int i;
  for (i=0; i<N; i++) u[i]-=v[i];
  return u;
}

template<class T, int N>
Static_vector<T,N> operator-(const Static_vector<T,N>& u, const Static_vector<T,N>& v)
{
  Static_vector<T,N> ans = u;
  ans -= v;
  return ans;
}


template<class T, int N>
Static_vector<T,N>& operator*=(Static_vector<T,N>& v, T s)
{
  int i;
  for (i=0; i<N; i++) v[i]*=s;
  return v;
}

template<class T, int N>
Static_vector<T,N> operator*(const Static_vector<T,N>& v, T s)
{
  Static_vector<T,N> ans = v;
  ans *= s;
  return ans;
}

template<class T, int N>
Static_vector<T,N> operator*(T s, const Static_vector<T,N>& v)
{
  return v*s;
}

template<class T, int N>
T operator*(const Static_vector<T,N>& u, const Static_vector<T,N>& v)
{
  return dot_product(u,v);
}

template<class T, int M, int N> class Static_matrix;

template<class T, int M, int N>
Static_vector<T,N> operator*(const Static_vector<T,M>& v, const Static_matrix<T,M,N>& a)
{
  Static_vector<T,N> ans;

  int i, k;
  for (i=0; i<N; i++) {
    T tmp = 0;
    for (k=0; k<M; k++) tmp += v[k]*a[k][i];
    ans[i] = tmp;
  }

  return ans;
}

template<class T, int N>
Static_vector<T,N-1> operator*(const Static_vector<T,N-1>& v, const Static_matrix<T,N,N>& a)
{
  Static_vector<T,N-1> ans;

  int i, k, n1=N-1;
  for (i=0; i<n1; i++) {
    T tmp = 0;
    for (k=0; k<n1; k++) tmp += v[k]*a[k][i];
    ans[i] = tmp + a[n1][i];
  }

  return ans;
}

template<class T, int N>
Static_vector<T,N>& operator/=(Static_vector<T,N>& v, T s)
{
  int i;
  for (i=0; i<N; i++) v[i]/=s;
  return v;
}

template<class T, int N>
Static_vector<T,N> operator/(const Static_vector<T,N>& v, T s)
{
  Static_vector<T,N> ans = v;
  ans/=s;
  return ans;
}

template<class T, int N>
std::ostream& operator<<(std::ostream& out, const Static_vector<T,N>& v)
{
  int i;
  for (i=0; i<N; i++) {
      if (i>0) out << '\t';
      out << v[i];
  }
  out << std::endl;

  return out;
}

#endif
