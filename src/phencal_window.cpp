/*
 phencal_window.cpp

 An FLTK dialog for the phencal program.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2020  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <string>
#include <sstream>
#include <FL/Fl_Group.H>
#include <FL/Fl_Box.H>
#include "main_window.h"
#include "phencal_window.h"

using namespace splits;

Phencal_window::Phencal_window(Main_window* MW)
  : Fl_Double_Window(620,400, "Phenology (phencal)"), mw(MW), wrt_idx(false)
{
  {
    Fl_Group *g = new Fl_Group(0,10,620,25);
    spl_file_inp = new Dataset_input(175,10,425,25, "Spline File:",
				     Dataset_input::SINGLE);
    spl_file_inp->callback(cb, this);
    spl_file_inp->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);
    g->end();
    g->resizable(spl_file_inp);
  }  
  {
    Fl_Group *g = new Fl_Group(0,40,620,25);

    Fl_Group *h1 = new Fl_Group(0,40,310,25);
    start_inp = new Date_input(175,40,160,25, "Start Date:");
    start_inp->callback(cb, this);
    start_inp->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);
    h1->end();
    h1->resizable(start_inp);

    Fl_Group *h2 = new Fl_Group(310,40,310,25);
    end_inp = new Date_input(440,40,160,25, "End Date:");
    end_inp->callback(cb, this);
    end_inp->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);
    h2->end();
    h2->resizable(end_inp);

    g->end();
  }
  {
    Fl_Group *g = new Fl_Group(0,70,620,25);
    green_inp = new Real_input(175,70,425,25, "Greenness Parameter:");
    green_inp->callback(cb, this);
    green_inp->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);
    g->end();
    g->resizable(green_inp);
  }
  {
    Fl_Group *g = new Fl_Group(0,100,620,25);
    index_btn = new Fl_Check_Button(175,100,420,25,
                    "Write Day Index Instead of DOY for Phenological Markers");
    index_btn->callback(cb, this);
    Fl_Box *b = new Fl_Box(595,100,5,25);
    g->end();
    g->resizable(b);
  }
  {
    Fl_Group *g = new Fl_Group(0,130,620,25);
    out_file_inp = new Dataset_input(175,130,425,25, "Output File:",
				     Dataset_input::CREATE);
    out_file_inp->callback(cb, this);
    out_file_inp->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);
    g->end();
    g->resizable(out_file_inp);
  }
  {
    Fl_Group *g = new Fl_Group(0,165,620,225);
    term = new Terminal(10,165,600,225,"phencal");
    g->end();
  }
  resizable(term);

  callback(window_cb);

  set_modal();
  hotspot(this);
  show();
}

void Phencal_window::update()
{
  std::stringstream s;
  std::string arguments = "";

  if (!start_inp->is_empty() && !end_inp->is_empty()) {
    arguments += " -C ";
    s << start_inp->date() << "," << end_inp->date();
    arguments += s.str();
  }

  if (!green_inp->is_empty()) {
    arguments += " -g ";
    s.str("");
    s << green_inp->number();
    arguments += s.str();
  }

  if (index_btn->value()) {
    arguments += " -i";
  }

  if (mw->nthread>1) {
    s.str("");
    s << " -M " << mw->nthread;
    arguments += s.str();
  }

  if (mw->quiet) {
    arguments += " -Q";
  }

  if (mw->current>=0 && mw->data[mw->current].is_southern()) {
    arguments += " -S";
  }

  if (!spl_file_inp->is_empty()) {
    arguments += " ";
    arguments += spl_file_inp->filename();
  }

  if (!out_file_inp->is_empty()) {
    arguments += " ";
    arguments += out_file_inp->filename();
  }

  term->set_args(arguments.c_str());
}

void Phencal_window::cb(Fl_Widget* w, void* data)
{
  Phencal_window *win = (Phencal_window*)data;
  win->update();
}

void Phencal_window::window_cb(Fl_Widget* w, void* data)
{
  Phencal_window* win = (Phencal_window*)w;
  if (win->term->is_busy()) return;
  Fl::default_atclose(win, data);
}
