/*
 meta_window.cpp

 FLTK dialog to handle metaheaders in splview.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <FL/Fl_Box.H>
#include "dataset.h"
#include "meta_window.h"

using namespace splits;

Meta_window::Meta_window()
  : Fl_Window(550,110, "Materialize Metaheader"), ok(false)
{
  {
    Fl_Group *g = new Fl_Group(0,10,550,25);
    meta_inp = new Dataset_input(160,10,375,25, "Phenology Metaheader:",
				 Dataset_input::SINGLE);
    g->end();
    g->resizable(meta_inp);
  }
  {
    Fl_Group *g = new Fl_Group(0,40,550,25);
    outp_inp = new Dataset_input(160,40,375,25, "Material Output File:",
				 Dataset_input::CREATE);
    g->end();
    g->resizable(outp_inp);
  }
  Fl_Group *g = new Fl_Group(0,75,550,25);
  Fl_Box *b = new Fl_Box(0,75,370,25);
  Fl_Return_Button *ok_btn = new Fl_Return_Button(370,75,80,25, "OK");
  ok_btn->callback(ok_cb, this);
  Fl_Button *cancel_btn = new Fl_Button(455,75,80,25, "Cancel");
  cancel_btn->callback(cancel_cb, this);
  g->end();
  g->resizable(b);

  meta_inp->callback(meta_cb, (void*)outp_inp);
  meta_inp->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY);

  resizable(this);
  size_range(550,110,0,110);

  set_modal();
  hotspot(this);

  show();
}

void Meta_window::ok_cb(Fl_Widget* w, void* data)
{
  Meta_window *win = (Meta_window*)data;
  win->set_okay();
  win->hide();
}

void Meta_window::cancel_cb(Fl_Widget* w, void* data)
{
  Meta_window* win = (Meta_window*)data;
  win->hide();
}

void Meta_window::meta_cb(Fl_Widget* w, void* data)
{
  Dataset_input *inp = (Dataset_input*)w;
  Dataset_input *otp = (Dataset_input*)data;
  Meta_dataset set(inp->filename());
  if (set.is_good() && otp->is_empty()) {
    std::string s = set.suggest_file();
    otp->set_filename(s.c_str());
  }
}
