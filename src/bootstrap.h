/*
 bootstrap.h

 Bootstrap methods to select spline parameters.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPLITS_BOOTSTRAP_H
#define SPLITS_BOOTSTRAP_H

#include <vector>
#define ARMA_NO_DEBUG
#define ARMA_DONT_PRINT_ERRORS
#include <armadillo>

namespace splits {

double mse(const arma::fvec& y0, const arma::fvec& y1, int df);
double mse(const arma::fvec& y0, const arma::fvec& y1,
	   const arma::fvec& w,int df);
 
float rss(const arma::fvec& y0, const arma::fvec& y1);

float rss(const arma::fvec& y0, const arma::fvec& y1, const arma::fvec& w);


float cv(const arma::fmat& x, const arma::fvec& y0, const arma::fvec& y1);

float cv(const arma::fmat& x, const arma::fvec& y0, const arma::fvec& y1,
	 int k);

float cv(const arma::fmat& x, const arma::fvec& y0, const arma::fvec& y1,
	 const arma::fmat& w);

float cv(const arma::fmat& x, const arma::fvec& y0, const arma::fvec& y1,
	 const arma::fmat& w, int k);


int select_num_pieces(int l, int r, int s,
		      float a, float b,
		      int d, bool peri,
		      const arma::fvec& xt, const arma::fvec& yt,
		      bool ignore,
		      int* num, float* score,
		      void (*progress)(float,void*)=0, void* data=0);

int select_num_pieces(int l, int r, int s,
		      float a, float b,
		      int d, bool peri,
		      const arma::fvec& xt, const arma::fvec& yt,
		      const arma::fmat& w,
		      bool ignore,
		      int* num, float* score,
		      void (*progress)(float,void*)=0, void* data=0);


typedef struct pieces_data
{
  float a, b;
  int d;
  bool peri, ignore;
  arma::fvec xt, yt;
  arma::fmat wt;
} pieces_data_t;

float pieces(float n, void* p);


int min_index(float* arr, int size);

} //namespace splits

#endif
