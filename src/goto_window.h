/*
 goto_window.h

 An FLTK window to specify image or map coordinates to go to.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2016  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_GOTO_WINDOW_H
#define SPLITS_GOTO_WINDOW_H

#include <FL/Fl_Window.H>
#include <FL/Fl_Toggle_Button.H>
#include <FL/Fl_Float_Input.H>
#include "static_vector.h"

namespace splits {

class Main_window;

class Goto_window : public Fl_Window
{
  Fl_Toggle_Button *pix_btn, *map_btn;
  Fl_Float_Input *inp1, *inp2;
  Main_window *mw;

public:
  Goto_window(Main_window* MW);
  ~Goto_window() {}

  void update();

  void reset();

  void pixel_pos(double* pos);

  static void pix_btn_cb(Fl_Widget* w, void* data);
  static void map_btn_cb(Fl_Widget* w, void* data);
  static void input_cb(Fl_Widget* w, void* data);
};

} //namespace splits

#endif
