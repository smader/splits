/*
 weight.h

 Support functions for weighted least squares fits of spline models.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPLITS_WEIGHT_H
#define SPLITS_WEIGHT_H

#define ARMA_NO_DEBUG
#include <armadillo>
#include "arbitrary_vector.h"

namespace splits {

Arbitrary_vector<float> read_weight_file(const char* fname);

int read_weight_file(const char* fname, arma::fmat& wt);

void load_weights(short int* buffer,
		  int bufsize,
		  Arbitrary_vector<float> lut,
		  arma::fvec& weights);

void safe_load_weights(short int* buffer,
		       int bufsize,
		       Arbitrary_vector<float> lut,
		       arma::fvec& weights);

} //namespace splits

#endif
