/*
 splcal.cpp

 Program to evaluate polynomial splines fitted to a time series image.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2020  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <gdal_priv.h>
#include <cpl_string.h>
#include <cpl_multiproc.h>
#define ARMA_NO_DEBUG
#include <armadillo>
#include "clargp.h"
#include "common.h"
#include "dataset.h"
#include "time_axis.h"
#include "spline.h"
#include "b_spline.h"

using namespace arma;
using namespace splits;

GDALDataType outtype;
const char *in_file, *ot_file, *sites_file;
volatile int xSize, ySize, count, site_count;
int degree, num_knots, dim_basis, deriv;
bool peri;
fvec xt, xt0, yh, coefs;
float *knots;
float ot_nodata = 0.0f;
float ot_offset = 0.0f, ot_gain = 1.0f;
Date d_beg, d_end;
bool verbose = true;
bool raw = false;
int nthread = 1;
void *mutex;
static volatile int line = 0;
static volatile int pending = 1;

ClArg_t arglst[] =
{
  string_arg("A",0),        //abscissae where to evaluate the spline
  string_arg("a",0),
  strlist_arg("C", 0),
  int_arg("D",0),            //which derivative to evaluate
  int_arg("M",0),            //Number of threads in multiprocessing
  real_arg("Nn",0),          //output nodata value (default 0)
  reallist_arg("Ll",0),      //output linear coefficients
  flag_arg("Qq",0),          //quiet
  string_arg("t",0),         //data type (one of Byte,Integer,Real)
  pos_arg("in_file"),
  pos_arg("ot_file"),
  last_arg
};

struct Buffer
{
  float *input;
  float *output;
  Spline *spline;

  Buffer() : input(0), output(0), spline(0) {}

  Buffer(float* in, int off1, float* ot, int off2, Spline* s)
  {
    input = in+off1;
    output = ot+off2;
    spline = s;
  }

  ~Buffer() {}
};

static void worker_func(void* p)
{
  Buffer *buf = (Buffer*)p;

  B_spline *bspl = (B_spline*)(buf->spline);

  int i;
  CPLAcquireMutex(mutex, 1000.0);
  i = line++;
  CPLReleaseMutex(mutex);

  GDALDataset *in_dset;
  CPLAcquireMutex(mutex, 1000.0);
  in_dset = (GDALDataset*) GDALOpen(in_file, GA_ReadOnly);
  CPLReleaseMutex(mutex);

  in_dset->RasterIO(GF_Read,
		    0,i,xSize,1,
		    buf->input,xSize,1,
		    GDT_Float32,
		    count,
		    NULL,
		    count*sizeof(float),
		    count*xSize*sizeof(float),
		    sizeof(float));

  CPLAcquireMutex(mutex, 1000.0);
  GDALClose(in_dset);
  CPLReleaseMutex(mutex);

  int j, k;
  for (j=0; j<xSize; j++) {
    fvec yh;
    fvec coefs = fvec((const float*)(buf->input+j*count), count);
    bool nonzero = false;
    for (k=0; k<coefs.n_elem; k++) {
      if (coefs[k]!=(float)0) nonzero = true;
      break;
    }
    if (nonzero) {
      bspl->coef(coefs);
      yh = bspl->eval(xt, deriv);
      yh = ot_offset + ot_gain*yh;
    } else {
      yh = fvec(site_count);
      yh.fill(ot_nodata);
    }
    for (k=0; k<site_count; k++) {
      buf->output[j*site_count+k] = yh[k];
    }
  }

  GDALDataset *ot_dset;
  CPLAcquireMutex(mutex, 1000.0);
  ot_dset = (GDALDataset*) GDALOpen(ot_file, GA_Update);

  ot_dset->RasterIO(GF_Write,
		    0,i,xSize,1,
		    buf->output,xSize,1,
		    GDT_Float32,
		    site_count,
		    NULL,
		    site_count*sizeof(float),
		    site_count*xSize*sizeof(float),
		    sizeof(float));

  GDALClose(ot_dset);
  CPLReleaseMutex(mutex);

  CPLAcquireMutex(mutex, 1000.0);
  pending--;
  CPLReleaseMutex(mutex);
}

int main(int argc, char *argv[])
{
  GDALAllRegister();
  CPLSetErrorHandler(CPLQuietErrorHandler);

  mutex = CPLCreateMutex();
  CPLReleaseMutex(mutex);

  parse_cl_args(argc, argv, arglst);

  int argsiz;
  void *parg;

  sites_file = (const char*)get_cl_arg(arglst, "A", &argsiz);

  parg = get_cl_arg(arglst, "C", &argsiz);
  if (parg) {
    if (argsiz!=2)
      bail_out("Argument -C is invalid! Exit.");
    char **papch = (char**)parg;
    d_beg = Date(papch[0]);
    d_end = Date(papch[1]);
  } else {
    if (sites_file!=0)
      bail_out("Argument -A is given without specifying -C! Exit.");
  }

  parg = get_cl_arg(arglst, "a", &argsiz);
  if (parg && !sites_file) {
    sites_file = (const char*)parg;
    raw = true;
  }

  parg = get_cl_arg(arglst, "D", &argsiz);
  if (parg) deriv = *(int*)parg; else deriv = 0;

  parg = get_cl_arg(arglst, "M", &argsiz);
  if (parg) {
    nthread = *(int*)parg;
    pending = nthread;
  }

  parg = get_cl_arg(arglst, "Nn", &argsiz);
  if (parg) ot_nodata = *(float*)parg;

  parg = get_cl_arg(arglst, "Ll", &argsiz);
  if (argsiz==2) {
    float *pfloat = (float*)parg;
    ot_offset = pfloat[0];
    ot_gain = pfloat[1];
  }

  parg = get_cl_arg(arglst, "Qq", &argsiz);
  if (argsiz>0) verbose = false;

  parg = get_cl_arg(arglst, "t", &argsiz);
  if (parg)
    outtype = get_data_type((const char*)parg);
  else
    outtype = GDT_Float32;

  in_file = (const char*)get_cl_arg(arglst, "in_file", &argsiz);
  ot_file = (const char*)get_cl_arg(arglst, "ot_file", &argsiz);

  Dataset d(in_file);

  knots = d.read_real_set("knot vector", &num_knots);
  if (knots==0) bail_out("Unable to get knot vector from header file! Exit.");

  if (!d.read_int("degree", &degree))
    bail_out("Unable to get degree from header file! Exit.");

  dim_basis = num_knots - (degree+1);

  xt0 = d.read_real_set("sites");

  int iper;
  d.read_int("periodic",&iper);
  if (iper==1) peri=true; else peri=false;

  if (sites_file!=0 && raw) {
    xt = load_reals(sites_file);
  } else if (sites_file!=0) {
    std::vector<Date> sites = load_dates(sites_file);
    if (sites.empty())
      bail_out("Unable to read DOY file (option -A)! Exit.");
    float xmn = xt0[0];
    float xmx = xt0[xt0.n_elem-1];
    xt = make_scaled_time_axis(sites, d_beg, d_end, xmn, xmx);
  } else {
    xt = xt0;
  }
  site_count = xt.n_elem;

  GDALDataset *in_dset = (GDALDataset*) GDALOpen(in_file,GA_ReadOnly);
  if (!in_dset) bail_out("Unable to open dataset for input! Exit.");
  xSize = in_dset->GetRasterXSize();
  ySize = in_dset->GetRasterYSize();
  count = in_dset->GetRasterCount();

  if (count!=dim_basis) {
    GDALClose(in_dset);
    bail_out("Invalid spline parameters in input dataset! Exit.");
  }

  GDALDriver *drv = (GDALDriver*) GDALGetDriverByName("ENVI");

  char **opts = NULL;
  opts = CSLSetNameValue(opts, "INTERLEAVE", "BIL");

  GDALDataset *ot_dset = drv->Create(ot_file,xSize,ySize,site_count,
				     outtype,opts);
  if (!ot_dset) {
    GDALClose(in_dset);
    bail_out("Unable to create output dataset! Exit.");
  }

  ot_dset->SetProjection(in_dset->GetProjectionRef());
  double adfGeoTransform[6];
  if (in_dset->GetGeoTransform(adfGeoTransform) == CE_None)
    ot_dset->SetGeoTransform(adfGeoTransform);

  GDALClose(ot_dset);
  GDALClose(in_dset);

  float *ibuf = new float[nthread*xSize*count];
  float *obuf = new float[nthread*xSize*site_count];

  int offs_in = xSize*count;
  int offs_ot = xSize*site_count;

  Spline **spl = new Spline*[nthread];

  int j;
  for (j=0; j<nthread; j++) {
    spl[j] = new B_spline(knots,dim_basis,degree+1,peri);
  }

  Buffer *buffers = new Buffer[nthread];
  for (j=0; j<nthread; j++) {
    buffers[j] = Buffer(ibuf, j*offs_in, obuf, j*offs_ot, spl[j]);
  }

  int i, n;
  for (i=0; i<ySize; i+=nthread) {
    if ((i+nthread)>=ySize)
      n = ySize-i;
    else
      n = nthread;
    pending = n;
    for (j=0; j<n; j++) {
      CPLCreateThread(worker_func, (void*)&buffers[j]);
    }
    while (pending > 0) CPLSleep(0.5);
    if (verbose) report_progress(nthread, ySize);
  }

  delete [] buffers;

  delete [] ibuf;
  delete [] obuf;

  delete [] spl;

  return 0;
}
