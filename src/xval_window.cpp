/*
 xval_window.cpp

 Cross validation to estimate spline parameters (e.g. smoothing,
 number of knots).

 The SPLITS splview program to fit and visualize spline models to
 remotely sensed time series images.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2016  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <cstdio>
#include <cstring>
#include <FL/Fl_Box.H>
#include <FL/Fl_File_Chooser.H>
#include "xval_window.h"
#include "bootstrap.h"
#include "golden.h"
#include "main_window.h"
#include "spline_window.h"
#include "weight_window.h"

using namespace std;
using namespace arma;
using namespace splits;

std::string Xval_window::dir = ".";

Xval_window::Xval_window(Main_window* MW)
  : Fl_Window(400,335,"Cross Validation"), mw(MW)
{
  {
    Fl_Group *g = new Fl_Group(0,10,400,25);
    Fl_Group *h1 = new Fl_Group(0,10,195,25);
    domain_inp1 = new Real_input(110,10,85,25, "Lower Bound:");
    h1->end();
    h1->resizable(domain_inp1);

    Fl_Group *h2 = new Fl_Group(195,10,205,25);
    domain_inp2 = new Real_input(305,10,85,25, "Upper Bound:");
    h2->end();
    h2->resizable(domain_inp2);
    g->end();
  }
  {
    Fl_Group *g = new Fl_Group(0,40,400,25);
    tol_inp = new Real_input(110,40,280,25, "Tolerance:");
    g->end();
    g->resizable(tol_inp);
  }
  {
    Fl_Group *g = new Fl_Group(0,70,400,25);
    Fl_Box *b = new Fl_Box(350,70,40,25);
    gold_btn = new Fl_Round_Button(220,70,130,25, "Golden Section");
    gold_btn->type(FL_RADIO_BUTTON);
    gold_btn->callback(round_btn_cb, this);
    grid_btn = new Fl_Round_Button(110,70,110,25, "Grid Search");
    grid_btn->type(FL_RADIO_BUTTON);
    grid_btn->callback(round_btn_cb, this);
    g->end();
    g->resizable(b);
  }
  {
    Fl_Group *g = new Fl_Group(0,100,400,25);
    iter_inp = new Real_input(110,100,280,25, "Iterations:");
    g->end();
    g->resizable(iter_inp);
  }
  {
    Fl_Group *g = new Fl_Group(0,130,400,25);
    ignore_btn = new Fl_Check_Button(110,130,140,25, "Ignore Ends");
    Fl_Box *b = new Fl_Box(250,130,140,25);
    g->end();
    g->resizable(b);
  }
  {
    Fl_Group *g = new Fl_Group(0,160,400,25);
    progress = new Fl_Progress(10,160,380,25);
    progress->minimum(0);
    progress->maximum(1);
    progress->value(0);
    g->end();
    g->resizable(progress);
  }

  outp = new Fl_Multiline_Output(10,190,380,100);

  {
    Fl_Group *g = new Fl_Group(0,300,400,25);
    Fl_Box *b = new Fl_Box(10,300,220,25);
    run_btn = new Fl_Button(350,300,40,25, "Run");
    reset_btn = new Fl_Button(285,300,60,25, "Reset");
    save_btn = new Fl_Button(230,300,50,25, "Save");
    save_btn->callback(save_cb, this);
    g->end();
    g->resizable(b);
  }

  resizable(outp);

  grid_btn->setonly();
  iter_inp->deactivate();

  set_non_modal();
}

void Xval_window::search_domain(float* mn, float* mx)
{
  *mn = domain_inp1->number();
  *mx = domain_inp2->number();
}

int Xval_window::search_type()
{
  if (grid_btn->value()==1) return GRID_SEARCH; else return GOLDEN_SECTION;
}

void Xval_window::add_result(const char* str)
{
  char *buf = (char*)malloc(strlen(outp->value()) + strlen(str) +1);
  strcpy(buf, outp->value());
  strcat(buf, str);
  outp->value(buf);
  free(buf);
}

void Xval_window::erase()
{
  progress->value(0);
  outp->value("");
  Fl::check();
}

void Xval_window::enable(bool b)
{
  if (b) run_btn->activate(); else run_btn->deactivate();
}

void Xval_window::start()
{
  run_btn->deactivate();
  reset_btn->deactivate();
  Fl::check;
}

void Xval_window::finish()
{
  run_btn->activate();
  reset_btn->activate();
  Fl::check;
}

void Xval_window::report(float fraction, void* data)
{
  Xval_window *win = (Xval_window*)data;
  win->progress->value(fraction);
  Fl::check();
}

void Xval_window::round_btn_cb(Fl_Widget* w, void* data)
{
  Xval_window *win = (Xval_window*)data;

  if (win->gold_btn->value()==1) {
    win->iter_inp->activate();
  } else if (win->grid_btn->value()==1) {
    win->iter_inp->deactivate();
  }
}

void Xval_window::save_cb(Fl_Widget* w, void* data)
{
  Xval_window *win = (Xval_window*)data;

  Fl_File_Chooser chooser(dir.c_str(), "*", Fl_File_Chooser::CREATE, "File?");
  chooser.preview(0);
  const char *val = chooser.value();
  if (val!=NULL) chooser.value(val);
  chooser.show();
  while (chooser.shown()) Fl::wait();
  if (chooser.value() != NULL) {
    std::string fname = chooser.value();
    const char *name = fl_filename_name(fname.c_str());
    if (name!=NULL) {
      int pos = fname.find(name);
      std::string str = fname.substr(0, pos-1);
      if (fl_filename_isdir(str.c_str())>0) win->dir = str;

      FILE *fp = fopen(fname.c_str(), "wt");
      if (fp==NULL) {
	fl_alert("Error saving result!");
      } else {
	fprintf(fp, "%s", win->result());
	fclose(fp);
      }
    }
  }
}


Span_xval_window::Span_xval_window(Main_window* MW) : Xval_window(MW)
{
  label("Cross Validation # of Spans");
  reset_btn->callback(reset_cb, this);
  run_btn->callback(run_cb, this);
  domain_inp1->tooltip("Number of spans (lowest)");
  domain_inp2->tooltip("Number of spans (highest)");
}

void Span_xval_window::reset()
{
  domain_inp1->set_number(1);
  domain_inp2->set_number(mw->data[mw->current].count()-1);
}

void Span_xval_window::reset_cb(Fl_Widget* w, void* data)
{
  Span_xval_window *win = (Span_xval_window*)data;
  win->reset();
}

void Span_xval_window::run_cb(Fl_Widget* w, void* data)
{
  Span_xval_window *win = (Span_xval_window*)data;

  bool peri;
  if (win->mw->knot_type[win->mw->current] == Spline_window::PERIODIC)
    peri = true;
  else
    peri = false;

  float l, r;
  win->search_domain(&l, &r);

  float eps = win->tolerance();
  
  win->erase();

  if (win->search_type()==Xval_window::GRID_SEARCH) {

    int ll = (int)l;
    int rr = (int)r;
    if (ll<1 || rr>win->mw->data[win->mw->current].count()-1) return;

    int stp = (int)eps;
    if (stp<1) return;

    int *num = new int[(rr-ll+1)/stp];
    float *score = new float[(rr-ll+1)/stp];

    win->start();

    int m;

    if (win->mw->wt.is_empty() ||
	!win->mw->ww->use() ||
	win->mw->ww->mode()==Weight_window::OFF) {

      m = select_num_pieces(ll,
			    rr,
			    stp,
			    0.0f,
			    1.0f,
			    win->mw->degree[win->mw->current],
			    peri,
			    win->mw->xt,
			    win->mw->yt,
			    (bool)(win->ignore_btn->value()),
			    num,
			    score,
			    Xval_window::report,
			    win);
    } else {

      m = select_num_pieces(ll,
			    rr,
			    stp,
			    0.0f,
			    1.0f,
			    win->mw->degree[win->mw->current],
			    peri,
			    win->mw->xt,
			    win->mw->yt,
			    win->mw->wt,
			    (bool)(win->ignore_btn->value()),
			    num,
			    score,
			    Xval_window::report,
			    win);
    }

    win->finish();

    if (m>0) {
      int i;
      for (i=0; i<m; i++) {
	char str[128];
	sprintf(str, "%d\t%g\n", num[i], score[i]);
	win->add_result(str);
      }
      win->mw->sw->set_nspan(num[min_index(score, m)]);
      win->mw->sw->nspan_cb(0,win->mw->sw);
    }

    delete [] num;
    delete [] score;

  } else {

    int k = win->iterations();
    if (k<=0) return;

    pieces_data_t *o = new pieces_data_t;

    o->a = 0.0f;
    o->b = 1.0f;
    o->d = win->mw->degree[win->mw->current];
    o->peri = peri;
    o->ignore = (bool)(win->ignore_btn->value());
    o->xt = win->mw->xt;
    o->yt = win->mw->yt;
 
    if (win->mw->wt.is_empty() ||
	!win->mw->ww->use() ||
	win->mw->ww->mode()==Weight_window::OFF) {
      o->wt = fmat();
    } else {
      o->wt = win->mw->wt;
    }

    float *xa = new float[k];
    float *xb = new float[k];
    float *y = new float[k];

    win->start();

    int m = golden(pieces, o,
		   l, r,
		   xa, xb, y,
		   k, 1.0f,
		   Xval_window::report, win);

    win->finish();

    int i;
    for (i=0; i<m; i++) {
      char str[128];
      sprintf(str,
	      "%d\t%g\t%d\t%d\n",
	      (int)((xa[i]+xb[i])/2.0f + 0.5f),
	      y[i],
	      (int)(xa[i]+0.5f),
	      (int)(xb[i]+0.5f));
      win->add_result(str);
    }

    if (m>0) {
      i = m-1;
      win->mw->sw->set_nspan((int)((xa[i]+xb[i])/2.0f + 0.5f));
      win->mw->sw->nspan_cb(0,win->mw->sw);
    }

    delete [] xa;
    delete [] xb;
    delete [] y;

    delete o;
  }
}
