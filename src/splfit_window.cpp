/*
 splfit_window.cpp

 An FLTK dialog for the splfit program.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2016  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include <string>
#include <sstream>
#include "main_window.h"
#include "spline_window.h"
#include "weight_window.h"
#include "splfit_window.h"

using namespace splits;

Splfit_window::Splfit_window(Main_window* MW)
  : Fl_Double_Window(620,305, "Fit (splfit)"), mw(MW)
{
  {
    Fl_Group *g = new Fl_Group(0,10,620,25);
    out_file_inp = new Dataset_input(110,10,495,25, "Output File:",
				     Dataset_input::CREATE);
    
    out_file_inp->callback(cb, this);
    g->end();
    g->resizable(out_file_inp);
  }
  {
    Fl_Group *g = new Fl_Group(0,40,620,25);
    rms_file_inp = new Dataset_input(110,40,495,25, "RMSE File:",
				     Dataset_input::CREATE);
    rms_file_inp->callback(cb, this);
    g->end();
    g->resizable(rms_file_inp);
  }
  {
    Fl_Group *g = new Fl_Group(0,75,620,220);
    term = new Terminal(10,75,600,220, "splfit");
    g->end();
    g->resizable(term);
  }
  resizable(term);

  callback(window_cb);

  set_modal();
  hotspot(this);
  show();
  update();
}

void Splfit_window::update()
{
  if (mw->current<0) return;

  char **files = mw->data[mw->current].file_list();

  std::string arguments = "";

  arguments += "-a ";
  arguments += files[1];

  arguments += " -C ";
  std::stringstream s;
  s << mw->data[mw->current].begin();
  arguments += s.str();
  arguments += ",";
  s.str("");
  s << mw->data[mw->current].end();
  arguments += s.str();
  
  arguments += " -d ";
  s.str("");
  s << mw->degree[mw->current];
  arguments += s.str();
 
  if (!rms_file_inp->is_empty()) {
    arguments += " -e ";
    arguments += rms_file_inp->filename();
  }

  switch (mw->knot_type[mw->current]) {
  case Spline_window::UNIFORM:
  case Spline_window::PERIODIC:
    arguments += " -k ";
    s.str("");
    s << mw->nspan[mw->current];
    arguments += s.str();
    break;
  default:
    break;
  }

  if ((mw->data[mw->current].offset()!=0.0f)||(mw->data[mw->current].gain()!=1.0f)) {
    arguments += " -l ";
    s.str("");
    s << mw->data[mw->current].offset() << "," << mw->data[mw->current].gain();
    arguments += s.str();
  }

  if (mw->nthread>1) {
    s.str("");
    s << " -M " << mw->nthread;
    arguments += s.str();
  }

  if (mw->data[mw->current].nodata()!=0.0f) {
    arguments += " -n ";
    s.str("");
    s << mw->data[mw->current].nodata();
    arguments += s.str();
  }

  switch (mw->spline_type[mw->current]) {
  case Spline_window::P_SPLINE:
    s.str("");
    s << " -o " << mw->pord[mw->current];
    arguments += s.str();
    break;
  default:
    break;
  }

  switch (mw->knot_type[mw->current]) {
  case Spline_window::AVERAGE_PERIODIC:
  case Spline_window::PERIODIC:
    arguments += " -p";
    break;
  default:
    break;
  }

  switch (mw->spline_type[mw->current]) {
  case Spline_window::SMOOTH_B_SPLINE:
  case Spline_window::P_SPLINE:
    s.str("");
    s << " -s " << mw->lambda[mw->current];
    arguments += s.str();
    break;
  default:
    break;
  }

  if (mw->quiet) {
    arguments += " -Q";
  }

  /*if ((!mw->w_file.empty())&&(mw->ww->use())) {
    if ((!mw->w_tab_file.empty())&&mw->wt_dset) {
      s.str("");
      s << " -W " << mw->w_file << "," << mw->w_tab_file;
      arguments += s.str();
    } else {
      s.str("");
      s << " -w " << mw->w_file;
      arguments += s.str();
    }
  }*/

  arguments += " ";
  arguments += files[0];

  if (!out_file_inp->is_empty()) {
    arguments += " ";
    arguments += out_file_inp->filename();
  }

  term->set_args(arguments.c_str());

  int i;
  for (i=0; i<3; i++) delete [] files[i];
  delete [] files;
}

void Splfit_window::cb(Fl_Widget* w, void* data)
{
  Splfit_window *win = (Splfit_window*)data;
  win->update();
}

void Splfit_window::window_cb(Fl_Widget* w, void* data)
{
  Splfit_window* win = (Splfit_window*)w;
  if (win->term->is_busy()) return;
  Fl::default_atclose(win, data);
}
