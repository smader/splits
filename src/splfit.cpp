/*
 splfit.cpp

 Program to fit a polynomial spline model to a time series image.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2020  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cfloat>
#include <iostream>
#include <vector>
#include <gdal_priv.h>
#include <cpl_string.h>
#include <cpl_multiproc.h>
#define ARMA_NO_DEBUG
#include <armadillo>
#include "clargp.h"
#include "arbitrary_vector.h"
#include "date.h"
#include "time_axis.h"
#include "weight.h"
#include "common.h"
#include "knot.h"
#include "b_spline.h"
#include "dataset.h"
#include "bootstrap.h"

using namespace arma;
using namespace splits;

const char *in_file, *ot_file, *x_file, *w_file=NULL, *e_file=NULL;
const char *k_file=NULL;
volatile int xSize, ySize, count;
std::vector<Date> xt_dates;
fvec xt;
float in_nodata = 0.0f;
float in_offset = 0.0f, in_gain = 1.0f;
int degree, dim_basis, nspan = -1;
float *knots;
bool peri = false;
float lambda=-999, pord=-999;
fvec coefs;
fmat wt;
Arbitrary_vector<float> w_tab;
Date d_beg, d_end;
int nthread = 1;
bool verbose = true;
void *mutex;
static volatile int line = 0;
static volatile int pending = 1;

ClArg_t arglst[] =
{
  string_arg("a", 1),        //Axis file (DOY image or list of dates)
  strlist_arg("C",0),        //Begin and end dates
  int_arg("d",1),            //Polynomial degree
  string_arg("e",0),         //File name for error output
  int_arg("k",0),            //Knots: number of spans
  string_arg("K",0),         //Knots: read nonuniform from file
  reallist_arg("l",0),       //Input linear coefficients
  int_arg("M",0),            //Number of threads in multiprocessing
  real_arg("n",0),           //Input nodata value (default 0)
  flag_arg("p",0),           //Periodic basis
  real_arg("s",0),           //Smoothness
  real_arg("o",0),           //Penalty order (for P-splines)
  flag_arg("Qq", 0),         //"quiet"
  string_arg("w",0),         //Weight vector file
  strlist_arg("W",0),        //Quality file and weight table
  pos_arg("in_file"),
  pos_arg("out_file"),
  last_arg
};

struct Buffer
{
  float *input;
  float *doy;
  short int *quality;
  float *output;
  float *rmse;

  Buffer() : input(0), doy(0), quality(0), output(0), rmse(0) {}

  Buffer(float* in, float* dy, short int* qu, int off1, float *ot, int off2,
	 float *rm, int off3)
  {
    input = in+off1;

    if (dy==0) {
      doy = 0;
    } else {
      doy = dy+off1;
    }

    if (qu==0) {
      quality = 0;
    } else {
      quality = qu+off1;
    }

    output = ot+off2;

    if (rm==0) {
      rmse = 0;
    } else {
      rmse = rm+off3;
    }
  }

  ~Buffer() {}
};

static void fit_coef(void* p)
{
  Buffer *buf = (Buffer*)p;

  int i;
  CPLAcquireMutex(mutex, 1000.0);
  i = line++;
  CPLReleaseMutex(mutex);

  GDALDataset *in_dset;
  CPLAcquireMutex(mutex, 1000.0);
  in_dset = (GDALDataset*) GDALOpen(in_file, GA_ReadOnly);
  CPLReleaseMutex(mutex);

  in_dset->RasterIO(GF_Read,
		    0,i,xSize,1,
		    buf->input,xSize,1,
		    GDT_Float32,
		    count,
		    NULL,
		    count*sizeof(float),
		    count*xSize*sizeof(float),
		    sizeof(float));

  CPLAcquireMutex(mutex, 1000.0);
  GDALClose(in_dset);
  CPLReleaseMutex(mutex);

  fvec xt_local;
  fvec wt_local;
  GDALDataset *dy_dset;
  if (buf->doy) {
    CPLAcquireMutex(mutex, 1000.0);
    dy_dset = (GDALDataset*) GDALOpen(x_file, GA_ReadOnly);
    CPLReleaseMutex(mutex);

    dy_dset->RasterIO(GF_Read,
		      0,i,xSize,1,
		      buf->doy,xSize,1,
		      GDT_Float32,
		      count,
		      NULL,
		      count*sizeof(float),
		      count*xSize*sizeof(float),
		      sizeof(float));

    CPLAcquireMutex(mutex, 1000.0);
    GDALClose(dy_dset);
    CPLReleaseMutex(mutex);
  } else {
    xt_local = xt;
  }

  GDALDataset *wt_dset;
  if (buf->quality) {
    CPLAcquireMutex(mutex, 1000.0);
    wt_dset = (GDALDataset*) GDALOpen(w_file, GA_ReadOnly);
    CPLReleaseMutex(mutex);

    wt_dset->RasterIO(GF_Read,
		      0,i,xSize,1,
		      buf->quality,xSize,1,
		      GDT_Int16,
		      count,
		      NULL,
		      count*sizeof(short int),
		      count*xSize*sizeof(short int),
		      sizeof(short int));

    CPLAcquireMutex(mutex, 1000.0);
    GDALClose(wt_dset);
    CPLReleaseMutex(mutex);
  } else {
    wt_local = wt;
  }

  std::vector<Date> ttab;
  fvec yt;

  int j, k, nobs = count;
  for (j=0; j<xSize; j++) {
    for (k=0; k<dim_basis; k++) buf->output[j*dim_basis+k] = 0.0f;

    if (buf->rmse) buf->rmse[j] = 0.0f;

    if (buf->doy) {
      nobs = count;
      for (k=count-1; k>=0; k--) {
	if (fabsf(buf->doy[j*count+k]-in_nodata)>=FLT_EPSILON) break;
	nobs--;
      }
      if (nobs==0) continue;

      fvec doy(nobs);
      for (k=0; k<nobs; k++) doy[k] = buf->doy[j*count+k];
      ttab = gen_timetable(doy, d_beg.year);
      xt_local = make_scaled_time_axis(ttab, d_beg, d_end, 0, 1);
      yt = fvec(nobs);
      for (k=0; k<nobs; k++)
	yt[k] = in_offset + buf->input[j*count+k]*in_gain;
    } else {
      fvec tmp(count);
      for (k=0; k<count; k++) tmp[k] = buf->input[j*count+k];
      //6.6.16: scan_nodata causing segfault, should be obsolete anyway -
      //better use weights and set them to zero instead of modifying all
      //the matrices and vectors
      //ttab = xt_dates;
      //nobs = scan_nodata(ttab, tmp, wt_local, in_nodata);
      //if (nobs==0) continue;
      nobs = count;
      yt = in_offset + tmp*in_gain;
    }

    if (buf->quality) {
      wt_local = fvec(nobs);
      safe_load_weights(buf->quality+j*count, nobs, w_tab, wt_local);
    }

    Spline *spl;
    if ((lambda>0.0f) && (pord>0.0f))
      spl = new P_spline(knots,dim_basis,degree+1,lambda,pord,peri);
    else if (lambda>0.0f)
      spl = new Smooth_spline(knots,dim_basis,degree+1,lambda,peri);
    else
      spl = new B_spline(knots,dim_basis,degree+1,peri);

    bool ok;
    if (wt_local.is_empty()) {
      ok = spl->fit(xt_local,yt);
    } else {
      ok = spl->fit(xt_local,yt,wt_local);
    }

    if (ok) {
      fvec coefs = ((B_spline*)spl)->coef();
      for (k=0; k<dim_basis; k++) buf->output[j*dim_basis+k] = coefs[k];

      if (buf->rmse) {
	fvec ys = ((B_spline*)spl)->eval(xt_local);
	float r;
	if (wt_local.is_empty()) {
	  buf->rmse[j] = sqrt(rss(ys, yt)/(float)yt.n_elem);
	} else {
	  buf->rmse[j] = sqrt(rss(ys, yt, wt_local)/sum(wt_local));
	}
      }
    }

    delete spl;
  }

  GDALDataset *ot_dset, *e_dset;
  CPLAcquireMutex(mutex, 1000.0);
  ot_dset = (GDALDataset*) GDALOpen(ot_file, GA_Update);

  ot_dset->RasterIO(GF_Write,
		    0,i,xSize,1,
		    buf->output,xSize,1,
		    GDT_Float32,
		    dim_basis,
		    NULL,
		    dim_basis*sizeof(float),
		    dim_basis*xSize*sizeof(float),
		    sizeof(float));

  GDALClose(ot_dset);
  if (e_file) {
    e_dset = (GDALDataset*) GDALOpen(e_file, GA_Update);
    e_dset->RasterIO(GF_Write,
		     0,i,xSize,1,
		     buf->rmse,xSize,1,
		     GDT_Float32,
		     1,
		     NULL,
		     sizeof(float),
		     xSize*sizeof(float),
		     sizeof(float));
    GDALClose(e_dset);
  }
  
  CPLReleaseMutex(mutex);

  CPLAcquireMutex(mutex, 1000.0);
  pending--;
  CPLReleaseMutex(mutex);
}

int main(int argc, char* argv[])
{
  GDALAllRegister();
  CPLSetErrorHandler(CPLQuietErrorHandler);

  mutex = CPLCreateMutex();
  CPLReleaseMutex(mutex);

  parse_cl_args(argc, argv, arglst);

  int argsiz;
  void *parg;

  x_file = (const char*)get_cl_arg(arglst, "a", &argsiz);

  parg = get_cl_arg(arglst, "C", &argsiz);
  if (parg) {
    if (argsiz!=2) bail_out("Argument -C is invalid! Exit.");
    char **papch = (char**)parg;
    d_beg = Date(papch[0]);
    d_end = Date(papch[1]);
  }

  degree = *(int*)get_cl_arg(arglst, "d", &argsiz);

  parg = get_cl_arg(arglst, "e", &argsiz);
  if (parg) {
    e_file = (const char*)parg;
  }

  parg = get_cl_arg(arglst, "K", &argsiz);
  if (parg) {
    k_file = (const char*)parg;
  }

  parg = get_cl_arg(arglst, "k", &argsiz);
  if (parg) nspan = *(int*)parg;

  parg = get_cl_arg(arglst, "l", &argsiz);
  if (argsiz==2) {
    float *pfloat = (float*)parg;
    in_offset = pfloat[0];
    in_gain   = pfloat[1];
  }

  parg = get_cl_arg(arglst, "M", &argsiz);
  if (parg) {
    nthread = *(int*)parg;
    pending = nthread;
  }

  parg = get_cl_arg(arglst, "n", &argsiz);
  if (parg) in_nodata = *(float*)parg;

  parg = get_cl_arg(arglst, "o", &argsiz);
  if (parg) pord = *(float*)parg;

  get_cl_arg(arglst, "p", &argsiz);
  if (argsiz > 0) peri = true;

  parg = get_cl_arg(arglst, "s", &argsiz);
  if (parg) lambda = *(float*)parg;

  get_cl_arg(arglst, "Qq", &argsiz);
  if (argsiz > 0) verbose = false;

  parg = get_cl_arg(arglst, "W", &argsiz);
  if (parg) {
    if (argsiz == 2) {
      char **papch = (char**)parg;
      w_file = papch[0];
      w_tab = read_weight_file(papch[1]);
      if (w_tab.size()==0) bail_out("Unable to read table of weights! Exit.");
    } else {
      bail_out("Argument -W is invalid! Exit.");
    }
  }

  parg = get_cl_arg(arglst, "w", &argsiz);
  if (parg&&(w_file==NULL)) {              // -W overrides -w
    w_file = (const char*)parg;
  }

  in_file = (const char*)get_cl_arg(arglst, "in_file", &argsiz);
  ot_file = (const char*)get_cl_arg(arglst, "out_file", &argsiz);

  GDALDataset *in_dset;
  in_dset = (GDALDataset*) GDALOpen(in_file, GA_ReadOnly);
  if (!in_dset) bail_out("Unable to open time series file! Exit.");
  xSize = in_dset->GetRasterXSize();
  ySize = in_dset->GetRasterYSize();
  count = in_dset->GetRasterCount();

  GDALDataset *dy_dset=0;
  dy_dset = (GDALDataset*) GDALOpen(x_file, GA_ReadOnly);
  if (dy_dset==NULL) {
    xt_dates = load_dates(x_file);
    if (xt_dates.size()!=count)
      bail_out("Wrong number of entries in DOY file (-a)! Exit.");
    d_beg = xt_dates[0];
    d_end = xt_dates[count-1];
  } else {
    if (dy_dset->GetRasterCount()!=count)
      bail_out("Wrong number of bands in image file %s! Exit.", x_file);
    parg = get_cl_arg(arglst, "C", &argsiz);
    if (!parg)
      bail_out("Missing argument -C! Exit.");
  }

  GDALDataset *wt_dset=0;
  if (w_file!=NULL) {
    if (w_tab.size()>0) {
      wt_dset = (GDALDataset*) GDALOpen(w_file, GA_ReadOnly);
      if (wt_dset==NULL)
	bail_out("Unable to open quality dataset! Exit.");
      if (wt_dset->GetRasterCount()!=count)
	bail_out("Wrong number of bands in image file %s! Exit.", w_file);
    } else {
      int nwt = read_weight_file(w_file, wt);
      if (nwt!=count)
	bail_out("Wrong number of entries in weight file (-w)! Exit.");
    }
  }

  if (k_file) {
    fvec v = load_reals(k_file);
    if (v.n_elem==0) bail_out("Unable to load knots from file (-K)! Exit.");
    if (peri) {
      knots = periodic_knot_vector(v.memptr(), v.n_elem, degree+1, &dim_basis);
    } else {
      knots = clamped_knot_vector(v.memptr(), v.n_elem, degree+1, &dim_basis);
    }
  } else if (nspan==-1) {
    fvec x = linspace<fvec>(0, count-1);
    if (peri) {
      knots = periodic_average_knot_vector(x.memptr(), count, degree+1,
					   &dim_basis);
    } else {
      knots = average_knot_vector(x.memptr(), count, degree+1, &dim_basis);
    }
  } else {
    float a = 0, b = 1;
    if (peri) {
      knots = periodic_uniform_knot_vector(a, b, nspan, degree+1, &dim_basis);
    } else {
      knots = clamped_uniform_knot_vector(a, b, nspan, degree+1, &dim_basis);
    }
  }

  GDALDriver *drv = (GDALDriver*) GDALGetDriverByName("ENVI");

  char **opts = NULL;
  opts = CSLSetNameValue(opts, "INTERLEAVE", "BIL");

  GDALDataset *ot_dset = drv->Create(ot_file, xSize, ySize, dim_basis,
				     GDT_Float32, opts);

  ot_dset->SetProjection(in_dset->GetProjectionRef());
  double adfGeoTransform[6];
  if (in_dset->GetGeoTransform(adfGeoTransform) == CE_None)
    ot_dset->SetGeoTransform(adfGeoTransform);

  GDALClose(ot_dset);

  if (e_file) {
    GDALDataset *e_dset = drv->Create(e_file, xSize, ySize, 1,
				      GDT_Float32, NULL);
    e_dset->SetProjection(in_dset->GetProjectionRef());
    if (in_dset->GetGeoTransform(adfGeoTransform) == CE_None)
      e_dset->SetGeoTransform(adfGeoTransform);

    GDALClose(e_dset);
  }

  // setup the necessary buffers...
  float *ibuf = new float[nthread*xSize*count];
  float *dbuf = 0;
  if (dy_dset) {
    dbuf = new float[nthread*xSize*count];
  } else {   //time axis is const...
    xt = make_scaled_time_axis(xt_dates, d_beg, d_end, 0, 1);
  }
  short int *wbuf = 0;
  if (wt_dset) wbuf = new short int[nthread*xSize*count];
  float *ebuf = 0;
  if (e_file) ebuf = new float[nthread*xSize];
  float *obuf = new float[nthread*xSize*count];

  if (dy_dset) GDALClose(dy_dset);
  if (wt_dset) GDALClose(wt_dset);
  GDALClose(in_dset);

  int offs_in = xSize*count;
  int offs_ot = xSize*dim_basis;

  int j;
  Buffer *buffers = new Buffer[nthread];
  for (j=0; j<nthread; j++) {
    buffers[j] = Buffer(ibuf, dbuf, wbuf, j*offs_in, obuf, j*offs_ot,
			ebuf, j*xSize);
  }

  int i, n;
  for (i=0; i<ySize; i+=nthread) {
    if ((i+nthread)>=ySize)
      n = ySize-i;
    else 
      n = nthread;
    pending = n;
    for (j=0; j<n; j++) {
	CPLCreateThread(fit_coef, (void*)&buffers[j]);
    }
    while (pending > 0) CPLSleep(0.5);
    if (verbose) report_progress(nthread, ySize);
  }

  Dataset d(ot_file);
  if (peri) d.write_int("periodic",1); else d.write_int("periodic",0);
  d.write_int("degree",degree);
  d.write_real_set("knot vector",knots,dim_basis+degree+1);
  if (xt.is_empty()) {
    //Create default sites vector for splines fitted to actual DOY data
    xt = linspace<fvec>(0,1,count);
    d.write_real_set("sites",xt.memptr(),xt.n_elem);
  } else {
    d.write_real_set("sites",xt.memptr(),xt.n_elem);
  }

  delete [] knots;

  delete [] buffers;

  delete [] ibuf;
  delete [] dbuf;
  delete [] wbuf;
  delete [] obuf;
  delete [] ebuf;

  return 0;
}
