/*
 optim_window.h

 Optimize the number of knots based on residual error.

 The SPLITS splview program to fit and visualize spline models to
 remotely sensed time series images.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2017  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_OPTIM_WINDOW_H
#define SPLITS_OPTIM_WINDOW_H

#include <string>
#include <FL/Fl_Window.H>
#include <FL/Fl_Progress.H>
#include <FL/Fl_Multiline_Output.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Round_Button.H>
#include <FL/Fl_Check_Button.H>
#include "real_input.h"

namespace splits {

class Main_window;

class Optim_window : public Fl_Window
{
  static std::string dir;

protected:
  Real_input *domain_inp1, *domain_inp2, *tol_inp, *step_inp;
  Fl_Progress *progress;
  Fl_Multiline_Output *outp;
  Fl_Button *save_btn, *reset_btn, *run_btn;

  Main_window *mw;

public:
  Optim_window(Main_window* MW);
  virtual ~Optim_window() {}

  void search_domain(int* mn, int* mx);

  int step_size();
  
  const char* result() {return outp->value();}
  void add_result(const char* str);

  void erase();

  void enable(bool b=true);

  void start();
  void finish();

  void reset();
  
  static void report(float fraction, void* data);

  static void save_cb(Fl_Widget* w, void* data);
  static void reset_cb(Fl_Widget* w, void* data);
  static void run_cb(Fl_Widget* w, void* data);
};

} //namespace splits

#endif
