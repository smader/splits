/*
 date_input.h

 FLTK input field for calendar dates as specified by the "Date"
 datatype (see "date.h").

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#ifndef SPLITS_DATE_INPUT_H
#define SPLITS_DATE_INPUT_H

#include <FL/Fl_Input.H>
#include "date.h"

namespace splits {

class Date_input : public Fl_Input
{
public:
  Date_input(int X, int Y, int W, int H, const char* l=0);
  ~Date_input() {}

  splits::Date date() const;
  void set_date(const splits::Date& d);

  bool is_empty() const;

  int handle(int e);
};

} //namespace splits

#endif
