/*
 knot.cpp

 Functions to generate knot vectors for B-splines.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "knot.h"

using namespace splits;

float* splits::clamped_knot_vector(float* x, int m, int k, int* size)
{
  int n=m-1;
  int k1=k-1;
  int mk=m+2*k1;
  float* knotv = new float[mk];
  int i;
  for (i=0; i<k1; i++) knotv[i] = x[0];
  for (i=0; i<m; i++) knotv[i+k1] = x[i];
  for (i=m+k1; i<mk; i++) knotv[i] = x[n];
  if (size!=NULL) *size = mk-k;
  return knotv;
}

float* splits::clamped_uniform_knot_vector(float a, float b, int n, int k,
					   int* size)
{
  float delta = (b-a)/(float)n;
  int m=n+1;
  int k1=k-1;
  int mk=m+2*k1;
  float* knotv = new float[mk];
  int i;
  for (i=0; i<k1; i++) knotv[i] = a;
  for (i=0; i<m; i++) knotv[i+k1] = a+(float)i*delta;
  for (i=m+k1; i<mk; i++) knotv[i] = b;
  if (size!=NULL) *size = mk-k;
  return knotv;
}

float* splits::periodic_knot_vector(float* x, int m, int k, int* size)
{
  int n=m-1;
  int k1=k-1;
  int mk=m+2*k1;
  float* knotv = new float[mk];
  int i;
  for (i=0; i<m; i++) knotv[i+k1] = x[i];
  for (i=0; i<k1; i++) knotv[i] = knotv[n+i] - x[n] + x[0];
  for (i=k1; i<(2*k1); i++) knotv[i+m] = knotv[i+1] + x[n] - x[0];
  if (size!=NULL) *size = mk-k;
  return knotv;
}

float* splits::periodic_uniform_knot_vector(float a, float b, int n, int k,
					    int* size)
{
  float delta = (b-a)/(float)n;
  int m=n+1;
  int k1=k-1;
  int mk=m+2*k1;
  float* knotv = new float[mk];
  int i;
  for (i=0; i<m; i++) knotv[i+k1] = a+(float)i*delta;
  for (i=0; i<k1; i++) knotv[i] = knotv[n+i] - b + a;
  for (i=k1; i<(2*k1); i++) knotv[i+m] = knotv[i+1] + b - a;
  if (size!=NULL) *size = mk-k;
  return knotv;
}

float* splits::average_knot_vector(float* x, int nx, int k, int* size)
{
  int m = nx-k+2, m2 = m-2;
  int k1=k-1;
  int mk=m+2*k1;
  float* knotv = new float[mk];
  int i, j;
  for (i=0; i<k; i++) knotv[i] = x[0];
  for (i=0; i<m2; i++) {
    float sum = 0;
    for (j=0; j<k1; j++) sum += x[i+1+j];
    knotv[i+k] = sum/(float)k1;
  }
  for (i=m+k1-1; i<mk; i++) knotv[i] = x[nx-1];
  if (size!=NULL) *size = mk-k;
  return knotv;
}

float* splits::periodic_average_knot_vector(float* x, int nx, int k, int* size)
{
  int m = nx-k+2, m2 = m-2;
  int k1 = k-1;
  int mk = m+2*k1;
  int n=m-1;
  float a=x[0], b=x[nx-1];
  float* knotv = new float[mk];
  int i, j;
  knotv[k1] = a;
  for (i=0; i<m2; i++) {
    float sum = 0;
    for(j=0; j<k1; j++) sum += x[i+1+j];
    knotv[i+k] = sum/(float)k1;
  }
  knotv[m+k1-1] = b;
  for (i=0; i<k1; i++) knotv[i] = knotv[n+i] - b + a;
  for (i=k1; i<(2*k1); i++) knotv[i+m] = knotv[i+1] + b - a;
  if (size!=NULL) *size = mk-k;
  return knotv;
}
