/*
 spline_window.cpp

 FLTK dialog window to edit spline parameters.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2017  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This program is based in part on the work of
 the FLTK project (http://www.fltk.org).
*/

#include "main_window.h"
#include "spline_window.h"

using namespace splits;

Spline_window::Spline_window(Main_window* MW)
  : Fl_Window(240,430,"Spline"), mw(MW)
{
        // spline group
        Fl_Group* g1 = new Fl_Group(10,20,220,180,"Spline:");
    g1->box(FL_BORDER_BOX);
    g1->align(Fl_Align(FL_ALIGN_TOP_LEFT));

        rd_bspline = new Fl_Round_Button(20, 30, 150, 25, "B-Spline");
        rd_bspline->type(FL_RADIO_BUTTON);
        rd_bspline->value(1);
        rd_bspline->callback(spline_button_cb, this );

        rd_smoothspline = new Fl_Round_Button(20, 55, 150, 25, "Smooth B-Spline");
        rd_smoothspline->type(FL_RADIO_BUTTON);
        rd_smoothspline->callback(spline_button_cb, this);

        rd_pspline = new Fl_Round_Button(20, 80, 150, 25, "P-Spline");
        rd_pspline->type(FL_RADIO_BUTTON);
        rd_pspline->callback(spline_button_cb, this);

        ct_degree = new Fl_Spinner(120,110,50,25,"Degree:");
        ct_degree->minimum(0);
	ct_degree->maximum(10);
        ct_degree->value(3);
	ct_degree->callback(degree_cb, this);

        fl_smoothness = new Fl_Float_Input(120,135,60,25,"Smoothness:");
	fl_smoothness->maximum_size(127);
        fl_smoothness->value("0");
        fl_smoothness->deactivate();
	fl_smoothness->callback(smoothness_cb,this);
	fl_smoothness->when(FL_WHEN_ENTER_KEY);

        fl_penalty = new Fl_Float_Input(120,160,60,25,"Penalty Order:");
	fl_penalty->maximum_size(127);
        fl_penalty->value("2");
        fl_penalty->deactivate();
	fl_penalty->callback(penalty_cb,this);
	fl_penalty->when(FL_WHEN_ENTER_KEY);
        g1->end();


        // knot vector group
        g2 = new Fl_Group(10,220,220,200,"Knot Vector:");
        g2->box(FL_BORDER_BOX);
        g2->align(Fl_Align(FL_ALIGN_TOP_LEFT));
	rd_average = new Fl_Round_Button(20, 230, 150, 25, "Average Clamped");
	rd_average->type(FL_RADIO_BUTTON);
	rd_average->callback(knot_button_cb, this);
   
	rd_average_periodic = new Fl_Round_Button(20, 255, 150, 25,
						  "Average Periodic");
	rd_average_periodic->type(FL_RADIO_BUTTON);
	rd_average_periodic->callback(knot_button_cb, this);

        rd_uniform = new Fl_Round_Button(20, 280, 150, 25, "Uniform Clamped");
        rd_uniform->type(FL_RADIO_BUTTON);
	rd_uniform->callback(knot_button_cb, this);
        
        rd_periodic = new Fl_Round_Button(20, 305, 150, 25, "Uniform Periodic");
        rd_periodic->type(FL_RADIO_BUTTON);
	rd_periodic->callback(knot_button_cb, this);

	rd_nonunif = new Fl_Round_Button(20, 330, 190, 25,
					 "Nonuniform Clamped");
        rd_nonunif->type(FL_RADIO_BUTTON);
	rd_nonunif->callback(knot_button_cb, this);

	rd_nonunif_periodic = new Fl_Round_Button(20, 355, 190, 25,
						  "Nonuniform Periodic");
        rd_nonunif_periodic->type(FL_RADIO_BUTTON);
	rd_nonunif_periodic->callback(knot_button_cb, this);
	
        in_nspan = new Fl_Int_Input(120,390,50,25,"Spans:");
	in_nspan->maximum_size(127);
        in_nspan->value("0");
	in_nspan->callback(nspan_cb,this);
	in_nspan->when(FL_WHEN_ENTER_KEY|FL_WHEN_NOT_CHANGED);

        g2->end();

	rd_average->setonly();
	in_nspan->deactivate();

	set_non_modal();
};

void Spline_window::check() {
        if(rd_bspline->value() == 1) {
                fl_smoothness->deactivate();
                fl_penalty->deactivate();
        }
        else if(rd_smoothspline->value() == 1) {
                fl_smoothness->activate();
                fl_penalty->deactivate();
        }
        else {
                fl_smoothness->activate();
                fl_penalty->activate();
        }

	if (rd_average->value() == 1) {
	  in_nspan->deactivate();
	} else if (rd_average_periodic->value() == 1) {
	  in_nspan->deactivate();
	} else {
	  in_nspan->activate();
	}
}

void Spline_window::set_spline_type(int type)
{
  if (type==B_SPLINE) {
    rd_bspline->value(1);
    rd_smoothspline->value(0);
    rd_pspline->value(0);
  } else if (type==SMOOTH_B_SPLINE) {
    rd_bspline->value(0);
    rd_smoothspline->value(1);
    rd_pspline->value(0);
  } else {
    rd_bspline->value(0);
    rd_smoothspline->value(0);
    rd_pspline->value(1);
  }
  check();
}

int Spline_window::spline_type()
{
  if (rd_bspline->value() == 1) 
    return B_SPLINE;
  else if (rd_smoothspline->value() == 1)
    return SMOOTH_B_SPLINE;
  else
    return P_SPLINE;
}

void Spline_window::set_knot_type(int type)
{
  switch (type)
  {
  case UNIFORM:
    rd_uniform->setonly();
    break;
  case PERIODIC:
    rd_periodic->setonly();
    break;
  case AVERAGE_PERIODIC:
    rd_average_periodic->setonly();
    break;
  case NONUNIFORM:
    rd_nonunif->setonly();
    break;
  case NONUNIFORM_PERIODIC:
    rd_nonunif_periodic->setonly();
    break;
  default:
    rd_average->setonly();
  }
  check();
}

int Spline_window::knot_type()
{
  if (rd_uniform->value()==1)
    return UNIFORM;
  else if (rd_periodic->value()==1)
    return PERIODIC;
  else if (rd_average_periodic->value()==1)
    return AVERAGE_PERIODIC;
  else if (rd_nonunif->value()==1)
    return NONUNIFORM;
  else if (rd_nonunif_periodic->value()==1)
    return NONUNIFORM_PERIODIC;
  else
    return AVERAGE;
}

void Spline_window::set_smoothness(float s)
{
  char str[128];
  sprintf(str, "%g", s);
  fl_smoothness->value(str);
}

void Spline_window::set_penalty(float p)
{
  char str[128];
  sprintf(str, "%g", p);
  fl_penalty->value(str);
}

void Spline_window::set_nspan(int n)
{
  char str[128];
  if (n>0) {
    sprintf(str, "%d", n);
    in_nspan->value(str);
  }
}

void Spline_window::spline_button_cb(Fl_Widget* w, void* data)
{
  Spline_window *s = (Spline_window*)data;
  s->check();
  s->mw->spline_type[s->mw->current] = s->spline_type();
  s->mw->update(false);
}

void Spline_window::knot_button_cb(Fl_Widget* w, void* data)
{
  Spline_window *s = (Spline_window*)data;
  s->check();
  s->mw->knot_type[s->mw->current] = s->knot_type();
  s->mw->update(false);
}

void Spline_window::degree_cb(Fl_Widget* w, void* data)
{
  Spline_window *s = (Spline_window*)data;
  s->mw->degree[s->mw->current] = s->degree();
  s->mw->update(false);
}

void Spline_window::smoothness_cb(Fl_Widget* w, void* data)
{
  Spline_window *s = (Spline_window*)data;
  s->mw->lambda[s->mw->current] = s->smoothness();
  s->mw->update(false);
}

void Spline_window::penalty_cb(Fl_Widget* w, void* data)
{
  Spline_window *s = (Spline_window*)data;
  s->mw->pord[s->mw->current] = s->penalty();
  s->mw->update(false);
}

void Spline_window::nspan_cb(Fl_Widget* w, void* data)
{
  Spline_window *s = (Spline_window*)data;
  if (s->nspan()<=0)
    s->mw->nspan[s->mw->current] = 1;
  else if (s->nspan()>=s->mw->data[s->mw->current].count())
    s->mw->nspan[s->mw->current]
      = s->mw->data[s->mw->current].count()-1;
  else
    s->mw->nspan[s->mw->current] = s->nspan();
  s->mw->update(false);
}
