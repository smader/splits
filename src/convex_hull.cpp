/*
 convex_hull.cpp

 Jarvis' march algorithm to compute the convex hull of a set of points.

 This file is part of Splits, a spline analysis tool for
 remotely sensed time series.

 Copyright (c) 2010-2016 Sebastian Mader

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the above copyright notice
 and this paragraph and the following paragraph appear in all copies.
	
 THIS SOFTWARE IS DISTRIBUTED "AS IS" AND WITHOUT ANY WARRANTY TO THE
 EXTENT PERMITTED BY APPLICABLE LAW; WITHOUT EVEN THE IMPLIED WARRANTY
 OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. THE COPYRIGHT
 HOLDER HAS NO OBLIGATIONS TO CORRECT DEFECTS, PROVIDE MAINTENANCE,
 SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS; NOR WILL HE BE LIABLE
 FOR ANY DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING
 IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
*/

#include <cfloat>
#include "convex_hull.h"

using namespace std;
using namespace arma;
using namespace splits;

vector<int> splits::convex_hull(const fvec& x, const fvec& y)
{
  vector<int> hull;

  if (x.n_elem<2) return hull;

  int n=x.n_elem;
  int n1=n-1;

  hull.reserve(n);

  hull.push_back(0);

  int i=0;
  while (i < n1) {
    int j, jmax=n1;
    float mmax = -FLT_MAX;
    for (j=i+1; j<n; j++) {
      float m = (y[j]-y[i])/(x[j]-x[i]);
      if (m >= mmax) {
	jmax = j;
	mmax = m;
      }
    }
    hull.push_back(jmax);
    i = jmax;
  }

  return hull;
}

fvec splits::hull_points(const fvec& x, const fvec& y, const vector<int>& hull)
{
  if (hull.empty()) return fvec();

  fvec h(x.n_elem);

  int n = hull.size()-1;

  int i;
  for (i=0; i<n; i++) {
    int a = hull[i];
    int b = hull[i+1];
    h[a] = y[a];
    h[b] = y[b];
    float m = (h[b]-h[a])/(x[b]-x[a]);
    int j;
    for (j=a+1; j<b; j++) h[j] = h[a]+(x[j]-x[a])*m;
  }

  return h;
}
