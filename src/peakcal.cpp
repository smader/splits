/*
 peakcal.cpp

 Analyze the structure of bottoms and peaks in a phenological time series.

 This file is part of Splits, a spline analysis tool for
 remotely sensed time series.

 Copyright (c) 2010-2020 Sebastian Mader

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the above copyright notice
 and this paragraph and the following paragraph appear in all copies.
	
 THIS SOFTWARE IS DISTRIBUTED "AS IS" AND WITHOUT ANY WARRANTY TO THE
 EXTENT PERMITTED BY APPLICABLE LAW; WITHOUT EVEN THE IMPLIED WARRANTY
 OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. THE COPYRIGHT
 HOLDER HAS NO OBLIGATIONS TO CORRECT DEFECTS, PROVIDE MAINTENANCE,
 SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS; NOR WILL HE BE LIABLE
 FOR ANY DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING
 IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
*/

#include <algorithm>
#include <gdal_priv.h>
#include <cpl_multiproc.h>
#define ARMA_NO_DEBUG
#include <armadillo>
#include "clargp.h"
#include "time_axis.h"
#include "dataset.h"
#include "spline.h"
#include "b_spline.h"
#include "common.h"
#include "convex_hull.h"

using namespace arma;
using namespace splits;

GDALDataType outtype;
const char *in_file, *ot_file;
volatile int xSize, ySize, count, nseas, nband;
fvec xt;
int degree, num_knots, dim_basis;
float *knots;
bool peri = false;
Date d_beg, d_end;
std::vector<float> seasons;
bool southern = false;
bool use_diff = false;
int nbot = 0;
float thres = 0.0f;
float *thresarr = NULL;
int nthres = 0;
bool verbose = true;
int nthread = 1;
void *mutex;
static volatile int line = 0;
static volatile int pending = 1;

ClArg_t arglst[] =
{
  strlist_arg("C",1),
  real_arg("b",0),           //count bottoms matching a baseline threshold
  reallist_arg("B",0),       //same as above for multiple different thresholds
  flag_arg("d",0),           //use difference instead of ratio
  int_arg("e",0),            //enumerate a number of lowest bottoms
  int_arg("M",0),            //number of threads in multiprocessing
  flag_arg("Qq",0),
  flag_arg("S",0),           //southern hemisphere: shift skeleton by 172 days
  string_arg("t",0),         //data type (one of Byte,Integer,Real)
  pos_arg("in_file"),
  pos_arg("ot_file"),
  last_arg
};

struct Buffer
{
  float *input;
  float *output;
  Spline *spline;
  
  Buffer() : input(0), output(0), spline(0) {}

  Buffer(float* in, int off1, float* ot, int off2, Spline* s)
  {
    input = in+off1;
    output = ot+off2;
    spline = s;
  }

  ~Buffer() {}
};

void extract_enumerate(Spline* spl, float* buf)
{
  fvec x, y;
  std::vector<int> s = extreme_points(spl, x, y);

  int i, j, k, n, nn;
  for (i=0; i<nseas; i++) {
    std::vector<int> ii = skeleton_search(seasons,x,i);
    n = ii.size();
    std::vector<int> sn(n);
    fvec xn(n);
    fvec yn(n);
    fvec hn(n);
    for (j=0; j<n; j++) {
      sn[j] = s[ii[j]];
      xn[j] = x[ii[j]];
      yn[j] = y[ii[j]];
    }
    std::vector<int> h = convex_hull(xn,yn);
    hn = hull_points(xn,yn,h);
    
    std::vector<float> d;
    d.reserve(n);
    if (use_diff) {
      for (j=0; j<n; j++)
	if (sn[j]>0) d.push_back(hn[j]-yn[j]);
    } else {
      for (j=0; j<n; j++)
	if (sn[j]>0) d.push_back((float)1-yn[j]/hn[j]);
    }
    
    std::sort(d.begin(), d.end());
    std::reverse(d.begin(), d.end());

    if (d.size()>nbot) nn=nbot; else nn=d.size();
    for (k=0; k<nn; k++) buf[i*nbot+k]=d[k];
  }
}

void extract_count(Spline* spl, float* buf)
{
  fvec x, y;
  std::vector<int> s = extreme_points(spl, x, y);

  int i, j, n;
  for (i=0; i<nseas; i++) {
    std::vector<int> ii = skeleton_search(seasons,x,i);
    n = ii.size();
    std::vector<int> sn(n);
    fvec xn(n);
    fvec yn(n);
    fvec hn(n);
    for (j=0; j<n; j++) {
      sn[j] = s[ii[j]];
      xn[j] = x[ii[j]];
      yn[j] = y[ii[j]];
    }
    std::vector<int> h = convex_hull(xn,yn);
    hn = hull_points(xn,yn,h);
    float count = 0;
    for (j=0; j<n; j++) {
      float d;
      if (use_diff) d=hn[j]-yn[j]; else d=(float)1-yn[j]/hn[j];
      if ((d>thres) && (sn[j]>0)) count+=1;
    }
    buf[i]=count;
  }
}

void extract_count_many(Spline* spl, float* buf)
{
  fvec x, y;
  std::vector<int> s = extreme_points(spl, x, y);

  int i, j, k, n;
  for (i=0; i<nseas; i++) {
    std::vector<int> ii = skeleton_search(seasons,x,i);
    n = ii.size();
    std::vector<int> sn(n);
    fvec xn(n);
    fvec yn(n);
    fvec hn(n);
    for (j=0; j<n; j++) {
      sn[j] = s[ii[j]];
      xn[j] = x[ii[j]];
      yn[j] = y[ii[j]];
    }
    std::vector<int> h = convex_hull(xn,yn);
    hn = hull_points(xn,yn,h);
    for (k=0; k<nthres; k++) {
      float t = thresarr[k];
      float count = 0;
      for (j=0; j<n; j++) {
	float d;
	if (use_diff) d=hn[j]-yn[j]; else d=(float)1-yn[j]/hn[j];
	if ((d>t) && (sn[j]>0)) count+=1;
      }
      buf[i*nthres+k]=count;
    }
  }
}

static void worker_func(void* p)
{
  Buffer *buf = (Buffer*)p;

  B_spline *bspl = (B_spline*)(buf->spline);

  int i;
  CPLAcquireMutex(mutex, 1000.0);
  i = line++;
  CPLReleaseMutex(mutex);

  GDALDataset *in_dset;
  CPLAcquireMutex(mutex, 1000.0);
  in_dset = (GDALDataset*) GDALOpen(in_file, GA_ReadOnly);
  CPLReleaseMutex(mutex);

  in_dset->RasterIO(GF_Read,
		    0,i,xSize,1,
		    buf->input,xSize,1,
		    GDT_Float32,
		    count,
		    NULL,
		    count*sizeof(float),
		    count*xSize*sizeof(float),
		    sizeof(float));

  CPLAcquireMutex(mutex, 1000.0);
  GDALClose(in_dset);
  CPLReleaseMutex(mutex);

  int j, k;
  for (j=0; j<xSize; j++) {
    for (k=0; k<nband; k++) buf->output[j*nband+k] = 0.0f;
    fvec coefs = fvec((const float*)(buf->input+j*count), count);
    bool nonzero = false;
    for (k=0; k<coefs.n_elem; k++) {
      if (coefs[k]!=(float)0) nonzero = true;
      break;
    }
    if (nonzero) {
      bspl->coef(coefs);
      if (nbot>0)
	extract_enumerate(bspl, buf->output+j*nband);
      else if (nthres>0)
	extract_count_many(bspl, buf->output+j*nband);
      else
	extract_count(bspl, buf->output+j*nband);
    }
  }

  GDALDataset *ot_dset;
  CPLAcquireMutex(mutex, 1000.0);
  ot_dset = (GDALDataset*) GDALOpen(ot_file, GA_Update);

  ot_dset->RasterIO(GF_Write,
		    0,i,xSize,1,
		    buf->output,xSize,1,
		    GDT_Float32,
		    nband,
		    NULL,
		    nband*sizeof(float),
		    nband*xSize*sizeof(float),
		    sizeof(float));

  GDALClose(ot_dset);
  CPLReleaseMutex(mutex);

  CPLAcquireMutex(mutex, 1000.0);
  pending--;
  CPLReleaseMutex(mutex);
}

int main(int argc, char* argv[])
{
  GDALAllRegister();
  CPLSetErrorHandler(CPLQuietErrorHandler);

  mutex = CPLCreateMutex();
  CPLReleaseMutex(mutex);

  parse_cl_args(argc, argv, arglst);

  int argsiz;
  void *parg;

  parg = get_cl_arg(arglst, "B", &argsiz);
  if (parg) {
    thresarr = (float*)parg;
    nthres = argsiz;
  } else {
    parg = get_cl_arg(arglst, "b", &argsiz);
    if (parg) {
      thres = *(float*)parg;
    } else {
      parg = get_cl_arg(arglst, "e", &argsiz);
      if (parg) {
	nbot = *(int*)parg;
	if (nbot<1) bail_out("Argument -e must be a positive integer! Exit.");
      }
    }
  }

  parg = get_cl_arg(arglst, "C", &argsiz);
  if (argsiz!=2) bail_out("Argument -C is invalid! Exit.");
  char **papch = (char**)parg;
  d_beg = Date(papch[0]);
  d_end = Date(papch[1]);

  parg = get_cl_arg(arglst, "d", &argsiz);
  if (argsiz>0) use_diff = true;

  parg = get_cl_arg(arglst, "M", &argsiz);
  if (parg) {
    nthread = *(int*)parg;
    pending = nthread;
  }

  parg = get_cl_arg(arglst, "Qq", &argsiz);
  if (argsiz>0) verbose = false;

  parg = get_cl_arg(arglst, "S", &argsiz);
  if (argsiz>0) southern = true;

  parg = get_cl_arg(arglst, "t", &argsiz);
  if (parg)
    outtype = get_data_type((const char*)parg);
  else
    outtype = GDT_Float32;

  in_file = (const char*)get_cl_arg(arglst, "in_file", &argsiz);
  ot_file = (const char*)get_cl_arg(arglst, "ot_file", &argsiz);

  Dataset d(in_file);

  knots = d.read_real_set("knot vector", &num_knots);
  if (knots==0) bail_out("Unable to get knot vector from header file! Exit.");

  if (!d.read_int("degree", &degree))
    bail_out("Unable to get degree from header file! Exit.");

  dim_basis = num_knots - (degree+1);

  int iper;
  d.read_int("periodic",&iper);
  if (iper==1) peri=true; else peri=false;

  xt = d.read_real_set("sites");

  GDALDataset *in_dset = (GDALDataset*) GDALOpen(in_file,GA_ReadOnly);
  if (!in_dset) bail_out("Unable to open dataset for input! Exit.");
  xSize = in_dset->GetRasterXSize();
  ySize = in_dset->GetRasterYSize();
  count = in_dset->GetRasterCount();

  if (count!=dim_basis) {
    GDALClose(in_dset);
    bail_out("Invalid spline parameters in input dataset! Exit.");
  }

  seasons = create_simple_skeleton(d_beg,d_end,southern,0,(float)xt.n_elem-1);
  nseas = seasons.size()-1;
  if (nbot>0)
    nband = nseas*nbot;
  else if (nthres>0)
    nband = nseas*nthres;
  else
    nband = nseas;

  GDALDriver *drv = (GDALDriver*) GDALGetDriverByName("ENVI");

  char **opts = NULL;
  opts = CSLSetNameValue(opts, "INTERLEAVE", "BIL");
  opts = CSLSetNameValue(opts, "SUFFIX", "ADD");

  GDALDataset *ot_dset;
  ot_dset = drv->Create(ot_file,xSize,ySize,nband,outtype,opts);

  if (!ot_dset) {
    GDALClose(in_dset);
    bail_out("Unable to create output dataset! Exit.");
  }

  ot_dset->SetProjection(in_dset->GetProjectionRef());
  double adfGeoTransform[6];
  if (in_dset->GetGeoTransform(adfGeoTransform) == CE_None)
    ot_dset->SetGeoTransform(adfGeoTransform);

  GDALClose(ot_dset);
  GDALClose(in_dset);

  float *ibuf = new float[nthread*xSize*count];
  float *obuf = new float[nthread*xSize*nband];

  int offs_in = xSize*count;
  int offs_ot = xSize*nband;

  Spline **spl = new Spline*[nthread];

  int j;
  for (j=0; j<nthread; j++) {
    spl[j] = new B_spline(knots,dim_basis,degree+1,peri);
  }

  Buffer *buffers = new Buffer[nthread];
  for (j=0; j<nthread; j++) {
    buffers[j] = Buffer(ibuf, j*offs_in, obuf, j*offs_ot, spl[j]);
  }

  int i, n;
  for (i=0; i<ySize; i+=nthread) {
    if ((i+nthread)>=ySize)
      n = ySize-i;
    else
      n = nthread;
    pending = n;
    for (j=0; j<n; j++) {
      CPLCreateThread(worker_func, (void*)&buffers[j]);
    }
    while (pending > 0) CPLSleep(0.5);
    if (verbose) report_progress(nthread, ySize);
  }

  d = Dataset(ot_file);
  if (nbot>0) {
    d.mk_meta_files(d_beg, nseas, nbot);
  } else if (nthres>0) {
    d.mk_meta_files(d_beg, nseas, nthres);
    d.write_real_set("bottom", thresarr, nthres);
  } else {
    d.mk_meta_files(d_beg, nseas, 1);
  }

  delete [] buffers;

  delete [] ibuf;
  delete [] obuf;

  delete [] spl;

  return 0;
}
