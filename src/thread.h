/*
 thread.h

 Multithreading support functions, declaration and impelentation.

 This file is part of SPLITS, a framework for spline analysis of
 time series.

 Copyright (C) 2010-2015  Sebastian Mader

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPLITS_THREAD_H
#define SPLITS_THREAD_H

#include "config.h"
#if HAVE_PTHREAD_H
#include <pthread.h>
#elif defined(_WIN32)
#include <windows.h>
#include <process.h>
#endif

namespace splits {

#if HAVE_PTHREAD_H
typedef pthread_t Thread;
extern "C" {
  typedef void*(thread_func)(void*);
}
static int create_thread(Thread& t, thread_func* f, void* p)
{
  return pthread_create((pthread_t*)&t, 0, f, p);
}
#elif defined(_WIN32)
typedef unsigned long Thread;
extern "C" {
  typedef void*(__cdecl thread_func)(void*);
}
static int create_thread(Thread& t, thread_func* f, void* p)
{
  return t = (Thread)_beginthread((void( __cdecl * )( void*))f, 0, p);
}
#endif

} //namespace splits

#endif
