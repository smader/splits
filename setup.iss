; setup.iss
;
; Inno Setup Script to create an executable installer for SPLITS
; under Windows
;
; This file is part of SPLITS, a framework for spline analysis of
; time series.
;
; Copyright (C) 2010-2015  Sebastian Mader

[Setup]
AppName=SPLITS
AppVersion=1.0
DefaultDirName={pf}\SPLITS
DefaultGroupName=SPLITS
LicenseFile=LICENSE.TXT
OutputBaseFilename=splits-1.0
Compression=lzma
SolidCompression=yes

[Files]
Source: "splfit.exe"; DestDir: "{app}"
Source: "splfit.txt"; DestDir: "{app}"
Source: "splcal.exe"; DestDir: "{app}"
Source: "splcal.txt"; DestDir: "{app}"
Source: "phencal.exe"; DestDir: "{app}"
Source: "phencal.txt"; DestDir: "{app}"
Source: "prepro.exe"; DestDir: "{app}"
Source: "prepro.txt"; DestDir: "{app}"
Source: "splview.exe"; DestDir: "{app}"
Source: "splview.txt"; DestDir: "{app}"
Source: "LICENSE.TXT"; DestDir: "{app}"
Source: "README.TXT"; DestDir: "{app}"; Flags: isreadme
; Libs needed from mingw32: gcc, stdc++, gfortran, iconv, quadmath
; Other libs: gdal, proj
; lapack, blas linked statically
Source: "libgcc_s_dw2-1.dll"; DestDir: "{app}"
Source: "libgdal-1.dll"; DestDir: "{app}"
Source: "libgfortran-3.dll"; DestDir: "{app}"
Source: "libiconv-2.dll"; DestDir: "{app}"
Source: "libproj-0.dll"; DestDir: "{app}"
Source: "libquadmath-0.dll"; DestDir: "{app}"
Source: "libstdc++-6.dll"; DestDir: "{app}"

[Icons]
Name: "{group}\splview"; Filename: "{app}\splview.exe"
Name: "{group}\SPLITS Command Prompt"; Filename: "{app}\splits_cmd.bat"
Name: "{group}\{cm:UninstallProgram,SPLITS}"; Filename: "{uninstallexe}"
Name: "{group}\LICENSE"; Filename: "{app}\LICENSE.TXT"
Name: "{group}\README"; Filename: "{app}\README.TXT"
Name: "{group}\Manual\splfit"; Filename: "{app}\splfit.txt"
Name: "{group}\Manual\splcal"; Filename: "{app}\splcal.txt"
Name: "{group}\Manual\phencal"; Filename: "{app}\phencal.txt"
Name: "{group}\Manual\prepro"; Filename: "{app}\prepro.txt"
Name: "{group}\Manual\splview"; Filename: "{app}\splview.txt"

[Code]
function CreateBatch(): boolean;
var
  fileName : string;
  lines : TArrayOfString;
begin
  Result := true;
  fileName := ExpandConstant('{app}\splits_cmd.bat');
  SetArrayLength(lines, 3);
  lines[0] := '@cd %temp%'
  lines[1] := ExpandConstant('@if exist "{app}" set path=%path%;"{app}"');
  lines[2] := '@cmd.exe /K "title SPLITS"';
  Result := SaveStringsToFile(fileName,lines,true);
  exit;
end;

const
  LF = #10;
  CR = #13;
  CRLF = CR + LF;

procedure ConvertLineEndings(fileName: string);
var
  filePath : string;
  fileContents : string;
begin
  filePath := ExpandConstant(fileName);
  LoadStringFromFile(filePath, fileContents);
  StringChangeEx(fileContents, LF, CRLF, false);
  SaveStringToFile(filePath, fileContents, false);
end;
 
procedure CurStepChanged(CurStep: TSetupStep);
begin
  if  CurStep=ssPostInstall then
    begin
         CreateBatch();
         ConvertLineEndings('{app}\LICENSE.TXT');
         ConvertLineEndings('{app}\README.TXT');
         ConvertLineEndings('{app}\splfit.txt');
         ConvertLineEndings('{app}\splcal.txt');
         ConvertLineEndings('{app}\phencal.txt');
         ConvertLineEndings('{app}\prepro.txt');
         ConvertLineEndings('{app}\splview.txt');
    end
end;

[UninstallDelete]
Type: files; Name: "{app}\splits_cmd.bat"
