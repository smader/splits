# configure.ac
#
# This file is part of SPLITS, a framework for spline analysis of
# time series.
#
# Copyright (C) 2010-2020 Sebastian Mader
#
#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ([2.68])
AC_INIT([splits], [1.11])
AC_CONFIG_SRCDIR([src/splview.cpp])
AC_CONFIG_HEADERS([config.h])

AC_ARG_ENABLE([gui],
AS_HELP_STRING([--disable-gui], [disable graphical user interface (splview)]),
[AS_CASE([$enableval], [yes], [gui=true], [no], [gui=false],
[AC_MSG_ERROR([bad value ${enableval} for --enable-gui])])], [gui=true])

AM_CONDITIONAL([GUI_ENABLE], [test x$gui = xtrue])

AC_ARG_ENABLE([api],
AS_HELP_STRING([--disable-api], [do not install API library and headers]),
[AS_CASE([$enableval], [yes], [api=true], [no], [api=false],
[AC_MSG_ERROR([bad value ${enableval} for --enable-api])])], [api=true])

AM_CONDITIONAL([API_ENABLE], [test x$api = xtrue])

# Use the foreign option. Look up "strictness" in the documentation.
AM_INIT_AUTOMAKE([foreign no-dependencies])

# Do not automatically add debugging or optimization options.
# This must be before the checks for programs.
CXXFLAGS="${CXXFLAGS:=}"
CFLAGS="${CFLAGS:=}"

# Checks for programs.
AC_PROG_CXX
AC_PROG_CC
AC_PROG_RANLIB
AC_PROG_INSTALL

AC_DEFUN([AC_PROG_PS2PDF],[
AC_CHECK_PROGS(ps2pdf,[ps2pdf ps2pdf14 ps2pdf13 ps2pdf12],false)
export ps2pdf;
if test $ps2pdf = "false" ;
then
AC_MSG_WARN([you cannot build pdf versions of manpages]);
fi
AC_SUBST(ps2pdf)
])

AC_PROG_PS2PDF

# Checks for libraries.
AC_CHECK_LIB([m], [log])

OLD_LIBS=$LIBS
LIBS="$LIBS-larmadillo"

AC_MSG_CHECKING([for armadillo wrapper library])
AC_LANG_PUSH([C++])
AC_LINK_IFELSE(
[AC_LANG_PROGRAM([#include <armadillo>], [arma::fmat dummy;])],
[AC_MSG_RESULT([yes])], [AC_MSG_RESULT([no]);LIBS=$OLD_LIBS])
AC_LANG_POP([C++])

AC_CHECK_LIB([gdal], [GDALOpen], [],
[AC_MSG_ERROR(gdal library cannot be found)])

AC_CHECK_LIB([pthread], [pthread_create])

GUI_LIBS=
OLD_LIBS=$LIBS
AS_IF([test x$gui = xtrue],
[AC_CHECK_LIB([fltk], [fl_utf8bytes], [LIBS=$OLD_LIBS;GUI_LIBS=`fltk-config --ldflags`],
[AC_MSG_ERROR([gui enabled, but fltk library cannot be found])])])
AC_SUBST(GUI_LIBS)

# Checks for header files.
AS_IF([test x$gui = xtrue],
[AC_CHECK_HEADER([FL/fl_utf8.h], [],
[AC_MSG_ERROR([gui enabled, but fltk headers cannot be found])])])

GDAL_CFLAGS=
AC_CHECK_PROG([gdalconfig],[gdal-config],[yes],[no])
if test $gdalconfig = "yes" ;
then
GDAL_CFLAGS=`gdal-config --cflags`
else
AC_CHECK_HEADER([gdal.h], [], [AC_MSG_ERROR(gdal headers cannot be found)])
fi
AC_SUBST(GDAL_CFLAGS)

AC_CHECK_HEADERS([pthread.h])

AC_CONFIG_FILES([Makefile])
AC_OUTPUT
