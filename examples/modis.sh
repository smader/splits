#!/bin/sh

# This is an example to show how to combine MODIS Terra and Aqua EVI data,
# fit a spline to the preprocessed stack and use it to calculate a complete
# set of phenological markers.

# This file is part of SPLITS, a framework for spline analysis of
# time series.
#
# Copyright (C) 2010-2016  Sebastian Mader
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.



# Preprocess all tiles h20v10 in the current directory (.), combine Terra
# and Aqua (-s MCD) and store the result in the same directory. 

prepro -s MCD -t h20v10 .


# Just keep the combined stack, delete Terra and Aqua only stacks.

rm MOD*STACK.*
rm MYD*STACK.*


# The number of threads (CPU cores) to use in the processing:

NUM_THREADS=4


# The name of the raster file to hold the spline coefficients:

SPLINE="spline.dat"


# Fit a spline model to the EVI data with the following settings, in order
# of occurrence:
#    * Use pixel based Day of Year (DOY) data, stored in file
#      "MCD13Q1_h20v10_DOY_STACK.dat". It is assumed that the domain covered
#      by the time series is from Feb. 18, 2000 (DOY 49) to Nov. 24, 2012
#      (DOY 328). The end date is the last day in the compositing period of
#      the last composite. Dates can be given as DOY/YYYY (e.g. 49/2000)
#      or M/D/YYYY (e.g. 2/18/2000). Here, the last composite is from Aqua
#      and starts at DOY 313, so it may contain data acquired as late as
#      DOY 328.
#    * Option -l is used to scale the data in the -1..1 range on input;
#      the scaling offset is 0, gain is 0.0001.
#    * The input nodata value is -32767.
#    * A cubic (-d 3) spline model of 79 polynomial pieces, i.e. 80 knots
#      is used.
#    * Use weights based on the quality data (usefulness index) in file
#      "MCD13Q1_h20v10_VIQ_STACK.dat"; Every quality flag is assigned a
#      weight according to the exponential scheme tabulated in the file
#      "06-weight-exp-150.txt".
#    * Since it runs within a script, splfit is executed quietly (-Q) without
#      terminal output.

splfit -a MCD13Q1_h20v10_DOY_STACK.dat -C49/2000,328/2012 -l0,0.0001 \
       -n -32767 -d 3 -k 79 -M $NUM_THREADS \
       -W MCD13Q1_h20v10_VIQ_STACK.dat,06-weight-exp-150.txt \
       -Q MCD13Q1_h20v10_EVI_STACK.dat $SPLINE


# Calculate a complete set of phenological markers based on the spline model
# fitted above:
#    * Tile h20v10 is on the southern hemisphere, so the option -S needs
#      to be set. When processing northern hemispheric data, the option
#      is omitted. 
#    * The optional -g indicates to include also markers based on a
#      threshold; the threshold is given as a fraction: Here it defines the
#      start (end) of greenness in a given year as the point when the
#      EVI exceeds (falls below) a value defined by the early (late) minimum
#      plus 20 percent of the amplitude in that year.
#    * Run quietly in scripting mode, do not report progress.

phencal -C 2/18/2000,11/24/2012 -S -g 0.2 -M $NUM_THREADS -Q $SPLINE \
        phenology.dat 
