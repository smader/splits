#!/bin/sh

# Discrete Fourier Transform (DFT) methods require samples at equal
# intervals and a sequence of observations with matching values at
# both ends.
# This simple example shows how to smooth an NDVI time series
# with a periodic regression spline using time stamped data and
# sample the spline model at equal time intervals. The goal is to
# mitigate spurious oscillations when DFT analysis is used to examine
# the resulting dataset.

# This file is part of SPLITS, a framework for spline analysis of
# time series.
#
# Copyright (C) 2010-2016  Sebastian Mader
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.



# Preprocess tiles h24v04 from the Terra platform (-s MOD) in the
# current directory (.), use Terra and write the result to the same
# directory. Option -N indicates that NDVI should be extracted instead
# of EVI.

prepro -s MOD -t h24v04 -N .


# The number of threads (CPU cores) to use in the processing:

NUM_THREADS=4


# Create a temporary file in the current directory:

TEMP=`mktemp -p . -t XXXXX`


# Create a periodic spline model in the temporary file, use time stamped
# data but ignore the quality dataset:
#    * The time stamps are found in file `MOD13Q1_h24v04_DOY_STACK.dat'
#    * The time domain covered by the time series extends from Jan. 1,
#      2001 until Jan. 3, 2011. The end date is the end of the last
#      compositing period in the data. The last Terra 16-day composite
#      in a year begins at Julian day 353 (Dec. 19) and extends three
#      days into the new year until Jan. 3.
#      N.B.: Using splines with periodic end conditions only makes sense
#      if the data begin and end roughly at the same time of the year,
#      so there is a chance that the observations at both ends can match.
#    * The input value indicating no data is -32767
#    * A polynomial spline of degree 3 is used that consists of 69
#      polynomial pieces (i.e. 70) knots. The knots are uniformly spaced
#      and we have ten years of 23 observations per year (in total 230
#      samples), so there are three polynomial pieces per year.
#    * The option -p indicates that a periodic spline should be used to
#      produce equal modeled values at both ends of the time series.
#      The default is to reproduce the actual data values.
#    * The option -Q makes the command execute quietly, which is useful
#      in scripts.

splfit -a MOD13Q1_h24v04_DOY_STACK.dat -C 1/1/2001,1/3/2011 -n -32767 \
       -d 3 -k 69 -p -M $NUM_THREADS -Q MCD13Q1_h20v10_EVI_STACK.dat $TEMP


# Evaluate the spline model at equal time intervals, write the result to
# the file `ndvi.dat'.
# The spline is evaluated at the dates in the file `dates.txt' to obtain
# a smooth time series with samples at equal intervals. You may use any
# set of dates within a given domain.
# The example file contains dates at 8-day intervals. Such regular files
# can be created in `splview' using `Create DOY File' from the `File' menu.
# A valid dates file must contain dates in the form M/D/YYYY or DOY/YYYY
# separated by whitespace.

splcal -A dates.txt -C 1/1/2001,1/3/2011 -M $NUM_THREADS -Q $TEMP ndvi.dat


# Clean up and delete the temporary files:

rm -f $TEMP*
