/* test_api.cpp -- Test the SPLITS C/C++ API */

#include <stdio.h>
#include <math.h>   /* sqrtf() */
#include <splits.h> /* API functions described here */

#define N 393

using namespace splits;

int main(void)
{
  FILE *fp;
  fp = fopen("test_input.txt", "rt");

  fscanf(fp, "%*s%*s\n"); /* Skip header */  
  
  float *doy = new float[N];
  float *y = new float[N];
    
  int i;
  for (i=0; i<N; i++) {
    fscanf(fp, "%f %f\n", &doy[i], &y[i]);
  }

  fclose(fp);


  /* Create a domain */
  
  Date d0("49/2000");   /* Start: DOY 49, 2000     */
  Date d1("3/22/2017"); /* End:   March 22nd, 2017 */

  Domain x(d0, d1, doy, N);


  /* Fit data y to domain x using a cubic spline of 150 pieces */
  
  Spline *s = create_spline(x, y, UNIFORM_BSPLINE, 150, 3);

  
  /* Calculate phenological parameters, report timing parameters in
     terms of DOY
  */
  
  std::map<int, Pheno_set> season;
  season = phenology(s, x, false, 0.2, PHENOLOGY_DOY);
  
  printf("Number of seasons: %d\n", (int)season.size());

  
  /* Get season of 2014, print early min., peak and late min. DOY */
  
  Pheno_set ph = season[2014];

  printf("%f %f %f\n", ph.doy_early_min, ph.doy_peak, ph.doy_late_min);


  /* Evaluate the spline, calculate root mean squared error based on the
     evaluation */

  float *ys = (float*) malloc(N*sizeof(float));
  evaluate(s, x, ys);

  float err, sum = 0.0f;
  for (i=0; i<N; i++) {
    err = y[i]-ys[i];
    sum += err*err;
  }

  err = sqrtf(sum/(float)N);
  
  printf("RMSE = %f\n", err);

  
  /* Clean up */
  
  destroy_spline(s);

  delete [] doy;
  delete [] y;
  
  free((void*)ys);
  
  return 0;
}
