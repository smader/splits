.TH splview 1 "August 2015" "" "SPLITS -- spline analysis of time series"
.SH NAME
splview \- graphical user interface for spline model exploration
.SH SYNOPSIS
.SY splview
.SH DESCRIPTION
.B splview
provides a graphical user interface to explore different spline models for the analysis of remotely sensed time series. The interface consists of a grayscale image viewer combined with a time series plot. Time series vectors from different locations of an image containing a time series may be analysed using various
spline models. Roots, extreme points and seasonal information (beginning/end of a season) may be displayed. The \fBsplview\fR program is the interactive
part of the SPLITS framework, which consists of a set of interactive and
non-interactive programs to explore, fit and use spline models for time series
analysis.
.SS Input data and modes of operation
The programs \fBsplview\fR and \fBsplfit\fR for exploring and fitting spline
models to remotely sensed time series images may operate in two different
modes, depending on the input data supplied. Apart from the main input
of a time series image cube (typically a vegetation index), the user has to
supply time information in form of a Day of Year (DoY) file. This file may
be an image file containing the Julian day of observation for each pixel in
the main input dataset, or it may be a text file with a list of dates of the
observations in the main input. In the latter case, the observation dates
for each pixel are treated as constant along the time axis. Observations need
not be regularly spaced along the time axis, irregular observation schedules
as well as missing data are acceptable. It is also OK to have multiple
observations at the same point in time.
.SS DoY images
Detailed DoY information on a per pixel basis is so far only supplied by the
MODIS vegetation index products. To use this information, MODIS HDF product
data have to be preprocessed in order to be used with \fBsplview\fR/\fBsplfit\fR. See the \fBmodis-prepro\fR manpage for details.
.SS DoY text files
DoY text files for sensors with a regular observation schedule may be prepared
using the abscissae tool in \fBsplview\fR: File/Create DOY File..., or they
may be created by the user utilizing e.g. a spreadsheet software for irregular
time series. Dates may be denoted in two different formats, either as a common
date (MM/DD/YYYY) or as Julian day and year (DOY/YYYY). Individual dates are
separated by blank space, for example

.nf
07/20/1969     201/1969
11/19/1969     323/1969
02/05/1971      33/1971
07/31/1971     212/1971
04/21/1972     111/1972
12/11/1972     345/1972
.fi

If the time input is an image, the user has to supply the start and end dates
of the time series. For time series created using maximum value or other
compositing methods, the start date is the date of beginning of the first
compositing period, and the end date is the date when the last compositing period ends, *not* the beginning of the last compositing period (e.g. if a compositing period is 10 days, and the last composite starts at December 1st, then the
end date is December 10th of that year). If time information is supplied as
a text file, the start and end dates are set to the first and last entry in
the text file, respectively.
.SS Quality data
The SPLITS exploration and fitting programs are capable of dealing with
quality information about the observations in a weighted least squares fit.
Similar to the time information, quality data may also be supplied either
as an image or as a text file. In the latter case, the text file has to supply
as many weights as there are observations in one time series, and those
weights are applied to every time series pixel when the main input image is
processed. This allows e.g. to downweigh observations during winter time when
the vegetation index is low and noisy, and upweigh observations in the
vegetation period when a good fit of the model is most important.

If the quality data is supplied as an image of bitfields with quality flags
for every observation, a weight is associated with every quality flag or
combination of quality flags using a weight table. Weight tables may be
created and edited using the weights dialog in \fBsplview\fR: Edit/Weights...
The following example is a weight table for the simple MODIS pixel reliability
dataset, where a flag, or rank of -1 means fill/no data, 0 means good data,
1 marginal data, 2 snow/ice and 3 cloudy:

.nf
-1   0.0
 0   1.0
 1   0.7
 2   0.0
 3   0.3
.fi

A weight table is merely an associative array,
quality ranks need not be in sequential (decreasing or increasing) order.
The first column of the text file gives the rank keys or quality flags, the
second column gives the associated weights. Columns may be separated by any
white space characters. The file must contain one line for every rank key
and associated weight. MODIS data processed with \fBmodis-prepro\fR use the
more detailed VI usefulness and cloud layers supplied with the HDF datasets.
VI usefulness scores are added to produce a range of values from 1 to 17,
where high values mean useless, and low values mean useful data. A value
of 17 is used for cloud affected data, the value 0 is used as a fill value.
As an example, the following table may be used to exponentially decrease
weights with decreasing observation quality:

.nf
0 0.000
1 1.000
2 0.769
3 0.588
4 0.446
5 0.336
6 0.251
7 0.186
8 0.136
9 0.099
10 0.070
11 0.049
12 0.033
13 0.021
14 0.013
15 0.007
16 0.003
17 0.000
.fi

The user may define any weight scheme (e.g. linear, log, square root) he
thinks suitable by defining an appropriate weight table. By defining weights
for similar quality layers (e.g. the status maps provided by the
SPOT VEGETATION S-products), weighted least squares fits can also be done
for other sensors.
.SS Spline fitting and analysis
Once the user has explored a dataset using \fBsplview\fR and setteled for a
certain spline model and fitting method, the model may be actually fitted to
the entire dataset using the \fBsplfit\fR command line utility.
As a convenience, there is a graphical tool in \fBsplview\fR: Commands...
which will set the command line parameters for \fBsplfit\fR according to the
current spline model, DoY and weight settings. Additional command line
parameters may be set under Options... which refer to parallel processing
using multiple processors on the machine that is running \fBsplfit\fR.
There is also a quiet mode where all terminal output is suppressed, which
is useful when the SPLITS command line tools are used in noninteractive
shell scripts processing multiple datasets in the same manner.
Graphical command line builders also exist for other noninteractive
programs in the SPLITS framework (e.g. \fBsplcal\fR and \fBphencal\fR).
Although the produced command may be run instantly by hitting 'Enter' in the
command line window, it is highly recommended that the assembled command
be copied and pasted to a system terminal window for execution. Execution
in a system terminal provides better means for process monitoring and,
if necessary, abortion.

The result
of a \fBsplfit\fR run is an image dataset with spline parameters (called a
spline file) that can
be used as input to other programs of the SPLITS framework, e.g. \fBsplcal\fR
to interpolate regular time series for analysis methods such as the discrete
Fourier transform which requires equally spaced observations; or \fBphencal\fR
to calculate phenological parameters. The \fBsplview\fR graphical interface
incorporates a minimal set of the methods used in \fBphencal\fR to calculate
phenological markers, so the user can evaluate whether a given spline model
seems suitable for phenological analysis. For example under View/Show, the
user may check Full Seasons to detect and display the major vegetation
periods from a spline model. The \fBsplview\fR interface also allows to
explore the roots or inverse function values of a spline model by setting
an arbitrary ordinate value under View/Roots... and checking the Roots
oprion under View/Show.
.SS Output format
The output format of all SPLITS
programs is ENVI BIL (ENVI is a trademark of Exelis Visual Information
Solutions). In the case of a spline file, special tags are added to the
header that describe the properties of the fitted spline model.
.SH SEE ALSO
.BR SPLITS (1)
.SH BUGS
See section `Known Issues' in the file README.TXT that comes with \fBsplview\fR.
.SH AUTHOR
Sebastian Mader
.SH COPYRIGHT
Copyright (C) 2010-2015  Sebastian Mader

\fBsplview\fR is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

\fBsplview\fR is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with \fBsplview\fR. If not, see <http://www.gnu.org/licenses/>.

\fBsplview\fR is based in part on the work of
the FLTK project (http://www.fltk.org).
