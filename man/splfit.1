.TH splfit 1 "July 2016" "" "SPLITS -- spline analysis of time series"
.SH NAME
splfit \- fit a spline model to a time series image
.SH SYNOPSIS
.SY splfit
\fB\-a\fR \fIname\fR
[\fB\-C\fR \fImm/dd/yyyy|doy/yyyy,mm/dd/yyyy|doy/yyyy\fR]
\fB\-d\fR \fIdegree\fR
[\fB\-e\fR \fIimage-file\fR]
[\fB\-K\fR \fIname\fR|\fB\-k\fR \fInumber\fR]
[\fB\-l\fR \fIoffset\fR,\fIgain\fR]
[\fB-M\fR \fInumber\fR]
[\fB-n\fR \fIvalue\fR]
[\fB-o\fR \fIvalue\fR]
[\fB-p\fR]
[\fB-Q\fR]
[\fB\-s\fR \fIvalue\fR]
[\fB-w\fR \fIname\fR|\fB\-W\fR \fIquality\-file,weight\-file\fR]
.I input\-image
.I output\-image
.SH DESCRIPTION
.B splfit
is used to fit spline models of a given form to remotely sensed time series
in an image. The type of spline model that is fitted depends
on the presence or absence of specific command line options
related to the spline (see below).

The output of
.B splfit
is another image containing the coefficients of the fitted spline model.
The output is in the format of a standard ENVI BIL file (ENVI is a trademark of
EXELIS Visual Information Solutions). Special header tags are
used to indicate the spline model used in the fit. This output may be
supplied to other programs of the Splits toolbox (namely \fBsplcal\fR and
\fBphencal\fR) for further processing.
.SH OPTIONS
.TP 10
\fB\-a\fR \fIname\fR
The name of a text file containing the dates as abscissae values
for the observations in the input image. Dates must be separated by
white space as described in detail in the \fBsplview\fR manpage.
The number of entries must match the number of bands in the image.
Alternatively, \fIname\fR may be the name of an image file containing the
Julian day of year (DOY) when the composite was acquired.
In this case, spline models
with irregularly spaced abscissae are constructed
for every image element. If \fIname\fR is a
DOY image, option \fB-C\fR must be given
to determine the correct spacings for the abscissae.
.TP
\fB\-C\fR \fImm/dd/yyyy,mm/dd/yyyy\fR
.TQ
\fB\-C\fR \fIdoy/yyyy,doy/yyyy\fR
This option
recieves an argument consisting of two dates, separated by a comma.
The first date marks the beginning of the observation period (i.e. the
earliest possible observation), the second date marks the observation
period's end (i.e. the latest possible observation).
If the file given as option \fB\-a\fR is an image file, the nodata value
used to pad the unoccupied bands must also be specified (see \fB-n\fR).
Nodata pixels are then detected by scanning the day of year dataset for
the specified nodata value (please refer to the \fBmodis\-prepro\fR
manpage for more details).
.TP
\fB\-e\fR \fIimage-file\fR
The name of an image file to recieve the root mean squared residual error
(RMSE) obtained in the model fit. The output is a single band ENVI file
with RMSE values (floating point) for every input pixel in the units of
the input data.
.TP
\fB\-l\fR \fIoffset\fR,\fIgain\fR
A set of linear coefficients to transform the input data values.
.TP
\fB\-M\fR \fInumber\fR
The number of processors and/or cores used in multiprocessing.
.TP
\fB-n\fR \fIvalue\fR
Input nodata value. Defaults to zero.
.TP
.B \-Q
Quiet (scripting) mode: suppress all terminal output
.SS Spline Options
The only mandatory spline related command line parameter is the degree
(\fB-d\fR). If a degree is given as the only parameter, a B-spline is fitted
in the least squares sense. If, in addition, \fB-s\fR is specified, a
smooth B-spline model is fitted by imposing a penalty term on the integral
of the second derivative of the fitted spline model. The value of \fB-s\fR
is a proportionality constant that adjusts the smoothness of the spline,
greater values of \fB-s\fR lead to smoother curves. If both \fB-s\fR and
\fB-o\fR are specified, a P-spline is used as the model function, where
the value of the \fB-s\fR argument is a proportionality constant as explained
before and the value of \fB-o\fR is the order of the penalty.
.TP 10
\fB\-d\fR \fIdegree\fR
The polynomial degree of the spline curve.
.TP
\fB\-K\fR \fIname\fR
Read the knot positions from text file \fIname\fR. Positions must be given
in raw coordinates, i.e. scaled from [0...1].
.TP
\fB\-k\fR \fInumber\fR
Option to control the number of spans/knots of the spline.
Per default, the number of knots is equal to the number of data points and
the knot positions are distributed in an average manner across the data range.
Option \fB\-k\fR is used to
specify the number of spans. The number of knots is then \fInumber\fR + 1,
and the knots are positioned uniformly along the horizontal axis.
.TP
\fB\-o\fR \fIvalue\fR
Order of the penalty for a P-spline model (usually 2.0).
.TP
\fB\-p\fR
Indicates that a periodic spline model is to be used.
.TP
\fB\-s\fR \fIvalue\fR
Smoothness of the spline: \fIvalue\fR is a proportionality constant that defines
the amount of smoothing applied when fitting a spline model. The larger
the real number given by \fIvalue\fR, the smoother the fitted curve
(i.e. the larger the penalty imposed on the integral of the curve's second
derivative.
.TP
\fB\-w\fR \fIname\fR
The name of an ASCII file with a vector of weights to be used for the
samples along the time axis (see the \fBsplview\fR manpage for more details).
.TP
\fB\-W\fR \fIquality-file,weight-file\fR
Use this option to perform a weighted fit to the time series in the input
image. The option has two arguments, separated by a comma. The first
argument is the name of an image file with the same number of bands as the
input image, containing quality information. The second argument is the name
of an ASCII text file with a table to associate an appropriate weight with
every quality flag or a combination of flags.
The quality flags contained in \fIquality-file\fR
must be expressed as decimal integers in \fIweight-file\fR.

The following is an example for a \fIweight-file\fR for a MODIS usefulness
dataset as produced by \fBprepro\fR, where a flag, or rank key of 0 means
fill/no data, 1 means good data, and quality is decreasing with increasing
decimal flag values up to 17; 16 indicates marginal data, and 17 clouds:

.nf
0 0.000
1 1.000
2 0.769
3 0.588
4 0.446
5 0.336
6 0.251
7 0.186
8 0.136
9 0.099
10 0.070
11 0.049
12 0.033
13 0.021
14 0.013
15 0.007
16 0.003
17 0.000
.fi

The first column of the text file gives the rank keys or quality flags, the
second column gives the associated weights. Columns may be separated by any
white space characters. The file must contain one line for every rank key
and associated weight. The above example shows an exponential weighting
scheme.
.SH SEE ALSO
.BR prepro(1)
.BR SPLITS(1)
.SH BUGS
See section `Known Issues' in the file README.TXT that comes with \fBsplfit\fR.
.SH AUTHOR
Sebastian Mader
.SH COPYRIGHT
Copyright (C) 2010-2016  Sebastian Mader

\fBsplfit\fR is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

\fBsplfit\fR is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with \fBsplfit\fR. If not, see <http://www.gnu.org/licenses/>.
