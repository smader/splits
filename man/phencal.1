.TH phencal 1 "July 2016" "" "SPLITS -- spline analysis of time series"
.SH NAME
phencal \- calculate phenological parameters from a fitted spline model
.SH SYNOPSIS
.SY phencal
\fB\-C\fR \fImm/dd/yyyy|doy/yyyy\fR,\fImm/dd/yyyy|doy/yyyy\fR
[\fB\-g\fR \fIfraction\fR]
[\fB-i\fR|\fB-r\fR]
[\fB\-M\fR \fInumber\fR]
[\fB\-Q\fR]
[\fB\-S\fR]
.I input\-file
.I output\-file
.SH DESCRIPTION
.B phencal
computes phenological parameters from a spline model obtained using
\fBsplfit\fR. At present, only annual phenologies can be modelled with
\fBphencal\fR. An annual skeleton is imposed on the extreme values
obtained from the spline model to determine the beginning, peaking and
ending of a vegetation period. Several phenological markers may be
obtained for each vegetation period (see section \fBOUTPUT\fR).

The output is in the format of a standard ENVI BIL file
(ENVI is a trademark of EXELIS Visual Information Solutions) with
a set of metafiles to access the individual phenological parameters.
The various phenological parameters are explained in the section \fBOUTPUT\fR
below.

The \fBsplview\fR interface under File/Materialize Metafile...
provides a tool to extract individual phenological parameters or annual
information from \fBphencal\fR output.
.SH OPTIONS
.TP 10
\fB\-C\fR \fImm/dd/yyyy\fR,\fImm/dd/yyyy\fR
.TQ
\fB\-C\fR \fIdoy/yyyy\fR,\fIdoy/yyyy\fR
The start and end date of the time series records modelled in the input
spline file (see the \fBsplview\fR manpage for more details). This option
must be supplied.
.TP
\fB\-g\fR \fIfraction\fR
Greenness parameter: determines how onset and cease of vegetation growth in a season are determined. At present, \fIfraction\fR is a fraction of the seasonal
amplitude. The onset (end) of greenenss is defined as the point where the measurements in the time series reach a value equivalent to the early minimum (late minimum) plus \fIfraction\fR times the amplitude (i.e. the difference between the seasonal peak value and the latent value). If \fB\-g\fR is omitted,
only a reduced set of phenological parameters is computed (see below).
.TP
\fB\-i\fR
Output day indices instead of day of year values for the phenological
markers. For example, the seasonal peak will be written to the output file as a
day index along the time axis instead of as the DOY when the peak occurs.
A day index starts at 1 for the first day in the series and ends with the
last day of the last composite in the series. If, e.g., the series consists
of 10 composites, where each composite is a decade long, then the day indices
are in the range (1,...,100).
.TP
\fB-M\fR \fInumber\fR
The number of processors and/or cores used in multiprocessing.
.TP
\fB\-Q\fR
Be quiet, do not show progress information.
.TP
\fB\-r\fR
Output raw time axis data, not DOY or day indices (see also \fB-i\fR). The raw time axis always runs from 0 to 1.
.TP
\fB\-S\fR
This flag indicates that the data are from the southern hemisphere, where
a growing season typically extends between years. The effect of the
\fB\-S\fR parameter is that the skeleton established to extract the phenological
parameters is shifted by six months (June \- May instead of Jan. \- Dec.).
.SH OUTPUT
This section describes the various phenological parameters output by
\fBphencal\fR. In addition to the default parameters, an extended parameter
set is available by specifying a value for the greenness (\fB\-g\fR) option.
Parameters related to inflection points exist only for models of at least
cubic degree.
A seasonal cycle is defined by looking at the sequence of minima and maxima
present in a modelled time series. A cycle is bounded by the lowest minima
to the left (early minimum) and right (late minimum) of an annual peak.
In general, the late minimum of a given year coincides with the early
minimum of the following year.
.SS Default Parameters
.TP 5
DOY_Early_Min
The day of year of the early minimum.
.TP
DOY_Early_Flex
The day of year of the inflection point that occurs early in the season
(between the early minimum and the largest annual peak). If there is more
than one such point, the one with the largest first derivative is chosen.
.TP
DOY_Peak
The day of year of the largest annual peak.
.TP
DOY_Late_Flex
The day of year of the inflection point that occurs late in the season
(between the annual maximum and the late minimum). If there is more than
one such point, the one with the smallest first derivative is chosen.
.TP
DOY_Late_Min
The day of year of the late minimum.
.TP
Early_Min_Val
The modelled value at the time of the early minimum.
.TP
Early_Flex_Val
The modelled value at the early inflection point. 
.TP
Peak_Val
The modelled annual peak value.
.TP
Late_Flex_Val
The modelled value at the late inflection point.
.TP
Late_Min_Val
The modelled value at the time of the late minimum.
.TP
Min_Min_Duration
The number of days between the early and late minima of a given year.
.TP
Amplitude
The difference between the modelled peak value and the latent value (see Latent_Val below).
.TP
Latent_Val
The average of the modelled values at the early and late minima.
.TP
Min_Min_Integral
The time integrated modelled value from the early to the late mimimum of a
given year.
.TP
Latent_Integral
The time integrated latent value from the early to the late minimum of a
given year (a "box" of width Duration and height Latent_Val).
.TP
Total_Integral
The sum of Min_Min_Integral and Latent_Integral.
.TP
Early_Flex_Rate
The value of the first derivative of the spline curve in the early inflection
point.
.TP
Late_Flex_Rate
The absolute value of the first derivative in the late inflection point.
.SS Greenness Parameters
The following parameters are controlled by the \fB\-g\fR option (see above).
.TP 5
DOY_Start_Green
The day of year of the onset of vegetation growth (greenness).
.TP
DOY_End_Green
The day of year when vegetation ceases to develop, i.e is completely
senescencent.
.TP
Green_Duration
The number of days between the start and end of greenness, a proxy for the
duration of active vegetation development.
.TP
Green_Integral
The time integrated modelled value over the duration of greenness (see above).
.TP
Greenup_Rate
The slope of a line connecting the point of onset of greenness and the
annual peak value. Describes the approximate rate of vegetation growth.
.TP
Senescence_Rate
The (positive) slope of a line connecting the annual peak value and the point
of end of greenness. Describes the approximate rate of advancement of
vegetation senescence.
.SH SEE ALSO
.BR SPLITS(1)
.SH BUGS
See section `Known Issues' in the file README.TXT that comes with \fBphencal\fR.
.SH AUTHOR
Sebastian Mader
.SH COPYRIGHT
Copyright (C) 2010-2016  Sebastian Mader

\fBphencal\fR is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

\fBphencal\fR is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with \fBphencal\fR. If not, see <http://www.gnu.org/licenses/>.
