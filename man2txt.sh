#!/bin/sh

if [ ! -d man ]; then
  exit
fi

list=`ls man/*.[0-9]`

for el in $list; do
  man -l $el | col -b > `echo $el | sed s/\.[^\.]*$//`.txt
done
